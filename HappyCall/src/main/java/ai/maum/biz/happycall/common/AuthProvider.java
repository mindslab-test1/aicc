package ai.maum.biz.happycall.common;

import java.util.ArrayList;
import java.util.List;

import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.dto.AuthenticaionDTO;
import ai.maum.biz.happycall.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("authProvider")
public class AuthProvider implements AuthenticationProvider  {

    @Autowired
    AuthService authService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String id = authentication.getName();
        String password = authentication.getCredentials().toString();
        CmAuthUserDTO user = authService.getAccount(id);

//        if (null == user || !passwordEncoder.matches(password, user.getPassword())) {
//            return null;
//        }

        List<GrantedAuthority> grantedAuthorityList = new ArrayList(authService.getAuthorities(id));

        return new AuthenticaionDTO(id, password, grantedAuthorityList, user);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
