package ai.maum.biz.happycall.common.config;

import ai.maum.biz.happycall.common.AuthProvider;
import ai.maum.biz.happycall.common.security.LogOutSuccessHandler;
import ai.maum.biz.happycall.common.security.SuccessHandler;
import ai.maum.biz.happycall.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configurable
@EnableWebSecurity
@EnableGlobalAuthentication
@Order(SecurityProperties.DEFAULT_FILTER_ORDER)
public class AuthConfigController extends WebSecurityConfigurerAdapter {

    @Autowired
    AuthProvider authProvider;

    @Autowired
    LogOutSuccessHandler logOutSuccessHandler;

    @Autowired
    AuthService authService;


    @Override
    public void configure(WebSecurity web) throws Exception
    {
        web.ignoring().antMatchers(
                "/favicon.ico",
                "/error/**",
                "/webjars/**",
                "/login/**",
                "/loginajax/**",
                "/resources/**",
                "/getRSAKeyValue/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {

        http
            .authorizeRequests()
                .antMatchers(  "/service","/service/**","/resources/**","/oauth/**", "/api/sso/properties/**").permitAll()
                .antMatchers("/**").authenticated()
                .and()
            .headers()
                .frameOptions().disable()
                .and()
            .formLogin()
                .loginPage("/oauth/login")
                .loginProcessingUrl("/loginProcess")
                .successHandler(successHandler())
                .permitAll()
                .and()
            .logout()
                .invalidateHttpSession(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .addLogoutHandler(logOutSuccessHandler)
                .logoutSuccessHandler(logOutSuccessHandler).permitAll()
                .and()
            .csrf()
                .csrfTokenRepository(new CookieCsrfTokenRepository())
                .and()
            .authenticationProvider(authProvider);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{

    }

    @Bean
    public BCryptPasswordEncoder ssoPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public AuthenticationProvider ssoAuthProvider()
    {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(authService);
        provider.setPasswordEncoder(ssoPasswordEncoder());
        return provider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    SecurityContextRepository securityContextRepository(){
        return new HttpSessionSecurityContextRepository();
    }

    @Bean
    public SuccessHandler successHandler(){
        return new SuccessHandler();
    }
}

