package ai.maum.biz.happycall.common.security;

import ai.maum.biz.happycall.common.util.HttpClientRes;
import ai.maum.biz.happycall.common.util.UtilHttpClient;
import ai.maum.biz.happycall.common.util.UtilProperties;
import ai.maum.biz.happycall.models.vo.OAuthTokenVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 로그아웃 성공 시
 * 핸들러 들어옴.
 * custom 으로 session 에 저장한것들은
 * 여기서 정리하고 logout 처리
 */
@Component("logOutSuccessHandler")
public class LogOutSuccessHandler implements LogoutHandler, LogoutSuccessHandler {

    Logger logger = LoggerFactory.getLogger(LogOutSuccessHandler.class);

    UtilProperties properties;

    @Autowired
    public LogOutSuccessHandler(UtilProperties properties)
    {
        this.properties = properties;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authentication) throws IOException, ServletException
    {
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }

        response.setStatus(HttpServletResponse.SC_OK);
//        response.sendRedirect("/login");
        response.sendRedirect("https://maum.ai");
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
    {
        HttpSession session = request.getSession();

        if(session.getAttribute("ssoToken") != null) {
            OAuthTokenVO ssoToken = (OAuthTokenVO) session.getAttribute("ssoToken");

            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("client_id", properties.getSsoMindslabClientId());
            paramMap.put("access_token", ssoToken.getAccess_token());

            try {
                HttpClientRes res = UtilHttpClient.getInstance().delete(properties.getSsoMindslabTokenReqUrl(), paramMap);
                logger.info("Logout/Token={}, DeleteRes={}", ssoToken.getAccess_token(), res.getStatusCode());
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        session.removeAttribute("userInfo");
        session.removeAttribute("ssoToken");
        session.removeAttribute("SPRING_SECURITY_CONTEXT");
    }
}

