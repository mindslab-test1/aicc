package ai.maum.biz.happycall.common.security;

import ai.maum.biz.happycall.common.util.CommonUtils;
import ai.maum.biz.happycall.common.util.HttpClientRes;
import ai.maum.biz.happycall.common.util.UtilHttpClient;
import ai.maum.biz.happycall.common.util.UtilProperties;
import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.vo.OAuthTokenVO;
import ai.maum.biz.happycall.service.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/oauth")
public class OAuthCtrl {

    private UtilProperties utilProperties;
    private AuthenticationManager authManager;

    @Autowired
    AuthService authService;


    public OAuthCtrl(UtilProperties utilProperties, AuthenticationManager authManager) {
        this.utilProperties = utilProperties;
        this.authManager = authManager;
    }


    @GetMapping("/login")
    public String loginPage(HttpServletRequest request)
    {
        String referer = request.getHeader("Referer");

        if(referer != null && referer.startsWith(utilProperties.getDomain())) {
            HttpSession session = request.getSession();
            session.setAttribute("prevPage", referer.replace(utilProperties.getDomain(), ""));
        }

        return "oauth_login";
    }

//    @GetMapping
//    @RequestMapping("/callback")
//    public ModelAndView callbackToken(HttpServletRequest request,
//                                      HttpServletResponse response,
//                                      @RequestParam("code") String code,
//                                      @RequestParam("state") String state)
//    {
//        String logTitle = "callbackToken/Code=[" + code + "], State[" + state + "], ";
//        log.info(logTitle + "[Start]");
//
//        ModelAndView mav = new ModelAndView();
//        mav.setViewName("/minutes/manage/minutesList");
//
//        try {
//            UtilHttpClient httpClient = UtilHttpClient.getInstance();
//            Map<String, String> paramsMap = new HashMap<>();
//            paramsMap.put("grant_type", "authorization_code");
//            paramsMap.put("code", code);
//
//            HttpClientRes res = httpClient.post(utilProperties.getSsoMindslabTokenReqUrl(), paramsMap);
//            log.info(logTitle + "Res={}", res.toString());
//
//            if(HttpStatus.SC_OK != res.getStatusCode()) {
//                //TODO:
//                return mav;
//            }
//
//            OAuthTokenVO token = CommonUtils.parseJsonStringToObject(res.getContent(), new OAuthTokenVO());
//            int addUserRslt = userSvc.addOAuthUserIfNotExist(token.getEmail());
//            log.info(logTitle + "Token[{}], AddUserRslt[{}]", addUserRslt);
//
//            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(token.getEmail(), "");
//            Authentication auth = authManager.authenticate(authToken);
//            SecurityContext securityContext = SecurityContextHolder.getContext();
//            securityContext.setAuthentication(auth);
//
//            UserVO userVo = new UserVO();
//            userVo.setSch_userId(token.getEmail());
//
//            UserDTO userDto = userSvc.getUserByName(userVo);
//
//            //TODO: The account is locked
////            if(userDto.getUser_use_yn().equals("N")) {
////            }
//
//            HttpSession session = request.getSession();
//            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
//            session.setAttribute("userInfo", userDto);
//
//            ObjectMapper mapper = new ObjectMapper();
//            String jsonString = mapper.writeValueAsString(userDto);
//            Cookie userCookie = new Cookie("userInfo", URLEncoder.encode(jsonString, "utf-8"));
//            response.addCookie(userCookie);
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return mav;
//    }

    @GetMapping
    @RequestMapping("/callback")
    public RedirectView callbackToken(HttpServletRequest request,
                                      HttpServletResponse response,
                                      @RequestParam("code") String code,
                                      @RequestParam("state") String state)
    {
        Object prevPageObj = request.getSession().getAttribute("prevPage");
        RedirectView redirectView = new RedirectView(prevPageObj != null ? prevPageObj.toString() : "/");
        request.getSession().removeAttribute("prevPage");

        try {
            UtilHttpClient httpClient = UtilHttpClient.getInstance();
            Map<String, String> paramsMap = new HashMap<>();
            paramsMap.put("grant_type", "authorization_code");
            paramsMap.put("code", code);

            HttpClientRes res = httpClient.post(utilProperties.getSsoMindslabTokenReqUrl(), paramsMap);

            if(HttpStatus.SC_OK != res.getStatusCode()) {
                //TODO:
//                redirectView = new RedirectView("/login");
                redirectView = new RedirectView("https://maum.ai");
                return redirectView;
            }

            OAuthTokenVO token = CommonUtils.parseJsonStringToObject(res.getContent(), new OAuthTokenVO());
            int addUserRslt = authService.addOAuthUserIfNotExist(token.getEmail());


            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(token.getEmail(), "");
            Authentication auth = authManager.authenticate(authToken);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);

            String userName = auth.getPrincipal().toString();
            CmAuthUserDTO cmAuthUserDTO = authService.getAccount(userName);

            //TODO: The account is locked
//            if(userDto.getUser_use_yn().equals("N")) {
//            }

            HttpSession session = request.getSession();
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            session.setAttribute("userInfo", cmAuthUserDTO);
            session.setAttribute("ssoToken", token);

            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(cmAuthUserDTO);
            Cookie userCookie = new Cookie("userInfo", URLEncoder.encode(jsonString, "utf-8"));
            response.addCookie(userCookie);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return redirectView;
    }


}
