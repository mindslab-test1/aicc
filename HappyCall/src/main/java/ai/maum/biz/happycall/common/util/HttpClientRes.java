package ai.maum.biz.happycall.common.util;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class HttpClientRes {

    private int statusCode;
    private String content;
}
