package ai.maum.biz.happycall.common.util;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Getter
@Service
public class UtilProperties {

    @Value("${sso.mindslab.request.authorize.url}")
    private String ssoMindslabAuthorizeReqUrl;

    @Value("${sso.mindslab.request.request-token.url}")
    private String ssoMindslabTokenReqUrl;

    @Value("${sso.mindslab.callback.token.url}")
    private String ssoMindslabCallbackTokenUrl;

    @Value("${sso.mindslab.client-id}")
    private String ssoMindslabClientId;

    @Value("${domain}")
    private String domain;

    @Value("${minute.site.mindslab-free.id}")
    private String siteIdMindslabFree;
}
