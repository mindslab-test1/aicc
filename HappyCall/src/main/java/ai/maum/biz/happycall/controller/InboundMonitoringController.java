package ai.maum.biz.happycall.controller;

import ai.maum.biz.happycall.common.CustomProperties;
import ai.maum.biz.happycall.common.util.Utils;
import ai.maum.biz.happycall.common.util.VariablesMng;
import ai.maum.biz.happycall.models.dto.*;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;

@Controller
public class InboundMonitoringController {

    @Autowired
    CommonService commonService;

    @Autowired
    CommonMonitoringService commonMonitoringService;

    @Autowired
    InboundMonitoringService inboundMonitoringService;

    @Autowired
    AuthService authService;

    @Autowired
    CustOpInfoService custOpInfoService;

    @Autowired
    CustomProperties customProperties;

    @Inject
    VariablesMng variablesMng;

    /*  인바운드 모니터링 */
    @RequestMapping(value="/callStatus", method = {RequestMethod.GET, RequestMethod.POST})
    public String callStatus(FrontMntVO frontMntVO, HttpServletRequest req, Model model, Principal principal) {
        frontMntVO.setCmOpInfoVO(custOpInfoService.getOpCompanyInfoById(principal.getName()));

        // 권한 확인
        boolean isHH = req.isUserInRole("ROLE_ADMIN");
        if(isHH) {
            List<SipAccountDTO> phoneListResult = inboundMonitoringService.getPhoneList(frontMntVO);
            model.addAttribute("phoneListResult", phoneListResult);
        }
        model.addAttribute("websocketUrl", customProperties.getWebsocketProtocol() + "://" + customProperties.getWebsocketIp() + customProperties.getWebsocketPort());

        //DB에서 전체 리스트 SELECT.
        List<CmContractDTO> latestCallList = inboundMonitoringService.getLatestCallList(frontMntVO);
        model.addAttribute("latestCallList", latestCallList);
        model.addAttribute("menuId", variablesMng.getMenuIdString("callStatus"));			// menuId 설정.
        model.addAttribute("username", Utils.getLogInAccount(authService).getName());     // 로그인한 사용자 이름

        return "/monitoring/inboundMonitoring";
    }

    @RequestMapping(value = "/inboundPop", method = {RequestMethod.GET, RequestMethod.POST})
    public String doInboundPop(FrontMntVO frontMntVO, Model model, Principal principal) {
        //검색 파트 코드값 세팅
        frontMntVO.setCmOpInfoVO(custOpInfoService.getOpCompanyInfoById(principal.getName()));
        List<CmCommonCdDTO> schCodeDto = commonService.getSeachCodeList(frontMntVO);
        if( schCodeDto != null && schCodeDto.size() > 0 ) {
            HashMap<String, String> fcd09 = new HashMap<String, String>();						// 모니터링 내용

            for(CmCommonCdDTO one : schCodeDto) {
                if( one.getFirstCd().equals( variablesMng.getMonitoringResultCode() ) ) {
                    fcd09.put( one.getCode(), one.getCdDesc() );
                }
            }
            model.addAttribute("monitoringResultCode", Utils.makeCallStatusTag(fcd09));			// 모니터링 내용
        }


        //DB에서 전체 리스트 SELECT.
        CmContractDTO topInfo = inboundMonitoringService.getInboundCallMntData(frontMntVO);
        String memo = commonMonitoringService.getRecentContractMemo(frontMntVO);
        frontMntVO.setCampaignId( topInfo.getCampaignId() );
        frontMntVO.setCallId(topInfo.getLastCallId());
        List<CmCampaignInfoDTO> mntResult = commonMonitoringService.getCallPopMonitoringResultList(frontMntVO);

        //수동, 자동 모니터링 팝업 가운데 우측 모니터링 결과 답  SELECT
        HashMap<String, String> score = Utils.makeHashMapForScore(commonMonitoringService.getScoreList(frontMntVO));

        //수동, 자동 모니터링 팝업 가운데 좌측 STT 결과 리스트 SELECT
        List<CmSttResultDetailDTO> sttResult = commonMonitoringService.getSttResultAllList(frontMntVO);

        //Front에 전달할 객체들 생성.
        model.addAttribute("topInfo", topInfo);
        model.addAttribute("memo", memo);
        model.addAttribute("score", score);
        model.addAttribute("sttResult", sttResult);
        model.addAttribute("mntResult", mntResult);
        model.addAttribute("frontMntVO", frontMntVO);
        model.addAttribute("websocketUrl", customProperties.getWebsocketProtocol() + "://" + customProperties.getWebsocketIp() + customProperties.getWebsocketPort());
        model.addAttribute("audioUrl",customProperties.getAudioIp() + customProperties.getAudioPort());

        return "monitoring/inboundMonitoringPopup";
    }
}
