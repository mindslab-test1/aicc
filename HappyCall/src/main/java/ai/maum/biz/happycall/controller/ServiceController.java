package ai.maum.biz.happycall.controller;

import ai.maum.biz.happycall.common.CustomProperties;
import ai.maum.biz.happycall.common.util.VariablesMng;
import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.models.vo.MntTargetMngVO;
import ai.maum.biz.happycall.service.CampaignService;
import ai.maum.biz.happycall.service.CommonService;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/service")
public class ServiceController {

    @Autowired
    CommonService commonService;

    @Autowired
    CampaignService campaignService;

    @Autowired
    CustomProperties customProperties;

    @Autowired
    VariablesMng variablesMng;

    @RequestMapping(value = "/voiceBot")
    public String voiceBotService(Model model) {
/*
        List<CmCommonCdDTO> schCodeDto = commonService.getSeachCodeList();
        HashMap<String, String> monitoringList = new HashMap<String, String>();

        for(CmCommonCdDTO one : schCodeDto) {
            if( one.getFcd().equals( variablesMng.getMntType() ) ) {
                monitoringList.put( one.getCode(), one.getCd_desc() );
            }
        }

        model.addAttribute("monitoringResultCode", Utils.makeCallStatusTag(monitoringList));*/
        model.addAttribute("websocketUrl", customProperties.getWebsocketProtocol() + "://" + customProperties.getWebsocketIp() + customProperties.getWebsocketPort());

        return "/service/voiceBot";
    }

    @RequestMapping(value = "/getCampaignTaskList")
    @ResponseBody
    public String getCampaignTaskList(int campaignId) {

        List<CmCampaignInfoDTO> taskList = campaignService.getCampaignTaskList(campaignId);

        String retListJson = new Gson().toJson(taskList);

        return retListJson;
    }

    @RequestMapping(value = "/createServiceMonitoring")
    @ResponseBody
    public int createServiceMonitoring(MntTargetMngVO mntTargetMngVO) {

        campaignService.createServiceMonitoring(mntTargetMngVO);

        return mntTargetMngVO.getContractNo();
    }

    @RequestMapping(value = "/getCampaignListByService")
    @ResponseBody
    public String getCampaignListByService(String serviceName) {

        List<CmCampaignInfoDTO> campaignList = campaignService.getCampaignListByService(serviceName);
        String retListJson = new Gson().toJson(campaignList);
        return retListJson;
    }

    //수동 모니터링 RestFul API 호출
    @RequestMapping(value = "/sendCM", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public String serviceSendCM(FrontMntVO frontMntVO) {

        HttpClient hc = HttpClients.createDefault();

        Gson gson = new Gson();
        HashMap<String, String> getJson = new HashMap<String, String>();
        getJson = gson.fromJson(frontMntVO.getSendMsgStr(), getJson.getClass());

        String callMngUrl = "";
        if (getJson.get("Event").equals("START")) {
            callMngUrl = "http://" + customProperties.getRestIp() + customProperties.getRestPort() + "/call/start";
        } else if (getJson.get("Event").equals("TRANSFER")) {
            callMngUrl = "http://" + customProperties.getRestIp() + customProperties.getRestPort() + "/call/transfer";
        } else if (getJson.get("Event").equals("CLOSE")) {
            callMngUrl = "http://" + customProperties.getRestIp() + customProperties.getRestPort() + "/call/hangup";
        } else if (getJson.get("Event").equals("LISTEN")) {
            callMngUrl = "http://" + customProperties.getRestIp() + customProperties.getRestPort() + "/call/play";
        } else if (getJson.get("Event").equals("STOP")) {
            callMngUrl = "http://" + customProperties.getRestIp() + customProperties.getRestPort() + "/call/stop";
        }

        HttpPost hp = new HttpPost(callMngUrl);
        StringEntity passJson;
        HttpResponse getRes;

        try {
            passJson = new StringEntity(frontMntVO.getSendMsgStr());

            hp.addHeader("Content-type", "application/json");
            hp.setEntity(passJson);

            getRes = hc.execute(hp);
            String json = EntityUtils.toString(getRes.getEntity(), "UTF-8");
        } catch (Exception e) {
            //log.info("### sendCM : error Message : " + e.getMessage());
        }

        return "SUCC";
    }
}
