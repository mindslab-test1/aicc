package ai.maum.biz.happycall.mapper;

import java.util.List;

import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.dto.CmAuthRoleDTO;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;

import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface AuthMapper {
    public CmAuthUserDTO getAccount(String username);

    public CmAuthUserDTO getAccountById(int id);

    public List<String> getAuthorities(String username);

    public List<String> getRoles(String username);

    public List<CmAuthRoleDTO> getAuthList(String username);

    public int insertUser(AuthVO account);

    public int delAuth(AuthVO authVO);

    public  int insertAuth(AuthVO authVO);

    public  int checkDup(AuthVO authVO);

    public List<CmAuthRoleDTO> getRoleList();

    public int updateAccount(AuthVO authVO);

    public int disableAccount(AuthVO authVO);

    public int enableAccount(AuthVO authVO);

    public List<CmAuthUserDTO> getUserList(FrontMntVO frontMntVO);

    public int getresultUserTotalCount();
}
