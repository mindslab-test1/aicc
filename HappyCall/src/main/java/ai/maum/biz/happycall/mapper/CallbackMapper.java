package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.CallbackVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CallbackMapper {

    List<CmContractDTO> getCallbackTargetList();

    // callbacklist 조회
    List<CmContractDTO> getCallbackList(CallbackVO callbackVO);

    // callbacklist 조회 페이징
    public int getResultMntTotalCount(CallbackVO callbackVO);

    // 콜백취소할 리스트
    public int undoCallback(CallbackVO callbackVO);

    // 콜백지정할 리스트
    public int doCallback(CallbackVO callbackVO);

    // 콜백 관련 정보 수정
    public int doCallbackModifiedRow(CallbackVO callbackVO);
}
