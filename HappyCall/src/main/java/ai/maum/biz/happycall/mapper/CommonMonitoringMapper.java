package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CallStatusDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignScoreDTO;
import ai.maum.biz.happycall.models.dto.CmSttResultDetailDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CommonMonitoringMapper {
    List<CallStatusDTO> getCountOfCall(FrontMntVO frontMntVO);

    String getRecentContractMemo(FrontMntVO frontMntVO);

    List<CmCampaignInfoDTO> getCallPopMonitoringResultList(FrontMntVO frontMntVO);

    List<CmCampaignScoreDTO> getScoreList(FrontMntVO frontMntVO);

    List<CmSttResultDetailDTO> getSttResultAllList(FrontMntVO frontMntVO);
}
