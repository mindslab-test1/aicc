package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmCompanyDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.dto.CmOpInfoDTO;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.CmOpInfoVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CustOpInfoMapper {
	int getMonitoringCount(FrontMntVO frontMntVO);
	List<CmContractDTO> getMonitoringList(FrontMntVO frontMntVO);

	int getOpCount(FrontMntVO frontMntVO);
	List<CmOpInfoDTO> getOpList(FrontMntVO frontMntVO);

	int getMonitoringCountByOp(FrontMntVO frontMntVO);
	List<CmContractDTO> getMonitoringListByOp(FrontMntVO frontMntVO);

	int setOpByRandom(FrontMntVO frontMntVO);
	int setAssign(FrontMntVO frontMntVO);
	int setOp(FrontMntVO frontMntVO);

	int cancelAssign(FrontMntVO frontMntVO);
	int cancelOp(FrontMntVO frontMntVO);

	int addOp(CmOpInfoVO cmOpInfoVO);
	int editOp(CmOpInfoVO cmOpInfoVO);

	List<CmCompanyDTO> getCompanyList();

    CmOpInfoVO getOpCompanyInfoById(String custOpId);
}
