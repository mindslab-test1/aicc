package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.SipAccountDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface InboundMonitoringMapper {

    List<CmContractDTO> getInboundCallMntList(FrontMntVO frontMntVO);

    int getInboundCallMntCount(FrontMntVO frontMntVO);

    CmContractDTO getInboundCallMntData(FrontMntVO frontMntVO);

    List<SipAccountDTO> getPhoneList(FrontMntVO frontMntVO);

    List<CmContractDTO> getLatestCallList(FrontMntVO frontMntVO);

    int updateMemo(FrontMntVO frontMntVO);
}
