package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OutboundMonitoringMapper {
    List<CmContractDTO> getOutboundCallMntList(FrontMntVO frontMntVO);

    int getOutboundCallMntCount(FrontMntVO frontMntVO);

    CmContractDTO getOutboundCallMntData(FrontMntVO frontMntVO);

    int updateMemo(FrontMntVO frontMntVO);

    List<String> getCallHistList(FrontMntVO frontMntVO);

}
