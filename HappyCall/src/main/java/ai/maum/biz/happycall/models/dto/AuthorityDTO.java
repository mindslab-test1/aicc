package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class AuthorityDTO {
    private String username;
    private String authority;
}
