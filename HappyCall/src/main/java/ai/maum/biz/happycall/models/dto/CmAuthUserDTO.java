package ai.maum.biz.happycall.models.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Getter
@Setter
public class CmAuthUserDTO implements UserDetails {
    private int userId;
    private String username;
    private String name;
    private String password;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;
    private List<String> roles;
    private Collection <? extends GrantedAuthority> authorities;

    private int creatorId;
    private int updaterId;
    private String createdDtm;
    private String updatedDtm;
}
