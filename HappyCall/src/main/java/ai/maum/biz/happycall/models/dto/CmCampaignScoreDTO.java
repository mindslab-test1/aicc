package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmCampaignScoreDTO {
	private String seqNum;
	private String callId;
	private String contractNo;
	private String infoSeq;
	private String infoTask;
	private String taskValue;
	private String reviewComent;
}
