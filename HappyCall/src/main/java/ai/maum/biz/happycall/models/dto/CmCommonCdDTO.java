package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmCommonCdDTO {
	
	/* 페이지 상단 검색 부분 관련 변수 */
	private String firstCd;
	private String secondCd;
	private String thirdCd;
	private String code;
	private String cdDesc;
	private String note;

	private int creatorId;
	private int updaterId;
	private String createdDtm;
	private String updatedDtm;
}
