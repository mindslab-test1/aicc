package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmCompanyDTO {
	private String companyId;
	private String companyName;
}
