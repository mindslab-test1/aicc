package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class SipAccountDTO {
    private String sipDomain;
    private String sipUser;
    private String telUri;
    private String pbxName;
    private String status;
    private int contractNo;
    private String customerPhone;
    private String bootTime;
    private String lastEvent;
    private String lastEventTime;
    private String campaignId;
    private String isInbound;
}
