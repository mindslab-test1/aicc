package ai.maum.biz.happycall.models.vo;

import lombok.Data;

@Data
public class CmOpInfoVO {
	private String custOpId;
	private String custOpNm;

	private String companyId;
}