package ai.maum.biz.happycall.models.vo;

import lombok.Data;

@Data
public class ExcelUploadVO {
	private String contractNo;
	private String campaignId;
	private String custNm;
	private String custTelNo;
	private String custOpId;
	private String targetDt;
}
