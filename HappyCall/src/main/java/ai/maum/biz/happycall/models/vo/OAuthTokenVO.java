package ai.maum.biz.happycall.models.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OAuthTokenVO {

    private String access_token;
    private String refresh_token;
    private String refresh_expire_time;
    private String access_expire_time;
    private String email;
}
