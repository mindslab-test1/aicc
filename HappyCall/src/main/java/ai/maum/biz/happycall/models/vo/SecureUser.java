package ai.maum.biz.happycall.models.vo;

import ai.maum.biz.happycall.mapper.AuthMapper;
import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import org.apache.ibatis.type.Alias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Alias("secureUser")
public class SecureUser extends User {

    @Autowired
    AuthMapper authMapper;
    static AuthMapper authMapperStatic;

    @PostConstruct
    private void initalize(){
        this.authMapperStatic = authMapper;
    }

    public SecureUser(CmAuthUserDTO member) {
        super(member.getUsername(), member.getPassword(), makeGrantedAuthority(member.getRoles()));
    }

    private static List<GrantedAuthority> makeGrantedAuthority(List<String> roles){
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String authority : roles) {
            authorities.add(new SimpleGrantedAuthority(authority));
        }
        return authorities;
    }
}
