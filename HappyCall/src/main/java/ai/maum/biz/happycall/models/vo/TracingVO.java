package ai.maum.biz.happycall.models.vo;

import ai.maum.biz.happycall.common.util.Utils;
import ai.maum.biz.happycall.mapper.AuthMapper;
import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Component
public class TracingVO {

	private int currentId;
	private String currentDtm;

	public TracingVO(){
		// TODO: 해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼해야돼
		/*
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if( authentication != null  && authentication.isAuthenticated()) {

			Object userInfo = authentication.getPrincipal();

			if(userInfo.getClass().equals(String.class)){
				authMapper.getAccount(userInfo.toString());
			} else {

			}


		}
		*/
		this.currentId = 1;

		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		this.currentDtm = format.format(now);
	}
}


