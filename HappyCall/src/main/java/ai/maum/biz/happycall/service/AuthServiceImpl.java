package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.AuthMapper;
import ai.maum.biz.happycall.mapper.CustOpInfoMapper;
import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.dto.CmAuthRoleDTO;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.CmOpInfoVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.models.vo.SecureUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthMapper authMapper;

    @Autowired
    CustOpInfoMapper custOpInfoMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        CmAuthUserDTO account = authMapper.getAccount(userId);
        account.setRoles(getRoles(userId));

    //    return account;
        return Optional.ofNullable(account)
                .map(m -> new SecureUser(m)).get();
    }

    public Collection<GrantedAuthority> getAuthorities(String username) {
        List<String> string_authorities = authMapper.getAuthorities(username);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String authority : string_authorities) {
            authorities.add(new SimpleGrantedAuthority(authority));
        }
        return authorities;

    }

    public List<String> getRoles(String username) {
         return  authMapper.getRoles(username);
    }

    public CmAuthUserDTO getAccount(String userId) {
        return authMapper.getAccount(userId);
    }

    //Role List
    public List<CmAuthRoleDTO> getRoleList() {
        return authMapper.getRoleList();
    }

    public List<CmAuthUserDTO> getUserList(FrontMntVO frontMntVO){
        return authMapper.getUserList(frontMntVO);
    }

    //사용자 추가할 데이터
    @Transactional("transactionManager")
    public int addAccount(AuthVO authVO) {
        authVO.setPassword(passwordEncoder.encode(authVO.getPassword()));
        authVO.setAccountNonExpired(true);
        authVO.setAccountNonLocked(true);
        authVO.setCredentialsNonExpired(true);
        authVO.setEnabled(true);
        authMapper.insertUser(authVO);

        CmOpInfoVO cmOpInfoVO = new CmOpInfoVO();
        cmOpInfoVO.setCompanyId(authVO.getCompanyId());
        cmOpInfoVO.setCustOpId(authVO.getUsername());
        cmOpInfoVO.setCustOpNm(authVO.getName());

        custOpInfoMapper.addOp(cmOpInfoVO);
        return authMapper.insertAuth(authVO);
    }

    // 계정 중복 확인
    public int checkDup(AuthVO authVO) {
        return authMapper.checkDup(authVO);
    }

    //Id와 일치하는 계정 정보 가져오기
    public CmAuthUserDTO getAccountById(int id) {
        return authMapper.getAccountById(id);
    }

    //paging
    public int getresultUserTotalCount(FrontMntVO frontMntVO) { return authMapper.getresultUserTotalCount(); }

    //기존 계정 수정 데이터
    @Override
    @Transactional("transactionManager")
    public int updateAccount(AuthVO authVO) {
        authVO.setNewPassword(passwordEncoder.encode(authVO.getNewPassword()));
        //수정시 권한 추가하는 경우
        if(authVO.getCheckedList() != null) {
           authMapper.insertAuth(authVO);
        }
        //수정시 권한 삭제하는 경우
        if(authVO.getDelAuthList() != null) {
           authMapper.delAuth(authVO);
        }

        CmOpInfoVO cmOpInfoVO = new CmOpInfoVO();
        cmOpInfoVO.setCompanyId(authVO.getCompanyId());
        cmOpInfoVO.setCustOpId(authVO.getUsername());
        cmOpInfoVO.setCustOpNm(authVO.getName());
        custOpInfoMapper.editOp(cmOpInfoVO);

        return authMapper.updateAccount(authVO);
    }

    //기존 계정중 체크된 거 비활성화할 계정
    @Override
    public int disableAccount(AuthVO authVO) { return authMapper.disableAccount(authVO);
    }

    //기존 계정중 체크된 거 활성화할 계정
    @Override
    public int enableAccount(AuthVO authVO) { return authMapper.enableAccount(authVO);
    }

    //기존 계정별 Auth List 데이터
    public List<CmAuthRoleDTO> getAuthList(String username) { return authMapper.getAuthList(username); }

    public int addOAuthUserIfNotExist(String username) {

        CmAuthUserDTO currentUser = authMapper.getAccount(username);
        if(currentUser != null) {
            return 1;
        }

        //BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        AuthVO tmpAuth = new AuthVO();
        tmpAuth.setUsername(username);
        tmpAuth.setPassword(passwordEncoder.encode(""));
        List authList = new ArrayList();
        authList.add("6"); // 기업사용자
        tmpAuth.setCheckedList(authList);
        tmpAuth.setName(username);
        tmpAuth.setCompanyId("005"); // SSO 테스트

        return addAccount(tmpAuth);

    }

}
