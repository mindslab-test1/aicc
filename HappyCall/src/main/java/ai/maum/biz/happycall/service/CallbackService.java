package ai.maum.biz.happycall.service;


import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.CallbackVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CallbackService {

    List<CmContractDTO> getCallbackTargetList();

    // callbacklist 조회
    List<CmContractDTO> getCallbackList(CallbackVO callbackVO);

    // callbacklist 조회 페이징
    public int getResultMntTotalCount(CallbackVO callbackVO);

    // 콜백 취소할 체크 리스트
    public int undoCallback(CallbackVO callbackVO);

    // 콜백 지정할 체크 리스트
    public int doCallback(CallbackVO callbackVO);

    // 모니터링 대상 업로드 - 1 row 수정된 내용 저장
    public int doCallbackModifiedRow(CallbackVO callbackVO);
}
