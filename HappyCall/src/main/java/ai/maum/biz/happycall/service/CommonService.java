package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.CmCommonCdDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;

import java.util.List;

public interface CommonService {

	// 모니터링 페이지 상단 검색 부분 관련 변수
	public List<CmCommonCdDTO> getSeachCodeList(FrontMntVO frontMntVO);

}
