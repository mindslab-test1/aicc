package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.CommonMapper;
import ai.maum.biz.happycall.models.dto.CmCommonCdDTO;
import ai.maum.biz.happycall.models.dto.CmOpInfoDTO;
import ai.maum.biz.happycall.models.vo.CmCommonCdVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonServiceImpl implements CommonService{

	@Autowired
	CommonMapper mapper;

	public List<CmCommonCdDTO> getSeachCodeList(FrontMntVO frontMntVO){
		return mapper.getSeachCodeList(frontMntVO);
	}
}
