package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.CmCompanyDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.dto.CmOpInfoDTO;
import ai.maum.biz.happycall.models.vo.CmOpInfoVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;

import java.util.List;

public interface CustOpInfoService {
	int getMonitoringCount(FrontMntVO frontMntVO);
	List<CmContractDTO> getMonitoringList(FrontMntVO frontMntVO);

	int getOpCount(FrontMntVO frontMntVO);
	List<CmOpInfoDTO> getOpList(FrontMntVO frontMntVO);

	int getMonitoringCountByOp(FrontMntVO frontMntVO);
	List<CmContractDTO> getMonitoringListByOp(FrontMntVO frontMntVO);

	boolean setOpByRandom(List<String> contractNoList);
	boolean setOp(List<String> contractNoList, String custOpId);

	boolean cancelAssign(List<String> cancelList);

	List<CmCompanyDTO> getCompanyList();

	CmOpInfoVO getOpCompanyInfoById(String custOpId);

}
