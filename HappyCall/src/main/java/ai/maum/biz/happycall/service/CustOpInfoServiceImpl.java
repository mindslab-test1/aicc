package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.CustOpInfoMapper;
import ai.maum.biz.happycall.models.dto.CmCompanyDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.dto.CmOpInfoDTO;
import ai.maum.biz.happycall.models.vo.CmOpInfoVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustOpInfoServiceImpl implements CustOpInfoService{

	@Autowired
	CustOpInfoMapper custOpInfoMapper;

	public int getMonitoringCount(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getMonitoringCount(frontMntVO);
	}

	public List<CmContractDTO> getMonitoringList(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getMonitoringList(frontMntVO);
	}

	public int getOpCount(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getOpCount(frontMntVO);
	}

	public List<CmOpInfoDTO> getOpList(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getOpList(frontMntVO);
	}

	public int getMonitoringCountByOp(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getMonitoringCountByOp(frontMntVO);
	}

	public List<CmContractDTO> getMonitoringListByOp(FrontMntVO frontMntVO) {
		return custOpInfoMapper.getMonitoringListByOp(frontMntVO);
	}

	@Transactional("transactionManager")
	public boolean setOpByRandom(List<String> contractNoList) {
		FrontMntVO frontMntVO = new FrontMntVO();
		for (int i = 0; i < contractNoList.size(); i++) {
			frontMntVO.setContractNo((contractNoList.get(i)));
			int result = custOpInfoMapper.setOpByRandom(frontMntVO);
			int result2 = custOpInfoMapper.setAssign(frontMntVO);
			if(result == 0 || result2 == 0) {
				return false;
			}
		}
		return true;
	}

	@Transactional("transactionManager")
	public boolean setOp(List<String> contractNoList, String custOpId) {
		FrontMntVO frontMntVO = new FrontMntVO();
		for (int i = 0; i < contractNoList.size(); i++) {
			frontMntVO.setContractNo((contractNoList.get(i)));
			frontMntVO.setCustOpId(custOpId);
			int result = custOpInfoMapper.setOp(frontMntVO);
			int result2 = custOpInfoMapper.setAssign(frontMntVO);
			if(result == 0 || result2 == 0) {
				return false;
			}
		}
		return true;
	}

	@Transactional("transactionManager")
	public boolean cancelAssign(List<String> cancelList) {
		FrontMntVO frontMntVO = new FrontMntVO();
		for (int i = 0; i < cancelList.size(); i++) {
			frontMntVO.setContractNo((cancelList.get(i)));
			int result = custOpInfoMapper.cancelAssign(frontMntVO);
			int result2 = custOpInfoMapper.cancelOp(frontMntVO);
			if(result == 0 || result2 == 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<CmCompanyDTO> getCompanyList() {
		return custOpInfoMapper.getCompanyList();
	}

	public CmOpInfoVO getOpCompanyInfoById(String custOpId) {
		return custOpInfoMapper.getOpCompanyInfoById(custOpId);
	}
}
