package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.InboundMonitoringMapper;
import ai.maum.biz.happycall.models.dto.SipAccountDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InboundMonitoringServiceImpl implements InboundMonitoringService {
    @Autowired
    InboundMonitoringMapper inboundMonitoringMapper;


    @Override
    public List<CmContractDTO> getInboundCallMntList(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.getInboundCallMntList(frontMntVO);
    }

    @Override
    public int getInboundCallMntCount(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.getInboundCallMntCount(frontMntVO);
    }

    @Override
    public CmContractDTO getInboundCallMntData(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.getInboundCallMntData(frontMntVO);
    }

    @Override
    public List<SipAccountDTO> getPhoneList(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.getPhoneList(frontMntVO);
    }

    @Override
    public List<CmContractDTO> getLatestCallList(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.getLatestCallList(frontMntVO);
    }

    @Override
    public int updateMemo(FrontMntVO frontMntVO) {
        return inboundMonitoringMapper.updateMemo(frontMntVO);
    }
}
