package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.MonitoringTargetUploadMapper;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MonitoringTargetUploadServiceImpl implements MonitoringTargetUploadService{

    @Autowired
    MonitoringTargetUploadMapper monitoringTargetUploadMapper;


    @Override
    public int getMonitoringTargetUploadCount(FrontMntVO frontMntVO) {
        return monitoringTargetUploadMapper.getMonitoringTargetUploadCount(frontMntVO);
    }

    @Override
    public List<CmContractDTO> getMonitoringTargetUploadList(FrontMntVO frontMntVO) {
        return monitoringTargetUploadMapper.getMonitoringTargetUploadList(frontMntVO);
    }

    @Override
    public List<CmContractDTO> getExcelTmpList() {
        return monitoringTargetUploadMapper.getExcelTmpList();
    }

    @Override
    public int getExcelTmpCount() {
        return monitoringTargetUploadMapper.getExcelTmpCount();
    }

    @Override
    public int uploadExcelTmp(List<Map<String, String>> paramMap) {
        return monitoringTargetUploadMapper.uploadExcelTmp(paramMap);
    }

    @Override
    public int resetExcelTmp() {
        return monitoringTargetUploadMapper.resetExcelTmp();
    }

    @Override
    @Transactional("transactionManager")
    public int uploadTarget(List<CmContractDTO> cmContractDTOList) {
        monitoringTargetUploadMapper.uploadTarget(cmContractDTOList);
        return monitoringTargetUploadMapper.uploadTargetOB(cmContractDTOList);

    }

    @Override
    @Transactional("transactionManager")
    public int updateUploadTarget(FrontMntVO frontMntVO) {
        monitoringTargetUploadMapper.updateUploadTarget(frontMntVO);
        return monitoringTargetUploadMapper.updateUploadTargetOB(frontMntVO);
    }

    @Override
    @Transactional("transactionManager")
    public int addUploadTarget(FrontMntVO frontMntVO) {
        monitoringTargetUploadMapper.addUploadTarget(frontMntVO);
        return monitoringTargetUploadMapper.addUploadTargetOB(frontMntVO);
    }
}
