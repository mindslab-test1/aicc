package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface OutboundMonitoringService {
    List<CmContractDTO> getOutboundCallMntList(FrontMntVO frontMntVO) throws ParseException;

    int getOutboundCallMntCount(FrontMntVO frontMntVO);

    CmContractDTO getOutboundCallMntData(FrontMntVO frontMntVO);

    int updateMemo(FrontMntVO frontMntVO);

    List<String> getCallHistList(FrontMntVO frontMntVO);
}
