<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/resources/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/resources/js/jquery.datetimepicker.full.js"></script>
<script type="text/javascript" src="/resources/js/common.js"></script>
<script type="text/javascript">
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 

$(document).ready(function() {
	checkMenu();
	noteToggle01();
	noteToggle02();
});

// publishing 단계에서 생성된 javascript
function noteToggle01() {
	$(".toggle_type01 .btn_toggle01").click(function() {
		if($(this).parents('.toggle_type01').hasClass("on")) {
			$(this).parents('.toggle_type01').removeClass("on");
		} else {
			$(this).parents('.toggle_type01').addClass("on");
		}
		return false;
	});
}
//publishing 단계에서 생성된 javascript
function noteToggle02() {
	$(".toggle_type02 .btn_toggle02").click(function() {
		if($(this).parents('.toggle_type02').hasClass("on")) {
			$(this).parents('.toggle_type02').removeClass("on");
			$(this).text("더보기");
		} else {
			$(this).parents('.toggle_type02').addClass("on");
			$(this).text("접기");
		}
		return false;
	});
}
//검색 부분 대상일자 datapicker를 위한 함수. 
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var checkEnd = now;
var checkStart = $('#schTopTargetDt').datepicker({
	  onRender: function(date) {
		return date.valueOf() > now.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  if (ev.date.valueOf() > checkEnd) {
		var newDate = new Date(ev.date)
		newDate.setDate(newDate.getDate() + 1);
		checkEnd = newDate;
	  }
	  checkStart.hide();
	}).data('datepicker');
	


//페이지 로딩 후 실행. 현재 메뉴임을 표시하는 보라색 + 볼드체 class 적용.
function checkMenu(){
	var menuId = '<c:out value="${menuId}" />';
	
	$('#snb_menu ul li').each(function(i, e){
		if( $(this).attr('id') == menuId ){
			$(this).addClass('on');
		} 
	});
}



//좌측 메뉴 클릭시 menuUrl로 이동
function goMenu(menuUrl){
	location.href=menuUrl;
}


//프로필 로그아웃 클릭시 로그아웃
function logout(){
	location.href="/logout";
}
</script>
