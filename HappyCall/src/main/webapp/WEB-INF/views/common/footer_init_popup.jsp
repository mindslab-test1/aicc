<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/resources/js/player.js"></script>
<script type="text/javascript" src="/resources/js/common.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.min.js"></script>
<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 

jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		$('.lyr_view').on('click',function(){
			$('.lyrWrap').fadeIn(300);
			$('#lyr_view').show();
		});	
	});	
});		
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
 
var checkin = $('#startDate').datepicker({
  onRender: function(date) {
	return date.valueOf() > now.valueOf() ? 'disabled' : '';
  }
}).on('changeDate', function(ev) {
  if (ev.date.valueOf() > checkout.date.valueOf()) {
	var newDate = new Date(ev.date)
	newDate.setDate(newDate.getDate() + 1);
	checkout.setValue(newDate);
  }
  checkin.hide();
  $('#endDate')[0].focus();
}).data('datepicker');
var checkout = $('#endDate').datepicker({
  onRender: function(date) {
	return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
  }
}).on('changeDate', function(ev) {
  checkout.hide();
}).data('datepicker');		

// audio
// $( function() { 
// 	$( 'audio' ).audioPlayer(); 
// 	} 
// );
</script>
