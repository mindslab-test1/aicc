<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="search_box">
	<ul>
		<li>
			<div class="select_type">
				<label for="schTopMntType">모니터링 종류</label>
				<select id="schTopMntType">
					<option value="">모니터링 종류</option>
					${mntType}
				</select>
			</div>
		</li>
		<li>
			<div class="select_type">
				<label for="schTopFinalResult">최종 결과</label>
				<select id="schTopFinalResult">
					<option value="">최종 결과</option>
					${finalResultCode}
				</select>
			</div>
		</li>
		<li><input type="text" id="schTopCustNm" placeholder="고객명" title="고객명" onkeydown="enterCheck(this)"></li>
		<li><input type="tel" id="schTopCustTelNo" placeholder="전화번호" title="전화번호"  onKeyup="checkKey('custTelNo')" onkeydown="enterCheck(this)"></li>
		<li>
			<div class="select_type">
				<label for="schTopCallState">콜상태</label>
				<select id="schTopCallState">
					<option value="">콜상태</option>
					${callStatusCode}
				</select>
			</div>
		</li>
		<li>
			<div class="select_type">
				<label for="schTopCallResult">모니터링 내용</label>
				<select id="schTopCallResult">
					<option value="">모니터링 내용</option>
					${monitoringResultCode}
				</select>
			</div>
		</li>
	</ul>
	<div class="btn_result">
		<button type="button" id="doSearch" class="btn_default01" onclick="goSearch(true)">결과보기</button>
	</div>
</div>
