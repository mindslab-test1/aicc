<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

		<!-- snb -->
		<div id="snb_menu">
			<ul>

					<li id="m01"><a href="javascript:goMenu('mntTargetUpload');"><i class="fas fa-upload"></i>모니터링 대상 업로드</a></li>
				<sec:authorize access="hasAnyAuthority('ROLE_ADMIN', 'ROLE_HH')">
					<%--<li id="m02"><a href="#"><i class="fas fa-clipboard-list"></i>모니터링 대상 관리</a></li>--%>
					<li id="m03"><a href="javascript:goMenu('opAssign');"><i class="fas fa-headset"></i>상담사 배정</a></li>
				</sec:authorize>
					<li id="m04"><a href="javascript:goMenu('manualCallMnt');"><i class="fas fa-hand-point-up"></i>수동 모니터링 실행</a></li>
				<sec:authorize access="hasAnyAuthority('ROLE_ADMIN', 'ROLE_HH')">
					<li id="m05"><a href="javascript:goMenu('autoCallMnt');"><i class="fas fa-atom"></i>자동 모니터링 실행</a></li>
				</sec:authorize>
					<li id="m06"><a href="javascript:goMenu('mntResult');"><i class="fas fa-laptop"></i>모니터링 결과</a></li>
					<li id="m07"><a href="javascript:goMenu('callStatus');"><i class="fas fa-laptop"></i>인바운드 현황</a></li>
					<li id="m08"><a href="javascript:goMenu('ibMntResult');"><i class="fas fa-phone-volume"></i>인바운드 결과</a></li>
				<sec:authorize access="hasAnyAuthority('ROLE_ADMIN', 'ROLE_HH')">
					<li id="m10"><a href="javascript:goMenu('callbackManage');"><i class="fas fa-undo-alt"></i>콜백 관리</a></li>
					<li id="m09"><a href="javascript:goMenu('userManage');"><i class="far fa-id-card"></i>사용자 관리</a></li>
				</sec:authorize>

				<li id="m99" style="bottom:20px;position:fixed;"><a href="/download/agent.zip">PC-AGENT 다운로드</a></li>
			</ul>
		</div>
		<!-- //snb -->
