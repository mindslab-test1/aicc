<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>
<body>
<form id="valueForm" name="valueForm">
    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox"><span></span><span></span><span></span><span></span></div>
    </div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu.jsp" %>
            <!-- //snb -->

            <!-- contents -->
            <div id="contents">

                <!-- data list -->
                <div class="data_list tbl_thead">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:10%;">
                            <col style="width:15%;">
                            <col style="width:25%;">
                            <col style="width:20%;">
                            <col style="width:20%;">
                            <col style="width:10%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">
                                <div class="checkbox form_type01">
                                    <input type="checkbox" id="chkBox" onclick="chkBoxAllCheck(this);"><label for="chkBox"></label>
                                </div>
                            </th>
                            <th scope="col">No.</th>
                            <th id='USERNAME' scope="col" onclick="onChangeSort(this.id)">ID</th>
                            <th id='NAME' scope="col" onclick="onChangeSort(this.id)">이름</th>
                            <th id='CREATED_DTM' scope="col" onclick="onChangeSort(this.id)">생성일</th>
                            <th id='UPDATED_DTM' scope="col" onclick="onChangeSort(this.id)">수정일</th>
                            <th id='ENABLED' scope="col" onclick="onChangeSort(this.id)">사용여부</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="data_list tbl_tbody">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:10%;">
                            <col style="width:15%;">
                            <col style="width:25%;">
                            <col style="width:20%;">
                            <col style="width:20%;">
                            <col style="width:10%;">
                        </colgroup>
                        <tbody>
                        <c:choose>
                            <c:when test="${fn:length(userList) gt 0}">
                                <c:forEach items="${userList}" var="userList" varStatus="status">
                                    <tr style="cursor: pointer;" onclick="editAccount('${userList.userId}');">
                                        <td onclick="event.cancelBubble=true">
                                            <div class="checkbox form_type01">
                                                <input type="checkbox" id="checkBox01${userList.userId}" name="checkbox1" value="${userList.userId}" onclick="chkBoxOneCheck(this);"><label for="checkBox01${userList.userId}"></label>
                                            </div>
                                        </td>
                                        <td>${userList.userId}</td>
                                        <td>${userList.username}</td>
                                        <td>${userList.name}</td>
                                        <td>${userList.createdDtm}</td>
                                        <td>${userList.updatedDtm}</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${userList.enabled}">
                                                    사용중
                                                </c:when>
                                                <c:otherwise>
                                                    미사용
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="13">데이터 없음</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
                <!-- //data list -->

                <!-- page_area -->
                <div class="page_area">
                    <div class="btn_monitor">
                        <button type="button" id="accountManageBtn" class="btn_default01" onclick="addUser();" >사용자 추가</button>
                        <button type="button" class="btn_default04" onclick="checkConfirm('inactive');">계정 비활성화</button>
                        <button type="button" class="btn_default04" onclick="checkConfirm('active');">계정 활성화</button>
                    </div>

                    <%@ include file="../common/paging.jsp" %>
                </div>
                <!-- //page_area -->

                <input type="hidden" name="checkedChkBox" id="checkedChkBox" />
                <input type="hidden" id="sortingTarget" name="sortingTarget" />
                <input type="hidden" id="direction" name="direction" />
                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>
                <input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}"/>

            </div>
            <!-- //contents -->

        </div>

        <%--// 각각의 알림창 관련 id--%>
        <div id="successDialog" class="dialog"></div>
        <div id="failDialog" class="dialog"></div>
        <div id="warnDialog" class="dialog"></div>
        <div id="activeDialog" class="dialog"></div>
        <div id="inactiveDialog" class="dialog"></div>
        <!-- //container -->

    </div>
    <!-- //wrap -->

    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- javascript link & init -->

    <script type="text/javascript">
        var sortingTarget = "${frontMntVO.sortingTarget}";
        var direction = "${frontMntVO.direction}";

        $(document).ready(function(){

            var successDialogMsg = '완료되었습니다.';
            var successDialogId = 'successDialog';
            var successDialogType = 'success';
            setDialog(successDialogId, successDialogMsg, successDialogType);

            var failDialogMsg = '실패되었습니다.';
            var failDialogId = 'failDialog';
            var failDialogType = 'fail';
            setDialog(failDialogId, failDialogMsg, failDialogType);

            var warnDialogMsg = '데이터를 선택해주세요.';
            var warnDialogId = 'warnDialog';
            var warnDialogType = 'warn';
            setDialog(warnDialogId, warnDialogMsg, warnDialogType);

            var activeDialogMsg = '활성화 시키시겠습니까?';
            var activeDialogId = 'activeDialog';
            var activeDialogType = 'active';
            setDialog(activeDialogId, activeDialogMsg, activeDialogType);

            var inactiveDialogMsg = '비활성화 시키시겠습니까?';
            var inactiveDialogId = 'inactiveDialog';
            var inactiveDialogType = 'inactive';
            setDialog(inactiveDialogId, inactiveDialogMsg, inactiveDialogType);

            var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
            if( rows != '' ){
                $('#pageInitPerPage').val(rows);
            }

            //체크박스 클릭시 현상태와 반대가 됨
            $("#chkBox").click(function () {
                var obj = document.getElementsByName("checkbox1");
                for (var i = 0; i < obj.length; i++) {
                    obj[i].checked = !obj[i].checked;
                }
            });

            $('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
            $('#direction').val( '<c:out value="${frontMntVO.direction}" />' );

            if(sortingTarget) {
                initSort();
            }
        });

        function initSort() {
            var backup = $('#' + sortingTarget).html();
            backup = backup.substring(backup.indexOf('<i>')).trim();
            if(direction === 'asc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
            } else if(direction === 'desc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
            } else {
                $('#' + sortingTarget).html(backup);
            }
        }

        function onChangeSort(id) {
            if (sortingTarget !== id) {
                sortingTarget = id;
                direction = 'asc';
            } else {
                if (direction === 'asc') {
                    direction = 'desc';
                } else if (direction === 'desc') {
                    sortingTarget = '';
                    direction = '';
                } else {
                    direction = 'asc';
                }
            }
            $('#sortingTarget').val(sortingTarget);
            $('#direction').val(direction);
        }

        function setDialog(id, msg, type) {
            var buttons;
            if(type === 'active') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            activeUser();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='inactive') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            deleteUser();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='warn') {
                buttons = [
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else {
                buttons = [
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("close");
                            location.reload();
                        }
                    }
                ];
            }

            $('#'+id).html(msg);
            $('#'+id).dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: buttons
            });
        }

        function checkConfirm(flag) {
            var checkedRow = $('input:checkbox[name=checkbox1]:checked');
            console.log(checkedRow);
            if(checkedRow.length == 0) {
                $('#warnDialog').dialog('open');
            } else {
                if(flag === 'active') {
                    $('#activeDialog').dialog('open');
                } else if (flag === 'inactive') {
                    $('#inactiveDialog').dialog('open');
                }
            }
        }

        //페이지 이동 관련
        function goPage(cp){
            $('#currentPage').val(cp);

            //TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
            goSearch();
        }

        function goSearch() {
            valueForm.action = "/userManage";
            valueForm.method = "POST";
            valueForm.submit();
        }

        //페이지 이동 관련
        function goPage(cp){
            $('#currentPage').val(cp);

            goSearch();
        }

        //체크 박스 전체 선택/해제
        function chkBoxAllCheck(obj){
            if( obj.checked ){
                $('input[name=chkBoxes]').prop("checked", true);
            }else{
                $('input[name=chkBoxes]').prop("checked", false);
            }
        }

        //체크박스 개별 선택/해제
        function chkBoxOneCheck(obj){
            if( obj.checked ){
                obj.checked = true;
            }else{
                obj.checked = false;
            }
        }

        //비활성화할 아이디 선택후 비활성화 해줌
        function deleteUser() {

            var chkbox = document.getElementsByName('checkbox1');
            var checked_arr = [];

            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            $.ajax({
                method: 'POST',
                url: '/deleteAccount',
                data: JSON.stringify(checked_arr),
                contentType : "application/json; charset=utf-8",
                beforeSend : function(xhr) {
                    /!*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*!/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //비활성화 성공시
            }).done(function(data){
                if(data === "SUCC"){
                $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                    chkBoxReset();
                }
                //비활성화 실패시
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });

        }

        //활성화 해줄 아이디 선택후 활성화 해줌
        function activeUser() {

            var chkbox = document.getElementsByName('checkbox1');
            var checked_arr = [];

            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            $.ajax({
                method: 'POST',
                url: '/activeAccount',
                data: JSON.stringify(checked_arr),
                contentType : "application/json; charset=utf-8",
                beforeSend : function(xhr) {
                    /!*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*!/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //활성화 성공시
            }).done(function(data){
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                    chkBoxReset();
                }
                //활성화 실패시
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });

        }

        //사용자 추가 팝업 실행하기
        function addUser(){
            var popupX = (document.body.offsetWidth / 2) - (400 / 2);
            var popupY= (document.body.offsetHeight / 2) - (300 / 2);
            var popSize = "width=400,height=400";
            var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no, left=" + popupX + ", top=" + popupY;
            var pop1 = window.open("/accountManagePop?popupType=insert", "pop1", popSize + "," + popOption);
        }

        //사용자 수정 팝업 실행하기
        function editAccount(userId){
            var popupX = (document.body.offsetWidth / 2) - (400 / 2);
            var popupY= (document.body.offsetHeight / 2) - (300 / 2);
            var popSize = "width=400,height=400";
            var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no, left=" + popupX + ", top=" + popupY;
            var pop2 = window.open("/accountManagePop?popupType=modify" + "&userId=" + userId, "pop2", popSize + "," + popOption);
        }
    </script>
</form>
</body>
</html>





















