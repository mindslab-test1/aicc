<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <sec:authentication var="user" property="principal"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>
<body style="width:400px; height:400px;">
<div id="popup_wrap" style="width:400px; height:350px;">
    <!-- popup_header -->
    <div class="popup_header">
        <h1>사용자 계정</h1>
    </div>
    <!-- //popup_header -->

    <!-- popup_contents -->
    <div class="popup_contents_b">
        <!-- customInfo_wrap -->
        <div class="customInfo_wrap_c">
            <div class="custom_list_c">
                <table>
                    <caption>계정 정보</caption>
                    <colgroup>
                        <col style="width:80px">
                        <col style="width:auto">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">계정</th>
                        <td>
                            <input type="text" style="border: none; width:90%;" id="addUserName" placeholder="ID" title="ID" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">이름</th>
                        <td>
                            <input type="text" style="border: none; width:90%;" id="addName" placeholder="NAME" title="NAME" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">비밀번호</th>
                        <td>
                            <input type="password" style="border: none; width:90%; " id="addPassword" placeholder="P/W" title="P/W" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">회사</th>
                        <td>
                            <div class="select_type">
                                <label for="addCompany">회사</label>
                                <select id="addCompany">
                                    ${companyList}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">권한</th>
                        <td>
                            <c:forEach items="${roleList}" var="role" varStatus="status">
                                <div style="cursor: pointer;" onclick="editAccount('${role.roleNm}');">
                                    <colspan onclick="event.cancelBubble=true">
                                        <div class="checkbox form_type01">
                                            <input type="checkbox" id="checkBox01${role.roleId}" name="checkbox1" value="${role.roleId}" onclick="chkBoxOneCheck(this);"><label for="checkBox01${role.roleId}"></label>
                                        </div>
                                        <colspan>${role.roleNm}</colspan>
                                    </colspan>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- //customInfo_wrap -->
    </div>
    <!-- //popup_contents -->

    <!-- popup_bottom -->
    <div class="popup_bottom">
        <div class="btnPage_wrap_b">
            <div style="display: flex; justify-content: space-between">
                <button type="button" id="account_save" class="btn_default02" onclick="checkConfirm();">저장</button>
                <button type="button" id="account_cancel" class="btn_default02" onclick="javascript:self.close();">취소</button>
            </div>
        </div>
    </div>

    <%--// 각각의 알림창 관련 id--%>
    <div id="successDialog" class="dialog"></div>
    <div id="failDialog" class="dialog"></div>
    <div id="warnDialog" class="dialog"></div>
    <div id="dupDialog" class="dialog"></div>
    <!-- //popup_bottom -->

    <!-- javascript link & init -->
    <%@ include file="../common/footer_init_popup.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">

        var authList = [];
        var popupType = '${popupType}';
        var checked_arr = [];
        var chkbox = document.getElementsByName('checkbox1');

        // 계정별 가진 권한 배열 생성
        <c:forEach items="${authList}" var="allAuth">
            var role_id = "${allAuth.roleId}";
            authList.push(role_id);
        </c:forEach>

        $(document).ready(function() {

            init();

            var username = '${username}';
            var name = '${name}';
            var companyId = '${companyId}';

            $('#addUserName').val(username);
            $('#addName').val(name);
            $('#addCompany').val(companyId);

            for(var i=0; i<authList.length; i++) {
                $("input:checkbox[name='checkbox1']:checkbox[value=" + authList[i] + "]").prop('checked', true);
            }



        });

        function init() {
            var warnDialogMsg = '정보를 입력해 주시기 바랍니다.';
            var warnDialogId = 'warnDialog';
            var warnDialogType = 'warn';
            setDialog(warnDialogId, warnDialogMsg, warnDialogType);

            var successDialogMsg = '저장 완료되었습니다.';
            var successDialogId = 'successDialog';
            var successDialogType = 'success';
            setDialog(successDialogId, successDialogMsg, successDialogType);

            var failDialogMsg = '저장 실패되었습니다.';
            var failDialogId = 'failDialog';
            var failDialogType = 'fail';
            setDialog(failDialogId, failDialogMsg, failDialogType);

            var dupDialogMsg = '아이디가 중복입니다. 다른 아이디를 입력해주세요.';
            var dupDialogId = 'dupDialog';
            var dupDialogType = 'dup';
            setDialog(dupDialogId, dupDialogMsg, dupDialogType);
        }

        function setDialog(id, msg, type) {
            var buttons;
            if(type === 'success' || type === 'fail') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            $(this).dialog("close");
                            opener.parent.location.reload();
                        }
                    }
                ];
            } else if(type ==='dup') {
                buttons = [
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='warn') {
                buttons = [
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            }

            $('#'+id).html(msg);
            $('#'+id).dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: buttons
            });
        }

        function checkConfirm() {
            var checkedRow = $('input:checkbox[name=checkbox1]:checked');
            if($('#addUserName').val().length < 1 || $('#addName').val().length < 1 || $('#addPassword').val().length < 1 || checkedRow.length == 0 || $('#addCompany').val().length < 1) {
                $('#warnDialog').dialog('open');
            } else if($('#addUserName').val().length > 0 && $('#addName').val().length > 0 && $('#addPassword').val().length > 0 && checkedRow.length != 0) {
                if(popupType == 'insert') {
                    addPopSave();
                } else if(popupType == 'modify'){
                    modifyPopSave();
                }
            }
        }

        if(popupType == 'modify') {
            // 수정 시 아이디는 읽기만 가능
            $("#addUserName").prop("readonly", true);
        }


        //계정 추가
        function addPopSave(){

            var addUserName = $('#addUserName').val();
            var addName = $('#addName').val();
            var addPassword = $('#addPassword').val();
            var addCompany = $('#addCompany').val();

            //체크된 권한 배열
            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            $.ajax({
                //단일값 및 배열을 controller로 넘길 때 필요한 옵션
                traditional: true,
                url : "/addAccount",
                data : {username:addUserName, name:addName, password:addPassword, checkedList:checked_arr, companyId: addCompany},
                type : "POST",
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //생성 성공시
            }).done(function(data){
                if(data == "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data == "FAIL"){
                    $('#failsDialog').dialog('open');
                } else if(data === "DUP") {
                    $('#dupDialog').dialog('open');
                    chkBoxReset();
                }
                //생성 실패시
            }).fail(function(data){
                if(data == "FAIL"){
                    $('#failsDialog').dialog('open');
                }
            });

        }

        // 기존 계정 수정
        function modifyPopSave(){

            var addAuth = [];
            var delAuth = [];
            var id = '${userId}';
            var addUserName = $('#addUserName').val();
            var addName = $('#addName').val();
            var addPassword = $('#addPassword').val();
            var addCompany = $('#addCompany').val();

            //체크된 권한 배열
            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            //기존 권한 정보 및 수정할 권한 비교해서 추가할 권한 배열
            for(var i = 0; i < checked_arr.length; i++) {
                if (!authList.includes(checked_arr[i])) {
                    addAuth.push(checked_arr[i]);
                }
            }

            //기존 권한 정보 및 수정할 권한 비교해서 삭제할 권한 배열
            for(var i = 0; i < authList.length; i++) {
                if(!checked_arr.includes(authList[i])) {
                    delAuth.push(authList[i]);
                }
            }

            $.ajax({
                traditional: true,
                url : "/editAccount",
                data : {userId:id, username:addUserName, name:addName, newPassword:addPassword, checkedList:addAuth, delAuthList:delAuth, companyId: addCompany},
                type : "POST",
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //수정 성공시
            }).done(function(data){
                if(data == "SUCC"){
                    $('#successDialog').dialog('open');
                } else {
                    $('#failsDialog').dialog('open');
                }
                //수정 실패시
            }).fail(function(data){
                if(data == "FAIL"){
                    $('#failsDialog').dialog('open');
                }
            });
        }
    </script>
</div>
</body>
</html>
