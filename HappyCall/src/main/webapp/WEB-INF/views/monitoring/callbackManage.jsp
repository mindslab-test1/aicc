<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox"><span></span><span></span><span></span><span></span></div>
    </div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu.jsp" %>
            <!-- //snb -->

            <!-- contents -->
            <div id="contents">

                <!-- Search BOX -->
                <%@ include file="../common/callbackHeaderSearch.jsp" %>
                <!-- //Search BOX -->

                <!-- data list -->
                <div class="data_list tbl_thead">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:10%;">
                            <col style="width:25%">
                            <col style="width:15%;">
                            <col style="width:20%;">
                            <col style="width:15%">
                            <col style="width:15%;">
                            <col style="width:70px;">
                            <col style="width:140px;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">
                                <div class="checkbox form_type01">
                                    <input type="checkbox" id="chkBox" onclick="chkBoxAllCheck(this);"><label for="chkBox"></label>
                                </div>
                            </th>
                            <th scope="col">No.</th>
                            <th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id)">계약 종류</th>
                            <th id='CUST_NM' scope="col" onclick="onChangeSort(this.id)">고객명</th>
                            <th id='CUST_TEL_NO' scope="col" onclick="onChangeSort(this.id)">전화번호</th>
                            <th id='CD_DESC' scope="col" onclick="onChangeSort(this.id)">콜백대상여부</th>
                            <th id='CALLBACK_DT' scope="col" onclick="onChangeSort(this.id)">콜백일자</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="data_list tbl_tbody">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:10%;">
                            <col style="width:25%">
                            <col style="width:15%;">
                            <col style="width:20%;">
                            <col style="width:15%">
                            <col style="width:15%;">
                            <col style="width:70px;">
                            <col style="width:140px;">
                            <col style="width: 0px";>
                        </colgroup>
                        <tbody>
                        <c:choose>
                            <c:when test="${fn:length(callbackList) gt 0}">
                                <c:forEach items="${callbackList}" var="callbackList" varStatus="status">
                                    <tr>
                                        <td onclick="event.cancelBubble=true">
                                            <div class="checkbox form_type01">
                                                <input type="checkbox" id="checkBox01${callbackList.contractNo}" name="checkbox1" value="${callbackList.callId}" onclick="chkBoxOneCheck(this);"><label for="checkBox01${callbackList.contractNo}"></label>
                                            </div>
                                        </td>
                                        <td>${callbackList.contractNo}</td>
                                        <td>${callbackList.campaignNm}</td>
                                        <td>${callbackList.custNm}</td>
                                        <td>${callbackList.telNo}</td>
                                        <td>${callbackList.callbackYn}</td>
                                        <td>${callbackList.callbackDt}</td>
                                        <td><button type="button" class="btn_detail" name="updateDt" style="cursor: pointer"><em>수정</em></button></td>
                                        <td><button type="button" class="btn_detail" style="cursor: pointer" onclick="resultDetail('${callbackList.contractNo}','${callbackList.callId}');"><em>상세보기</em></button></td>
                                        <td style="display: none;">${callbackList.lastCallId}</td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="13">데이터 없음</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
                <!-- //data list -->

                <!-- page_area -->
                <div class="page_area">
                    <div class="btn_monitor">
                        <button type="button" class="btn_default04" onclick="checkConfirm('callbackCancel');">콜백취소</button>
                        <button type="button" class="btn_default04" onclick="checkConfirm('callback');">콜백지정</button>
                    </div>

                    <%@ include file="../common/paging.jsp" %>
                </div>
                <!-- //page_area -->


                <input type="hidden" name="checkedChkBox" id="checkedChkBox" />
                <input type="hidden" id="schCampaignId" name="schCampaignId" />
                <input type="hidden" id="schCustNm" name="schCustNm" />
                <input type="hidden" id="schCustTelNo" name="schCustTelNo" />
                <input type="hidden" id="schCallbackStatus" name="schCallbackStatus" />
                <input type="hidden" id="schCallbackDt" name="schCallbackDt" />

                <input type="hidden" id="sortingTarget" name="sortingTarget" />
                <input type="hidden" id="direction" name="direction" />
                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>

                <input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}" />

            </div>
            <!-- //contents -->
        </div>

        <%--// 각각의 알림창 관련 id--%>
        <div id="successDialog" class="dialog"></div>
        <div id="failDialog" class="dialog"></div>
        <div id="warnDialog" class="dialog"></div>
        <div id="callbackDialog" class="dialog"></div>
        <div id="callbackCancelDialog" class="dialog"></div>
        <div id="checkKey1Dialog" class="dialog"></div>
        <div id="onlyOneDialog" class="dialog"></div>

        <!-- //container -->

        <!-- footer -->
        <div id="footer">

        </div>
        <!-- //footer -->
    </div>
    <!-- //wrap -->


    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">
        var sortingTarget = "${callbackVO.sortingTarget}";
        var direction = "${callbackVO.direction}";
        var modifyFlag = 0;					// 테이블 한 로우 수정여부 flag

        $(document).ready(function(){
            $('#schTopCallbackDt').datepicker();

            var rows = '<c:out value="${callbackVO.pageInitPerPage}" />';
            var successDialogMsg = '완료되었습니다.';
            var successDialogId = 'successDialog';
            var successDialogType = 'success';
            setDialog(successDialogId, successDialogMsg, successDialogType);

            var failDialogMsg = '실패되었습니다.';
            var failDialogId = 'failDialog';
            var failDialogType = 'fail';
            setDialog(failDialogId, failDialogMsg, failDialogType);

            var warnDialogMsg = '대상을 선택해주세요.';
            var warnDialogId = 'warnDialog';
            var warnDialogType = 'warn';
            setDialog(warnDialogId, warnDialogMsg, warnDialogType);

            var callbackDialogMsg = '콜백 지정 하시겠습니까?';
            var callbackDialogId = 'callbackDialog';
            var callbackDialogType = 'callback';
            setDialog(callbackDialogId, callbackDialogMsg, callbackDialogType);

            var callbackCancelDialogMsg = '콜백 취소 하시겠습니까?';
            var callbackCancelDialogId = 'callbackCancelDialog';
            var callbackCancelDialogType = 'callbackCancel';
            setDialog(callbackCancelDialogId, callbackCancelDialogMsg, callbackCancelDialogType);

            var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
            var checkKey1DialogId = 'checkKey1Dialog';
            var checkKey1DialogType = 'checkKey1';
            setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

            var onlyOneDialogMsg = "1건씩 수정 가능합니다.";
            var onlyOneDialogId = 'onlyOneDialog';
            var onlyOneDialogType = 'onlyOne';
            setDialog(onlyOneDialogId, onlyOneDialogMsg, onlyOneDialogType);

            var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
            if( rows != '' ){
                $('#pageInitPerPage').val(rows);
            }

            //체크박스 클릭시 현상태와 반대가 됨
            $("#chkBox").click(function () {
                var obj = document.getElementsByName("checkbox1");
                for (var i = 0; i < obj.length; i++) {
                    obj[i].checked = !obj[i].checked;
                }
            });

            $('#schTopCampaignId').val( '<c:out value="${callbackVO.schCampaignId}" />' );
            $('#schTopCustNm').val( '<c:out value="${callbackVO.schCustNm}" />' );
            $('#schTopCustTelNo').val( '<c:out value="${callbackVO.schCustTelNo}" />' );
            $('#schTopCallbackStatus').val( '<c:out value="${callbackVO.schCallbackStatus}" />' );
            $('#schTopCallbackDt').val( '<c:out value="${callbackVO.schCallbackDt}" />' );

            $('#sortingTarget').val( '<c:out value="${callbackVO.sortingTarget}" />' );
            $('#direction').val( '<c:out value="${callbackVO.direction}" />' );

            if(sortingTarget) {
                initSort();
            }

        $('button[name="updateDt"]').click(function() {
            if( modifyFlag == 1 ){
                $('#onlyOneDialog').dialog('open');
                return false;
            }else{
                modifyFlag = 1;
            }

            //클릭한 로우 값을 가져옴
            var $item = $(this).closest("tr").find("td");

            //각 td값은 array로 저장
            var tmpArr = [];
            $.each($item, function (index) {
                if (index < ($item.length)) {
                    tmpArr.push($(this).text());
                }
            });

            //string format 함수 호출
            var madeTag = strFmt(tmpModifyTag, tmpArr);

            //클릭한 로우에 수정 가능한 폼으로 업데이트
            var $tr = $(this).closest("tr");
            $tr.replaceWith(madeTag);

            // 콜백날짜 변경
            $("#modifyingCallbackDt").datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                pickDate: false,
                pickTime: true,
                useSeconds: false,
                step: 10

            });
        });

        // 수정을 위한 Templete Tag
        var tmpModifyTag = "<tr>";
        tmpModifyTag = tmpModifyTag + "<td>{0}</td>";
        tmpModifyTag = tmpModifyTag + "<td>{1}</td>";
        tmpModifyTag = tmpModifyTag + "<td>{2}</td>";
        tmpModifyTag = tmpModifyTag + "<td>{3}</td>";
        tmpModifyTag = tmpModifyTag + "<td>{4}</td>";
        tmpModifyTag = tmpModifyTag + "<td>{5}</td>";
        tmpModifyTag = tmpModifyTag + "<td><input type='text' id='modifyingCallbackDt' value='{6}' size='17'></td>";
        tmpModifyTag = tmpModifyTag + "<td></td>";
        tmpModifyTag = tmpModifyTag + "<td><button type='button' class='btn_detail' onclick='modifiedSave(this, {9})'><em>저장</em></button></td>";
        tmpModifyTag = tmpModifyTag + "</tr>";

        });


        function initSort() {
            var backup = $('#' + sortingTarget).html();
            backup = backup.substring(backup.indexOf('<i>')).trim();
            if(direction === 'asc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
            } else if(direction === 'desc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
            } else {
                $('#' + sortingTarget).html(backup);
            }
        }

        function onChangeSort(id) {
            if(sortingTarget !== id) {
                sortingTarget = id;
                direction = 'asc';
            } else {
                if(direction === 'asc') {
                    direction = 'desc';
                } else if (direction === 'desc') {
                    sortingTarget = '';
                    direction = '';
                } else {
                    direction = 'asc';
                }
            }
            $('#sortingTarget').val(sortingTarget);
            $('#direction').val(direction);

            //TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
            goSearch();
        }

        function setDialog(id, msg, type) {
            var buttons;
            if(type === 'callback') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            doCallback();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='callbackCancel') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            undoCallback();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='warn') {
                buttons = [
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else {
                buttons = [
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("close");
                            location.reload();
                        }
                    }
                ];
            }

            $('#'+id).html(msg);
            $('#'+id).dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: buttons
            });
        }

        function checkConfirm(flag) {
            var checkedRow = $('input:checkbox[name=checkbox1]:checked');
            if(checkedRow.length == 0) {
                $('#warnDialog').dialog('open');
            } else {
                if(flag === 'callback') {
                    $('#callbackDialog').dialog('open');
                } else if (flag === 'callbackCancel') {
                    $('#callbackCancelDialog').dialog('open');
                }
            }
        }

        //검색실행
        function goSearch(condition){
            if(condition ==  true) {
                $('#currentPage').val(1);
            }
            var schCampaignId         = $('#schTopCampaignId').val();
            var schCustNm             = $('#schTopCustNm').val();
            var schCustTelNo          = $('#schTopCustTelNo').val();
            var schCallbackStatus     = $('#schTopCallbackStatus').val();
            var schCallbackDt         = $('#schTopCallbackDt').val();

            $('#schCampaignId').val(schCampaignId);
            $('#schCustNm').val(schCustNm);
            $('#schCustTelNo').val(schCustTelNo);
            $('#schCallbackStatus').val(schCallbackStatus);
            $('#schCallbackDt').val(schCallbackDt);

            valueForm.method = "POST";
            valueForm.action = "/callbackManage";
            valueForm.submit();
        }

        //페이지 이동 관련
        function goPage(cp){
            $('#currentPage').val(cp);

            goSearch(false);
        }

        //체크 박스 전체 선택/해제
        function chkBoxAllCheck(obj){
            if( obj.checked ){
                $('input[name=chkBoxes]').prop("checked", true);
            }else{
                $('input[name=chkBoxes]').prop("checked", false);
            }
        }

        //체크박스 개별 선택/해제
        function chkBoxOneCheck(obj){
            if( obj.checked ){
                obj.checked = true;
            }else{
                obj.checked = false;
            }
        }

        // 콜백 리스트 상세보기 팝업
        function resultDetail(contract_no, call_id){
            var popSize = "width=800,height=600";
            var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
            winpop1 = window.open("/mntResultPop?ctn="+call_id+"&cno="+contract_no,"winpop1", popSize + "," + popOption);
        }

        //콜백비대상할 계약 선택후 변경
        function undoCallback() {

            var chkbox = document.getElementsByName('checkbox1');
            var checked_arr = [];

            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            $.ajax({
                method: 'POST',
                url: '/undoCallback',
                data: JSON.stringify(checked_arr),
                contentType : "application/json; charset=utf-8",
                beforeSend : function(xhr) {
                    /!*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*!/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //취소 성공시
            }).done(function(data){
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
                //취소 실패시
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });

        }

        //콜백대상할 계약 선택후 변경
        function doCallback() {

            var chkbox = document.getElementsByName('checkbox1');
            var checked_arr = [];

            for (var i = 0; i < chkbox.length; i++) {
                if (chkbox[i].checked) {
                    checked_arr.push(chkbox[i].value);
                }
            }

            $.ajax({
                method: 'POST',
                url: '/doCallback',
                data: JSON.stringify(checked_arr),
                contentType : "application/json; charset=utf-8",
                beforeSend : function(xhr) {
                    /!*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*!/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //지정 성공시
            }).done(function(data){
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
                //지정 실패시
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });
        }

        //키 입력시 허용 값 체크
        function checkKey( objName ){
            if( objName == 'custTelNo' ){
                if( check_key_reg(1, $('#schTopCustTelNo').val() ) == false ){
                    $('#schTopCustTelNo').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }else if( objName == 'callbackDt' ){
                if( check_key_reg(1, $('#schTopCallbackDt').val() ) == false ){
                    $('#schTopCallbackDt').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }
        }

        //값 체크 정규식 함수
        function check_key_reg( mode, text ){
            if( mode == 1 ){
                var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용

            }else if( mode == 2 ){
                var regexp = /[0-9]/; 				// 숫자만 허용

            }else if( mode == 3 ){
                var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용

            }else if( mode == 4 ){
                var regexp = /[a-zA-Z]/; 			// 영문만 허용

            }

            for( var i=0; i<text.length; i++){
                if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
                    return false;
                }
            }
        }

        //엔터값 체크
        function enterCheck(){
            if( window.event.keyCode == 13 ){
                goSearch(true);
            }else{
                return false;
            }
        }

        //string format과 같이 기호 치환
        var strFmt = function (str, col) {
            col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

            return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
                if (m == "{{") { return "{"; }
                if (m == "}}") { return "}"; }
                return col[n];
            });
        };

        //1 row은 수정 후 저장 버튼 클릭 시 실행
        function modifiedSave(event, callId){
            var modifyingCallbackDt = $('#modifyingCallbackDt').val();

            $.ajax({
                url : "/callbackUploadRowModify",
                data : {
                    callId:callId,
                    modifyingCallbackDt: modifyingCallbackDt
                },
                type : "POST",
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
            }).done(function(data){
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });
        }

    </script>

</form>

</body>

</html>
