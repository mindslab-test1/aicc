<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<sec:authentication var="user" property="principal"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<%@ include file="../common/header_headLink.jsp" %>

	<title>Happy Call</title>
</head>
<body style="width:800px; height:650px;">
<div id="popup_wrap">
	<!-- popup_header -->
	<div class="popup_header">
		<h1>모니터링 <span>상세보기</span></h1>
	</div>
	<!-- //popup_header -->

	<!-- popup_contents -->
	<div class="popup_contents">
		<!-- customInfo_wrap -->
		<div class="customInfo_wrap">
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">고객명</th>
						<td>${topInfo.custNm}</td>
					</tr>
					<tr>
						<th scope="row">상품명</th>
						<td>${topInfo.prodNm}</td>
					</tr>
					<tr>
						<th scope="row">주민번호</th>
						<td>${topInfo.juminNo}</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">실행일자</th>
						<td></td>
					</tr>
					<tr>
						<th scope="row">고객 관리 번호</th>
						<td>${topInfo.custId}</td>
					</tr>
					<tr>
						<th scope="row">모니터링 종류</th>
						<td>${topInfo.campaignNm}</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //customInfo_wrap -->
		<!-- cont_left -->
		<div class="cont_left">
			<h2>상담콜 실시간 모니터링</h2>
			<div class="monitor_state"><!-- 비활성화일 때 class에 off를 추가해 주세요 -->
				<i class="fas fa-grin-beam"><span class="hide">Good</span></i>
				<i class="fas fa-grin-beam-sweat off"><span class="hide">Not Good</span></i>
				<i class="fas fa-tired off"><span class="hide">Bad</span></i>
			</div>
			<!-- call_box -->
			<div class="call_box">
				<!-- call_cont -->
				<div class="call_cont">
					<!-- call_list -->
					<ul id="call_list" class="call_list">
						<c:choose>
							<c:when test="${fn:length(sttResult) gt 0}">
								<c:forEach items="${sttResult}" var="sttResultList" varStatus="status">
									<c:choose>
										<c:when test="${sttResultList.speakerCode eq 'ST0002'}">
											<!-- agent -->
											<li class="agent">
												<span class="thumb"><i class="fas fa-robot"></i><span class="speaker">AI</span></span>
												<span class="cont"><em class="txt">${sttResultList.sentence}</em></span>
											</li>
											<!-- //agent -->
										</c:when>
										<c:otherwise>
											<!-- client -->
											<li class="client">
												<span class="thumb"><i class="fas fa-user"></i><span class="speaker">고객</span></span>
												<c:choose>
													<c:when test="${sttResultList.ignored eq 'Y'}">
														<span class="cont"><em class="txt_ignored">${sttResultList.sentence} - IGNORED</em></span>
													</c:when>
													<c:otherwise>
														<span class="cont"><em class="txt">${sttResultList.sentence}</em></span>
													</c:otherwise>
												</c:choose>
											</li>
											<!-- //client -->
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					</ul>
					<a href="#" id="call_list_bottom"></a>
					<!-- //call_list -->
				</div>
				<!-- //call_cont -->
			</div>
			<!-- //call_box -->
		</div>
		<!-- //cont_left -->
		<!-- cont_right -->
		<div class="cont_right">
			<!-- 모니터링 결과 -->
			<h2>모니터링 결과</h2>
			<div class="tbl_customTh">
				<table>
					<caption>모니터링 결과 제목</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:134px">
					</colgroup>
					<thead>
					<tr>
						<th scope="col">No.</th>
						<th scope="col">구간</th>
						<th scope="col">탐지</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="tbl_customTd">
				<table>
					<caption>모니터링 결과 내용</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:128px">
					</colgroup>
					<tbody>

					<c:choose>
						<c:when test="${fn:length(mntResult) gt 0}">
							<c:forEach items="${mntResult}" var="mntResult" varStatus="status">
								<tr>
									<td class="txt_center">${status.index + 1}</td>
									<td>${mntResult.taskInfo}</td>
									<td>
										<c:choose>
											<c:when test="${score[mntResult.seq] eq 'Y'}">
												<div class="radiobox form_type01">
													<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
												</div>
												<div class="radiobox form_type01">
													<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
												</div>
											</c:when>
											<c:when test="${score[mntResult.seq] eq 'N'}">
												<div class="radiobox form_type01">
													<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
												</div>
												<div class="radiobox form_type01">
													<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
												</div>
											</c:when>
											<c:otherwise>
												<div class="radiobox form_type01">
													<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
												</div>
												<div class="radiobox form_type01">
													<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
												</div>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</c:when>
					</c:choose>

					</tbody>
				</table>
			</div>
			<!-- //모니터링 결과 -->
			<!-- 상담 메모 -->
			<div id="op_memo" class="textarea_wrap single02">
				<textarea id="popMemo" rows="3" cols="50" onKeyup="checkPopMemoLen()" placeholder="상담 메모 (글자수 최대 255자)" class="textarea_box">${memo}</textarea>
				<span id="popMemoLen">0/255</span>
			</div>
			<!-- //상담 메모 -->
		</div>
		<!-- //cont_right -->
	</div>
	<!-- //popup_contents -->
	<!-- popup_bottom -->
	<div class="popup_bottom">
		<div id="sign_calling" class="txt_state">
			<!-- <p class="headset"><i class="fas fa-headset"></i> 고객과 직접 통화중</p> -->
			<p class="robot"><i class="fas fa-robot"></i>고객과 AI 통화중</p>
		</div>
		<div class="btnPage_wrap">
            <div style="display: flex; justify-content: space-between">
                <button type="button" id="call_listen" class="btn_default02" onClick="startAudioStream(false)">콜 청취</button>
                <button type="button" id="call_change_op" class="btn_default02" onclick="startAudioStream(true)">콜 상담사로 전환</button>
				<button type="button" id="call_end_next" class="btn_default02">다음 콜 걸기</button>
            </div>
            <div style="display: flex; justify-content: space-between; margin-top: 15px;">
				<button type="button" id="call_memo_save" class="btn_default01" onClick="javascript:confirmSave();">메모 저장</button>
				<button type="button" class="btn_default01" onClick="javascript:self.close();">확인</button>
                <div id="cur_count" class="popup_paging" style="position: relative !important;">전체 <strong>0</strong> / 0</div>
            </div>

			<%--// 각각의 알림창 관련 id--%>
			<div id="successDialog" class="dialog"></div>
			<div id="failDialog" class="dialog"></div>
			<div id="saveDialog" class="dialog"></div>
			<div id="confirmDialog" class="dialog"></div>
			<div id="checkKey1Dialog" class="dialog"></div>
			<div id="nextCallDialog" class="dialog"></div>

		</div>
	</div>
	<!-- //popup_bottom -->
</div>

<input type="hidden" id="cno" name="cno" value="${frontMntVO.cno}" />

<!-- javascript link & init -->
<%@ include file="../common/footer_init_popup.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">

	var user = "${user}";
	var internalNumber = "";

	var p_chk_list = window.opener.document.getElementById('checkedChkBox');		// 부모창에서 선택된 체크박스 ID 값을 읽음.
	var ulAdd = $('#call_list');						// 실시간 메세지 부분 객체 전역
	var ws;												// 웹소켓 전역
	var getd_cust_tel_no;								// 고객 전화번호 전역
	var getd_cust_op_id;								// 상담사ID 전역
	var getd_contract_no;								// 고객관리번호(contract_no) 전역
	var getd_call_status;
	var chk_cnt;										// 체크한 체크 박스 전체 수
	var transferYN = 0;																// AI에서 상담사로 전환 여부
	var optionTag = "${monitoringResultCode}";
	var isCall = "${isCall}";
	var monitoringSelBox = '<div class="monitor_cont">';
	var ws_agent;
	monitoringSelBox = monitoringSelBox + '<label for="popMntCont">모니터링 내용</label>';
	monitoringSelBox = monitoringSelBox + '<select id="popMntCont">';
	monitoringSelBox = monitoringSelBox + '<option value="">모니터링 내용</option>' + optionTag;
	monitoringSelBox = monitoringSelBox + '</select>';
	monitoringSelBox = monitoringSelBox + '</div>';

	$(document).ready(function(){

		init();													//초기 세팅
	});


	//실시간 메세지 관련 팝업 실행시 값 설정 및 초기화
	function init(){
		mntRunBtn = window.opener.document.getElementById('mntRunBtn');				// 리스트 페이지 "모니터링 실행" 객체.

		//"콜 상담사 전환", "통화완료, 다음 콜 걸기" 버튼 활성/비활성 초기 세팅. "콜 상담사 전환" 버튼 활성, "다음 콜 걸기" 비활성.
		//$('#call_change_op').on("click", doCallOpChg);
		$('#call_end_next').attr("class", "btn_default03");
		$('#call_end_next').on("click", doCallNext);
		$('#call_end_next').attr("disabled", true);

		if('${frontMntVO.isCall}' == 'N') {
            $('#call_listen').attr("disabled", true);
            $('#call_change_op').attr("disabled", true);

            $('#call_listen').attr("class", "btn_default03");
            $('#call_change_op').attr("class", "btn_default03");
        }

		chk_cnt = '<c:out value="${frontMntVO.chk_cnt}" />';					// 선택한 체크 박스 전체 수

		getd_campaign_id = '<c:out value="${topInfo.campaignId}" />';			// 캠페인ID
		getd_cust_tel_no = '<c:out value="${topInfo.telNo}" />';			// 고객 전화 번호 값 세팅
		getd_cust_op_id = '<c:out value="${topInfo.custOpId}" />';			// 상담사ID 값 세팅
		getd_contract_no = '<c:out value="${topInfo.contractNo}" />';			// 고객관리 번호 값 세팅
		getd_call_status = '<c:out value="${topInfo.callStatus}"/>';

		if(getd_call_status === 'CS0003' || getd_call_status === 'CS0005' || getd_call_status ==='CS0010') {
			$("#sign_calling").html("<p class='finish'><i class='fas fa-phone-slash'></i>통화 종료</p>");
		}

		//call 대상 contract_no 리스트 및 리스트 편집.
		p_chk_list = window.opener.document.getElementById('checkedChkBox');		// 부모창에서 선택된 체크박스 ID 값을 읽음.
		var p_val = p_chk_list.value;
		var p_val_arr = p_val.split(",");
		var cur_chk_cnt;
		if( p_val_arr.length > 0 ){
			p_val_arr.remove(getd_contract_no);										// 선택한 체크 박스 ID중에서 사용한 ID 삭제.
			p_chk_list.value = p_val_arr;											// 남은 리스트 부모 창에 다시 세팅

			if( p_val_arr.length > chk_cnt ){
				cur_chk_cnt = chk_cnt - p_val_arr.length;
			}else{
				cur_chk_cnt = chk_cnt;
			}
			$('#cur_count').html("전체 <strong>" + cur_chk_cnt + "</strong> / " + chk_cnt);

			if( p_val_arr.length == 0 ){											// 1이면 현재 건에 해당하고, 다음건이 없으므로 "모니터링 실행"버튼 활성.
				$(mntRunBtn).attr("disabled", false);
			}
		}

		//수동 실행 중인 콜에 해당하는 체크 박스 false 로.
		window.opener.document.getElementById(eval("'chkBox" + getd_contract_no + "'")).checked = false;

		var successDialogMsg = '저장 완료되었습니다.';
		var successDialogId = 'successDialog';
		var successDialogType = 'success';
		setDialog(successDialogId, successDialogMsg, successDialogType);

		var failDialogMsg = '저장 실패되었습니다.';
		var failDialogId = 'failDialog';
		var failDialogType = 'fail';
		setDialog(failDialogId, failDialogMsg, failDialogType);

		var saveDialogMsg = '상담메모는 최대 255자까지 가능합니다.';
		var saveDialogId = 'saveDialog';
		var saveDialogType = 'save';
		setDialog(saveDialogId, saveDialogMsg, saveDialogType);

		var confirmDialogMsg = '저장하시겠습니까?';
		var confirmDialogId = 'confirmDialog';
		var confirmDialogType = 'confirm';
		setDialog(confirmDialogId, confirmDialogMsg, confirmDialogType);

		var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
		var checkKey1DialogId = 'checkKey1Dialog';
		var checkKey1DialogType = 'checkKey1';
		setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

		var nextCallDialogMsg = "선택된 다음 콜이 없습니다.";
		var nextCallDialogId = 'nextCallDialog';
		var nextCallDialogType = 'nextCall';
		setDialog(nextCallDialogId, nextCallDialogMsg, nextCallDialogType);

		checkPopMemoLen();

		//전화 시작 RestFull 메세지 전송
		sendMsg = '{"EventType":"STT", "Event":"START", "Caller":"' + getd_cust_tel_no + '", "Agent":"' + getd_cust_op_id + '", "contractNo":"' + getd_contract_no + '", "campaignId":"' + getd_campaign_id + '"}';
		conn_ws(sendMsg);
	}

	function confirmSave() {
		$('#confirmDialog').dialog('open');
	}


	//websocket main function
	function conn_ws(sendMsg){
console.log('${websocketUrl}/callsocket');
		//소켓 서버 연결
		ws = new WebSocket( '${websocketUrl}/callsocket');

		//소켓 메세지 수신
		ws.onmessage = function(e){
			var rcv_data = JSON.parse(e.data);
			internalNumber = rcv_data.Agent;
			if( rcv_data.EventType == 'STT' ){
				if( transferYN == 0 && rcv_data.Event == 'interim' ){
					if(rcv_data.Direction == 'TX'){
						makeMsgTX(rcv_data)
					}else if(rcv_data.Direction == 'RX'){
						makeMsgRX(rcv_data)
					}
				}else if( rcv_data.Event == 'stop' ){
					$("#sign_calling").html("<p class='finish'><i class='fas fa-phone-slash'></i>통화 종료</p>");

					//상담사 전환이 아닌 경우 "콜상담사로 전환"버튼 비활성, "통화완료, 다음 콜 걸기 활성"
					if( transferYN == 0 ){
						$('#call_change_op').attr("disabled", true);
						$('#call_change_op').attr("class", "btn_default03");
                        $('#call_listen').attr("disabled", true);
                        $('#call_listen').attr("class", "btn_default03");
					}

					$('#call_end_next').attr("disabled", false);
					$('#call_end_next').attr("class", "btn_default01");
				}
			}else if( transferYN == 0 && rcv_data.EventType == 'DETECT' ){
				if( getd_contract_no == rcv_data.contract_no ){
					if( rcv_data.Result == 'Y' ){
						$('#resultYes' + rcv_data.No).focus();
						$('#resultYes' + rcv_data.No).prop("checked", true);
					}else{
						$('#resultNo' + rcv_data.No).focus();
						$('#resultNo' + rcv_data.No).prop("checked", true);
					}
				}

			}
		};

		//소켓 메세지 송신
		ws.onopen = function(){
			sendMsg = '{"EventType":"STT", "Event":"subscribe", "Caller":"' + getd_cust_tel_no + '", "Agent":"' + getd_cust_op_id + '", "contract_no":"' + getd_contract_no + '", "campaignId":"' + getd_campaign_id + '"}';
			ws.send(sendMsg);
			sendMsg = '{"EventType":"STT", "Event":"START", "Caller":"' + getd_cust_tel_no + '", "Agent":"' + getd_cust_op_id + '", "contractNo":"' + getd_contract_no + '", "campaignId":"' + getd_campaign_id + '"}';
			if(isCall === "true") {
				sendMsgRestful("START", sendMsg);
			}
		}
	}

	//상담사용 실시간 메세지 생성
	function makeMsgTX(rcv_data){
		var result = String.format("<li class='agent'><span class='thumb'><i class='fas fa-robot'></i><span class='speaker'>{0}</span></span><span class='cont'><em class='txt'>{1}</em></span></li>", "AI", rcv_data.Text);
		ulAdd.append(result);
		window.setTimeout(function () {
			document.getElementById('call_change_op').focus();
			document.getElementById('call_list_bottom').focus();
		}, 0);
	}

	//고객용 실시간 메세지 생성
	function makeMsgRX(rcv_data){
		var result = '';
		if (rcv_data.ignored == 'Y') {
			rcv_data.Text = rcv_data.Text + " - IGNORED";
			result = String.format("<li class='client'><span class='thumb'><i class='fas fa-user'></i><span class='speaker'>{0}</span></span><span class='cont'><em class='txt_ignored'>{1}</em></span></li>", "고객", rcv_data.Text);
		} else {
			result = String.format("<li class='client'><span class='thumb'><i class='fas fa-user'></i><span class='speaker'>{0}</span></span><span class='cont'><em class='txt'>{1}</em></span></li>", "고객", rcv_data.Text);
		}
		ulAdd.append(result);
		window.setTimeout(function () {
			document.getElementById('call_change_op').focus();
			document.getElementById('call_list_bottom').focus();
		}, 0);
	}


	//"콜 상담사로 전환" 클릭
	function doCallOpChg(){

		var sendMsg = {
			Event: "TRANSFER",
			AgentId: user,
			dialNo: internalNumber,
		};

		sendMsg = JSON.stringify(sendMsg);

		sendMsgRestful("TRANSFER", sendMsg);
		console.log("#### SEND TRANSFER ####");

		var memoVal = $('#popMemo').val();
		var memo = '<div id="op_memo" class="textarea_wrap single01">';
		memo = memo + '<textarea id="popMemo" rows="1" cols="20" onKeyup="checkPopMemoLen()" placeholder="상담 메모 (글자수 최대 255자)" class="textarea_box">' + memoVal + '</textarea>';
		memo = memo + '<span id="popMemoLen">0/255</span>';
		memo = memo + '</div>';
		memo = memo + monitoringSelBox;
		memo = memo + '<div class="callback_date">';
		memo = memo + '	<label for="callbackDate">콜백 요청 일시</label>';
		memo = memo + '	<div class="dateBox">';
		memo = memo + '	<input autocomplete="off" type="text" id="callbackDate" onclick="callDatePicker()" onkeyup="checkKey()" class="ipt_txt">';
		memo = memo + '	<i class="far fa-calendar-alt"></i>';
		memo = memo + '	</div>';
		memo = memo + '</div>';
		$('#op_memo').replaceWith(memo);
		callDatePicker();
		checkPopMemoLen();

        $('#call_listen').attr("disabled", true);
        $('#call_listen').attr("class", "btn_default03");

		// "콜 상담사 전환"버튼을 "저장"버튼으로 변경
		$("#sign_calling").html("<p class='headset'><i class='fas fa-headset'></i> 고객과 직접 통화중</p>");
		$("#call_change_op").attr("class", "btn_default01");
		$("#call_change_op").off("click");
		$("#call_change_op").on("click", doCloseCall);
		$("#call_change_op").text("통화종료");

		// "통화완료, 다음콜 걸기"버튼을 "다음 콜 걸기"버튼으로 변경
		$("#call_end_next").off("click");
		$("#call_end_next").on("click", doCallNext);
		$("#call_end_next").attr("class", "btn_default01");
		$("#call_end_next").text("다음 콜 걸기");
	}

    function doListenCall() {

        $('#call_listen').attr("disabled", true);
        $('#call_listen').attr("class", "btn_default03");

        var sendMsg = {
            Event: "LISTEN",
            AgentId: user,
            dialNo: internalNumber,
        };

        sendMsg = JSON.stringify(sendMsg);

        sendMsgRestful("LISTEN", sendMsg);
    }

    function doCloseCall() {

        $('#call_change_op').attr("disabled", true);
        $('#call_change_op').attr("class", "btn_default03");

        var sendMsg = {
            Event: "CLOSE",
            AgentId: user,
            dialNo: internalNumber,
        };

        sendMsg = JSON.stringify(sendMsg);

        sendMsgRestful("CLOSE", sendMsg);
    }

	//"저장" 클릭
	function doPopSave(){

		var cno = $('#cno').val();											//contract_no
		var callId = '<c:out value="${topInfo.callId}" />';				//call_id
		var campId = '<c:out value="${topInfo.campaignId}" />';			//campaign_id
		var popMemo = $('#popMemo').val();									//상담메모
		var popMntCont = $('#popMntCont').val();							//모니터링 내용
		var callbackDate = $('#callbackDate').val();						//콜백 요청 일시
		var checkedArr = [];												//체크된 체크박스 값
		var checkedDetect = '';												//checkedArr을 string으로 변경.

		//체크된 체크박스 확인하여 string으로 변경.
		$('input[type=radio]:checked').each(function(){
			checkedArr.push($(this).val());
		});
		if( checkedArr.length > 0 ){
			checkedDetect = checkedArr.join(',');
		}

		if( popMemo.length > 255 ) {
			$('#saveDialog').dialog('open');
			return false;
		}

		$.ajax({
			url : "/manualPopSave",
			data : {cno:cno, callId:callId, campaignId:campId, popMemo:popMemo, popMntCont:popMntCont, callbackDate:callbackDate, checkedDetect:checkedDetect},
			type : "POST",
			beforeSend : function(xhr) {
				/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
			},
		}).done(function(data){
			if(data == "SUCC"){
				$("#call_end_next").attr("disabled", false);				//저장되었으므로 "다음 콜 걸기"버튼 활성화
				$('#successDialog').dialog('open');
			} else {
				$('#failsDialog').dialog('open');
			}
		}).fail(function(data){
			if(data == "FAIL"){												//저장 실패이므로 추가 액션 없음.
				$('#failsDialog').dialog('open');
			}
		});
	}

	//"통화완료, 다음 콜 걸기" 클릭
	function doCallNext(){
		p_chk_list = window.opener.document.getElementById('checkedChkBox');		// 부모창에서 선택된 체크박스 ID 값을 읽음.
		var mntRunBtn = window.opener.document.getElementById('mntRunBtn');				// 리스트 페이지 "모니터링 실행" 객체.

		var getOne;
		if( p_chk_list.value.length > 0){
			var p_val = p_chk_list.value;
			var p_val_arr = p_val.split(",");

			if( p_val_arr.length > 0 ){
				getOne = p_val_arr[0];
				p_val_arr.remove(getOne);										// 선택한 체크 박스 ID중에서 사용한 ID 삭제.
				p_chk_list.value = p_val_arr;									// 남은 리스트 부모 창에 다시 세팅

				if( p_val_arr.length == 0 ){							// 0 이면 현재 건에 해당하고, 다음건이 없으므로 "모니터링 실행"버튼 활성.
					$(mntRunBtn).attr("disabled", false);
				}
			}else{
				$('#nextCallDialog').dialog('open');
				$(mntRunBtn).attr("disabled", false);
				return false;
			}
		}else{
			$('#nextCallDialog').dialog('open');
			$(mntRunBtn).attr("disabled", false);
			return false;
		}

		chk_cnt = '<c:out value="${frontMntVO.chk_cnt}" />';					// 선택한 체크 박스 전체 수
		window.document.location = "/manualPop?pcnt=<c:out value='${frontMntVO.pcnt}' />&chk_cnt=" + chk_cnt + "&cno=" + getOne + "&isCall=Y";
	}

	//통화연결 restful
	function sendMsgRestful(event, sendMsgStr){
		var sendUrl = "/sendCM";

		$.ajax({
			url : sendUrl,
			data : {sendMsgStr:sendMsgStr},
			type : "POST",
            beforeSend : function(xhr) {
			    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
            },
		}).done(function(data){
			// 통화 성공시 '통화종료' -> '통화중' 으로 변경
			$("#sign_calling").html("<p class='robot'><i class='fas fa-robot'></i>고객과 AI 통화중</p>");
		}).fail(function(data){
			console.log("### FAIL : " + data);
		});
	}

	//콜백 요청일
	function callDatePicker(){
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		var checkEnd = now;
		var checkin = $('#callbackDate').datepicker({
			onRender: function(date) {
				return date.valueOf() > now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev) {
			checkin.hide();
		}).data('datepicker');

		$('.dropdown-menu').css('top', '282px')
		$('.dropdown-menu').css('left', '507.594px')
	}

	//상담메모 입력 문자 길이 체크
	function checkPopMemoLen(){
		var popMemo = $('#popMemo').val();
		$('#popMemoLen').html(popMemo.length + "/255");
	}

	function setDialog(id, msg, type) {
		var buttons;
		if(type === 'confirm') {
			buttons= [
				{
					text: "OK",
					click: function() {
						doPopSave();
						$(this).dialog("close");
					}
				},
				{
					text: "Cancel",
					click: function() {
						$(this).dialog("close");
					}
				}
			];
		} else {
			buttons = [
				{
					text: "Cancel",
					click: function () {
						$(this).dialog("close");
					}
				}
			];
		}

		$('#'+id).html(msg);
		$('#'+id).dialog({
			autoOpen: false,
			resizable: false,
			modal: true,
			buttons: buttons
		});
	}

	//String.format overwrite.
	String.format = function() {
		var theString = arguments[0];

		for (var i = 1; i < arguments.length; i++) {
			var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
			theString = theString.replace(regEx, arguments[i]);
		}

		return theString;
	}

	//Array 내 값 삭제 overwrite.
	Array.prototype.remove = function() {
		var what, a = arguments, L = a.length, ax;
		while (L && this.length) {
			what = a[--L];
			while ((ax = this.indexOf(what)) !== -1) {
				this.splice(ax, 1);
			}
		}
		return this;
	};

	//키 입력시 허용 값 체크
	function checkKey(){
		if( check_key_reg(1, $('#callbackDate').val() ) == false ){
			$('#callbackDate').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}


	//값 체크 정규식 함수
	function check_key_reg( mode, text ){
		if( mode == 1 ){
			var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용

		}else if( mode == 2 ){
			var regexp = /[0-9]/; 				// 숫자만 허용

		}else if( mode == 2 ){
			var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용

		}else if( mode == 4 ){
			var regexp = /[a-zA-Z]/; 			// 영문만 허용

		}

		for( var i=0; i<text.length; i++){
			if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
				return false;
			}
		}
	}

	//상담메모 입력 문자 길이 체크
	function checkPopMemoLen(){
		var popMemo = $('#popMemo').val();
		$('#popMemoLen').html(popMemo.length + "/255");
	}

	function startAudioStream(mic_on) {
		var host = window.location.hostname;
		var port = '5800';
		var dial_no = internalNumber;
		var agent_id = user;

		var audioQueue = [];
		if (ws_agent != null) {
			ws_agent.close();
			ws_agent = null;
		}
		console.log("wss://" + host + '/relay');
		ws_agent = new WebSocket("wss://" + host + '/relay');
		ws_agent.binaryType = 'arraybuffer';
		var ws = ws_agent;

		var trans_type = 'recv_only';
		if (mic_on) {
			if (navigator.getUserMedia){
				trans_type = 'recv_send';
				navigator.getUserMedia({audio:true},
						function(stream) {
							send_audio(ws, stream);
						},
						function(e) {
							alert('Error capturing audio.');
						});
			} else alert('getUserMedia not supported in this browser.');
		}

		var audioCtx = new (window.AudioContext || window.webkitAudioContext)({
			sampleRate: 8000
		});
		var source = audioCtx.createBufferSource();
		var scriptNode = audioCtx.createScriptProcessor(1024, 0, 1);
		scriptNode.onaudioprocess = function(audioProcessingEvent) {
			var outputBuffer = audioProcessingEvent.outputBuffer;
			var outputData = outputBuffer.getChannelData(0);
			// console.log("outputBuffer length is " + outputBuffer.length);
			// console.log("audioQueue length is " + audioQueue.length);
			if (audioQueue.length < (outputBuffer.length * 2) ) {
				return;
			}
			for (var sample = 0; sample < outputBuffer.length; sample++) {
				var hRawAudio = audioQueue.shift();
				var lRawAudio = audioQueue.shift();
				// For little endian
				var unsignedWord = (hRawAudio & 0xff) + ((lRawAudio & 0xff) << 8);
				// For big endian
				//var unsignedWord = ((hRawAudio & 0xff) << 8) + (lRawAudio & 0xff);
				var signedWord = (unsignedWord + 32768) % 65536 - 32768;
				outputData[sample] = signedWord / 32768.0;
			}
		}

		if (ws_agent) {
			ws.onopen = function () {
				console.log("Connection is established...");
				source.connect(scriptNode);
				scriptNode.connect(audioCtx.destination);
				source.start();
				var msg = {
					"opcode": "REGISTER",
					"system_type" : "PC_AGENT",
					"dial_no": dial_no,
					"trans_type": trans_type,
					"agent_id": agent_id
				};
				var data = JSON.stringify(msg);
				ws.send(data);
				console.log("send REGISTER (" + trans_type + ")");
			};

			ws.onmessage = function (evt) {
				var received_msg = new Uint8Array(evt.data); // firefox에서 동작
				// console.log("Message is received... " + received_msg.length);
				// audioQueue.push(received_msg);
				for (var i = 0; i < received_msg.length; i++) {
					//console.log("push audio samples");
					audioQueue.push(received_msg[i]);
				}
				// console.log("Queue is received... " + audioQueue.length);
			};

			ws.onclose = function () {
				try {
					source.disconnect(scriptNode);
					scriptNode.disconnect(audioCtx.destination);
					if (mic_on) {
						console.log("Disconnect mic");
						microphone_stream.disconnect(micScriptNode);
						micScriptNode.disconnect(micAudioCtx.destination);
						voice_stream.getTracks()[0].stop();
					}
				}
				catch (e) {
					console.log(e);
				}
				console.log("Connection is closed...");
			};
		} else {

			// The browser doesn't support WebSocket
			alert("WebSocket NOT supported by your Browser!");
		}
	}

	function stopAudioStream() {
		if (ws_agent) {
			ws_agent.close();
		}
	}

	function floatTo16BitPCM(input) {
		var output = new DataView(new ArrayBuffer(input.length * 2));
		for (var i = 0; i < input.length; i++) {
			var multiplier = input[i] < 0 ? 0x8000 : 0x7fff; // 16-bit signed range is -32768 to 32767
			output.setInt16(i * 2, input[i] * multiplier | 0, true); // index, value, little edian
		}
		return output.buffer;
	}

	function send_audio(ws, stream /* from getUserMedia */) {
		voice_stream = stream;
		//var micAudioCtx = new (window.AudioContext || window.webkitAudioContext)({
		micAudioCtx = new (window.AudioContext || window.webkitAudioContext)({
			sampleRate: 8000
		});

		microphone_stream = micAudioCtx.createMediaStreamSource(stream);

		// Create a ScriptProcessorNode with a bufferSize of 4096 and a single input and output channel
		//var scriptNode = micAudioCtx.createScriptProcessor(1024, 1, 1);
		micScriptNode = micAudioCtx.createScriptProcessor(1024, 1, 1);
		// Give the node a function to process audio events
		micScriptNode.onaudioprocess = function (audioProcessingEvent) {
			// The input buffer is the song we loaded earlier
			var inputBuffer = audioProcessingEvent.inputBuffer;
			var inputData = inputBuffer.getChannelData(0);
			var sendBuffer = floatTo16BitPCM(inputData);
			ws.send(sendBuffer);
		}
		microphone_stream.connect(micScriptNode);
		micScriptNode.connect(micAudioCtx.destination);
	}
</script>
</body>
</html>
