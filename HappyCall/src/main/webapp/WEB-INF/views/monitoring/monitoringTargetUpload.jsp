<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="../common/header_headLink.jsp" %>

<title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading"><div class="loading_itemBox"><span></span><span></span><span></span><span></span></div></div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap"> 
	<!-- header -->
	<%@ include file="../common/header_headProfile.jsp" %>
	<!-- //header -->

	<!-- container -->
	<div id="container">
		<!-- snb -->
		<%@ include file="../common/leftMenu.jsp" %>
		<!-- //snb -->
		
		<!-- contents -->
		<div id="contents">
			<div class="search_box">
				<div class="excel_wrap">
					<button type="button" id="downloadBtn" class="btn_excel" onclick="location.href='/download/upload_campaign.xlsx'"><i class="fas fa-file-excel"></i>양식 <em>다운로드</em><i class="fas fa-file-download"></i></button>
					<button type="button" id="uploadBtn" class="btn_excel"><i class="fas fa-file-excel"></i>엑셀 <em>업로드</em><i class="fas fa-file-upload"></i></button>
					<input type='file' id="uploadFile" name='uploadFile' multiple onchange="upload()">
				</div>
				<ul class="search_list">
					<li>
						<input type="text"  id="schTopCampId" placeholder="캠페인 ID" title="캠페인 ID"  onKeyup="checkKey('campId')" onkeydown="enterCheck(this)">
					</li>
					<li>
						<div class="select_type">
							<label for="schTopMntType">모니터링 종류</label>
							<select id="schTopMntType">
								<option value="">모니터링 종류</option>
								${mntType}
							</select>
						</div>
					</li>
					<li>
						<div class="select_type">
							<label for="schTopOpNm">상담사명</label>
							<select id="schTopOpNm">
								<option value="">상담사명</option>
								${custInfoCode}
							</select>
						</div>
					</li>
					<li>
						<div class="dateBox02">
							<input autocomplete="off" type="text" id="schTopTargetDt" placeholder="대상일자" title="대상일자" class="ipt_txt" onKeyup="checkKey('targetDt')" onkeydown="enterCheck(this)">
						</div>
					</li>
					<li><input type="text" id="schTopCustNm" placeholder="고객명" title="고객명" onkeydown="enterCheck(this)"></li>
					<li><input type="text" id="schTopCustId" placeholder="고객관리번호" title="고객관리번호" onKeyup="checkKey('custId')" onkeydown="enterCheck(this)"></li>
					<li><input type="tel" id="schTopCustTelNo" placeholder="전화번호" title="전화번호" onKeyup="checkKey('custTelNo')" onkeydown="enterCheck(this)"></li>
					<li>
						<div class="select_type">
							<label for="schTopTargetYn">대상 여부</label>
							<select id="schTopTargetYn" name="schTopTargetYn">
								<option value="">대상 여부</option>
								<option value="">전체</option>
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</li>
				</ul>		
				<div class="btn_result">
					<button type="button" id="btn_search" class="btn_default01" onclick="goSearch(true)">결과보기</button>
				</div>
				<div class="btn_result2">
					<button type="button" id="btn_addNewRow" class="btn_default01" onclick="addNewRow()">추가</button>
				</div>
			</div>
			<!-- data list -->
			<div class="data_list tbl_thead">
				<table>
					<colgroup>
						<col style="width:45px;">
<%-- 						<col style="width:50px;"> --%>
						<col style="width:170px;">
						<col style="width:100px;">
						<col style="width:120px;">
						<col style="width:120px;">
						<col style="width:50px;">
						<col style="width:100px;">
						<col style="width:100px;">
						<col style="width:70px;">
					</colgroup>
					<thead>
						<th scope="col">No.</th>
<!-- 						<th scope="col">캠페인 ID</th> -->
						<th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id)" >모니터링 종류</th>
						<th id='CUST_NM' scope="col" onclick="onChangeSort(this.id)">고객명</th>
						<th id='CUST_ID' scope="col" onclick="onChangeSort(this.id)">고객관리번호</th>
						<th id='TEL_NO' scope="col" onclick="onChangeSort(this.id)">전화번호</th>
						<th id='TARGET_YN' scope="col" onclick="onChangeSort(this.id)">대상 여부</th>
						<th id='TARGET_DT' scope="col" onclick="onChangeSort(this.id)">대상일자</th>
						<th id='CUST_OP_NM' scope="col" onclick="onChangeSort(this.id)">상담사명</th>
						<th scope="col"></th>
					</thead>
				</table>
			</div>
			<div class="data_list tbl_tbody">
				<table>
					<colgroup>
						<col style="width:45px;">
<%-- 						<col style="width:50px;"> --%>
						<col style="width:170px;">
						<col style="width:100px;">
						<col style="width:120px;">
						<col style="width:120px;">
						<col style="width:50px;">
						<col style="width:100px;">
						<col style="width:100px;">
						<col style="width:70px;">
					</colgroup>
					<tbody id="tDataBody">
					<c:choose>
					<c:when test="${fn:length(list) gt 0}">
						<c:forEach var="data" items="${list}" varStatus="status"> 
							<tr name="row">
								<td>${data.contractNo}</td>
								<td>${data.campaignNm}</td>
								<td>${data.custNm }</td>
								<td>${data.custId }</td>
								<td>${data.telNo }</td>
								<td>${data.targetYn }</td>
								<td>${data.targetDt }</td>
								<td>${data.custOpNm }</td>
							<td><button type="button" class="btn_detail"><em>수정</em></button></td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
							<tr>
								<td colspan="13">데이터 없음</td>
							</tr>
					</c:otherwise>
					</c:choose>
					</tbody>
				</table>
			</div>
			<!-- //data list -->
			
			<!-- paging -->
			<div class="page_area">
				<%@ include file="../common/paging.jsp" %>
            </div>			
            
   			<input type="hidden" id="schCampId" name="schCampId" />
			<input type="hidden" id="schMntType" name="schMntType" />
			<input type="hidden" id="schOpNm" name="schOpNm" />
			<input type="hidden" id="schTargetDt" name="schTargetDt" />
			<input type="hidden" id="schFinalResult" name="schFinalResult" />
			<input type="hidden" id="schCallCnt" name="schCallCnt"  />
			<input type="hidden" id="schCustNm" name="schCustNm" />
			<input type="hidden" id="schCustId" name="schCustId" />
			<input type="hidden" id="schCustTelNo" name="schCustTelNo" />
			<input type="hidden" id="schMemo" name="schMemo" />
			<input type="hidden" id="schCallState" name="schCallState" />
			<input type="hidden" id="schCallResult" name="schCallResult" />
			<input type="hidden" id="schTargetYn" name="schTargetYn" />

			<input type="hidden" id="sortingTarget" name="sortingTarget" />
			<input type="hidden" id="direction" name="direction" />
			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>

  			<input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}" /> 
		
			<!-- //paging -->
		</div>
		<!-- //contents -->
	</div>

	<%--// 각각의 알림창 관련 id--%>
	<div id="warnDialog" class="dialog"></div>
	<div id="successDialog" class="dialog"></div>
	<div id="failDialog" class="dialog"></div>
	<div id="checkKey1Dialog" class="dialog"></div>
	<div id="checkKey2Dialog" class="dialog"></div>
	<div id="onlyOneDialog" class="dialog"></div>
	<div id="dupDialog" class="dialog"></div>

	<!-- //container -->
	<!-- footer -->
	<div id="footer">
		
	</div>
	<!-- //footer -->
</div>
<!-- //wrap -->

<!-- javascript link & init -->
<%@ include file="../common/footer_init.jsp" %>
<!-- //javascript link & init -->
</form>
</body>

<script type="text/javascript">
var modifyFlag = 0;					// 테이블 한 로우 수정여부 flag
var addFlag = 0;
var makeSelBox = "";
var typeArray = {};
var sortingTarget = "${frontMntVO.sortingTarget}";
var direction = "${frontMntVO.direction}";

$(document).ready(function(){
	var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
	if( rows != '' ){
		$('#pageInitPerPage').val(rows);
	}

	var warnDialogMsg = '정보를 입력해 주시기 바랍니다.';
	var warnDialogId = 'warnDialog';
	var warnDialogType = 'warn';
	setDialog(warnDialogId, warnDialogMsg, warnDialogType);

	var successDialogMsg = '완료되었습니다.';
	var successDialogId = 'successDialog';
	var successDialogType = 'success';
	setDialog(successDialogId, successDialogMsg, successDialogType);

	var failDialogMsg = '실패되었습니다.';
	var failDialogId = 'failDialog';
	var failDialogType = 'fail';
	setDialog(failDialogId, failDialogMsg, failDialogType);

	var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
	var checkKey1DialogId = 'checkKey1Dialog';
	var checkKey1DialogType = 'checkKey1';
	setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

	var checkKey2DialogMsg = "숫자만 입력 가능합니다.";
	var checkKey2DialogId = 'checkKey2Dialog';
	var checkKey2DialogType = 'checkKey2';
	setDialog(checkKey2DialogId, checkKey2DialogMsg, checkKey2DialogType);

	var onlyOneDialogMsg = "1건씩 수정 가능합니다.";
	var onlyOneDialogId = 'onlyOneDialog';
	var onlyOneDialogType = 'onlyOne';
	setDialog(onlyOneDialogId, onlyOneDialogMsg, onlyOneDialogType);

	var dupDialogMsg = "추가와 수정 동시에 불가능 합니다.";
	var dupDialogId = 'dupDialog';
	var dupDialogType = 'dup';
	setDialog(dupDialogId, dupDialogMsg, dupDialogType);

	$("#uploadBtn").on("click", function(e){
		console.log("click");
		e.preventDefault();
		$('#uploadFile').click();
	});

	$('#schTopCampId').val( '<c:out value="${frontMntVO.schCampId}" />' );
	$('#schTopMntType').val( '<c:out value="${frontMntVO.schMntType}" />' );
 	$('#schTopOpNm').val( '<c:out value="${frontMntVO.schOpNm}" />' );
 	$('#schTopTargetDt').val( '<c:out value="${frontMntVO.schTargetDt}" />' );
 	$('#schTopFinalResult').val( '<c:out value="${frontMntVO.schFinalResult}" />' );
 	$('#schTopCallCnt').val( '<c:out value="${frontMntVO.schCallCnt}" />' );
 	$('#schTopCustNm').val( '<c:out value="${frontMntVO.schCustNm}" />' );
 	$('#schTopCustId').val( '<c:out value="${frontMntVO.schCustId}" />' );
 	$('#schTopCustTelNo').val( '<c:out value="${frontMntVO.schCustTelNo}" />' );
 	$('#schTopMemo').val( '<c:out value="${frontMntVO.schMemo}" />' );
 	$('#schTopCallState').val( '<c:out value="${frontMntVO.schCallState}" />' );
 	$('#schTopCallResult').val( '<c:out value="${frontMntVO.schCallResult}" />' );

	$('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
	$('#direction').val( '<c:out value="${frontMntVO.direction}" />' );

	if(sortingTarget) {
		initSort();
	}

 	$('.btn_detail').click(function(){
 		if( modifyFlag == 1 ){
			$('#onlyOneDialog').dialog('open');
 			return false;
 		}else if( addFlag == 1 ){
			$('#dupDialog').dialog('open');
 			return false;
 		}else{
 			modifyFlag = 1;
 		}
 		
 		//클릭한 로우 값을 가져옴
 		var $item = $(this).closest("tr").find("td");
 		
 		//각 td값은 array로 저장
 		var tmpArr = [];
 		$.each($item, function(index){
 			if( index < ($item.length - 1) ){
				tmpArr.push($(this).text());
 			}
 		});
 		
 		//string format 함수 호출
 		var madeTag = strFmt(tmpModifyTag, tmpArr);
 		
		//클릭한 로우에 수정 가능한 폼으로 업데이트 		 		
 		var $tr = $(this).closest("tr");
 		$tr.replaceWith(madeTag);

 		var modDate = $("#modifyingTargetDate").datepicker().on('changeDate', function(){
			modDate.hide();
		}).data('datepicker');
 		$("#modifyingMntType").val(typeArray[tmpArr[1]]);
 	});
 	
 	// 모니터링 종류 select box tag, unescape
 	var mntTypeTag ='<c:out value="${mntType}" />';
 	mntTypeTag = mntTypeTag.replace(/&gt;/gi, '>');
 	mntTypeTag = mntTypeTag.replace(/&lt;/gi, '<');
 	mntTypeTag = mntTypeTag.replace(/&#039;/g, "'");

 	var mntType = JSON.parse('${mntTypeOrigin}');

 	for( key in mntType ) {
 		typeArray[mntType[key]] = key;
	}

 	// selectbox tag
 	makeSelBox = "<select id='modifyingMntType'>";
 	makeSelBox = makeSelBox + mntTypeTag;
 	makeSelBox = makeSelBox + "</select>";

 	// 수정을 위한 Templete Tag
	var tmpModifyTag = "<tr>";
	tmpModifyTag = tmpModifyTag + "<td>{0}</td>"; 
	tmpModifyTag = tmpModifyTag + "<td>" + makeSelBox + "</td>";
	tmpModifyTag = tmpModifyTag + "<td>{2}</td>";
	tmpModifyTag = tmpModifyTag + "<td><input type='text' id='modifyingCustNo' value='{3}' size='10'></td>";
	tmpModifyTag = tmpModifyTag + "<td>{4}</td>";

	tmpModifyTag = tmpModifyTag + "<td>{5}</td>";
	tmpModifyTag = tmpModifyTag + "<td><input type='text' id='modifyingTargetDate' value='{6}' size='10'></td>";
	tmpModifyTag = tmpModifyTag + "<td>{7}</td>";
	tmpModifyTag = tmpModifyTag + "<td><button type='button' class='btn_detail' onclick='modifiedSave(this, {0})'><em>저장</em></button></td>";
	tmpModifyTag = tmpModifyTag + "</tr>";
});

function initSort() {
	var backup = $('#' + sortingTarget).html();
	backup = backup.substring(backup.indexOf('<i>')).trim();
	if(direction === 'asc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
	} else if(direction === 'desc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
	} else {
		$('#' + sortingTarget).html(backup);
	}
}

function onChangeSort(id) {
	if(sortingTarget !== id) {
		sortingTarget = id;
		direction = 'asc';
	} else {
		if(direction === 'asc') {
			direction = 'desc';
		} else if (direction === 'desc') {
			sortingTarget = '';
			direction = '';
		} else {
			direction = 'asc';
		}
	}
	$('#sortingTarget').val(sortingTarget);
	$('#direction').val(direction);

	//TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
	goSearch();
}

function setDialog(id, msg, type) {
	var buttons;

	if(type ==='warn') {
		buttons = [
			{
				text: "Cancel",
				click: function() {
					$(this).dialog("close");
				}
			}
		];
	} else if(type === 'checkKey1' || type === 'checkKey2'){
		buttons = [
			{
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
					location.reload();
				}
			}
		];
	} else {
		buttons = [
			{
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
					location.reload();
				}
			}
		];
	}

	$('#'+id).html(msg);
	$('#'+id).dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: buttons
	});
}

//검색실행
function goSearch(condition){
	if(condition ==  true) {
		$('#currentPage').val(1);
	}

	var schCampId      = $('#schTopCampId').val();
	var schMntType     = $('#schTopMntType').val();
	var schOpNm        = $('#schTopOpNm').val();
	var schTargetDt    = $('#schTopTargetDt').val();
	var schFinalResult = $('#schTopFinalResult').val();
	var schCallCnt     = $('#schTopCallCnt').val();
	var schCustNm      = $('#schTopCustNm').val();
	var schCustId     = $('#schTopCustId').val();
	var schCustTelNo   = $('#schTopCustTelNo').val();
	var schMemo        = $('#schTopMemo').val();
	var schCallState   = $('#schTopCallState').val();
	var schCallResult  = $('#schTopCallResult').val();
	var schTargetYn  = $('#schTopTargetYn').val();

	$('#schCampId').val(schCampId);
	$('#schMntType').val(schMntType);
	$('#schOpNm').val(schOpNm);
	$('#schTargetDt').val(schTargetDt);
	$('#schFinalResult').val(schFinalResult);
	$('#schCallCnt').val(schCallCnt);
	$('#schCustNm').val(schCustNm);
	$('#schCustId').val(schCustId);
	$('#schCustTelNo').val(schCustTelNo);
	$('#schMemo').val(schMemo);
	$('#schCallState').val(schCallState);
	$('#schCallResult').val(schCallResult);
	$('#schTargetYn').val(schTargetYn);
	
	
	valueForm.method = "POST";
	valueForm.action = "/mntTargetUpload";
	valueForm.submit();
}

//페이지 이동 관련
function goPage(cp){
	if( cp != 0){
		$('#currentPage').val(cp);
	}
	
	goSearch(false);
}
function upload(){
	var formData = new FormData();
	var inputFile = $("input[name='uploadFile']");
	var files = inputFile[0].files;
	
	console.log(files);
	for(var i=0; i<files.length; i++){
		formData.append("uploadFile",files[i]);
	}
	console.log("formData: "+formData);
	$.ajax({
		url:'/uploadAction',
		processData: false,
		contentType: false,
		data: formData,
		type: 'POST',
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
		success: function(data){
			if(data === "SUCC"){
				$('#successDialog').dialog('open');
				refresh();
			} else if(data === "FAIL") {
				$('#failDialog').dialog('open');
			} else if(data === "exception error") {
				console.log('here');
				$('#dupDialog').dialog('open');
			}
		}, // fail이나 error 시 구분 추가
		fail: function(data){
			$('#failDialog').dialog('open');
		}
	});
}


//키 입력시 허용 값 체크
function checkKey( objName ){
	if( objName == 'campId' ){
		if( check_key_reg(1, $('#schTopCampId').val() ) == false ){
			$('#schTopCampId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'targetDt' ){
		if( check_key_reg(1, $('#schTopTargetDt').val() ) == false ){
			$('#schTopTargetDt').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'callCnt' ){
		if( check_key_reg(2, $('#schTopCallCnt').val() ) == false ){
			$('#schTopCallCnt').val('');
			$('#checkKey2Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'custId' ){
		if( check_key_reg(1, $('#schTopCustId').val() ) == false ){
			$('#schTopCustId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}		
	}else if( objName == 'custTelNo' ){
		if( check_key_reg(2, $('#schTopCustTelNo').val() ) == false ){
			$('#schTopCustTelNo').val('');
			$('#checkKey2Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'modiCustTelNo' ){
		if( check_key_reg(2, $('#modifyingPhoneNo').val() ) == false ){
			$('#modifyingPhoneNo').val('');
			$('#checkKey2Dialog').dialog('open');
			return false;
		}
	}
}

//값 체크 정규식 함수
function check_key_reg( mode, text ){
	if( mode == 1 ){
	    var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용
	    
	}else if( mode == 2 ){
		var regexp = /[0-9]/; 				// 숫자만 허용
		
	}else if( mode == 2 ){
		var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용
		
	}else if( mode == 4 ){
		var regexp = /[a-zA-Z]/; 			// 영문만 허용
		
	}
  
  for( var i=0; i<text.length; i++){
      if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
			return false;
      }
  }  
}

//엔터값 체크
function enterCheck(){
	if( window.event.keyCode == 13 ){
		goSearch(true);
	}else{
		return false;
	}
}

//string format과 같이 기호 치환
var strFmt = function (str, col) {
    col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);
    
    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
        if (m == "{{") { return "{"; }
        if (m == "}}") { return "}"; }
        return col[n];
    });
};

//1 row은 수정 후 저장 버튼 클릭 시 실행
function modifiedSave(event, cno){
	var modifyingMntType = $('#modifyingMntType').val();
	var modifyingCustName = $('#modifyingCustName').val();
	var modifyingCustNo = $('#modifyingCustNo').val();
	var modifyingPhoneNo = $('#modifyingPhoneNo').val();
	var modifyingTargetDate = $('#modifyingTargetDate').val();
	var modifyingAssignedYN = '';

	if( modifyingCustNo.length < 1 ){
		$('#warnDialog').dialog('open');
		$('#modifyingCustNo').focus();
		return false;
	}

	if( modifyingTargetDate == '' || modifyingTargetDate == null) {
		modifyingAssignedYN = 'N';
	} else {
		modifyingAssignedYN = 'Y';
	}
	
	$.ajax({
		url : "/mntTargetUploadRowModify",
		data : {
			cno:cno,
			modifyingMntType: modifyingMntType,
			modifyingCustName: modifyingCustName,
			modifyingCustNo: modifyingCustNo,
			modifyingPhoneNo: modifyingPhoneNo,
			modifyingAssignedYN: modifyingAssignedYN,
			modifyingTargetDate: modifyingTargetDate
		},
		type : "POST",
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
	}).done(function(data){
		if(data === "SUCC"){
			$('#successDialog').dialog('open');
		} else if(data === "FAIL") {
			$('#failDialog').dialog('open');
		}
	}).fail(function(data){
		$('#failDialog').dialog('open');
	});
}

//1 row "추가" 버튼 클릭 시
function addNewRow(){
	if( addFlag == 1 ){
		$('#onlyOneDialog').dialog('open');
		return false;
	}else if( modifyFlag == 1 ){
		$('#dupDialog').dialog('open');
		return false;
	}else{
		addFlag = 1;
	}

	// 추가을 위한 Templete Tag
	var tmpAddTag = "<tr>";
	tmpAddTag = tmpAddTag + "<td></td>"; 
	tmpAddTag = tmpAddTag + "<td>" + makeSelBox + "</td>";
	tmpAddTag = tmpAddTag + "<td></td>";
	tmpAddTag = tmpAddTag + "<td><input type='text' id='modifyingCustNo' 	size='10'></td>";
	tmpAddTag = tmpAddTag + "<td></td>";
	tmpAddTag = tmpAddTag + "<td></td>";
	tmpAddTag = tmpAddTag + "<td><input type='text' id='modifyingTargetDate' size='10'></td>";
	tmpAddTag = tmpAddTag + "<td></td>";
	tmpAddTag = tmpAddTag + "<td><button type='button' class='btn_detail' onclick='addSave(this)'><em>저장</em></button></td>";
	tmpAddTag = tmpAddTag + "</tr>";

	if($('#tDataBody > tr[name=row]').length < 1) {
		$('#tDataBody').empty();
		$('#tDataBody').append(tmpAddTag);
	} else {
		$('#tDataBody > tr:first').before(tmpAddTag);
	}
	var modDate = $("#modifyingTargetDate").datepicker().on('changeDate', function(){
		modDate.hide();
	}).data('datepicker');
}

function addSave(event){
	var modifyingMntType = $('#modifyingMntType').val();
	var modifyingCustName = $('#modifyingCustName').val();
	var modifyingCustNo = $('#modifyingCustNo').val();
	var modifyingPhoneNo = $('#modifyingPhoneNo').val();
	var modifyingTargetDate = $('#modifyingTargetDate').val();
	var modifyingAssignedYN = '';

	$.ajax({
		url : "/mntTargetUploadRowAdd",
		data : {
			modifyingMntType:modifyingMntType,
			modifyingCustName:modifyingCustName,
			modifyingCustNo:modifyingCustNo,
			modifyingPhoneNo:modifyingPhoneNo,
			modifyingTargetDate: modifyingTargetDate,
			modifyingAssignedYN: modifyingAssignedYN

		},
		type : "POST",
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
	}).done(function(data){
		if(data === "SUCC"){
			$('#successDialog').dialog('open');
		} else if(data === "FAIL") {
			$('#failDialog').dialog('open');
		}
	}).fail(function(data){
		$('#failDialog').dialog('open');
	});
}
</script>
<style>
	#uploadFile {display: none;}
</style>
</html>


