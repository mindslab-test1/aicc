<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="utf-8">
<head>
    <link rel="stylesheet" type="text/css" href="/resources/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/font.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/voiceBot.css">

    <script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
</head>
<body>

<div class="chat_tit">
    <p>Voice bot, 직접 경험해보세요!</p>
    <span class="sub">보험상품 완전판매 여부를 자동으로 모니터링하는 Voice bot을 체험해보실 수 있습니다.</br>
    maum.ai의 음성인식, 음성생성 기술과 챗봇을 결합하여 만든 어플리케이션입니다.</br>전화번화를 입력하시면 Voice bot이 직접 전화를 걸어드려요.</span>
</div>
<!-- .chat_iptBox-->
<div class="chat_iptBox">
    <dl>
        <dt>통화 시나리오</dt>
        <dd>
            <div class="selectType02">
                <label for="campaignList">시나리오</label>
                <select id="campaignList">
                </select>
            </div>
        </dd>
    </dl>
    <dl>
        <dt>전화번호</dt>
        <dd>
            <input type="number" id="phoneNum1" class="ipt_txt pn_top" maxlength="3">
            <input type="number" id="phoneNum2" class="ipt_txt pn_md" maxlength="3">
            <input type="number" id="phoneNum3" class="ipt_txt pn_btm" maxlength="3">
        </dd>
        <dt>이름</dt>
        <dd>
            <input type="text" id="userName" class="ipt_txt pn_top">
        </dd>
        <dd>
            <button type="button" id="callBot" class="btnS_smallBasic" disabled="disabled">
                <i class="ico_tel"></i>전화걸기
            </button>
        </dd>
    </dl>
</div>
<!-- //.chat_iptBox -->
<!-- .chatTblBox -->
<div class="chatTblBox">
    <!-- .chatbot_box -->
    <div class="chatbot_box" style="display:inline-block">
        <h3 class="h3_tit">통화 모니터링</h3>
        <!-- .chat_mid -->
        <div class="chat_mid">
            <!-- .talkLst -->
            <ul class="talkLst" style="display:block" id="talkList">
            </ul>
        </div>
        <!-- //.chat_mid -->
    </div>
    <!-- //.chatbot_box -->
    <!-- .chatbot_tblBox -->
    <div class="chatbot_tblBox">
        <h3 class="h3_tit">구간탐지 or 결과분석</h3>
        <table class="tbl_gray">
            <caption class="hide">통화내용 결과분석 표</caption>
            <colgroup>
                <col width="40">
                <col>
                <col>
            </colgroup>
            <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">구간</th>
                <th scope="col">탐지</th>
            </tr>
            </thead>
            <tbody id="taskList">
            </tbody>
        </table>
    </div>
    <!-- //.chatbot_tblBox -->
</div>

<script type="text/javascript">

    $(document).ready(function(){

        getCampaignList();

        $("#campaignList").change(getCampaignTaskList);

        $("#callBot").click(validateChecker);

        $('#phoneNum1, #phoneNum2, #phoneNum3, #userName').val('');

        // key를 눌렀을 때 키값 확인
        $('#phoneNum1, #phoneNum2, #phoneNum3').on('keydown', function () {
            if (event.key == 'e' || event.key == 'E' || event.key == '+') {
                event.preventDefault();
                return false;
            }
        });


        $( '#phoneNum1, #phoneNum2, #phoneNum3, #userName').on('keyup', function () {

            var numValLth1 = $('#phoneNum1').val().length;
            var numValLth2 = $('#phoneNum2').val().length;
            var numValLth3 = $('#phoneNum3').val().length;
            var numValLth = numValLth1 + numValLth2 + numValLth3;
            var userNameVal = $('#userName').val().length;

            if (numValLth < 11 || userNameVal == 0) {
                $('.chat_iptBox button').attr("disabled", "disabled");
            } else {
                $('.chat_iptBox button').removeAttr("disabled", "disabled");
            }
        });

        $('#phoneNum1').on('keypress', function () {
            if ($(this).val().length > 2) {
                $(this).val($(this).val().substring(0, 2));
            }
        });

        $('#phoneNum2, #phoneNum3').on('keypress', function () {
            if ($(this).val().length > 3) {
                $(this).val($(this).val().substring(0, 3));
            }
        });
    });

    function validateChecker() {
        var numValLth1 = $('#phoneNum1').val().length;
        var numValLth2 = $('#phoneNum2').val().length;
        var numValLth3 = $('#phoneNum3').val().length;
        var userNameVal = $('#userName').val().length;

        if(numValLth1 != 3) {
            alert("올바른 번호를 입력해 주세요.");
            $('#phoneNum1').focus();
            event.preventDefault();
        } else if(numValLth2 != 4) {
            alert("올바른 번호를 입력해 주세요.");
            $('#phoneNum2').focus();
            event.preventDefault();
        } else if(numValLth3 != 4) {
            alert("올바른 번호를 입력해 주세요.");
            $('#phoneNum3').focus();
            event.preventDefault();
        } else if(userNameVal == '') {
            alert("이름을 입력해 주세요.");
            $('#userName').focus();
            event.preventDefault();
        } else {
            createServiceMonitoring();
        }
    }

    function createServiceMonitoring() {

        var createServiceData = {
            campaignId: campaignId = $("#campaignList").val(),
            custTelNo: "" + $('#phoneNum1').val() + $('#phoneNum2').val() + $('#phoneNum3').val(),
            custNm: $('#userName').val()
        };

        $.ajax({
            type: "GET",
            url: "/service/createServiceMonitoring",
            data: createServiceData,
            success: function(result) {

                createServiceData["contract_no"] = result;
                callingBot(createServiceData);

            }
        })

    }

    function callingBot(csData) {
        var sendMsg = '{"EventType":"STT", "Event":"START", "Caller":"' + csData.custTelNo + '", "Agent":"", "contractNo":"' + csData.contract_no + '", "campaignId":"' + csData.campaignId + '"}';
        conn_ws(sendMsg, csData);
    }

    function conn_ws(sendMsg, csData){

        //소켓 서버 연결
        ws = new WebSocket( '${websocketUrl}/callsocket');

        //소켓 메세지 수신
        ws.onmessage = function(e){
            var rcv_data = JSON.parse(e.data);
            internalNumber = rcv_data.Agent;

            var time = Math.floor(rcv_data.Start);
            var mtime = Math.floor(time / 60);
            var stime = (time - ( mtime * 60 ));
            var timeStr = '';

            if (time >= 60 ) {
                timeStr = mtime + "분 " + stime + "초";
            } else {
                timeStr = stime + "초";
            }

            rcv_data['timeStr'] = timeStr;

            if( rcv_data.EventType == 'STT' ){
                if( rcv_data.Event == 'interim' ){
                    if(rcv_data.Direction == 'TX'){
                        makeMsgTX(rcv_data)
                    }else if(rcv_data.Direction == 'RX'){
                        makeMsgRX(rcv_data)
                    }
                }else if( rcv_data.Event == 'stop' ){
                    // 전화 종료 후 전화걸기 버튼 활성화
                    document.getElementById("callBot").disabled = false;
                }
            }else if( rcv_data.EventType == 'DETECT' ){
                if( rcv_data.Result == 'Y' ){
                    $('#taskList_' + rcv_data.No + "_Y").focus();
                    $('#taskList_' + rcv_data.No + "_Y").prop("checked", true);
                }else{
                    $('#taskList_' + rcv_data.No + "_N").focus();
                    $('#taskList_' + rcv_data.No + "_N").prop("checked", true);
                }

            }
        };

        //소켓 메세지 송신
        ws.onopen = function(){
            console.log("==> Socket Opened");
            sendMsg = '{"EventType":"STT", "Event":"subscribe", "Caller":"' + csData.custTelNo + '", "Agent":"", "contract_no":"' + csData.contract_no + '", "campaignId":"' + csData.campaignId + '"}';
            ws.send(sendMsg);
            sendMsg = '{"EventType":"STT", "Event":"START", "Caller":"' + csData.custTelNo + '", "Agent":"", "contractNo":"' + csData.contract_no + '", "campaignId":"' + csData.campaignId + '"}';
            sendMsgRestful("START", sendMsg);
        }
    }

    //상담사용 실시간 메세지 생성
    function makeMsgTX(rcv_data){

        // TODO: STT 시간 정리 되면 주석 해제 및 정리 필요
        var botMsg = $(
            "<li class='bot'>" +
            "   <span class='thumb'>" +
            "       <img src='/resources/images/ico_agent_bk.png'>" +
            "   </span>" +
            "   <span class='cont'>" +
            "       <em class='txt'>" + rcv_data.Text + "</em>" +
            /*"       <em class='date'>" +  rcv_data.timeStr + "</em>" +*/
            "   </span>" +
            "</li>"
        );

        $("#talkList").append(botMsg);
        $(".chat_mid").scrollTop($(talkList).height());                  // 실시간 대화 시 대화 구간으로 스크롤 이동
    };

    //고객용 실시간 메세지 생성
    function makeMsgRX(rcv_data){

        if (rcv_data.ignored == 'Y') {
            rcv_data.Text = rcv_data.Text + " - IGNORED";
        }

        // TODO: STT 시간 정리 되면 주석 해제 및 정리 필요
        var clientMsg = $(
            "<li class='user'>" +
            "   <span class='cont'>" +
            "       <em class='txt'>" + rcv_data.Text + "</em>" +
            /*"       <em class='date'>" +  rcv_data.timeStr + "</em>" +*/
            "   </span>" +
            "</li>"
        );

        $("#talkList").append(clientMsg);
        $(".chat_mid").scrollTop($(talkList).height());                  // 실시간 대화 시 대화 구간으로 스크롤 이동
    };

    //통화연결 restful
    function sendMsgRestful(event, sendMsgStr){
        sendUrl = "/service/sendCM";

        $.ajax({
            url : sendUrl,
            data : {sendMsgStr:sendMsgStr},
            type : "GET"
        }).done(function(data){
            console.log("### SUCC : " + data);
             if(event == 'START') {
                // 전화 시 전화걸기 버튼 비활성화
                document.getElementById("callBot").disabled = true;

                 // 다시 전화걸기 누르면 기존 stt, checkbox 결과 초기화해줌
                 $("#talkList").empty();
                 $("input[type=checkbox]").prop("checked",false);
             }
        }).fail(function(data){
            console.log("### FAIL : " + data);
        });
    }


    function getCampaignList() {

        $.ajax({
            type: "GET",
            url: "/service/getCampaignListByService",
            data: {serviceName: "maum.ai"},
            success: function(result) {
                var campaignList = JSON.parse(result);

                campaignList.forEach(function(item, idx){
                    var selected = '';
                    if(idx == 0) selected = 'selected';

                    var optionItem = $(
                        "<option value='" + item.campaignId + "'" + selected + ">" + item.campaignNm + "</option>"
                    );

                    $("#campaignList").append(optionItem);
                });

                getCampaignTaskList();
            }
        });
    }

    function getCampaignTaskList() {
        var campaignId = $("#campaignList").val();

        $.ajax({
            type: "GET",
            url: "/service/getCampaignTaskList",
            data: {campaignId: campaignId},
            success: function(result) {
                $("#taskList").empty();

                var taskList = JSON.parse(result);

                taskList.forEach(function(item){
                    var tr = $("<tr></tr>");
                    var tdSeq = $("<td scope='row'>" + item.seq + "</td>");
                    var tdName = $("<td>" + item.category + "</td>");
                    var tdContent = $(
                        "<td>" +
                        "   <div class='checkbox'>" +
                        "       <input type='checkbox' id='taskList_" + item.seq + "_Y' onclick='return false;'><label for='taskList_" + item.seq + "_Y'> Yes</label>" +
                        "   </div>" +
                        "   <div class='checkbox'>" +
                        "       <input type='checkbox' id='taskList_" + item.seq + "_N' onclick='return false;'><label for='taskList_" + item.seq + "_N'> No</label>" +
                        "   </div>" +
                        "</td>"
                    );

                    tr.append(tdSeq);
                    tr.append(tdName);
                    tr.append(tdContent);

                    $("#taskList").append(tr);
                });
            },
            error: function() {

            }
        })
    }

</script>
</body>
</html>
