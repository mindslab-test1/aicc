$(document).ready(function() {
	
	// select design 
	var selectTarget = $('.selectbox select'); 
	
	selectTarget.change(function(){ 
	
	var select_name = $(this).children('option:selected').text(); 
	$(this).siblings('label').text(select_name); 
	}); 
	
	// header user
	$('#header .etcmenu .userBox dl dd > a').on('click',function(){
		$(this).parent().parent().addClass('active');
	});	
	$('#header .etcmenu .userBox').on('focusout',function(){
		$('#header .etcmenu .userBox dl').removeClass('active');
	});
	
	// snb
	$('.snb ul.nav li a').on('click',function(){
		$('.snb ul.nav li').removeClass('active');
		
		$(this).parents().addClass('active');
	});
	$('.snb ul.sub_nav > li > a').on('click',function(){
		$('.snb ul.sub_nav li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});	
	$('.snb ul.third_nav > li > a').on('click',function(){
		$('.snb ul.third_nav > li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});
	
	// select	
	$('.selectbox select').on('focus',function(){
		$(this).parent().addClass('active');
	});
	$('.selectbox select').on('focusout',function(){
		$(this).parent().removeClass('active');
	});	
	
;
	
});
