# AICC sdn, hcall, call manager, ib-call-manager setup 가이드

${MAUM_ROOT} = /srv/maum/

## git clone repository 가이드

```
git clone https://github.com/mindslab-ai/aicc.git

cd aicc

git submodule update --init

```
sdn_install.yaml 에 포함되어 있음.

## ansilbe 설치

yum install ansible (ansible playbook 실행시 필요)

## ansible playbook yaml 파일 설명

sdn_instal.yaml 파일을 통해 아래의 바이너리를 설정할 수 있음.
- sdn

aicc_ansible.yaml 파일을 통해 아래의 바이너리들을 설정할 수 있음.
- hcall
- call manager
- ib-call-manager



## ansible playbook yaml 파일 사용법
```
cd ./aicc (git repository)

#sdn 설치 및 설정

ansible-playbook -vv sdn_install.yaml -K

- root password 입력

#hcall, call manager, ib-call-manager 설치 및 설정

ansible-playbook -vv aicc_ansible.ymal -K

- root password 입력
```



### sdn_install.yaml 의 내용

```
pip install -r /srv/maum/aicc/src/sdn/requiremnets.txt

cd ./aicc
git submodule update ./aicc/src/sdn/pysoundtouch

git submodule update src/sdn/pysoundtouch

cd ./aicc/src/sdn/pysoundtouch/soundtouch
./bootstrap
./configure --enable-integer-samples CXXFLAGS="-fPIC"
make
sudo make install

cd ./aicc/src/sdn/pysoundtouch
sudo python setup.py install

cd ./src/sdn/proto
make pb

cd ./aicc/src/sdn
sudo make install

cp ./aicc/maum-phone_conf/sdn.yaml /srv/maum/etc/
cp ./aicc/amum-phone_conf/supervisor/ /srv/maum/etc/
```



### aicc_ansible.yaml 의 내용

```
yum:

- python-pip
- epel-release
- zeromq-devel
- golang
- python-devel
- MariaDB-shared
- mariadb-devel
- curl-devel
- rapidjson-devel
- cmake3
- libuuid
- libuuid-devel
- alsa-lib-devel
- boost-devel

pip:

- pip install --upgarade pip
- grpcio
- grpcio-tools
- gunicorn
- MySQL-python


cd ./aicc
tar -xvf maum-phone.conf.tar -C /srv/maum/etc/

alias mcmake='cmake3 -DCMAKE_INSTALL_PREFIX=/srv/maum'

cd ./aicc/src/call-manager

make dep

go build

make install

cp ./aicc/config/aicc.conf.sample /srv/maum/etc/aicc.conf

cd ./aicc/src/hcall

tar xvfj pjproject-2.7.2.tar.bz2

cd ./aicc/src/hcall/pjproject-2.7.2
patch -p1 < ../rtp_pt_telephone.patch

cd ./aicc/src/hcall/pjproject-2.7.2
./configure
make dep
make clean
make -j
sudo make install

cd ./aicc/src/hcall
tar xvfz spdlog-1.4.1.tar.gz
unzip ../third-party/rapidjson-master.zip
mv rapidjson-master/include/rapidjson .
rm -rf rapidjson-master

### build libmaum ###

cd ./aicc/submodule/libmaum
./build.sh

cd ./aicc/src/hcall/soci
mkdir build

cd ./aicc/src/hcall/soci/build
cmake3 -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} -DSOCI_LIBDIR=${MAUM_ROOT}/lib ..
make -j
sudo make install

cd ./aicc/src/hcall/tts
sh build_proto.sh

cd ./aicc/src/hcall
mkdir build

cd ./aicc/src/hcall/build
cmake3 -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} -DUSE_MYSQL=ON ..
make -j
sudo make install

cd ./aicc/src/call-wsserver
pip install -r requirements.txt

cp ./aicc/maum-phone_conf/call-manager.conf /srv/maum/etc/
cp ./aicc/maum-phone_conf/happy-call.conf /srv/maum/etc/
cp ./maum-phone_conf/superviosr/ /srv/maum/etc

```
