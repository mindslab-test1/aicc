< 사전 설치 작업 >

* pip 설치
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py

sudo pip install grpcio
sudo pip install grpcio-tools
sudo pip install gunicorn

yum install python-devel

* libmaum 설치

* proto 설치
cd proto
make install

* call-manager
go mod init call-manager
go build

* sdn
sd/README.txt 참고

* map_proxy
sudo pip install flask
sudo pip install flask_restful

< git bundle 사용법 >
* office
프로젝트 개발서버에 반입한 소스의 마지막 커밋이 8ddaf 라고 하자
그 이후 본사에서 작업한 커밋이 있을 때 (원격으로 푸시가 끝난 커밋이라고 전제하자)
다음 명령으로 bundle 파일을 생성한다. 다음 예에서 번들이름은 hihi.bundle 로 가정

$ git bundle create hihi.bundle origin/develop ^8ddaf

위 명령은 새로운 커밋만을 반영하고 있고 다음 명령처럼 브랜치 전체를 만들수도 있다

$ git bundle create hihi.bundle origin/develop

* 고객사

$ git fetch hihi.bundle origin/develop:remotes/origin/develop

위 명령으로 폐쇄망에 있는 원격브랜치가 업데이트 되었으므로 로컬 브랜치도 적용하자.

$ # develop 브랜치라고 가정
$ git merge origin/develop


다음 명령으로 적용이 잘 되었는지 확인하자.

$ git log --decorate
