"""aicc_rest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

import call_statistics.api

app_name='call_statistics'

from rest_framework.routers import SimpleRouter


class OptionalSlashRouter(SimpleRouter):

    def __init__(self):
        self.trailing_slash = '/?'
        super(SimpleRouter, self).__init__()

# router = routers.DefaultRouter(trailing_slash=False)
router = OptionalSlashRouter()
# router = routers.DefaultRouter()
router.register('call_history', call_statistics.api.CallHistoryViewSet)
router.register('call_op', call_statistics.api.OpViewSet)
router.register('call_stat', call_statistics.api.CallstatViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/doc', get_swagger_view(title='Rest API Document')),
    # url(r'^api/', include((router.urls, 'call_statistics'), namespace='api2')),
    url(r'', include((router.urls, 'call_statistics'))),
    # url(r'^api/v1/call_stat', call_statistics.api.CallstatAPIView.as_view()),
]
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()

