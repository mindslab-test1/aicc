from .models import CallHistory, CM_OP_INFO, Callstat
from rest_framework import serializers, viewsets

from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import serializers

from rest_framework.views import APIView
from datetime import datetime

class CallHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = CallHistory
        fields = '__all__'


class CallHistoryViewSet(viewsets.ModelViewSet):
    queryset = CallHistory.objects.all()
    serializer_class = CallHistorySerializer

class OpSerializer(serializers.ModelSerializer):

    class Meta:
        model = CM_OP_INFO
        fields = '__all__'


class OpViewSet(viewsets.ModelViewSet):
    queryset = CM_OP_INFO.objects.all()
    serializer_class = OpSerializer


CALLSTAT_SQL = '''
SELECT
    DATE(FROM_UNIXTIME(R.CALL_TIME)) as start_date,
    TIME(FROM_UNIXTIME(R.CALL_TIME)) as start_time,
    R.CALL_TOTAL as call_total,
    R.call_ai,
    R.call_agent,
    R.call_stop,
    R.call_noresp,
    R.avg_duration
FROM (
    SELECT
        C.CAMPAIGN_ID as CAMP_ID,
        count(*) as CALL_TOTAL,
        count(case WHEN CALL_STATUS = 'CS0005' THEN 1 END) as call_ai,
        count(case WHEN CALL_STATUS = 'CS0010' THEN 1 END) as call_agent,
        count(case WHEN CALL_STATUS = 'CS0003' THEN 1 END) as call_stop,
        count(case WHEN CALL_STATUS = 'CS0004' THEN 1 END) as call_noresp,
        avg(DURATION) as avg_duration,
	FLOOR(UNIX_TIMESTAMP(START_TIME)/({0} * 60)) * {0} * 60 AS CALL_TIME
    FROM
        CALL_HISTORY H
        LEFT JOIN CM_CONTRACT C
        ON H.CONTRACT_NO = C.CONTRACT_NO
    WHERE
        START_TIME >= %s and START_TIME < %s
        AND C.CAMPAIGN_ID = %s
        AND C.IS_INBOUND = 'N'
    GROUP BY
        CALL_TIME, C.CAMPAIGN_ID
) as R;
'''

class CallstatSerializer(serializers.ModelSerializer):

    class Meta:
        model = Callstat
        fields = '__all__'


class CallstatViewSet(viewsets.ModelViewSet):
    # model = Callstat
    queryset = Callstat.objects.all()
    serializer_class = CallstatSerializer

    def list(self, request):
        start_time = request.query_params.get('start_time') or datetime.now().strftime('%Y%m%d')
        end_time = request.query_params.get('end_time') or datetime.now().strftime('%Y%m%d')
        minute_unit = request.query_params.get('minute_unit') or 60
        campaign_id = request.query_params.get('campaign_id') or 0
        print start_time
        # queryset = Callstat.objects.raw('select 0 as id,1 as start_time, 2 as call_total, 3 as call_normal')
        query = CALLSTAT_SQL.format(minute_unit)
        queryset = Callstat.objects.raw(query, [start_time, end_time, campaign_id])
        serializer = CallstatSerializer(queryset, many=True)
        return Response(serializer.data)

# class CallstatAPIView(APIView):
#     def get(self, request):
#         serializer = CallstatSerializer(Callstat.objects.all(), many=True)
#         return Response(serializer.data)
