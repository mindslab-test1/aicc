# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class CallHistory(models.Model):
    call_id = models.CharField(max_length=200)
    contract_no = models.CharField(max_length=200)
    start_time = models.CharField(max_length=200)
    end_time = models.CharField(max_length=200)
    cust_op_id = models.CharField(max_length=200)

class CM_OP_INFO(models.Model):
    cust_op_id = models.CharField(max_length=200)
    cust_op_nm = models.CharField(max_length=200)
    company_id = models.CharField(max_length=200)

class Callstat(models.Model):
    start_date = models.CharField(max_length=100, primary_key=True)
    start_time = models.CharField(max_length=100)
    call_total = models.IntegerField()
    call_ai = models.IntegerField()
    call_agent = models.IntegerField()
    call_stop = models.IntegerField()
    call_noresp = models.IntegerField()
    avg_duration = models.IntegerField()

    class Meta:
        unique_together = (("start_date", "start_time"),)
