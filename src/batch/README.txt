* detect_keyword.py
- CALL_HISTORY, CM_CONTRACT, CM_STT_RSLT_DETAIL 테이블에서 조회
- DETECT_DICTIONARY 에서 키워드 설정
- 탐지된 결과 DETECT_RESULT 생성

* hmd 탐지 프로세스
- hmdclass.py: hmd 연동 인터페이스
- run_aicc_hmd.py: hmd 실행
- aicc_sample.tsv: hmd sample model data

< virtualenv tips>
* virtualenvwrapper 사용
- .bashrc 설정
  export WORKON_HOME=~/.virtualenvs
  source /usr/local/bin/virtualenvwrapper.sh
- mkvirtualenv aicc_ta --python=/usr/bin/python3
- 가상환경 활성화
  workon aicc_ta
- 가상환경 비활성화
  deactivate

< python2 -> python3 소스 변환 >
- 파일 비교
  2to3 some.py
- 비교후 원본 소스 수정
  2to3 -w some.py
