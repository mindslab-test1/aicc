import os
import configparser


class AppConfig:
    def __init__(self):
        self.cfg_path = os.environ['MAUM_ROOT'] + '/etc/aicc.conf'
        self.config = configparser.ConfigParser()
        self.config.read(self.cfg_path)
        self.dbtype = self.config.get('database', 'type')
        self.host   = self.config.get('database', 'host')
        self.port   = self.config.get('database', 'port')
        self.user   = self.config.get('database', 'user')
        self.passwd = self.config.get('database', 'passwd')
        self.db     = self.config.get('database', 'db')
