#!/usr/bin/python

import sys
import logging
import datetime as dt


class MyFormatter(logging.Formatter):
    converter = dt.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


def getLogger(name):
    # formatter = MyFormatter(fmt='%(asctime)s %(message)s',datefmt='%Y-%m-%d,%H:%M:%S.%f')
    formatter = MyFormatter(fmt='[%(asctime)s] [%(levelname)8s] %(message)s')
    # logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
    # stdout을 지정하지 않을 경우 stderr로 출력됨
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    return logger


if __name__ == '__main__':
    logger = getLogger('test')
    logger.info('test')
