#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import os

from sqlalchemy import create_engine
from flashtext import KeywordProcessor
import pdb
import traceback
from threading import Thread as th
import zmq

import logger


class Detect():
    def __init__(self):
        self.logger = logger.get_timed_rotating_logger(
             logger_name='DA',
             log_dir_path=os.getenv('MAUM_ROOT')+'/logs/',
             log_file_name='detect_batch.log',
             backup_count=5,
             log_level='debug')
        db_pool = create_engine('mysql+pymysql://minds:msl1234~@10.122.64.152:3306/HAPPYCALL4',
                                       pool_size=1, max_overflow=1)
        self.db_conn = db_pool.connect()
        self.logger.info('connect db')
        self.init_detect_keyword()

    def check_dic(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind('tcp://0.0.0.0:7777')
        self.logger.info('start check dic thread')
        while True:
            try:
                msg = socket.recv()
                if msg.strip() == 'update':
                    self.logger.info('receive msg to update keyword dic')
                    self.init_detect_keyword()
                    socket.send('success')
                else:
                    socket.send('fail')
                print(msg)
            except:
                self.logger.error('error occurs :: {}'.format(traceback.format_exc()))

    def init_detect_keyword(self):
        query = '''
            SELECT * fROM HAPPYCALL4.DETECT_DICTIONARY
                '''
        result = self.db_conn.execute(query).fetchall()
        print(result)

        self.dissatis_instance = KeywordProcessor()
        self.dissatis_instance.set_non_word_boundaries('')

        self.negative_instance = KeywordProcessor()
        self.negative_instance.set_non_word_boundaries('')

        self.civil_instance = KeywordProcessor()
        self.civil_instance.set_non_word_boundaries('')

        for row in result:
            if row[0].strip() == '불만':
                self.dissatis_instance.add_keyword(row[1].strip())
            elif row[0].strip() == '부정':
                self.negative_instance.add_keyword(row[1].strip())
            elif row[0].strip() == '민원':
                self.civil_instance.add_keyword(row[1].strip())
        self.logger.info('init detect keyword instance')

    def do_work(self):
        self.logger.debug('start work'.format())
        query = '''
        SELECT
            SQL_NO_CACHE A.CALL_ID, A.START_TIME, A.CONTRACT_NO, B.TEL_NO, B.IS_INBOUND
        FROM
            HAPPYCALL4.CALL_HISTORY A
            INNER JOIN HAPPYCALL4.CM_CONTRACT B on A.contract_no = B.contract_no
        WHERE
            A.START_TIME > DATE_ADD(NOW(), INTERVAL -30 DAY)
            and A.TA_STATUS IS NULL
            and A.END_TIME IS NOT NULL;
                '''
        try:
            call_result = self.db_conn.execute(query).fetchall()
            self.db_conn.execute('commit')
            self.logger.info('select call list, lenth is :: {}'.format(len(call_result)))
        except:
                self.logger.error('error occurs :: {}'.format(traceback.format_exc()))

        for call_row in call_result:
            query = '''
            SELECT SPEAKER_CODE, SENTENCE FROM HAPPYCALL4.CM_STT_RSLT_DETAIL WHERE CALL_ID = '{}'
                    '''.format(call_row[0])
            try:
                sentence_result = self.db_conn.execute(query).fetchall()
            except:
                self.logger.error('error occurs :: {}'.format(traceback.format_exc()))
            for sentence_row in sentence_result:
                detect_list = []
                if sentence_row[0].strip() == 'ST0001':
                    found_ban_keyword = self.dissatis_instance.extract_keywords(sentence_row[1].strip())
                    if found_ban_keyword:
                        self.insert_detect_result(call_row, sentence_row, found_ban_keyword, '불만')

                    found_ban_keyword = self.civil_instance.extract_keywords(sentence_row[1].strip())
                    if found_ban_keyword:
                        self.insert_detect_result(call_row, sentence_row, found_ban_keyword, '민원')
                elif sentence_row[0].strip() == 'ST0002':
                    found_ban_keyword = self.negative_instance.extract_keywords(sentence_row[1].strip())
                    if found_ban_keyword:
                        self.insert_detect_result(call_row, sentence_row, found_ban_keyword, '부정')
            query = '''
                UPDATE HAPPYCALL4.CALL_HISTORY SET TA_STATUS = 'D'
                WHERE CALL_ID = {}
                    '''.format(call_row[0])
            try:
                self.db_conn.execute(query)
                self.logger.info('detect finished call id :: {}'.format(call_row[0]))
            except :
                self.logger.error('error occurs :: {}'.format(traceback.format_exc()))

    def insert_detect_result(self, call_row, sentence_row, detect_list, flag):
        for keyword in detect_list:
            query = '''
            INSERT INTO HAPPYCALL4.DETECT_RESULT
                (CONTRACT_NO, CATEGORY, KEYWORD, SPEAKER_CODE, SENTENCE, CONSULT_TIME, IS_INBOUND, TEL_NO)
            VALUES
                ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
            '''.format(call_row[2],
                       flag, keyword,
                       sentence_row[0],
                       sentence_row[1],
                       call_row[1],
                       call_row[4],
                       call_row[3])
            try:
                self.db_conn.execute(query)
            except :
                self.logger.error('error occurs :: {}'.format(traceback.format_exc()))


if __name__ == '__main__':
    a = Detect()
    t = th(target=a.check_dic)
    t.daemon = True
    t.start()
    while True:
        a.do_work()
        time.sleep(5)
