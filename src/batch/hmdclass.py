#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
# reload(sys)
# sys.setdefaultencoding('utf8')

import grpc
# from google.protobuf import json_format
from google.protobuf import empty_pb2

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python'
sys.path.append(lib_path)

from maum.brain.hmd import hmd_pb2
from maum.brain.hmd import hmd_pb2_grpc
from maum.common import lang_pb2
from common.config import Config


class HmdClient:
    conf = Config()
    stub = None

    def __init__(self, remote):
        # remote = 'localhost:' + self.conf.get('brain-ta.hmd.front.port')
        self.remote = remote
        channel = grpc.insecure_channel(self.remote)
        self.stub = hmd_pb2_grpc.HmdClassifierStub(channel)

    def set_model(self):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = 'news'

        rules = []
        rule1 = hmd_pb2.HmdRule()
        # 형태소 분석 결과를 바탕으로 원형 단어를 이용
        # ex) 안녕하세요. -> 안녕하/pa 시/ep 어/ec 요/jx ./s (ETRI 기준)
        # 안녕하, 시어, 요 를 이용 rule을 제작
        rule1.rule = '(안녕하)'
        rule1.categories.extend(['테스트1'])
        # rule1.categories.extend(['level1', 'level2'])
        rules.append(rule1)
        rule2 = hmd_pb2.HmdRule()
        rule2.rule = '(자연)'
        rule2.categories.extend(['테스트3'])
        # rule2.categories.extend(['level1', 'level3'])
        rules.append(rule2)
        model.rules.extend(rules)
        self.stub.SetModel(model)

        model_key = hmd_pb2.ModelKey()
        model_key.lang = lang_pb2.kor
        model_key.model = 'news'
        ret_model = self.stub.GetModel(model_key)
        print(json_format.MessageToJson(ret_model))

    def set_model2(self, file_name, model_name):
        model = hmd_pb2.HmdModel()
        model.lang = lang_pb2.kor
        model.model = model_name

        rules = []
        f = open(file_name)
        for line in f.readlines():
            tokens = line.strip().split('\t')
            # level1, level2, level3...
            levels = tokens[:-1]
            # Last element of tokens
            keyword = tokens[-1]
            print(levels, keyword)
            rule = hmd_pb2.HmdRule()
            rule.rule = keyword
            rule.categories.extend(levels)
            rules.append(rule)
        model.rules.extend(rules)
        self.stub.SetModel(model)

        model_key = hmd_pb2.ModelKey()
        model_key.lang = lang_pb2.kor
        model_key.model = model_name
        ret_model = self.stub.GetModel(model_key)
        print(ret_model)

    def get_class_bytext(self, model, lang, text):
        in_doc = hmd_pb2.HmdInputText()
        in_doc.text = text
        in_doc.model = model
        if lang.lower() == 'kor':
            in_doc.lang = lang_pb2.kor
        elif lang.lower() == 'eng':
            in_doc.lang = lang_pb2.eng

        result = self.stub.GetClassByText(in_doc)
        # print result.cls[0].category
        # print result.cls[:1]
        # print len(result.cls)
        # return result.cls
        if len(result.cls) < 1:
            return None
        return result.cls[0]

        #
        # self.stub.AnalyzeMultiple()
        # SEE grpc.io python examples


def test_hmd():
    hmd_client = HmdClient()
    # hmd_client.get_class_bytext('안녕하세요. 자연어 처리 엔진을 원격으로 호출해요.')
    # hmd_client.get_class_bytext('자세한 보험계약 상담 및 설계를 위해 개인신용정보 처리에 대한 동의 녹음을 진행하겠습니다.')
    # hmd_client.get_class_bytext('자세한 보험계약 상담 및 설계를 위해 개인신용정보 처리에 대한 동의 녹음을 진행하다.')
    # hmd_client.get_class_bytext('필수 동의 사항으로 본 동의를 거부하시는 경우 계약 체결과 유지가 불가능합니다')
    # hmd_client.get_class_bytext('eng01', '안녕하세요 현대해상 상담원입니다 현재시간은 12시입니다')
    hmd_client.get_class_bytext('eng01', 'hello i have got a problem')


def set_hmd():
    # print 'set hmd'
    # raw_input()
    hmd_client = HmdClient()
    hmd_client.set_model2('hyundai.tsv', 'hyundai')


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta.conf')
    if len(sys.argv) < 2:
        test_hmd()
    else:
        set_hmd()
