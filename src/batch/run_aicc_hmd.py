#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
hmd 모델 생성 - 인바운드 상담
hmd client 생성
주기적으로 cm_stt_rslt_det 테이블에서 hmd 실행
"""
import argparse
import time
import grpc
import os
import sys
# sys.path.insert(0, os.environ['MAUM_ROOT'] + '/lib/python')
from multiprocessing import Pool
from sqlalchemy import create_engine
from hmdclass import HmdClient
import aicc_config
import biz_log

INTERVAL = 5
AICC_CFG = aicc_config.AppConfig()
CONN_STR = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
    AICC_CFG.user,
    AICC_CFG.passwd,
    AICC_CFG.host,
    AICC_CFG.port,
    AICC_CFG.db)
DB_POOL = create_engine(CONN_STR,
                        encoding='utf-8',
                        pool_size=10, max_overflow=1, echo=True)
LOGGER = biz_log.getLogger('aicc-hmd')
HMD_ADDR = ''


def do_hmd(params):
    model, lang, stt_rslt_detail_id, sentence = params
    LOGGER.debug('start do_hmd')
    # for child process
    DB_POOL.dispose()

    hmd = HmdClient(HMD_ADDR)
    result = None
    try:
        LOGGER.error('start grpc')
        result = hmd.get_class_bytext(model, lang, sentence)
    except grpc.RpcError as e:
        LOGGER.error('grpc error:', str(e))
    except:
        LOGGER.error('unknown error')

    sql = "INSERT INTO HMD_RESULT " \
          "    (STT_RESULT_DETAIL_ID, CATEGORY1, CATEGORY2, CATEGORY3) " \
          "VALUES " \
          "    (%s, %s, %s, %s)"
    conn = DB_POOL.connect()
    if result:
        c = result.category.split('_')
        conn.execute(sql, stt_rslt_detail_id, c[0], c[1], c[2])
        print('--------------------->', result.category)

    LOGGER.debug('end do_hmd')


def gen_hmd(start, end):
    """
SELECT S.CALL_ID, GROUP_CONCAT(S.SENTENCE order by S.SENTENCE_ID asc SEPARATOR '\n' ) \
          FROM  \
              CM_STT_RSLT_DETAIL S \
              LEFT JOIN HMD_RESULT H \
              ON S.STT_RESULT_DETAIL_ID = H.STT_RESULT_DETAIL_ID  \
          WHERE \
              H.STT_RESULT_DETAIL_ID IS NULL \
              AND UPDATED_DTM >= 20200301 \
              AND UPDATED_DTM < 20200305 group by S.CALL_ID limit 2 \
    """
    sql = '''
    SELECT CA.HMD_MODEL,
           CA.LANG,
           S.STT_RESULT_DETAIL_ID,
           S.SENTENCE
    FROM   CM_STT_RSLT_DETAIL S
           LEFT JOIN HMD_RESULT H
                  ON S.STT_RESULT_DETAIL_ID = H.STT_RESULT_DETAIL_ID
           LEFT JOIN CALL_HISTORY C
                  ON S.CALL_ID = C.CALL_ID
           LEFT JOIN CM_CONTRACT CC
                  ON C.CONTRACT_NO = CC.CONTRACT_NO
           LEFT JOIN CM_CAMPAIGN CA
                  ON CC.CAMPAIGN_ID = CA.CAMPAIGN_ID
    WHERE  H.STT_RESULT_DETAIL_ID IS NULL
           AND S.UPDATED_DTM >= %s
           AND S.UPDATED_DTM < %s
    '''
    st = time.time()
    conn = DB_POOL.connect()
    rows = conn.execute(sql, start, end).fetchall()
    DB_POOL.dispose()

    # hmd = HmdClient()
    # for row in rows:
    #     print(row)
    #     result = hmd.get_class_bytext('hyundai', row[0])
    #     if result:
    #         print('--------------------->', result.category)
    #     # hmd_client.get_class_bytext(row.sentence)
    #     # categories = result.split('_')
    #     # some.insert(insert_sql)

    with Pool(2) as p:
        p.map(do_hmd, [(row[0], row[1], row[2], row[3]) for row in rows])
    print(len(rows), time.time() - st)


def set_hmd(model, file_name):
    hmd_client = HmdClient(HMD_ADDR)
    hmd_client.set_model2(file_name, model)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start', nargs='?', const='', type=str, metavar='START_DATETIME',
                        help='start time')
    parser.add_argument('-e', '--end', nargs='?', const='', type=str, metavar='END DATETIME',
                        help='end time')
    parser.add_argument('--once', action='store_true',
                        help='run once')
    parser.add_argument('--set_model', nargs='?', const='', type=str, metavar='MODEL NAME',
                        help='set hmd model')
    parser.add_argument('--set_file', nargs='?', const='', type=str, metavar='TSV FILE',
                        help='set hmd file')
    parser.add_argument('--remote', nargs='?', default='127.0.0.1:9815', metavar='HMD ADDRESS',
                        help='set hmd file')
    args = parser.parse_args()

    global HMD_ADDR
    HMD_ADDR = args.remote

    if args.set_model:
        set_hmd(args.set_model, args.set_file)
        return
    if args.once:
        gen_hmd(args.start, args.end)
        return

    while True:
        now = time.time()
        next_time = int(now) // INTERVAL * INTERVAL + INTERVAL
        start = time.strftime('%Y%m%d%H%M%S', time.localtime(next_time - INTERVAL))
        start_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(next_time))
        LOGGER.debug('Next process time is {}'.format(start_str))
        time.sleep(next_time - now)
        end = time.strftime('%Y%m%d%H%M%S', time.localtime(next_time))
        gen_hmd(start, end)


if __name__ == '__main__':
    main()
