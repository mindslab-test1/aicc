package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"time"

	zmq "github.com/pebbe/zmq4"
)

// AppMessage : message format
type AppMessage struct {
	Command int32
	CallID  int32
	AgentID [32]byte
}

// enum APP_COMMAND_ENUM {
const (
	HangUp       = 0
	Disconnected = 1
	CheckPkt     = 2
	PlayAudio    = 3
	CallTransfer = 4
)

// RequestToPhone : SoftPhone으로 zmq인터페이스를 통해 이벤트/명령 전달
func RequestToPhone(command int32, dialNo string, agentID string) {
	//  Socket to talk to server
	requester, _ := zmq.NewSocket(zmq.REQ)
	defer requester.Close()
	endpoint := fmt.Sprintf("ipc://%s/run/%s.cmd.ipc", os.Getenv("MAUM_ROOT"), dialNo)
	requester.Connect(endpoint)

	var rawBuf bytes.Buffer
	var szAgentID [32]byte
	copy(szAgentID[:], agentID)
	msg := AppMessage{command, 0, szAgentID}
	binary.Write(&rawBuf, binary.LittleEndian, msg)
	requester.Send(rawBuf.String(), zmq.DONTWAIT)
	log.Printf("Send ReuqestToPhone")
	// Wait for reply:
	_ = requester.SetRcvtimeo(time.Millisecond * 100)
	reply, err := requester.Recv(0)
	if err != nil {
		log.Printf("SoftPhone didn't response. err is \"%v\"", err)
	} else {
		log.Printf("SoftPhone response: %+v", reply)
	}
}
