package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
    "syscall"
	"path"
	"strconv"
	"strings"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	_ "gopkg.in/goracle.v2"
)

// CallRequest comment
type CallRequest struct {
	ContractNo string `json:"contractNo"`
	CampaignID string `json:"campaignId"`
	PhoneNo    string `json:"phoneNo"`
}

// CallLogin comment
type CallLogin struct {
	User     string `json:"User"`
	Password string `json:"Password"`
	Addr     string `json:"Addr"`
}

// CallDirect comment
type CallDirect struct {
	ContractNo string `json:"contractNo"`
	CampaignID string `json:"campaignId"`
	// PhoneNo  string `json:"PhoneNo"`
	AgentID string `json:"AgentID"`
}

// CallStatusRequest comment
type CallStatusRequest struct {
	ContractNo string `json:"contractNo"`
	CampaignID string `json:"campaignId"`
}

// AgentRequest comment
type AgentRequest struct {
	DialNo  string `json:"dialNo"`
	AgentID string `json:"agentId"`
}

// Response comment
type Response struct {
	Code    int    `json:"code"`
	Message string `json:"msg,omitempty"`
	Data    string `json:"data,omitempty"`
}

// StatusResponse comment
type StatusResponse struct {
	Code    int    `json:"code"`
	Message string `json:"msg,omitempty"`
	Data    string `json:"data,omitempty"`
}

type managerAddr struct {
	IP string
}

// Done : check end of call
var Done = make(chan string)

// CallMap : 프로세스 목록
var CallMap map[string]*exec.Cmd

var callCommandChannel chan *CallCommand

var recordDir string

var dbConnectionString string
var dbType string

var managerIP string

func replacePlaceHolder(s string) string {
	if dbType == "mysql" {
		return s
	}
	var converted string
	offset := 0
	for i := 0; ; i++ {
		n := strings.Index(s[offset:], "?")
		if n < 0 {
			converted += s[offset:]
			break
		}

		converted += s[offset:offset+n] + ":" + strconv.Itoa(i+1)
		offset += n + 1
	}
	return converted
}

/*
#define CS_READY      "CS0001" // Outbound Call 시작 준비가 된 상태.
#define CS_PROCESSING "CS0002" // Connected 상태. (통화 중)
#define CS_STOPPED    "CS0003" // 진행 중지 상태. Retry 하지 않는다.
#define CS_RETRY      "CS0004" // 전화연결이 되지 않아서 재시도. Retry
                               // count를 별도로 관리한다. Max. Count
                               // 도달 시 Stopped로 변경.
#define CS_COMPLETED  "CS0005" // 통화 완료
#define CS_CONNECTING "CS0006" // 전화 걸기 시도 중.
#define CS_DROPPED    "CS0007" // 통화 중 끊어진 상태, Callback (unknown message) 3회 이상
*/
func updateCallStatus(status string, contractNo string, campaignID string) {
	// update db status
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	defer db.Close()
	nContractNo, err := strconv.Atoi(contractNo)
	nCampaignID, err := strconv.Atoi(campaignID)
	result, err := db.Exec(replacePlaceHolder(`
		UPDATE CALL_HISTORY
		SET CALL_STATUS=?
		WHERE CALL_ID=(
    		SELECT A.LAST_CALL_ID
    		FROM CM_CONTRACT A
    		WHERE A.CONTRACT_NO=? AND A.CAMPAIGN_ID=?)`), status, nContractNo, nCampaignID)
	if err != nil {
		log.Printf("database execute error: %v", err)
		return
	}
	n, err := result.RowsAffected()
	log.Printf("%v rows affected, %v, %v, %v", n, status, nContractNo, nCampaignID)
	if err != nil {
		log.Printf("updateCallStatus err is %v", err)
	}
}

func getContractNo(phoneNo string, campaignID string) int64 {
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	defer db.Close()
	var contractNo int64
	nCampaignID, err := strconv.Atoi(campaignID)

	var sqlString string
	if dbType == "mssql" {
		sqlString = `SELECT	TOP 1 CONTRACT_NO
		FROM	CM_CONTRACT
		WHERE	CAMPAIGN_ID = ? AND TEL_NO = ? ORDER BY CONTRACT_NO DESC`
	} else {
		sqlString = `SELECT	CONTRACT_NO
		FROM	CM_CONTRACT
		WHERE	CAMPAIGN_ID = ? AND TEL_NO = ? ORDER BY CONTRACT_NO DESC LIMIT 1`
	}
	err = db.QueryRow(sqlString, nCampaignID, phoneNo).Scan(&contractNo)

	if err != nil {
		log.Printf("getContractNo: sql execute error: %v", err)
		result, err := db.Exec(`
			INSERT INTO MNT_TARGET_MNG
				(campaign_id, cust_tel_no, create_dt, is_inbound)
			VALUES
				(?, ?, NOW(), 'N')`, nCampaignID, phoneNo)
		contractNo, _ = result.LastInsertId()
		log.Printf("get new contract_no where phone_no is %s", phoneNo)
		if err != nil {
			log.Printf("database execute error: %v", err)
			return -1
		}
	} else {
		log.Printf("contract_no is existed where phone_no is %s", phoneNo)
	}
	return contractNo
}

func callStart(w http.ResponseWriter, r *http.Request) {
	var request CallRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	if request.ContractNo == "" {
		request.ContractNo = strconv.FormatInt(getContractNo(request.PhoneNo, request.CampaignID), 10)
	}
	enqueueCall(callCommandChannel, request.ContractNo, request.CampaignID)
	resp := Response{Code: 0, Message: "SUCCESS"}

	// DB 정보 업데이트
	// updateCallStatus("CS0008", request.ContractNo, request.CampaignID)

	// 응답 설정
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callStatus(w http.ResponseWriter, r *http.Request) {
	// requestDump, err := httputil.DumpRequest(r, true)
	// fmt.Println(string(requestDump))

	body, err := ioutil.ReadAll(r.Body)
	fmt.Println("result", string(body), len(body))
	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	//params := mux.Vars(r)
	var request CallStatusRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	defer db.Close()
	var callStatus string
	err = db.QueryRow(replacePlaceHolder(`
		SELECT	H.CALL_STATUS
		FROM	CM_CONTRACT C LEFT JOIN CALL_HISTORY H
					ON C.LAST_CALL_ID = H.CALL_ID
		WHERE	C.CONTRACT_NO = ? and C.CAMPAIGN_ID = ?`), request.ContractNo, request.CampaignID).Scan(&callStatus)

	resp := Response{Code: 0, Message: "SUCCESS", Data: callStatus}

	if err != nil {
		log.Printf("err is %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		resp = Response{Code: 500, Message: err.Error(), Data: callStatus}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callStop(w http.ResponseWriter, r *http.Request) {
	var request CallRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	resp := Response{Code: 0, Message: "SUCCESS"}
	key := request.ContractNo + request.CampaignID
	if cmd, ok := CallMap[key]; ok {
        // DONE: 2020-08-26 by shinwc
        // Modified to kill process using SIGTERM signal 
        // because the cmd.Process.Kill used SIGKILL which 
        // cannot be caught by kernel design
		//if err := cmd.Process.Kill(); err != nil {
		if err := cmd.Process.Signal(syscall.SIGTERM); err != nil {
			log.Printf("err is %v", err)
			resp = Response{Code: 0, Message: err.Error()}
		} else {
			// update db status
			updateCallStatus("CS0007", request.ContractNo, request.CampaignID)
		}
	} else {
		resp = Response{Code: 0, Message: "There is no such call"}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callCancel(w http.ResponseWriter, r *http.Request) {
	var request CallRequest
	_ = json.NewDecoder(r.Body).Decode(&request)
	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	removeCall(callCommandChannel, request.ContractNo, request.CampaignID)
	resp := Response{Code: 0, Message: "SUCCESS"}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callTransfer(w http.ResponseWriter, r *http.Request) {
	var request AgentRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	resp := Response{Code: 0, Message: "SUCCESS"}

	RequestToPhone(CallTransfer, request.DialNo, request.AgentID)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callHangup(w http.ResponseWriter, r *http.Request) {
	var request AgentRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	resp := Response{Code: 0, Message: "SUCCESS"}

	RequestToPhone(HangUp, request.DialNo, request.AgentID)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callPlay(w http.ResponseWriter, r *http.Request) {
	var request AgentRequest
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	resp := Response{Code: 0, Message: "SUCCESS"}

	RequestToPhone(PlayAudio, request.DialNo, request.AgentID)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func callLogin(w http.ResponseWriter, r *http.Request) {
	var request CallLogin
	_ = json.NewDecoder(r.Body).Decode(&request)

	// update db status
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("database open error: %v", err)
	}
	defer db.Close()
	var password string
	err = db.QueryRow(replacePlaceHolder(`
		SELECT	PASSWORD
		FROM    CM_OP_INFO
		WHERE	CUST_OP_ID = ?`), request.User).Scan(&password)

	resp := Response{Code: 0, Message: "SUCCESS", Data: ""}

	if err != nil {
		log.Printf("database execute error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		resp = Response{Code: 500, Message: err.Error(), Data: ""}
	} else if request.Password != password {
		log.Printf("invalid password")
		w.WriteHeader(http.StatusInternalServerError)
		resp = Response{Code: 500, Message: "Invalid Password", Data: ""}
	} else {
		log.Printf("User (%v) Login Success from (%v)...", request.User, request.Addr)
		_, err := db.Exec(replacePlaceHolder(`
			UPDATE	CM_OP_INFO
			SET		IP_ADDR = ?
			WHERE	CUST_OP_ID = ?`), request.Addr, request.User)
		if err != nil {
			log.Printf("Can't update agent (id:%v) address: %v, err is %v", request.User, request.Addr, err)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func getSIPInfo(agentID string) obPhoneInfo {
	var info obPhoneInfo
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	defer db.Close()
	err = db.QueryRow(replacePlaceHolder(`
                select
                    SIP_DOMAIN, SIP_USER, PASSWD, CLIENT_PORT, SIP_DOMAIN2
                from
                    SIP_ACCOUNT
                where
                    CUST_OP_ID = ? and IS_INBOUND = 'N' and DIRECT_CALL = 'Y'`), agentID).
		Scan(&info.Domain, &info.User, &info.Password, &info.SIPPort, &info.Domain2)
	if err != nil {
		log.Printf("Can't select sip info with id:(%v)", agentID)
	}
	return info
}

func callDirect(w http.ResponseWriter, r *http.Request) {
	var request CallDirect
	_ = json.NewDecoder(r.Body).Decode(&request)

	log.Printf("Request: %v\n\tBody: %+v", r.URL.Path, request)

	resp := Response{Code: 0, Message: "SUCCESS"}

	go func() {
		phone := getSIPInfo(request.AgentID)
		log.Printf("phone is %v", phone)
		log.Printf(PhoneExePath, "-u", phone.User, "--passwd", phone.Password, "-p", phone.SIPPort, "--contract-no", request.ContractNo, "-c", request.CampaignID, "--domain", phone.Domain, "--domain2", phone.Domain2, "--direct")
		cmd := exec.Command(
			PhoneExePath, "-u", phone.User, "--passwd", phone.Password, "-p", phone.SIPPort, "--contract-no", request.ContractNo, "-c", request.CampaignID, "--domain", phone.Domain, "--domain2", phone.Domain2, "--direct")
		err := cmd.Run()
		log.Printf("Command finished with error: %v", err)
	}()

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(resp)
}

// /call/record/{callId}/{contractNo}/{campaignId}
func downloadFile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	// w.Header().Set("Transfer-Encoding", "chunked")
	//record_%s_%s.wav
	log.Printf("Request: recv call record request")
	fname := fmt.Sprintf("record_%s_%s_%s.wav", params["callId"], params["contractNo"], params["campaignId"])
	fname = path.Join(recordDir, fname)
	log.Printf("file name is %v", fname)

	http.ServeFile(w, r, fname)
}

func postTest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Transfer-Encoding", "chunked")
}

func readConfig(filename string, defaults map[string]interface{}) (*viper.Viper, error) {
	v := viper.New()
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
	// v.SetConfigFile(filename)
	// v.SetConfigName(filename)
	v.SetConfigFile(os.Getenv("MAUM_ROOT") + "/etc/" + filename)
	v.SetConfigType("toml")
	// v.AddConfigPath(".")
	// v.AddConfigPath("$MAUM_ROOT/etc/")
	v.AutomaticEnv()
	err := v.ReadInConfig()
	return v, err
}

func runCallWorker(phone *obPhoneInfo) {
	for {
		contractNo, campaignID := dequeueCall(callCommandChannel, phone.User)
		if len(contractNo) > 0 {
			// do something
			log.Printf("job ---> contract no: %v, campaign id: %v", contractNo, campaignID)

			key := contractNo + campaignID
			// // err := os.Chdir("build")
			log.Printf(PhoneExePath, "-u", phone.User, "--passwd", phone.Password, "-p", phone.SIPPort, "--contract-no", contractNo, "-c", campaignID, "--domain", phone.Domain, "--domain2", phone.Domain2)
			cmd := exec.Command(
				PhoneExePath, "-u", phone.User, "--passwd", phone.Password, "-p", phone.SIPPort, "--contract-no", contractNo, "-c", campaignID, "--domain", phone.Domain, "--domain2", phone.Domain2)
			//cmd.Stdout = os.Stdout
			CallMap[key] = cmd
			cmd.Stdout = os.Stderr
			cmd.Stderr = os.Stderr
			err := cmd.Run()
			log.Printf("Command finished with error: %v", err)
			// TODO: 2020-08-18 by shinwc ---------------------
			// DB delete(db.Exec('DELETE ...) logic was removed in dbDequeCall 
			// and locate here using dbDeleteCall() function 
			dbDeleteCall(contractNo, campaignID) 
			// Commented out to delete CallMap explicitly 
			delete(CallMap, key)
			// ------------------------------------------------
			// // update db status
			// //updateCallStatus("CS0005", request.CallSeq, request.CampaignID)
			// Done <- "done"
			// delete(CallMap, key)
			// time.Sleep(5 * time.Second)
			log.Printf("Job is done")
		} else {
			//log.Printf("There is no job")
		}
		time.Sleep(1 * time.Second)
	}
}

func htmlTest(w http.ResponseWriter, r *http.Request) {
	ip := managerAddr{managerIP}
	tmpl := template.Must(template.ParseFiles("test_rest.html"))
	tmpl.Execute(w, ip)
}

// our main function
func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	log.SetOutput(os.Stdout)
	CallMap = make(map[string]*exec.Cmd)
	v1, err := readConfig("call-manager.conf", map[string]interface{}{
		"port":         5000,
		"ip":           "",
		"phoneExePath": "./happy-call",
		"recordDir":    ".",
	})
	if err != nil {
		panic(fmt.Errorf("Error when reading config: %v", err))
	} else {
		dbType = v1.GetString("database.type")
		if dbType == "oracle" {
			dbType = "goracle"
			// username@[//]host[:port][/service_name][:server][/instance_name]
			dbConnectionString = fmt.Sprintf("%s/%s@%s:%d/%s",
				v1.GetString("database.user"),
				v1.GetString("database.passwd"),
				v1.GetString("database.host"),
				v1.GetInt("database.port"),
				v1.GetString("database.database_name"))
		} else if dbType == "mysql" {
			// "aicc:ggoggoma@tcp(aicc-bqa.cjw9kegbaf8s.ap-northeast-2.rds.amazonaws.com:3306)/happycall"
			dbConnectionString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
				v1.GetString("database.user"),
				v1.GetString("database.passwd"),
				v1.GetString("database.host"),
				v1.GetInt("database.port"),
				v1.GetString("database.database_name"))
		} else if dbType == "mssql" {
			// "mssql", "server=(local);user id=sa;password=pwd;database=pubs"
			dbConnectionString = fmt.Sprintf("server=%s;port=%d;user id=%s;password=%s;database=%s",
				v1.GetString("database.host"),
				v1.GetInt("database.port"),
				v1.GetString("database.user"),
				v1.GetString("database.passwd"),
				v1.GetString("database.database_name"))
		}
		GetPhoneList(dbType, dbConnectionString)
		log.Printf("ip: [%s], port:[%d]", v1.GetString("ip"), v1.GetInt("port"))
		log.Printf("phoneExePath: [%s]", v1.GetString("phoneExePath"))
		log.Printf("dbConnection: [%s]", dbConnectionString)

		managerIP = v1.GetString("ip")

		for i, info := range PhoneList {
			log.Printf("phoneList[%d] info : %v", i, *info)
		}
	}
	go func() {
		var msg string
		for {
			msg = <-Done
			log.Printf("%v", msg)
		}
	}()
	PhoneExePath = v1.GetString("phoneExePath")

	callCommandChannel = startCallQueueManager()
	// run call worker
	for _, phone := range PhoneList {
		go func(phone *obPhoneInfo) {
			runCallWorker(phone)
		}(phone)
	}
	addr := fmt.Sprintf(":%d", v1.GetInt("port"))
	recordDir = v1.GetString("recordDir")
	router := mux.NewRouter()
	router.HandleFunc("/call/start", callStart).Methods("POST")
	router.HandleFunc("/call/status", callStatus).Methods("POST")
	router.HandleFunc("/call/stop", callStop).Methods("POST")
	router.HandleFunc("/call/cancel", callCancel).Methods("POST")
	router.HandleFunc("/call/test", postTest).Methods("POST")
	router.HandleFunc("/call/transfer", callTransfer).Methods("POST")
	router.HandleFunc("/call/hangup", callHangup).Methods("POST")
	router.HandleFunc("/call/play", callPlay).Methods("POST")
	router.HandleFunc("/call/record/{callId}/{contractNo}/{campaignId}", downloadFile).Methods("GET")
	router.HandleFunc("/call/test", htmlTest).Methods("GET")
	router.HandleFunc("/call/login", callLogin).Methods("POST")
	router.HandleFunc("/call/direct", callDirect).Methods("POST")

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./js/")))
	log.Fatal(http.ListenAndServe(addr, router))
}
