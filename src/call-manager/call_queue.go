package main

import (
	"database/sql"
	"log"
	"strings"
)

type callCommandInfo struct {
	CallSeq    string
	CampaignID string
	DialNo     string
}

// CallCommand comment
type CallCommand struct {
	Command string
	Info    callCommandInfo
	// CallSeq         string
	// CampaignID      string
	ResponseChannel chan string
}

func startCallQueueManager() chan *CallCommand {
	var callCmd = make(chan *CallCommand)
	go func() {
		for {
			switch msg := <-callCmd; msg.Command {
			case "start":
				dbEnqueueCall(msg.Info.CallSeq, msg.Info.CampaignID)
				msg.ResponseChannel <- "ok"
			case "cancel":
				dbDeleteCall(msg.Info.CallSeq, msg.Info.CampaignID)
				msg.ResponseChannel <- "ok"
			case "pop":
				contractNo, campaignID := dbDequeueCall(msg.Info.DialNo)
				if len(contractNo) > 0 {
					msg.ResponseChannel <- contractNo + "," + campaignID
				} else {
					msg.ResponseChannel <- "ok"
				}
				// if l.Len() > 0 {
				// 	e := l.Front()
				// 	l.Remove(e)
				// 	info := e.Value.(callCommandInfo)
				// 	delete(CallMap, info)
				// 	msg.ResponseChannel <- contractNo + "," + campaignID
				// } else {
				// 	msg.ResponseChannel <- "ok"
				// }
			default:
				log.Printf("unknown message")
				msg.ResponseChannel <- "ok"
			}
		}
	}()
	return callCmd
}

func dbDequeueCall(dialNo string) (contractNo string, campaignID string) {
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	defer db.Close()
	// HI_PHONE 테이블의 OB용 내선번호에 대해서
	// campaign_id가 지정됐다면 지정된 캠페인만 가져오고
	// 널이라면 내선번호가 지정되지 않은 캠페인 중 아무거나 가져오도록 한다.
	var sqlString string
	if dbType == "goracle" {
		sqlString = `
		SELECT * FROM (
		SELECT	A.CONTRACT_NO, A.CAMPAIGN_ID
		FROM	OB_CALL_QUEUE A
				JOIN SIP_ACCOUNT B ON B.SIP_USER = :1
				JOIN 
					(SELECT LISTAGG(CAMPAIGN_ID, ',') WITHIN GROUP(ORDER BY CAMPAIGN_ID) AS CID FROM SIP_ACCOUNT WHERE IS_INBOUND = 'N') C ON 1=1
		WHERE	(B.CAMPAIGN_ID IS NULL AND (C.CID IS NULL OR NOT INSTR(C.CID, A.CAMPAIGN_ID, 1, 1) > 0))
		OR		INSTR(B.CAMPAIGN_ID, A.CAMPAIGN_ID, 1, 1) > 0
		ORDER BY A.CONTRACT_NO
		) resultSet
		WHERE	ROWNUM=1`
	} else if dbType == "mysql" {
		// DONE: 2021-06-14 modified by shinwc
		// To do the same query as mssql which was applied to Hanwha life on 2020-08-19
		//sqlString = `
		//SELECT	A.CONTRACT_NO, A.CAMPAIGN_ID
		//FROM	OB_CALL_QUEUE A JOIN SIP_ACCOUNT B ON B.SIP_USER = ?
		//		JOIN
		//			(SELECT GROUP_CONCAT(DISTINCT CAMPAIGN_ID SEPARATOR ',') AS CID FROM SIP_ACCOUNT WHERE IS_INBOUND = 'N') C
		//WHERE	CASE
		//			WHEN	B.CAMPAIGN_ID IS NULL THEN (C.CID IS NULL) OR (NOT FIND_IN_SET(A.CAMPAIGN_ID, C.CID))
		//			ELSE	FIND_IN_SET(A.CAMPAIGN_ID, B.CAMPAIGN_ID)
		//		END
		//LIMIT 1`
		sqlString = `
		SELECT 	A.CONTRACT_NO, A.CAMPAIGN_ID
		FROM 	OB_CALL_QUEUE A
		  JOIN SIP_ACCOUNT B
		  ON ( B.SIP_USER = ? AND A.STATUS = 'N' AND A.CAMPAIGN_ID = B.CAMPAIGN_ID)
		LIMIT 1`
	} else if dbType == "mssql" {
		// TODO: 2020-08-19 Deleted query string by shinwc
		// SELECT	TOP 1 A.OB_CALL_QUEUE_ID, A.CONTRACT_NO, A.CAMPAIGN_ID
		// FROM	OB_CALL_QUEUE A JOIN SIP_ACCOUNT B ON A.STATUS = 'N' AND B.SIP_USER = ? ORDER BY A.OB_CALL_QUEUE_ID ASC
		// TODO: need to update sqlString for 'mysql' & 'oracle' using STATUS field in OB_CALL_QUEUE
		sqlString = `
		SELECT	TOP 1 A.CONTRACT_NO, A.CAMPAIGN_ID
		FROM	OB_CALL_QUEUE A 
				JOIN SIP_ACCOUNT B	
				ON ( B.SIP_USER = ? AND A.STATUS = 'N' AND A.CAMPAIGN_ID = B.CAMPAIGN_ID)
		`
	}
	//} else if dbType == "mssql" {
	//	sqlString = `
	//	SELECT	TOP 1 A.CONTRACT_NO, A.CAMPAIGN_ID
	//	FROM	OB_CALL_QUEUE A, SIP_ACCOUNT B 
	//	WHERE A.USER_ID = B.SIP_USER
	//	AND B.SIP_USER = ?
	//	`
	//}

	err = db.QueryRow(sqlString, dialNo).Scan(&contractNo, &campaignID)

	if err != nil {
		if err != sql.ErrNoRows {
			log.Printf("dbDequeueCall: sql execute error: %v", err)
		}
		return "", ""
	}
	log.Printf("dbDequeueCall: sql execute success: contractNo [%v], campaignID [%v]", contractNo, campaignID)

	// TODO: 2020-08-18 by shinwc ---------------------
	_, err = db.Exec(replacePlaceHolder(`
		UPDATE OB_CALL_QUEUE
		SET STATUS = 'Y'
		WHERE CONTRACT_NO = ? AND CAMPAIGN_ID = ?`), contractNo, campaignID)
	if err != nil {
		log.Printf("database dbDequeueCall() update error: %v", err)
	} else {
		log.Printf("database dbDequeueCall() update success: %v", err)
	}
	// -------------------------------------------------

	// contractNo = strconv.FormatInt(nContractNo, 10)
	// campaignID = strconv.FormatInt(nCampaignID, 10)
	return contractNo, campaignID
}

func dbEnqueueCall(contractNo string, campaignID string) {
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	log.Printf("info: %s, %s", contractNo, campaignID)
	defer db.Close()

	_, err = db.Exec(replacePlaceHolder(`
		INSERT INTO OB_CALL_QUEUE
			(CONTRACT_NO, CAMPAIGN_ID)
		VALUES
			(?, ?)`), contractNo, campaignID)
	if err != nil {
		log.Printf("database dbEnqueueCall() insert error: %v", err)
	} else {
		log.Printf("database dbEnqueueCall() insert success: %v", err)
	}
}

func dbDeleteCall(contractNo string, campaignID string) {
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Printf("err is %v", err)
	}
	log.Printf("info: %s, %s", contractNo, campaignID)
	defer db.Close()

	_, err = db.Exec(replacePlaceHolder(`
		DELETE	FROM OB_CALL_QUEUE
		WHERE	CONTRACT_NO = ? AND CAMPAIGN_ID = ?`), contractNo, campaignID)
	if err != nil {
		log.Printf("database dbDeleteCall() delete error: %v", err)
	} else {
		log.Printf("database dbDeleteCall() delete success: %v", err)
	}
}

func enqueueCall(cmdChan chan *CallCommand, callSeq string, campaignID string) {
	msg := CallCommand{
		"start",
		callCommandInfo{callSeq, campaignID, ""},
		make(chan string),
	}
	cmdChan <- &msg
	resp := <-msg.ResponseChannel
	if resp != "ok" {
		log.Printf("resp not ok")
	}
}

func removeCall(cmdChan chan *CallCommand, callSeq string, campaignID string) {
	msg := CallCommand{
		"cancel",
		callCommandInfo{callSeq, campaignID, ""},
		make(chan string),
	}
	cmdChan <- &msg
	resp := <-msg.ResponseChannel
	if resp != "ok" {
		log.Printf("resp not ok")
	}
}

func countCall(cmdChan chan *CallCommand) {
	msg := CallCommand{
		"count",
		callCommandInfo{"", "", ""},
		make(chan string),
	}
	cmdChan <- &msg
	resp := <-msg.ResponseChannel
	if resp != "ok" {
		log.Printf("resp not ok")
	}
}

func dequeueCall(cmdChan chan *CallCommand, dialNo string) (callSeq string, campaignID string) {
	msg := CallCommand{
		"pop",
		callCommandInfo{"", "", dialNo},
		make(chan string),
	}
	cmdChan <- &msg
	resp := <-msg.ResponseChannel
	if resp != "ok" {
		tokens := strings.Split(resp, ",")
		callSeq = tokens[0]
		campaignID = tokens[1]
		log.Printf("resp %v %v", callSeq, campaignID)
	}
	return callSeq, campaignID
}
