// Unit Test파일, 파일이름은 _test로 끝나야 한다.
package main

import (
	"fmt"
	"log"
	"strconv"
	"testing"
)

// Queue Test
func TestQueue(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	dbConnectionString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		"minds",
		"msl1234~",
		"10.122.64.152",
		3306,
		"HAPPYCALL3")

	cmd := startCallQueueManager()
	log.Printf("start enqueue")

	for i := 5; i <= 10; i++ {
		seq := strconv.Itoa(i)
		enqueueCall(cmd, seq, "2")
	}

	log.Printf("end enqueue")

	for i := 5; i <= 10; i++ {
		callSeq, campaignID := dequeueCall(cmd, "6006")
		log.Printf("dequeue: %s, %s", callSeq, campaignID)
	}

	// for i := 1; i <= 100000; i++ {
	// 	seq := strconv.Itoa(i)
	// 	enqueueCall(cmd, seq, "1")
	// }
	// removeCall(cmd, "101", "2")
	// log.Printf("start cancel")
	// for i := 1; i <= 100000; i++ {
	// 	seq := strconv.Itoa(i)
	// 	removeCall(cmd, seq, "2")
	// }
	// log.Printf("end cancel")
	// countCall(cmd)

}

func TestGetContractNo(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	dbConnectionString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		"minds",
		"msl1234~",
		"10.122.64.152",
		3306,
		"HAPPYCALL3")
	contractNo := getContractNo("01087658146", "1")
	log.Printf("contract_no is %v", contractNo)
}
