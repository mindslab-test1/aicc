module mindslab-ai/call-manager

go 1.15

require (
	github.com/denisenkom/go-mssqldb v0.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/pebbe/zmq4 v1.2.7
	github.com/spf13/viper v1.7.1
	gopkg.in/goracle.v2 v2.24.1
)
