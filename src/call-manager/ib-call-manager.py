#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import ConfigParser
import multiprocessing
import subprocess

# for log
import sys
import logging
import datetime as dt


class MyFormatter(logging.Formatter):
    converter = dt.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


def getLogger(name):
    # formatter = MyFormatter(fmt='%(asctime)s %(message)s',datefmt='%Y-%m-%d,%H:%M:%S.%f')
    formatter = MyFormatter(fmt='[%(asctime)s] [%(levelname)s] %(message)s')
    # logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
    # stdout을 지정하지 않을 경우 stderr로 출력됨
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    return logger


LOGGER = getLogger(__name__)


class AppConfig:
    def __init__(self):
        self.cfg_path = os.environ['MAUM_ROOT'] + '/etc/aicc.conf'
        self.config = ConfigParser.ConfigParser()
        self.config.read(self.cfg_path)
        self.dbtype = self.config.get('database', 'type')
        self.host   = self.config.get('database', 'host')
        self.port   = self.config.get('database', 'port')
        self.user   = self.config.get('database', 'user')
        self.passwd = self.config.get('database', 'passwd')
        self.db     = self.config.get('database', 'db')

        self.phone_exe  = self.config.get('phone', 'path')


def run_inbound(phone, port):
    domain, dial_no, passwd, campaign_id, phone_exe, domain2 = phone
    while True:
        # 컬럼이 고정사이즈인 경우 오른쪽 여백에 스페이스로 채워지는 경우 방지
        dial_no = dial_no.rstrip()
        passwd = passwd.rstrip()
        LOGGER.info("Start outbound call: {}".format((phone_exe, dial_no, passwd, domain, port, campaign_id)))
        if domain2 is None:
            domain2 = ''
        cmd = "%s -u %s --passwd '%s' -p %d -c %s -i --once --domain %s --domain2 '%s'" % (
            phone_exe, dial_no, passwd, port, campaign_id, domain, domain2)
        subprocess.call(cmd, shell=True)
        LOGGER.info("End outbound call: {}".format((phone_exe, dial_no, passwd, domain, port, campaign_id)))


def get_phone_list():
    config = AppConfig()
    phones = list()

    try:
        if config.dbtype == 'mysql':
            import MySQLdb
            con = MySQLdb.connect(host=config.host,
                                  port=int(config.port),
                                  user=config.user,
                                  passwd=config.passwd,
                                  db=config.db)
        elif config.dbtype == 'oracle':
            import cx_Oracle
            con = cx_Oracle.connect(config.user, config.passwd, "%s:%s/%s" % (config.host, config.port, config.db))
        elif config.dbtype == 'mssql':
            import pymssql
            con = pymssql.connect(server=config.host, user=config.user, password=config.passwd, database=config.db)
        cur = con.cursor()
        qry = "SELECT SIP_DOMAIN, SIP_USER, PASSWD, CAMPAIGN_ID, SIP_DOMAIN2 FROM SIP_ACCOUNT where is_inbound = 'Y'"
        cur.execute(qry)
        for row in cur.fetchall():
            phones.append((row[0], row[1], row[2], row[3], config.phone_exe, row[4]))

        con.close()
    except Exception as e:
        LOGGER.error(e)
    LOGGER.info('Phone count is %d' % len(phones))
    return phones


def main():
    LOGGER.info('Start Inbound Call Manager daemon...')
    phones = get_phone_list()
    process_list = list()
    port = 5060
    for phone in phones:
        p = multiprocessing.Process(target=run_inbound, args=(phone, port))
        p.daemon = True
        p.start()
        process_list.append(p)
        port += 1

    for p in process_list:
        p.join()


if __name__ == '__main__':
    main()

