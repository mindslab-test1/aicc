#!/usr/bin/python

import MySQLdb

class DBConfig:
    def __init__(self):
        self.host = '10.122.64.152'
        self.user = 'minds'
        self.passwd = 'msl1234~'
        self.db = 'HAPPYCALL3'

def get_campaigns():
    campaigns = list()
    try:
        config = DBConfig()
        con = MySQLdb.connect(config.host, config.user, config.passwd, config.db)
        cur = con.cursor()
        qry = "SELECT DISTINCT camp_id FROM CAMPAIGN_MNG"
        cur.execute(qry)
        for row in cur.fetchall():
            campaigns.append(row[0])
        con.close()
    except Exception as e:
        print str(e)

    print campaigns
    return campaigns

def get_phones():
    phones = list()
    try:
        config = DBConfig()
        con = MySQLdb.connect(config.host, config.user, config.passwd, config.db)
        cur = con.cursor()
        qry = "SELECT number FROM HI_PHONE WHERE is_inbound = 'N'"
        cur.execute(qry)
        for row in cur.fetchall():
            phones.append(row[0])
        con.close()
    except Exception as e:
        print str(e)

    print phones
    return phones

def main():
    try:
        campaigns = get_campaigns()
        phones = get_phones()

        config = DBConfig()

        con = MySQLdb.connect(config.host, config.user, config.passwd, config.db)
        cur = con.cursor()

        for phone in phones:
            for camp in campaigns:
                qry = "INSERT INTO OB_PHONE_CAMPAIGN SELECT '{}', '{}'".format(phone, camp)
                cur.execute(qry)
        con.commit()
        print 'success'
        con.close()
    except Exception as e:
        print str(e)

main()


