package main

import (
	"database/sql"
	"log"
	"fmt"
)

type obPhoneInfo struct {
	User     string
	Password string
	SIPPort  string
	Domain   string
	Domain2  string
}

// PhoneExePath : happy-call 실행 파일위치
var PhoneExePath string

// PhoneList : 인공지능 상담사 전화번호
var PhoneList = []*obPhoneInfo{}

// GetPhoneList : 전화 정보 설정
func GetPhoneList(dbType string, connStr string) {
	db, err := sql.Open(dbType, connStr)
	if err != nil {
		log.Printf("err is %v", err)
		return
	}
	defer db.Close()
	var ifnull string
	if (dbType == "mysql") {
		ifnull = "IFNULL"
	} else if (dbType == "mssql") {
		ifnull = "ISNULL"
	}
	query := fmt.Sprintf(`
					SELECT
						SIP_USER, PASSWD, CLIENT_PORT, SIP_DOMAIN, %s(SIP_DOMAIN2, '')
					FROM
						SIP_ACCOUNT
					WHERE
						IS_INBOUND = 'N' and DIRECT_CALL = 'N'`, ifnull)
	rows, err := db.Query(query)
	if err != nil {
		log.Printf("err is %v", err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var info obPhoneInfo
		err := rows.Scan(&info.User, &info.Password, &info.SIPPort, &info.Domain, &info.Domain2)
		if err != nil {
			log.Printf("rows.Scan err is %v", err)
		}
		PhoneList = append(PhoneList, &info)
	}
}
