* virtualenv 설정하기
- 가상환경 설정하기 (venv 디렉토리로 설정했을 때)
$ virtualenv venv
- 가상환경 활성화하기
$ source venv/bin/activate
- 가상환경 빠져나오기
$ deactivate

- 설치된 패키지 목록
$ pip freeze > requirements.txt

- txt파일로부터 설치하기
$ pip install -r requirements.txt

<apache 설치>
sudo yum install httpd
sudo service httpd start
sudo systemctl enable httpd.service

<ssl 설정>
yum install mod_ssl
sudo service httpd start

mkdir /etc/httpd/cert
/etc/httpd/cert 디렉토리로 인증서 파일 복사

/etc/httpd/conf.d/ssl.conf 파일 수정
Listen 443 https 다음 라인에 아래 추가
Listen 8443 https

<VirtualHost _default_:8443>
        SSLEngine On
        SSLCertificateFile /etc/httpd/cert/sslCert-_maum_ai.crt
        SSLCertificateKeyFile /etc/httpd/cert/maum.ai.key
</VirtualHost>


<reverse proxy 설정>

        ProxyRequests Off
        ProxyPreserveHost On
        ProxyPass /callsocket ws://localhost:13254/callsocket disablereuse=on
        ProxyPassReverse /callsocket ws://localhost:13254/callsocket


sudo service httpd start

시스템 설정 변경
/usr/sbin/setsebool httpd_can_network_connect true
