#!/usr/bin/python
# -*- coding: utf-8 -*-

import zmq
import datetime as dt
import time
import sys
import getopt
import multiprocessing
import websocket
import logging

WS_ADDR = 'ws://127.0.0.1:13254/callsocket'

start_message = '''
{"EventType": "STT",
 "Event": "start",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "1"
}
'''

stop_message = '''
{"EventType": "STT",
 "Event": "stop",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "%d"
}
'''

subscribe_message = '''
{"EventType": "STT",
 "Event": "subscribe",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "%d"
}
'''

interim_message = '''
{"EventType": "STT",
 "Event": "interim",
 "Caller": "01022223333",
 "Agent": "6001",
 "Direction": "RX",
 "Start": "0.01",
 "End": "1.23",
 "Text": "안녕하십니까? 마인즈보험 로봇상담원입니다. 홍길동 고객님 되십니까?",
 "contract_no": "%d"
}
'''

call_status_message = '''
{"EventType": "CALL",
 "Event": "status",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "%d",
 "call_status": "CS0002"
}
'''


class MyFormatter(logging.Formatter):
    converter=dt.datetime.fromtimestamp
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


def do_test(params):
    no, duration = params
    ws = websocket.create_connection(WS_ADDR, setdefaulttimeout=1)
    ws.send(subscribe_message % no)
    start = time.time()
    cnt = 0
    while time.time() - start < duration:
        ws.send(interim_message % no)
        ws.recv()
        cnt += 1
        # time.sleep(0.01)

    ws.close()
    return cnt


def usage():
    print '%s [-c channel_count] [-d duration]' % sys.argv[0]

def main():
    session_cnt = 1
    duration = 1

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'c:d:', [])
        for option, value in opts:
            if option == '-c':
                session_cnt = int(value)
            elif option == '-d':
                duration = int(value)
        if len(opts) < 2:
            usage()
            sys.exit(2)

    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)


    p = multiprocessing.Pool(session_cnt)
    logger = logging.getLogger(__name__)
    logger.info('start to send message for %d seconds...' % duration)

    results = p.map(do_test, [(x, duration) for x in range(session_cnt)])
    logger.info('TPS is %.3f per seconds.' % (sum(results) / float(duration)))

    return


if __name__ == '__main__':
    formatter = MyFormatter(fmt='%(asctime)s %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(__name__)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    main()
