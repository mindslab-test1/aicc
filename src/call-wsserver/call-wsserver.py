#!/usr/bin/python
# -*- coding: utf-8 -*-

import zmq
import datetime as dt

# old
import multiprocessing
import signal
import time
import traceback
import socket
import sys
import json
##

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid
import websocket
import collections
import struct

from tornado.options import define, options

WS_ADDR = 'ws://127.0.0.1:13254/callsocket'
# 콜게이트 운영
CALLGATE_IP = '100.10.10.3'
CALLGATE_PORT = 60093
CALLGATE_AUTH_KEY = '4989AE41D9E8A501B64AA33CB978622AF8B9EAB9'
# 콜게이트 개발
#CALLGATE_IP = '100.10.10.24'
#CALLGATE_PORT = 60093
#CALLGATE_AUTH_KEY = 'EC1D05C75671AD7F8D7B19AF99F625077DC0FBFB'

class MyFormatter(logging.Formatter):
    converter=dt.datetime.fromtimestamp
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s

define("port", default=13254, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/", MainHandler), (r"/callsocket", CallSocketHandler)]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        super(Application, self).__init__(handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html", messages=CallSocketHandler.cache)


class CallSocketHandler(tornado.websocket.WebSocketHandler):
    # Key: contract_no, value: socketHandler
    sessions = dict()           # STT 이벤트 Subscribers
    call_event_subscribers = set()

    calls = dict()
    waiters = set()
    cache = []
    cache_size = 200

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def check_origin(self, origin):
        return True

    def open(self):
        self.contract_no = None
        CallSocketHandler.waiters.add(self)

    def on_close(self):
        logging.info("on_close")
        if self.contract_no and self.contract_no in CallSocketHandler.sessions:
            CallSocketHandler.sessions[self.contract_no].remove(self)
            logging.info("unsubscribe {}".format(self.contract_no))

        if self in CallSocketHandler.call_event_subscribers:
            CallSocketHandler.call_event_subscribers.remove(self)

        CallSocketHandler.waiters.remove(self)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size :]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def process_detect(self, message, data):
        contract_no = data['contract_no']
        event = data['event']
        if contract_no not in CallSocketHandler.sessions:
            CallSocketHandler.sessions[contract_no] = set()

        if event == 'result':
            for client in CallSocketHandler.sessions[contract_no]:
                try:
                    client.write_message(message)
                except:
                    logging.error("Error sending message", exc_info=True)
                logging.info('send detect event - contract_no: {}, no: {}'.format(data['contract_no'], data['no']))
        else:
            logging.error('unknown event: {}'.format(event))

    @classmethod
    def process_stt(cls, handler, message, data):
        contract_no = data['contract_no']
        event = data['event']
        if contract_no not in cls.sessions:
            cls.sessions[contract_no] = set()

        if event == 'subscribe':
            if handler not in cls.sessions[contract_no]:
                cls.sessions[contract_no].add(handler)
                handler.contract_no = contract_no
            else:
                logging.info('Host (%s:%s) already subscribed contract_no - %s' %
                             (handler.request.remote_ip, handler.stream.socket.getpeername()[1]), contract_no)
        elif event in ('start', 'stop', 'interim'):
            for client in cls.sessions[contract_no]:
                try:
                    client.write_message(message)
                except:
                    logging.error("Error sending message", exc_info=True)
                logging.info('send stt result: %s' % message)
            if event == 'stop':
                del cls.sessions[contract_no]

    def process_call_event(self, message, data):
        if data['event'] == 'subscribe':
            if self not in CallSocketHandler.call_event_subscribers:
                CallSocketHandler.call_event_subscribers.add(self)
                logging.info('Host (%s:%s) subscribe Call Event' %
                             (self.request.remote_ip, self.stream.socket.getpeername()[1]))
        elif data['event'] in ('status', 'mntresult', 'transfer'):
            if (data['call_status'] == 'CS0006'):
                self.callgate(data['caller'])
            for subscriber in CallSocketHandler.call_event_subscribers:
                subscriber.write_message(message)

    def on_message(self, message):
        try:
            logging.info("GET MESSAGE - %s" % message)
            data = json.loads(message)
            data = dict((k.lower(), v) for k, v in data.iteritems())
            event_type = data['eventtype']
            if event_type == 'STT':
                CallSocketHandler.process_stt(self, message, data)
            elif event_type == 'CALL':
                self.process_call_event(message, data)
            elif event_type == 'DETECT':
                self.process_detect(message, data)
            else:
                logging.error('unknown event type: {}'.format(eventtype))
        except KeyError as e:
            logging.error("there is no event type")
            logging.error("recv message: %s" % message)
        except ValueError as e:
            logging.error("json error: {}".format(e))
            logging.error("recv message: %s" % message)
        return

        # CallSocketHandler.update_cache(chat)
        # CallSocketHandler.send_updates(chat)

    def callgate(self, phone_number, agent, contract_no):
        socket_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        is_connected = False
        filed_size = {'MESSAGE_SIZE': 4, 'MESSAGE_NO': 20,
                      'CUSTOMER_SERVICE_ID': 16, 'MESSAGE_TYPE': 10,
                      'AUTH_KEY': 40, 'SVC_CODE': 16, 'USER_MDN': 16,
                      'BOUND_TYPE': 3, 'PROLOGUE_CONTENT_CODE': 6,
                      'PROLOGUE_CUSTOM_URL': 200, 'EPILOGUE_CONTENT_CODE': 6,
                      'EPILOGUE_CUSTOM_URL': 200, 'CALLER_MDN_CHECK_YN': 1, 'CALLER_MDN': 20}
        packet_data = ''

        # connect tcp socket
        try:
            logging.info('[CALL_GATE] addr: ():()'.format(CALLGATE_IP, CALLGATE_PORT))
            socket_client.connect((CALLGATE_IP, CALLGATE_PORT))
            logging.info('[CALL_GATE] socket connect success')
            is_connected = True
        except Exception as e:
            logging.error('[CALL_GATE] socket connect failed')
            logging.error(e)

        # use tcp socket
        if is_connected:
            logging.info('[CALL_GATE] use tcp socket start')
            json_dic = collections.OrderedDict()
            json_dic['MESSAGE_SIZE'] = '558'
            json_dic['MESSAGE_NO'] = str(contract_no)
            json_dic['CUSTOMER_SERVICE_ID'] = 'HWLF002'
            json_dic['MESSAGE_TYPE'] = 'IP1000Q'
            json_dic['AUTH_KEY'] = CALLGATE_AUTH_KEY
            json_dic['SVC_CODE'] = '158863633'
            json_dic['USER_MDN'] = str(phone_number)
            json_dic['BOUND_TYPE'] = 'OUT'
            json_dic['PROLOGUE_CONTENT_CODE'] = '0000'
            json_dic['PROLOGUE_CUSTOM_URL'] = 'http://calltoweb.co.kr/hanwhalife/prologue.php?code=5001'
            json_dic['EPILOGUE_CONTENT_CODE'] = 'NONE'
            json_dic['CALLER_MDN_CHECK_YN'] = 'N'
            json_dic['CALLER_MDN'] = str(agent)

        for key, value in json_dic.iteritems():
            packet_data = packet_data + value.ljust(filed_size.get(key), ' ')

        logging.info('[CALL_GATE] send data: [()]'.format(packet_data))
        try:
            data = packet_data.encode()
            socket_client.sendall(data)
            logging.info('[CALL_GATE] send data success')

            res_data_leng = socket_client.recv(4)
            res_data = socket_client.recv(int(res_data_leng))
            res_packet_data = res_data.decode()
            logging.info('[CALL_GATE] get response: [()]'.format(res_packet_data))
            split_data = res_packet_data.split()
            logging.info('[CALL_GATE] result_code: [()]'.format(split_data[-2]))
        except Exception as e:
            logging.error('[CALL_GATE] send/get data failed')
            logging.error(e)
        logging.info('[CALL_GATE] use tcp socket end')

class CallEventReceiver(multiprocessing.Process):
    def __init__(self):
        multiprocessing.Process.__init__(self)
        self.ws = None

    def connect_ws(self):
        try:
            # Websocket Server에 메시지를 보내기 위한 websocket client
            # self.ws = websocket.create_connection(WS_ADDR, setdefaulttimeout=0)
            self.ws = websocket.create_connection(WS_ADDR, setdefaulttimeout=3)
        except socket.error as e:
            if e.errno == 111:  # [Errno 111] Connection refused
                print e.message
            else:
                print e
        except Exception as e:
            print traceback.format_exc()

    def send_ws(self, message):

        if self.ws is None:
            self.connect_ws()

        if self.ws:
            self.ws.send(message)

    def run(self):
        logger = logging.getLogger(__name__)

        self.connect_ws()

        done = False

        context = zmq.Context()
        endpoint = "tcp://0.0.0.0:9830"
        collector = context.socket(zmq.PULL)
        collector.bind(endpoint)

        poller = zmq.Poller()
        poller.register(collector, zmq.POLLIN)

        cnt = 0
        while done is False:
            try:
                socks = dict(poller.poll())
                if collector in socks and socks[collector] == zmq.POLLIN:
                    message = collector.recv()
                    cnt += 1
                    self.send_ws(message)

            except KeyboardInterrupt:
                print 'program stopped'
                done = True
            except Exception as e:
                print traceback.format_exc()
                done = True


def main():
    #er = CallEventReceiver()
    #er.start()

    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port, address='0.0.0.0')
    tornado.ioloop.IOLoop.current().start()

    #er.join()

if __name__ == '__main__':
    # formatter = MyFormatter(fmt='%(asctime)s %(message)s',datefmt='%Y-%m-%d,%H:%M:%S.%f')
    formatter = MyFormatter(fmt='%(asctime)s %(message)s')
    # logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(__name__)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    main()


