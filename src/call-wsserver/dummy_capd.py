#!/usr/bin/python
# -*- coding: utf-8 -*-

import zmq
import time

start_message = '''
{"EventType": "STT",
 "Event": "start",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "1"
}
'''

stop_message = '''
{"EventType": "STT",
 "Event": "stop",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "1"
}
'''

subscribe_message = '''
{"EventType": "STT",
 "Event": "subscribe",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "1"
}
'''

interim_message = '''
{"EventType": "STT",
 "Event": "interim",
 "Caller": "01022223333",
 "Agent": "6001",
 "Direction": "RX",
 "Start": "0.01",
 "End": "1.23",
 "Text": "안녕하세요",
 "contract_no": "1"
}
'''

call_status_message = '''
{"EventType": "CALL",
 "Event": "status",
 "Caller": "01022223333",
 "Agent": "6001",
 "contract_no": "1",
 "call_status": "CS0002"
}
'''





def process_cmd(server):
    print '1. start'
    print '2. stop'
    print '3. subscribe'
    print '4. interim'
    print '5. call status'
    cmd = raw_input('input number:')
    if cmd == '1':
        server.send(start_message)
    elif cmd == '2':
        server.send(stop_message)
    elif cmd == '3':
        server.send(subscribe_message)
    elif cmd == '4':
        server.send(interim_message)
    elif cmd == '5':
        server.send(call_status_message)

def main():
    context = zmq.Context()
    endpoint = "tcp://127.0.0.1:9830"
    server = context.socket(zmq.PUSH)
    server.connect(endpoint)

    while True:
        process_cmd(server)

    start = time.time()
    cnt = 0
    while time.time() - start < 1:
        #server.send('FROM,고객,상담사,0.00,1.11,CALLID-1234,안녕하세요')
        server.send(interim_message)
        cnt += 1
        # 초당 1000 ~ 2000개 메시지
        time.sleep(0.0005)
#        time.sleep(0.1)
    #    break

    print cnt

if __name__ == '__main__':
    main()
