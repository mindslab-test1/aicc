#ifndef AGENT_MESSAGE_H
#define AGENT_MESSAGE_H

#include "message_buffer.h"

enum AgentSystemType
{
  AI_AGENT,
  PC_AGENT
};

enum TransferType
{
  RECV_ONLY,
  SEND_ONLY,
  RECV_SEND
};

enum OpCode
{
  REGISTER,
  TRANSFER,
  PUBLISH,
  SUBSCRIBE,
  RINGING,
  NOTIFY,
  CONNECT,
  DISCONNECT,
};

struct AgentMessageHeader
{
  char    Version[2];
  int16_t SystemType;
  int16_t OpCode;
  int16_t Length;
};

struct RegisterBody
{
  uint32_t GetContractNo() { return ntohl(ContractNo); }
  uint32_t GetTransType()  { return ntohl(TransType); }

  void SetContractNo(uint32_t no)   { ContractNo = htonl(no); }
  void SetTransType(uint32_t t) { TransType = htonl(t); }

  uint32_t ContractNo;
  char     AgentID[20];
  char     DialNo[20];
  uint32_t TransType;
};

struct AgentMessage : public AgentMessageHeader
{
  int16_t GetSystemType() { return ntohs(SystemType); }
  int16_t GetOpCode()     { return ntohs(OpCode); }
  int16_t GetLength()     { return ntohs(Length); }

  void SetVersion()              { Version[0] = 'M'; Version[1] = 'L'; }
  void SetSystemType(int16_t t)  { SystemType = htons(t); }
  void SetOpCode(int16_t code)   { OpCode = htons(code); }
  void SetLength(int16_t length) { Length = htons(length); }

  char    Body[1024];
};

class AgentMessageBuffer : public MessageBuffer
{
public:
  AgentMessageBuffer() {}
  ~AgentMessageBuffer() {}

  virtual int GetMessageSize(char* buf, int len) {
    AgentMessage* msg = (AgentMessage*)buf;
    return msg->GetLength();
  }

  virtual int GetMinimumSize() {
    return sizeof(AgentMessageHeader);
  }
};

#endif /* AGENT_MESSAGE_H */
