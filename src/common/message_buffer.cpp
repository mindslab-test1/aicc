#include <string.h>
#include "message_buffer.h"
#include <stdio.h>

MessageBuffer::MessageBuffer()
{
    rd_ptr = buffer;
    wr_ptr = buffer;
}

MessageBuffer::~MessageBuffer()
{
}

char* MessageBuffer::GetBuffer()
{
    if ((wr_ptr - rd_ptr) < MAX_DATA_SIZE) {
        // error... buffer has no enough space...
        // we have to process buffer after call GetMessage() 
    }

    return wr_ptr;
}

void MessageBuffer::Commit(size_t bytes_read)
{
    // after socket read(), we have to move wr_ptr
    wr_ptr += bytes_read;
}

void MessageBuffer::Reset()
{
    rd_ptr = buffer;
    wr_ptr = buffer;
}

char* MessageBuffer::GetMessage(int &len)
{
    char* data; // return value
    int recv_length;    // data length to process
    int message_size;   // message size

    recv_length = wr_ptr - rd_ptr;

    if (recv_length < GetMinimumSize()) {
        memmove(buffer, rd_ptr, recv_length);
        rd_ptr = buffer;
        wr_ptr = buffer + recv_length;
        return NULL;
    }

    message_size = GetMessageSize(rd_ptr, recv_length);
    len = message_size;

    if (message_size > MAX_BUFF_SIZE) {
        // error or invalid client !!!
        // maybe we must disconnect socket...
        Reset();
        return NULL;
    } else if (recv_length < message_size) {
        //memcpy(buffer, rd_ptr, recv_length);  
        memmove(buffer, rd_ptr, recv_length);
        rd_ptr = buffer;
        wr_ptr = buffer + recv_length;
        return NULL;
    } else if (recv_length == message_size) { // don't need?? due to while loop GetMessage()
        data = rd_ptr;
        rd_ptr = buffer;
        wr_ptr = buffer;
        return data;
    } else if (recv_length > message_size) {
        data = rd_ptr;
        rd_ptr += message_size;
        return data;
    }

    return NULL;
}

int MessageBuffer::GetMessageSize(char* buf, int len)
{
    return len;
}

int MessageBuffer::GetMinimumSize()
{
    return 1;
}
