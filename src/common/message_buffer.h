#ifndef MESSAGE_BUFFER_H
#define MESSAGE_BUFFER_H

#define MAX_BUFF_SIZE 32768
#define MAX_DATA_SIZE 32768

class MessageBuffer
{
public:
    MessageBuffer();
    virtual ~MessageBuffer();

    char* GetBuffer();
    char* GetMessage(int &len);
    void  Commit(size_t bytes_read);
    void  Reset();

    virtual int GetMessageSize(char* buf, int len);
    virtual int GetMinimumSize();

private:
    char buffer[MAX_BUFF_SIZE + MAX_DATA_SIZE];

    char* rd_ptr;
    char* wr_ptr;
};

#endif
