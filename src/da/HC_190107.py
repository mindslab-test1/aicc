#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from concurrent import futures
import argparse
import grpc
import os
import random
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
import time
import syslog
import pymysql
import datetime
import re
import json

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da.v3 import talk_pb2
from maum.m2u.facade import front_pb2
from maum.m2u.common import card_pb2

# Custom import
#from qa.util import *
from qa import basicQA
#from custom_hs.sds import SDS
from custom_hs.sds_second import SDS
from qa.util import Util

# echo_simple_classifier = {
#    "echo_test": {
#        "regex": [
#            "."
#        ]
#    }

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
Session_Value = []
_NOTHING_FOUND_INTENT_ = "의도를 찾지 못했습니다."
model = 'Happy_Call_HH'

#def setSessionValue(session_id, key, value):
#    try:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        findIndex = sessionList.index(session_id)
#        Session_Value[findIndex][key] = value
#    except Exception as err:
#        pass
#
#def getSessionValue(session_id, key):
#    keyValue = None
#    isNew = False
#
#    if len(Session_Value) > 0:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        try:
#            findIndex = sessionList.index(session_id)
#            keyValue = Session_Value[findIndex][key]
#        except Exception as err:
#            isNew = True
#    else:
#        isNew = True
#
#    if isNew:
#        Session_Value.append({"session_id" : session_id, "model" : ""})
#        keyValue = Session_Value[len(Session_Value)-1][key]
#
#    return keyValue
#
#def RegEx(self, session_id):
#    print("RegEx call!!")
#
#    print("get model : " + str(getSessionValue(session_id, "model")))
#    modelname = str(getSessionValue(session_id, "model"))
#    if modelname == "":
#        intent = "Happy_Call_HH"
#        setSessionValue(session_id, "model", intent)
#    elif modelname == "2":
#        intent = "privacy"
#        setSessionValue(session_id, "model", intent)
#
#    #intent = str(getSessionValue(session_id, "model"))
#    return intent

class EchoDa(talk_pb2_grpc.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'control'
    provider.description = 'control intention return DA'
    provider.version = '0.1'
    provider.single_turn = True
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # PARAMETER

    def __init__(self):
        syslog.syslog('init')
        self.state = provider_pb2.DIAG_STATE_IDLE
        syslog.syslog(str(self.state))
        self.qa_util = Util()
        self.Sds = SDS()

    #
    # INIT or TERM METHODS
    #

    def IsReady(self, empty, context):
        print 'V3 ', 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'V3 ', 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'V3 ', 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, empty, context):
        print 'V3 ', 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = userattr_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        loc.title = '기본 지역'
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '기본으로 조회할 지역을 지정해주세요.'
        attrs.append(loc)

        device = userattr_pb2.UserAttribute()
        device.name = 'device'
        device.title = '기본 디바이스'
        device.type = userattr_pb2.DATA_TYPE_STRING
        device.desc = '기본으로 사용할 디바이스를 지정해주세요.'
        attrs.append(device)

        country = userattr_pb2.UserAttribute()
        country.name = 'time'
        country.title = '기준 국가 설정'
        country.type = userattr_pb2.DATA_TYPE_STRING
        country.desc = '기본으로 조회할 국가를 지정해주세요.'
        attrs.append(country)

        result.attrs.extend(attrs)
        return result

    #
    # PROPERTY METHODS
    #

    def GetProviderParameter(self, empty, context):
        print 'V3 ', 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'V3 ', 'GetRuntimeParameters', 'called'
        result = provider_pb2.RuntimeParameterList()
        params = []

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '171.64.122.134'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_INT
        db_port.desc = 'Database Port'
        db_port.default_value = '7701'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'minds'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_AUTH
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'minds67~'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'ascar'
        db_database.required = True
        params.append(db_database)

        result.params.extend(params)
        return result
    def OpenSession(self, request, context):
        print "openSession"

        #param = {}
        #bizRes = {}
        lectureInfo = {}
        resMessage = 'success'
        meta = ''
        lectureNum = ''
        session_id = request.session.id
        #self.showKeyValue(context.invocation_metadata())

        if 'meta' in request.utter.meta:
             #meta = eval(request.utter.meta['meta'].replace('null','\"\"'))
             meta = request.utter.meta['meta']

             if 'intent' in meta:
                slots = meta['intent']['slots']
                if 'lectureNumber' in slots:
                    lectureNum = slots['lectureNumber']['value']

        #requestParam = Common.setMetaToParamMap(lectureNum=lectureNum,userTalk=' ' ,request=request, isopenRequest=True,session_id=session_id)
        localSessionObj = session_id
        print 'OpenSession id: '+str(request.session.id)

        result = talk_pb2.TalkResponse()
        res_meta = struct.Struct()
        #res_meta['response'] = bizRes
        result.response.meta.CopyFrom(res_meta)

        #session 정보 ,session data 10k
        result.response.session_update.id = session_id
        res_context = struct.Struct()
        res_context['session_data'] = str(lectureInfo)
        result.response.session_update.context.CopyFrom(res_context)

        print 'OpenSession_'
        return result

    def OpenSkill(self, request, context):
        print "OpenSkill start"
        print 'Open request: '+str(request)
        session_id = request.session.id
        print 'open_session_data: '+ str(session_id)+', '+str(context)
        result = talk_pb2.TalkResponse()
        print 'OpenSkill end'
        return result


    def CloseSkill(self, request, context):

        result = talk_pb2.CloseSkillResponse()
        return result

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()


    def DBConnect(self, query):
        conn = pymysql.connect(user="aicc",
                               password="ggoggoma",
                               host="aicc-bqa.cjw9kegbaf8s.ap-northeast-2.rds.amazonaws.com",
                               database="happycall",
                               charset="utf8",
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        curs.execute(query)
        print("query good!")
        rows = curs.fetchall()

        print(rows)
        curs.execute("commit;")
        curs.close()
        conn.close()
        return rows
    def DBConnect_HC(self, query):
        conn = pymysql.connect(user="maum",
                               password="ggoggoma",
                               host="localhost",
                               database="HappyCall",
                               charset="utf8",
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        curs.execute(query)
        print("query good!")
        rows = curs.fetchall()

        print(rows)
        curs.execute("commit;")
        curs.close()
        conn.close()
        return rows


    def Talk(self, talk, context):
        #print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        #print(talk)
        #print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        a = {}
        """
        talk.utter.utter : 사용자 발화
        talk_res를 return

        talk_res.response.speech.utter : 챗봇 발화
        """
        print("talk : ", talk.utter.utter)

        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        print("talk : ", talk.utter.utter)
        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
       
        # SDS.dp.slots["product_code"] = "A"
        session_id = talk.session.id

        talk_res = talk_pb2.TalkResponse()
        print ('===============' * 8)
        print ('talk_res' + str(talk_res))
        print ('===============' * 8)

        ##초기 모델 설정
        #model = "ediya_kiosk"
        model = "ediya"

        #question = talk.utter.utter
        meta = dict()
        #meta['seq_id'] = util.time_check(0)
        meta['log_type'] = 'SVC'
        meta['svc_name'] = 'DA'

        #output = ""
        original_answer = ""
        engine_path = list()
        answer_engine = "None"
        status_code = ""
        status_message = ""
        flag = False
        weight = 0
        code = 'None'
        sds_intent = ""




        # SDS
        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)

        print("sds_intent의 값 : " + str(sds_intent))


        #그렇다면 넘어가는 로직을 여기다가 잡아놔야 할 것 같음
        
        # SDS
#        sds_list = []
##        tasknumber = task.find('task':)
#        #### 로테이션 문항 진행
#        if product_code == 'b': 
#            sds_list.append(9)
#            sds_list.append(11)
#            #sds_list.append(13)
##        if product_code == 'a': sds_list.append(9,10,11,12,13,14)
##        if product_code == 'a': sds_list.append(9,10,11,12,13,14)
#        
#        print (sds_list)
#        task9_info = ""
#        task11_info = ""
#        task13_info = ""
#        
#        if sds_list == []:
#            pass
#        elif max(sds_list) == 9:
#            task9_info = "다음부터 진행되는 질문은 담보문항에 대한 질문입니다."
#        elif max(sds_list) == 11:
#            task11_info = "다음부터 진행되는 질문은 담보문항에 대한 질문입니다."
#        elif max(sds_list) == 13:
#            task13_info = "다음부터 진행되는 질문은 담보문항에 대한 질문입니다."
#        for sds_temp in sds_list:
#            dbtask = self.DBConnect("select task from campaign_target_list_tb where contract_no = '" + seq + "';")
#            task = dbtask[0]['task']
#            if task is None or task == "":
#                tasknumber = ""
#            else:
#                tasknumber = task[4:]
#                print("tasknumber, sds_temp : "+tasknumber + str(sds_temp))
#                if tasknumber == str(sds_temp):
#                    talk.utter.utter = "$task"+ str(sds_temp) +"$"
#                    print("utter가 들어간 값은 : " + talk.utter.utter)
#                    self.DBConnect("update campaign_target_list_tb set task='task"+str(sds_temp+1)+"' where contract_no = '" + seq + "';")
#                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
#                    if tasknumber == 13:
#                        pass
#                    else:
#                        talk.utter.utter = "$task"+ str(sds_temp+1) +"$"
#                        print("utter가 들어간 값은 : " + talk.utter.utter)
#                        self.DBConnect("update campaign_target_list_tb set task='task"+str(sds_temp+2)+"' where contract_no = '" + seq + "';")
#                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
#
#        print(sds_res)
        #print("===============test=============")

        original_answer = sds_res['response']
        answer_engine = "SDS"
        engine_path.append("SDS")

        #original_answer = self.unknown_answer()
        #첫 SDS 답변 입력사항
        original_answer = sds_res['response']
        talk_res.response.speech.utter = original_answer

        #시간 테스트
        #talk_time = dbsession[0]['talk_time']
        #talk_strp = datetime.datetime.strptime(talk_time, '%Y-%m-%d %H:%M:%S')
        #talk_month = talk_strp.month
        #talk_day = talk_strp.day
        #talk_hour = talk_strp.hour
        #talk_minute = talk_strp.minute

        #테스크 종류
        # Greet
        # RECOGNITION
        # ORDER
        # CUP_TYPE
        # END


        if sds_res['current_task'] == 'ORDER':
            a = []
            drink_name1 = '카페모카'
            drink_name2 = '카라멜 마끼아또'
            drink_name3 = '아메리카노'
            drink_id1 = ""
            drink_id2 = ""
            drink_id3 = ""
            drink_id1 = self.product_code(drink_name1)
            drink_id2 = self.product_code(drink_name2)
            drink_id3 = self.product_code(drink_name3)
            json_data = json.dumps(
                [
                    {
                        "drink_id1" : drink_id1,
                        "drink_name1" : "카페모카",
                        "drink_size1" : "R",
                        "shot1" : "0",
                        "cream1" : "N",
                        "choco_chips1" : "N",
                        "caramel_syrup1" : "N",
                        "vanilar_syrup1" : "N",
                        "hazelot_syrup1" : "N",
                        "choco_source1" : "N",
                        "caramel_source1" : "N",
                        "carrier1" : "N",
                        "count1" : "1"
                    },
                    {
                        "drink_id2" : drink_id2,
                        "drink_name2" : "카라멜 마끼아또",
                        "hot_cold2" : "C",
                        "drink_size2" : "R",
                        "shot2" : "0",
                        "cream2" : "N",
                        "choco_chips2" : "N",
                        "caramel_syrup2" : "Y",
                        "vanilar_syrup2" : "N",
                        "hazelot_syrup2" : "N",
                        "choco_source2" : "N",
                        "caramel_source2" : "N",
                        "carrier2" : "N",
                        "count2" : "1"
                    },
                    {
                        "drink_id3" : drink_id3,
                        "drink_name3" : "아메리카노",
                        "hot_cold3" : "C",
                        "drink_size3" : "EX",
                        "shot3" : "0",
                        "cream3" : "N",
                        "choco_chips3" : "N",
                        "caramel_syrup3" : "Y",
                        "vanilar_syrup3" : "N",
                        "hazelot_syrup3" : "N",
                        "choco_source3" : "N",
                        "caramel_source3" : "N",
                        "carrier3" : "N",
                        "count3" : "1"
                    },
                ]
            )
            
            drink_id1 = ""
            drink_id2 = ""
            drink_id3 = ""
            print('%' * 70)
            print(dict(talk.session.context))
            print(type(dict(talk.session.context)))
#            try:
#                print(json.loads(dict(talk.session.context).get('context')).get('prod1'))
#            except Exception as e:
#                print(e)
#                pass
            print('%' * 70)

            session_data = struct.Struct()
            session_data['id'] = talk.session.id
            session_data['order'] = json_data
            talk_res.response.session_update.context.CopyFrom(session_data)
            dic = dict(talk.session.context)
            print(dict(talk.session.context))
            try:
                print(dic.get('context').get('prod1'))
            except:
                pass
            #print('%' * 70)


#            drink_name = ""
#            items = sds_res['intent.filled_slots.items']
#            print(items)
#            for id in items:
#                a[id[0]] = id[1]
#                print(id)
#            if a['DRINK_NAME'] == "" : drink_name = ""
#            else: drink_name = a['DRINK_NAME']
            print("*" * 30)

#            print drink_name

            print("*" * 30)
        #테스크 종류
        # Greet
        # RECOGNITION
        # ORDER
        # CUP_TYPE
        # END

        if sds_res['current_task'] == 'CUP_TYPE':
            print(dict(talk.session.context))
            card = card_pb2.Card()
            card_data = struct.Struct()
            card.custom.type =  'json'
            card_data['order'] = json.dumps(json.loads(dict(talk.session.context).get('order')),ensure_ascii=False)
            card.custom.card_data.CopyFrom(card_data)
            #card.custom.card_data = json.dumps(dict(talk.session.context))
#            help(card.custom.card_data)
            talk_res.response.cards.extend([card])
            

            
        #if sds_intent == "RECOGNITION" or sds_intent == "ORDER" or sds_intent == "CUP_TYPE":
            

        if sds_intent == "cup_type":
            try:
                print(json.loads(dict(talk.session.context).get('order')))
            except Exception as e:
                print(e)
                pass
                
        if sds_res['current_task'] == 'timeAffirm':
            if talk.utter.utter == "$hourMiss$" or talk.utter.utter == "$dayMiss$":
                print("dcdddddddddddddddddddddddddddddddddddd")
                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
            else:
                print("===============test=============")
                print("next_month : " + str(next_month))
                print("next_day : " + str(next_day))
                print("next_part : " + str(next_part))
                print("next_hour : " + str(next_hour))
                print("next_minute : " + str(next_minute))
                print("===============test=============")
                talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + self.readNumberMinute(next_month) +"월"+ self.readNumberMinute(next_day) + "일 "+str(next_part) +", " +  self.readNumberHour(next_hour) + "시 "+ self.readNumberMinute(next_minute) + "분이 맞습니까?"
                self.DBConnect("update campaign_target_list_tb set next_time='"+str(next_time)+"' where contract_no = '" + seq + "';")
            #talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + next_month +"월"+ next_day + "일 "+next_part +", " +  next_hour + "시 "+ next_minute + "분이 맞습니까?"
            #말씀하신 일정이 11월 19일 오전 3시 30분이 맞습니까?
        #if sds_res['current_task'] == 'timeEnd':
            #self.DBConnect("update hc_hh_campaign_score set next_time='"+str(next_time)+"' where contract_no = '" + seq + "';")





        #질문 수정사항, 답변 문구 정리
        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
        #task1
        if sds_res['current_task'] == 'task1':
            talk_res.response.speech.utter = "안녕하십니까?, 현대해상 고객센터입니다, " + user_name + "고객님 되십니까?"
        if sds_res['current_task'] == 'time':
            if sds_intent == "hourmiss":
                talk_res.response.speech.utter = "통화 가능 시를 말씀해주시지 않았습니다.통화가능 시를 말씀해주세요."
            if sds_intent == "daymiss":
                talk_res.response.speech.utter = "통화 가능 요일을 말씀해주시지 않았습니다.통화가능 요일을 말씀해주세요."
        #task14       
        if sds_res['current_task'] == 'task14':
            #talk_res.response.speech.utter = "네 고객님, 소중한시간 내주셔서 감사합니다. 현대해상 고객센터였습니다. $complete$"
            talk_res.response.speech.utter = "긴 시간 답변 감사드립니다. 향후 불편하시거나 궁금하신 점 있으시면 담당자나 고객콜센터로 언제든지 연락주시기 바랍니다. 오늘도 행복한 하루 보내세요. 현대해상 고객센터였습니다. $complete$"


        print("[ANSWER]: " + original_answer)
        #print("[SESSION_KEY] :" + )
        print("[ENGINE]: " + answer_engine)

        #위치 전송
        #self.DBConnect("update hc_hh_campaign_score set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")



        return talk_res

    def product_code(self, input):
        b = {
            'S-ICED 페퍼민트 티': '00000002',
            'S-ICED 캐모마일 레드 티': '00000003',
            'S-ICED 보이차(디카페인)': '00000004',
            'S-ICED 로즈 쟈스민 티': '00000005',
            'S-ICED 얼그레이홍차(블랙티)': '00000006',
            'S-ICED 어린잎녹차(작설차)': '00000007',
            'S-밀크티': '00000008',
            'S-캐모마일 레드 티': '00000009',
            'S-페퍼민트 티': '00000010',
            'S-로즈 쟈스민 티': '00000011',
            'S-보이차(디카페인)': '00000012',
            'S-어린잎녹차(작설차)': '00000013',
            'S-얼그레이홍차(블랙티)': '00000014',
            'S-유자 플랫치노': '00000015',
            'S-자몽 플랫치노': '00000016',
            'S-초콜릿 칩 플랫치노': '00000017',
            'S-망고 플랫치노': '00000018',
            'S-녹차 요거트 플랫치노': '00000019',
            'S-블루베리 요거트 플랫치노': '00000020',
            'S-녹차 플랫치노': '00000021',
            'S-카라멜 플랫치노': '00000022',
            'S-모카 플랫치노': '00000023',
            'S-복숭아 아이스티': '00000024',
            'S-커피 플랫치노': '00000025',
            'S-레몬 아이스티': '00000026',
            'S-ICED 카라멜 크림넛 라떼': '00000027',
            'S-ICED 토피넛 라떼': '00000028',
            'S-ICED 시나몬 초콜릿': '00000029',
            'S-ICED 민트 초콜릿': '00000030',
            'S-ICED 녹차 라떼': '00000031',
            'S-ICED 차이티 라떼': '00000032',
            'S-ICED 화이트 초콜릿': '00000033',
            'S-ICED 우유': '00000034',
            'S-ICED 초콜릿': '00000035',
            'S-카라멜 크림넛 라떼': '00000036',
            'S-차이티 라떼': '00000037',
            'S-토피넛 라떼': '00000038',
            'S-녹차 라떼': '00000039',
            'S-민트 초콜릿': '00000040',
            'S-화이트 초콜릿': '00000041',
            'S-시나몬 초콜릿': '00000042',
            'S-우유': '00000043',
            'S-초콜릿': '00000044',
            'S-ICED 스위트 카라멜넛 라떼': '00000045',
            'S-ICED 바닐라 라떼': '00000046',
            'S-ICED 민트 모카': '00000047',
            'S-ICED 화이트 초콜릿 모카': '00000048',
            'S-ICED 화이트 카라멜 모카': '00000049',
            'S-ICED 시나몬 모카': '00000050',
            'S-ICED 카라멜 마끼아또': '00000051',
            'S-ICED 카라멜 카페모카': '00000052',
            'S-ICED 카페모카': '00000053',
            'S-ICED 카푸치노': '00000054',
            'S-ICED 카페라떼': '00000055',
            'S-에스프레소 마끼아또': '00000056',
            'S-에스프레소 콘파냐': '00000057',
            'S-ICED 아메리카노': '00000058',
            'S-에스프레소': '00000059',
            'S-스위트 카라멜넛 라떼': '00000060',
            'S-바닐라 라떼': '00000061',
            'S-민트 모카': '00000062',
            'S-카라멜 마끼아또': '00000063',
            'S-화이트 초콜릿 모카': '00000064',
            'S-화이트 카라멜 모카': '00000065',
            'S-시나몬 모카': '00000066',
            'S-카라멜 카페모카': '00000067',
            'S-카페모카': '00000068',
            'S-카페라떼': '00000069',
            'S-카푸치노': '00000070',
            '에스프레소': '00000071',
            'S-아메리카노': '00000072',
            '향시럽 700ml': '00000073',
            '루이보스티': '00000074',
            '향시럽 300ml': '00000075',
            '메스티머그컵': '00000076',
            '아이리쉬머그컵': '00000077',
            '데미타세잔': '00000078',
            '유리머그컵': '00000079',
            '스위트 포테이토 샌드': '00000080',
            '파니니튜나 샌드위치': '00000081',
            '머그컵': '00000082',
            '샌드위치': '00000083',
            '후르츠': '00000084',
            '쏘야 브래드 웰빙': '00000085',
            '치즈 머핀': '00000086',
            '초코칩 머핀': '00000087',
            '치킨샌드위치': '00000091',
            '마카다미아 넛 쿠키': '00000092',
            '블루베리 머핀': '00000093',
            '록키로드 쿠키': '00000094',
            '오트밀 레이즌 쿠키': '00000095',
            '초코렛칩 청크 쿠키': '00000097',
            '카라멜 월넛': '00000102',
            '휘난시에': '00000104',
            '마드레드': '00000105',
            '블루베리치즈': '00000106',
            '드라마케익(조각모음)': '00000107',
            '레아치즈': '00000108',
            '마블크림치즈': '00000109',
            '수플레': '00000110',
            '녹차무스': '00000111',
            '쵸코티라미스': '00000112',
            '쵸코무스': '00000113',
            '산딸기무스': '00000114',
            '티라미스': '00000115',
            '고구마': '00000116',
            '블루베리치즈': '00000117',
            '레아치즈': '00000118',
            '수플레': '00000119',
            '마블크림치즈': '00000120',
            '쵸코티라미스': '00000121',
            '녹차무스': '00000122',
            '쵸코무스': '00000123',
            '티라미스': '00000124',
            '고구마': '00000125',
            '산딸기무스': '00000126',
            '쇼콜라 드 노아': '00000127',
            '쵸코티라미스': '00000128',
            '녹차무스': '00000129',
            '블루베리치즈': '00000130',
            '쵸코무스': '00000131',
            '티라미스': '00000132',
            '고구마': '00000133',
            '산딸기무스': '00000134',
            '플레인치즈 스틱케익': '00000137',
            '가또오랑쥬': '00000138',
            '무화과': '00000139',
            '블루베리치즈': '00000140',
            '자허': '00000141',
            '레아치즈': '00000142',
            '수플레': '00000143',
            '마블크림치즈': '00000144',
            '녹차무스': '00000145',
            '쵸코티라미스': '00000146',
            '산딸기무스': '00000147',
            '티라미스': '00000148',
            '쵸코무스': '00000149',
            '이곡 라떼': '00000150',
            '시럽추가(카라멜)': '00000151',
            '샷추가': '00000152',
            '휘핑추가': '00000153',
            '고구마': '00000156',
            'ice 캐모마일 레드티': '00000158',
            'ice 로즈쟈스민티': '00000159',
            'ice 페퍼민트티': '00000160',
            'ice 탈카페인 보이차': '00000161',
            'ice 작설차': '00000162',
            'ice 홍차(얼그레이)': '00000163',
            '밀크티': '00000164',
            '캐모마일 레드티': '00000165',
            '페퍼민트티': '00000166',
            '탈카페인 보이차': '00000167',
            '작설차': '00000168',
            '로즈쟈스민티': '00000169',
            '홍차(얼그레이)': '00000170',
            '유자플랫치노': '00000171',
            '자몽플랫치노': '00000172',
            '망고 플랫치노': '00000173',
            '초코렛 칩 플랫치노': '00000174',
            '녹차 요거트플랫치노': '00000175',
            '블루베리 요거트플랫치노': '00000176',
            '녹차 플랫치노': '00000177',
            '모카 플랫치노': '00000178',
            '카라멜 플랫치노': '00000179',
            '커피 플랫치노': '00000180',
            '레몬아이스티': '00000181',
            '복숭아아이스티': '00000182',
            'ice 민트초코렛': '00000183',
            'ice 차이티라떼': '00000185',
            'ice 토피넛라떼': '00000186',
            'ice 카라멜 크림 넛 라떼': '00000187',
            'ice 시나몬초코렛': '00000188',
            'ice 화이트초코렛': '00000189',
            'ice 초코렛': '00000190',
            'ice 우유': '00000191',
            '카라멜 크림 넛 라떼': '00000192',
            '녹차라떼': '00000193',
            '차이티라떼': '00000194',
            '토피넛라떼': '00000195',
            '민트초코렛': '00000196',
            '시나몬초코렛': '00000197',
            '화이트초코렛': '00000198',
            '우유': '00000199',
            '초코렛': '00000200',
            'ice 시나몬모카': '00000201',
            'ice 민트모카': '00000202',
            'ice 스위트 바닐라 라떼': '00000203',
            'ice 스위트 카라멜 넛 라떼': '00000204',
            'ice 화이트카라멜모카': '00000205',
            'ice 화이트초코렛모카': '00000206',
            'ice 카라멜 마끼아또': '00000207',
            'ice 카라멜 카페모카': '00000208',
            'ice 카페라떼': '00000209',
            'ice 카푸치노': '00000210',
            'ice 카페모카': '00000211',
            '에스프레소콘파냐': '00000212',
            'ice 아메리카노': '00000213',
            '에스프레소 마끼아또': '00000214',
            '에스프레소': '00000215',
            '스위트 카라멜 넛 라떼': '00000216',
            '민트모카': '00000217',
            '스위트 바닐라 라떼': '00000218',
            '시나몬모카': '00000219',
            '카라멜 마끼아또': '00000220',
            '화이트초코렛모카': '00000221',
            '화이트카라멜모카': '00000222',
            '카라멜 카페모카': '00000223',
            '카페모카': '00000224',
            '아메리카노': '00000225',
            '카페라떼': '00000226',
            '카푸치노': '00000227',
            '프레즐': '00000228',
            '레몬에이드(병)': '00000229',
            '핑크레몬에이드(병)': '00000230',
            '필라델피아치즈': '00000231',
            '키위주스': '00000232',
            '딸기주스': '00000233',
            '토마토주스': '00000234',
            '파인애플주스': '00000235',
            '바나나주스': '00000236',
            '딸기+바나나': '00000239',
            '키위+바나나': '00000240',
            '플레인요거트': '00000242',
            '라즈베리요거트': '00000243',
            '블루베리요거트': '00000244',
            '망고요거트': '00000245',
            '커플요거트(2인-토핑포함)': '00000246',
            '토핑4가지': '00000247',
            '요거트빙수(1인)': '00000248',
            '요거트빙수(2인)': '00000249',
            '샌드위치 세트': '00000251',
            '베이글세트': '00000252',
            '토스트세트': '00000253',
            '머핀(set)': '00000254',
            '상품권': '00000255',
            '튜나샌드위치': '00000257',
            '모닝Set': '00000259',
            '클럽샌드위치': '00000260',
            '시럽추가(카페시럽)': '00000261',
            '블루베리베이글': '00000262',
            '시나몬베이글': '00000263',
            '어니언베이글': '00000264',
            '플레인베이글': '00000265',
            '상품권': '00000266',
            '베이글+아메리카노': '00000267',
            '베이글+카페라떼': '00000268',
            '프레즐+아메리카노': '00000269',
            '프레즐+카페라떼': '00000270',
            '햄치즈 아메리카노': '00000271',
            '햄치즈 카페라떼': '00000273',
            '소시지샌드위치': '00000274',
            '크로와상아메리카노': '00000275',
            '크로와상카페라떼': '00000276',
            '딸기 플랫치노': '00000277',
            '딸기 요거트 플랫치노': '00000278',
            '홍차': '00000280',
            '페퍼민트티': '00000281',
            '탈카페인 보이차': '00000282',
            '캐모마일 레드티': '00000283',
            '작설차': '00000284',
            '밀크티': '00000285',
            '로즈쟈스민티': '00000286',
            'ice 홍차': '00000287',
            'ice 페퍼민트티': '00000288',
            'ice 탈카페인 보이차': '00000289',
            'ice 캐모마일 레드티': '00000290',
            'ice 작설차': '00000291',
            'ice 로즈쟈스민티': '00000292',
            '커피 플랫치노': '00000293',
            '카라멜 플랫치노': '00000294',
            '초코렛 칩 플랫치노': '00000295',
            '자몽플랫치노': '00000296',
            '유자플랫치노': '00000297',
            '블루베리 요거트플랫치노': '00000298',
            '모카 플랫치노': '00000299',
            '망고 플랫치노': '00000300',
            '딸기 플랫치노': '00000301',
            '스트로베리요거트플랫치노': '00000302',
            '녹차 플랫치노': '00000303',
            '녹차 요거트플랫치노': '00000304',
            '복숭아아이스티': '00000305',
            '레몬아이스티': '00000306',
            'ice 화이트초코렛': '00000307',
            'ice 토피넛라떼': '00000308',
            'ice 카라멜 크림 넛 라떼': '00000309',
            'ice 초코렛': '00000310',
            'ice 차이라떼': '00000311',
            'ice 우유': '00000312',
            'ice 시나몬초코렛': '00000313',
            'ice 민트초코렛': '00000314',
            'ice 녹차라떼': '00000315',
            'ice 화이트카라멜모카': '00000316',
            'ice 화이트초코렛모카': '00000317',
            'ice 카푸치노': '00000318',
            'ice 카페모카': '00000319',
            'ice 카페라떼': '00000320',
            'ice 카라멜 카페모카': '00000321',
            'ice 카라멜 마끼아또': '00000322',
            'ice 아메리카노': '00000323',
            'ice 시나몬모카': '00000324',
            'ice 스위트 카라멜 넛 라떼': '00000325',
            'ice 스위트 바닐라 라떼': '00000326',
            'ice 민트모카': '00000327',
            '화이트초코렛': '00000328',
            '토피넛라떼': '00000329',
            '카라멜 크림 넛 라떼': '00000330',
            '초코렛': '00000331',
            '차이라떼': '00000332',
            '우유': '00000333',
            '시나몬초코렛': '00000334',
            '민트초코렛': '00000335',
            '녹차라떼': '00000336',
            '화이트카라멜모카': '00000337',
            '화이트초코렛모카': '00000338',
            '카푸치노': '00000339',
            '카페모카': '00000340',
            '카페라떼': '00000341',
            '카라멜 카페모카': '00000342',
            '카라멜 마끼아또': '00000343',
            '에스프레소 콘파냐': '00000344',
            '에스프레소 마끼아또': '00000345',
            '에스프레소': '00000346',
            '아메리카노': '00000347',
            '시나몬모카': '00000348',
            '스위트 카라멜 넛 라떼': '00000349',
            '바닐라 라떼': '00000350',
            '민트모카': '00000351',
            'S-딸기 요거트 플랫치노': '00000353',
            '샌드위치세트A': '00000354',
            '샌드위치세트B': '00000355',
            '팥빙수': '00000356',
            '파인애플주스': '00000357',
            '토마토주스': '00000358',
            '키위주스': '00000359',
            '키위+바나나': '00000360',
            '이곡 라떼': '00000361',
            '오렌지주스': '00000362',
            '생과일 주스': '00000363',
            '바나나주스': '00000364',
            '딸기주스': '00000365',
            '딸기+바나나': '00000366',
            '푸생샌드위치': '00000367',
            '파인애플+바나나': '00000371',
            '파인애플+바나나': '00000373',
            '파인애플+키위': '00000374',
            '커플요거트(2인용)': '00000377',
            '샌드A + 아메리카노': '00000378',
            '샌드A + 라떼/카푸치노': '00000379',
            '샌드B + 아메리카노': '00000380',
            '샌드B + 라떼/카푸치노': '00000381',
            '크림치즈(포션)': '00000383',
            '천도복숭아': '00000384',
            '샌드위치+아메리카노': '00000385',
            '조각케익+아메리카노': '00000386',
            '치즈케익+아메리카노': '00000387',
            '후로즌 아포카토(초코렛)': '00000388',
            '후로즌 아포카토(카라멜)': '00000389',
            '오렌지에이드(병)': '00000390',
            '블루베리에이드(병)': '00000391',
            '병음료': '00000392',
            '쿠폰': '00000393',
            '베이직머그': '00000394',
            '오토머그': '00000395',
            '피크닉머그': '00000396',
            '토스트': '00000397',
            '필라델피아 라즈베리치즈': '00000398',
            '필라델피아 터틀치즈': '00000399',
            '고구마치즈 스틱케익': '00000400',
            '블루베리치즈 스틱케익': '00000401',
            '녹차치즈 스틱케익': '00000402',
            '파인애플+오렌지': '00000404',
            '샌드위치+아메리카노': '00000405',
            '샌드위치+카페라떼': '00000406',
            '샌드위치+생과일주스': '00000407',
            '클럽샌드위치': '00000408',
            '치킨샌드위치': '00000409',
            '소시지샌드위치+아메리카노': '00000410',
            '투나샌드위치': '00000411',
            '핫도그': '00000412',
            'ice 이곡라떼': '00000413',
            '아포카토': '00000414',
            '아이스크림': '00000415',
            '와플': '00000416',
            '아이스와플': '00000417',
            '보이차': '00000421',
            '페퍼민트': '00000422',
            '캐모마일레드': '00000423',
            '얼그레이': '00000424',
            '작설차': '00000425',
            '로즈쟈스민': '00000426',
            '잉글리쉬블랙퍼스트': '00000427',
            '쵸코 스틱케익': '00000428',
            '참치 샌드위치': '00000430',
            '햄치즈 샌드위치': '00000431',
            '햄치즈 샌드위치': '00000432',
            '휘핑 X': '00000434',
            '생크림 와플': '00000435',
            '메이플 와플': '00000436',
            '크림치즈 와플': '00000437',
            '베리스페셜 와플': '00000438',
            '플레인와플': '00000439',
            '고구마라떼': '00000440',
            'ice 고구마라떼': '00000441',
            '크림치즈베이글': '00000443',
            '크리스마스케익': '00000444',
            '크리스마스케익': '00000445',
            '크리스마스케익-1': '00000446',
            '크리스마스케익': '00000447',
            '머그잔': '00000448',
            '전동그라인더': '00000449',
            '샌드위치+아메리카노': '00000450',
            '샌드위치+카페라떼': '00000451',
            '샌드위치 반쪽': '00000452',
            '꽃잎차': '00000453',
            '고구마라떼': '00000454',
            'ice 고구마라떼': '00000455',
            '생과일쥬스': '00000456',
            '아이스크림와플': '00000457',
            '쵸코렛': '00000460',
            '탄산수': '00000461',
            '상품권': '00000462',
            '에스프레소(400g)': '00000463',
            '젤라또 싱글(콘)': '00000465',
            '젤라또 더블(컵)': '00000466',
            '젤라또 미디움': '00000467',
            '젤라또 딸기쉐이크': '00000468',
            '젤라또빙수(1인)': '00000469',
            '젤라또와플': '00000470',
            '바닐라 아포가토': '00000471',
            '아메리카노': '00000472',
            'ice 아메리카노': '00000473',
            '카페라떼': '00000474',
            'ice 카페라떼': '00000475',
            '카푸치노': '00000476',
            'ice 카푸치노': '00000477',
            '카페모카': '00000478',
            'ice 카페모카': '00000479',
            '쵸콜렛': '00000480',
            'ice 쵸콜렛': '00000481',
            '아이스티-레몬': '00000482',
            '아이스티-복숭아': '00000483',
            '카라멜 마끼아또': '00000484',
            'ice 카라멜 마끼아또': '00000485',
            '녹차라떼': '00000486',
            'ice 녹차라떼': '00000487',
            '젤라또빙수(2인)': '00000488',
            '민트 초코렛 칩 플랫치노': '00000489',
            'S-민트 초콜릿 칩 플랫치노': '00000490',
            '스트로베리 요거트플랫치노': '00000491',
            '미니와플': '00000492',
            '이디야 도서': '00000493',
            '수플레': '00000494',
            '망고에이드': '00000495',
            '레드빈플랫치노': '00000499',
            '고구마플랫치노': '00000500',
            '요거트빙수(3인)': '00000501',
            '딸기': '00000502',
            '커피바리가또': '00000503',
            '티라미수': '00000504',
            '바닐라': '00000505',
            '비스초코': '00000506',
            '그린티': '00000507',
            '뚜또보스코': '00000508',
            '페스카란차': '00000509',
            '요거트': '00000510',
            '젤라또 싱글(컵)': '00000511',
            '젤라또 요거트쉐이크': '00000513',
            '젤라또 녹차쉐이크': '00000514',
            '젤라또 초코쉐이크': '00000515',
            '커피 바리가또': '00000516',
            '티라미슈 아포가토': '00000517',
            '초코 아포가토': '00000518',
            '포켓와플': '00000519',
            '베이직와플': '00000520',
            '블루베리와플': '00000521',
            '아몬드와플': '00000522',
            '스페셜와플': '00000523',
            '와플세트': '00000524',
            '와플패밀리세트': '00000525',
            '팥빙수': '00000526',
            '녹차빙수': '00000527',
            '생크림와플+아메리카노': '00000532',
            '생크림와플+카페라떼': '00000533',
            '베이글+아메리카노(모닝)': '00000534',
            '베이글+카페라떼(모닝)': '00000535',
            '프레즐+아메리카노(모닝)': '00000536',
            '프레즐+카페라떼(모닝)': '00000537',
            '포도주스': '00000538',
            '기타': '00000540',
            '딸기쉐이크': '00000541',
            '쿠키앤바닐라': '00000542',
            '토마토요거트플랫치노': '00000543',
            '젤라또 녹차쉐이크': '00000544',
            '젤라또 딸기쉐이크': '00000545',
            '젤라또 요거트쉐이크': '00000546',
            '젤라또 쵸코쉐이크': '00000547',
            '복숭아아이스티': '00000548',
            '레몬아이스티': '00000549',
            'ice아메리카노': '00000551',
            '아메리카노': '00000552',
            '오늘의음료 ice': '00000560',
            '오늘의음료 hot': '00000563',
            '생과일주스': '00000565',
            '하우스플레인': '00000568',
            '머핀+아메리카노': '00000569',
            '에그샌드위치': '00000570',
            '머핀+카페라떼': '00000571',
            '햄쨈토스트&샐러드': '00000572',
            '햄치즈샌드위치+아메리카노': '00000573',
            '하우스토스트': '00000574',
            '쿠키(set)': '00000575',
            '셋트A/B': '00000576',
            '빙수': '00000577',
            '요거트빙수(4인)': '00000578',
            '팥빙수(中)': '00000579',
            '팥빙수(大)': '00000580',
            '아메리카노 3+1 (런치)': '00000581',
            '조각케익+아메리카노': '00000582',
            '조각케익+카페라떼': '00000583',
            '플레인와플+아메리카노': '00000584',
            '플레인와플+카페라떼': '00000585',
            '딸기 플랫치노': '00000586',
            '유자차': '00000587',
            '쿠즈코초코': '00000588',
            '과일빙수1인': '00000595',
            '과일빙수1인': '00000597',
            '과일빙수2인': '00000598',
            '녹차빙수1인': '00000599',
            '녹차빙수2인': '00000600',
            '커피빙수1인': '00000601',
            '커피빙수2인': '00000602',
            '레몬차(hot/iced)': '00000606',
            '유자차': '00000607',
            '아포가토': '00000608',
            '유자에이드': '00000609',
            '햄&모짜렐라 샌드위치': '00000610',
            '호도베이글': '00000611',
            '쏘야브래드웰빙': '00000612',
            '사과주스': '00000613',
            '8구초콜릿': '00000614',
            '타르트': '00000615',
            '아메리카노SET': '00000616',
            '16구초콜릿': '00000617',
            '젤라또싱글SET': '00000618',
            '젤라또더블SET': '00000619',
            '복숭아아이스티': '00000620',
            '하트초콜릿': '00000621',
            '오가닉초코렛': '00000622',
            'S-ICED 오가닉 초콜릿': '00000623',
            'ice 오가닉초코렛': '00000624',
            'S-오가닉 초콜릿': '00000625',
            '홍시주스': '00000626',
            '쿠키': '00000627',
            '오늘의음료1': '00000628',
            '오늘의음료2': '00000629',
            '오늘의음료3': '00000630',
            '오늘의음료4': '00000631',
            '베이글': '00000632',
            '아메리카노': '00000633',
            '레몬아이스티': '00000634',
            '호두파이': '00000635',
            '토스트': '00000636',
            '쿠키앤넛플랫치노': '00000637',
            '스노우플랫치노': '00000638',
            '블루베리라떼': '00000639',
            '레몬티플랫치노': '00000640',
            '오미자차': '00000641',
            '햄에그샌드위치': '00000642',
            '햄치즈샌드위치': '00000643',
            '베이컨샌드위치': '00000644',
            '치킨샌드위치': '00000645',
            '클럽샌드위치': '00000646',
            '에그쨈샌드위치': '00000647',
            '베이컨+아이스티': '00000648',
            '햄에그+아이스티': '00000650',
            '햄치즈+아이스티': '00000651',
            '젤라또 라지': '00000652',
            '젤라또 미디움': '00000653',
            '그린티모카': '00000654',
            '망고요거트플랫치노': '00000656',
            '오리지널핫번': '00000657',
            '핫치즈번': '00000658',
            '오리지널핫번&아메리카노세트': '00000659',
            '오리지널핫번&카페라떼세트': '00000660',
            '쿠키': '00000661',
            '허니브레드': '00000662',
            '10oz': '00000663',
            '에그샌드위치': '00000665',
            '튜나샌드위치': '00000666',
            '에그샌드위치+아메리카노': '00000667',
            '에그샌드위치+카페라떼': '00000668',
            '베이컨샌드위치+아메리카노': '00000669',
            '베이컨샌드위치+카페라떼': '00000670',
            '튜나샌드위치+아메리카노': '00000671',
            '튜나샌드위치+카페라떼': '00000672',
            '모짜렐라샌드위치+샐러드': '00000673',
            '햄+크림치즈샌드위치': '00000674',
            '모짜렐라샌드위치&샐러드+아메리카노': '00000675',
            '모짜렐라샌드위치&샐러드+카페라떼': '00000676',
            '햄.크림치즈샌드위치+아메리카노': '00000677',
            '햄.크림치즈샌드위치+카페라떼': '00000678',
            '메이플와플+아메리카노': '00000681',
            '메이플 와플+카페라떼': '00000682',
            '생과일 주스': '00000683',
            '솜사탕': '00000684',
            '애플포테이토샌드위치': '00000685',
            'BLT샌드위치': '00000686',
            '쿠폰': '00000687',
            '플레인토스트': '00000688',
            '아이스크림토스트': '00000689',
            '체리주스': '00000690',
            '크림치즈토스트': '00000691',
            'ice 민트초코렛': '00000718',
            'ice 녹차라떼': '00000720',
            '패션프룻오렌지 플랫치노': '00000767',
            '초코칩머핀': '00000784',
            '치즈머핀': '00000785',
            '블루베리머핀': '00000786',
            '로키로드쿠키': '00000787',
            '마카다미아넛쿠키': '00000788',
            '초코청크쿠키': '00000789',
            '오트밀레이즌쿠키': '00000790',
            '아이스아메리카노': '00000791',
            '아이스라떼': '00000792',
            '딸기생크림와플': '00000793',
            '야채클럽샌드위치': '00000794',
            '체리요거트 플랫치노': '00000795',
            '플레인요거트 플랫치노': '00000796',
            '페리에 라임 (병)': '00000797',
            '페리에 레몬 (병)': '00000798',
            '고구마플랫치노': '00000799',
            '체리요거트 플랫치노': '00000800',
            'S-플레인 요거트 플랫치노': '00000801',
            '카푸치노컵': '00000802',
            'ice 이곡라떼': '00000803',
            '야채클럽셋트+아메리카노': '00000804',
            '파니니샌드위치+아메리카노': '00000805',
            '햄에그세트+아이스티': '00000806',
            '베이컨세트+아이스티': '00000807',
            '치킨세트+아이스티': '00000808',
            '클럽샌드위치세트+음료': '00000809',
            '팥빙수 2인': '00000810',
            '팥빙수 3인': '00000811',
            '녹차빙수 小': '00000812',
            '녹차빙수 大': '00000813',
            '팥빙수': '00000814',
            '자몽주스': '00000816',
            '오렌지주스': '00000817',
            '컵빙수': '00000819',
            '수박주스': '00000821',
            '핫치즈번+아메리카노': '00000822',
            '핫치즈번+카페라떼': '00000823',
            '크랩베이글샌드위치': '00000824',
            '튜나베이글샌드위치': '00000825',
            '자몽에이드': '00000826',
            '체리에이드': '00000827',
            '수정과': '00000828',
            '매실차': '00000829',
            '천도복숭아+바나나': '00000830',
            '토마토+바나나': '00000831',
            '자두주스': '00000832',
            '골드키위주스': '00000833',
            '망고주스': '00000834',
            '계란베이컨호밀빵': '00000835',
            '야채': '00000836',
            '단호박쉐이크': '00000837',
            '전복': '00000838',
            '단팥': '00000839',
            '기타': '00000840',
            '상품권': '00000841',
            '메론주스': '00000842',
            '레몬차': '00000843',
            '레몬차': '00000844',
            '마스코타치즈': '00000845',
            '200g 이디야 다크블루': '00000846',
            '200g 이디야 바이올렛': '00000847',
            '200g 이디야 로스트': '00000848',
            '200g 바이올렛 set': '00000849',
            '200g 로스트 set': '00000850',
            '200g 다크블루 set': '00000851',
            '200g 바이올렛+로스트': '00000852',
            '200g 바이올렛+다크블루': '00000853',
            '200g 다크블루+로스트': '00000854',
            '애플 시나몬 브레드': '00000855',
            '허니 카라멜 브레드': '00000856',
            '메이플 넛 브레드': '00000857',
            '과일주스-망고(병)': '00000858',
            '과일주스-오렌지(병)': '00000859',
            '과일주스-복숭아(병)': '00000860',
            '루이보스티 라떼': '00000861',
            '핑크로즈티': '00000862',
            'ice 핑크로즈티': '00000863',
            'S-루이보스 티 라떼': '00000864',
            'S-핑크 로즈 티': '00000865',
            'S-ICED 핑크 로즈 티': '00000866',
            '하리오핸드밀': '00000867',
            '기타': '00000868',
            '핑크로즈티': '00000869',
            '레몬차': '00000870',
            '모과차': '00000871',
            '에스프레소': '00000872',
            '체리': '00000873',
            '로쉐': '00000874',
            '코코넛': '00000875',
            '바나나': '00000876',
            'S-(-)유자차': '00000877',
            '아이스크림컵(바닐라초쿄녹차딸기)': '00000878',
            '아이스크림스틱(바닐라앤밀크쵸코렛쿠키앤크림크런치)': '00000879',
            '크리스피샌드위치(딸기카라멜)': '00000880',
            '팝콘': '00000881',
            '루이보스티': '00000882',
            '칼리타드리퍼(플라스틱)': '00000883',
            '칼리타드리퍼(도자기)': '00000884',
            '여과지': '00000885',
            '핸드드립SET': '00000886',
            '망고': '00000887',
            '눈꽃송이텀블러': '00000888',
            '10주년기념텀블러': '00000889',
            '진동벨1': '00000893',
            '진동벨2': '00000894',
            '진동벨3': '00000895',
            '진동벨4': '00000896',
            '진동벨5': '00000897',
            '진동벨6': '00000898',
            '진동벨7': '00000899',
            '진동벨8': '00000900',
            '진동벨9': '00000901',
            '진동벨10': '00000902',
            '블루베리 주스': '00000903',
            '체리바나나': '00000904',
            '호두바나나': '00000905',
            '체리': '00000906',
            '레몬에이드': '00000907',
            '자몽티': '00000908',
            '진동벨11': '00000909',
            '진동벨12': '00000910',
            '진동벨13': '00000911',
            '진동벨14': '00000912',
            '진동벨15': '00000913',
            '진동벨16': '00000914',
            '진동벨17': '00000915',
            '진동벨18': '00000916',
            '진동벨19': '00000917',
            '진동벨20': '00000918',
            '주방메모': '00000920',
            '자몽에이드': '00000921',
            '레몬에이드': '00000922',
            '석류쥬스': '00000923',
            '고구마플랫치노': '00000926',
            '고구마요거트': '00000927',
            '무료이용권(아메리카노)': '00000928',
            '무료이용권(카페라떼)': '00000929',
            '무료이용권(ice 아메리카노)': '00000930',
            '무료이용권(ice 카페라떼)': '00000931',
            '200g GAIA(10주년)': '00000932',
            '머그잔(10주년)': '00000933',
            '아메리카노+베이글튜나샌드위치': '00000935',
            '카페라떼+베이글튜나샌드위치': '00000936',
            '인삼차': '00000937',
            '레몬차': '00000938',
            '유자차': '00000939',
            '인삼차(ice)': '00000940',
            '레몬차(ice)': '00000941',
            '유자차(ice)': '00000942',
            '민트라떼': '00000943',
            '시나몬라떼': '00000944',
            '쿠폰': '00000945',
            '탄산수': '00000946',
            '초코렛치즈 스틱케익': '00000947',
            'S-루이보스 티': '00000948',
            '(신)아메리카노': '00000949',
            '(신)ice아메리카노': '00000950',
            '아포가토': '00000951',
            '젤라또쉐이크': '00000952',
            '그린애플플랫치노': '00000953',
            '그린애플요거트플랫치노': '00000954',
            'S-그린애플 플랫치노': '00000955',
            'S-그린애플 요거트 플랫치노': '00000956',
            '야채클럽샌드위치세트': '00000957',
            '모닝세트': '00000958',
            '치즈케익': '00000959',
            '시럽추가(바닐라)': '00000960',
            '시럽추가(헤이즐넛)': '00000961',
            '시럽추가(아이리쉬)': '00000962',
            '뉴욕치즈케익': '00000963',
            '블랙포레스트': '00000964',
            '크렘 오 까망베르': '00000965',
            '그랑카카오': '00000966',
            '아메리칸치즈케익': '00000967',
            '블루베리 요거트': '00000968',
            '부산_ice 아메리카노(L)': '00000970',
            '부산_ice 카페라떼(L)': '00000971',
            '부산_아메리카노(행)': '00000972',
            '부산_ice 아메리카노(행)': '00000973',
            '진저마끼아또': '00000974',
            '헤이즐넛라떼': '00000975',
            'ice 진저마끼아또': '00000976',
            'ice 헤이즐넛라떼': '00000977',
            '진저마끼아또': '00000978',
            '헤이즐넛라떼': '00000979',
            'ice 진저마끼아또': '00000980',
            'ice 헤이즐넛라떼': '00000981',
            '허니 카라멜 브레드': '00000982',
            '크리스마스케익-2': '00000983',
            '크리스마스케익-3': '00000984',
            '골든메달 애플주스': '00000985',
            '랭거스-망고': '00000986',
            '랭거스-자몽': '00000987',
            '아메리카노': '00000988',
            'ice아메리카노': '00000989',
            '팟파이(미트)': '00000990',
            '팟파이(베이컨)': '00000991',
            '팟파이(치킨)': '00000992',
            '치아바타(치킨)': '00000993',
            '치아바타(스테이크)': '00000994',
            '진동벨21': '00000995',
            '진동벨22': '00000996',
            '진동벨23': '00000997',
            '진동벨24': '00000998',
            '진동벨25': '00000999',
            '진동벨26': '00001000',
            '진동벨27': '00001001',
            '진동벨28': '00001002',
            '진동벨29': '00001003',
            '진동벨30': '00001004',
            '오렌지카페라떼': '00001005',
            'ice 오렌지카페라떼': '00001006',
            '오렌지미니브레드': '00001007',
            '오렌지 카페라떼': '00001008',
            'ice 오렌지카페라떼': '00001009',
            '컵받침_컵': '00001010',
            '컵받침_열매': '00001011',
            '웨이브스타일': '00001012',
            '웨이브필터': '00001013',
            '베이직텀블러_오크': '00001014',
            '베이직텀블러_블루': '00001015',
            '티포트': '00001016',
            '베이직머그_화이트': '00001017',
            '베이직머그_블랙': '00001018',
            '베이직머그_그레이': '00001019',
            '빈티지머그_브라운': '00001020',
            '빈티지머그_블루': '00001021',
            '바닐라시럽': '00001022',
            '스피어스초코렛_다크': '00001023',
            '스피어스초코렛_밀크': '00001024',
            '젤리텀블러_로즈': '00001025',
            '젤리텀블러_퍼플': '00001026',
            '이디야파우더_바닐라': '00001027',
            '이디야파우더_토피넛': '00001028',
            '이디야파우더_프랄린': '00001029',
            '카라멜시럽': '00001030',
            '핸드밀': '00001031',
            '드립서버': '00001032',
            '웨이브드리퍼': '00001033',
            '캡슐텀블러_화이트': '00001034',
            '캡슐텀블러_그린': '00001035',
            '캡슐텀블러_블루': '00001036',
            '도피오잔': '00001037',
            '(EX)아메리카노': '00001038',
            '(EX)카페라떼': '00001039',
            '(EX)ice 아메리카노': '00001040',
            '(EX)ice 카페라떼': '00001041',
            'S-(EX)아메리카노': '00001042',
            'S-(EX)카페라떼': '00001043',
            'S-(EX)ICED 아메리카노': '00001044',
            'S-(EX)ICED 카페라떼': '00001045',
            '매직팝플랫치노(피스타치오)': '00001046',
            '매직팝플랫치노(체리)': '00001047',
            'S-피스타치오 매직 팝 플랫치노': '00001048',
            'S-체리 매직 팝 플랫치노': '00001049',
            '블루머그컵': '00001050',
            '스트라치아텔라': '00001051',
            '머그잔': '00001052',
            '피지워터': '00001053',
            '모바일 교환권 테스트': '00010000',
            '비니스트 25 마일드 5개입 교환권': '00010001',
            '비니스트 25 오리지널 5개입 교환권': '00010002',
            '아메리카노 교환권': '00010003',
            '체리베리 초코렛': '00010004',
            '체리베리 모카(HOT)': '00010005',
            '체리베리 초코렛(서비스)': '00010006',
            '체리베리 모카(서비스)': '00010007',
            '블루레몬에이드': '00010008',
            '레몬에이드': '00010009',
            '자몽에이드': '00010010',
            '블루레몬에이드(EX)': '00010011',
            '레몬에이드(EX)': '00010012',
            '자몽에이드(EX)': '00010013',
            'S-블루레몬 에이드': '00010014',
            'S-레몬 에이드': '00010015',
            'S-자몽 에이드': '00010016',
            'S-(EX)블루레몬 에이드': '00010017',
            'S-(EX)레몬 에이드': '00010018',
            'S-(EX)자몽 에이드': '00010019',
            'ice 체리베리 초코렛': '00010020',
            'ice 체리베리 초코렛(서비스)': '00010021',
            'ice 체리베리 초코렛 교환권': '00010022',
            '체리베리 초코렛 교환권': '00010023',
            '밀크 버블티': '00010024',
            '초코 버블티': '00010025',
            '카페 버블티': '00010026',
            '타로 버블티': '00010027',
            '토피 넛 버블티': '00010028',
            '밀크 버블티(EXTRA)': '00010029',
            '초코 버블티(EXTRA)': '00010030',
            '카페 버블티(EXTRA)': '00010031',
            '타로 버블티(EXTRA)': '00010032',
            '토피 넛 버블티(EXTRA)': '00010033',
            'S-밀크 버블티': '00010034',
            'S-초코 버블티': '00010035',
            'S-카페 버블티': '00010036',
            'S-타로 버블티': '00010037',
            'S-토피넛 버블티': '00010038',
            'S-(EX)밀크 버블티': '00010039',
            'S-(EX)초코 버블티': '00010040',
            'S-(EX)카페 버블티': '00010041',
            'S-(EX)타로 버블티': '00010042',
            'S-(EX)토피넛 버블티': '00010043',
            '밀크 버블티': '00010044',
            '초코 버블티': '00010045',
            '카페 버블티': '00010046',
            '타로 버블티': '00010047',
            '토피 넛 버블티': '00010048',
            '펄 추가': '00010049',
            'STORY 레드 그레이프': '00010050',
            'STORY 화이트 그레이프': '00010051',
            '섬씽내추럴 스파클링(핑크 그레이프프룻)': '00010052',
            '모바일교환권': '00010053',
            '모바일교환권': '00010054',
            '모바일교환권': '00010055',
            '모바일교환권': '00010056',
            '모바일교환권': '00010057',
            '모바일교환권': '00010058',
            '모바일교환권': '00010059',
            '모바일교환권': '00010060',
            '모바일교환권': '00010061',
            '모바일교환권': '00010062',
            '모바일교환권': '00010063',
            '모바일교환권': '00010064',
            '모바일교환권': '00010065',
            '모바일교환권': '00010066',
            '모바일교환권': '00010067',
            '모바일교환권': '00010068',
            '모바일교환권': '00010069',
            '모바일교환권': '00010070',
            '모바일교환권': '00010071',
            '모바일교환권': '00010072',
            '모바일교환권': '00010073',
            '모바일교환권': '00010074',
            '모바일교환권': '00010075',
            '모바일교환권': '00010076',
            '모바일교환권': '00010077',
            '모바일교환권': '00010078',
            '모바일교환권': '00010079',
            '모바일교환권': '00010080',
            '모바일교환권': '00010081',
            '모바일교환권': '00010082',
            '모바일교환권': '00010083',
            '모바일교환권': '00010084',
            '모바일교환권': '00010085',
            '모바일교환권': '00010086',
            '모바일교환권': '00010087',
            '모바일교환권': '00010088',
            '모바일교환권': '00010089',
            '모바일교환권': '00010090',
            '모바일교환권': '00010091',
            '모바일교환권': '00010092',
            '모바일교환권': '00010093',
            '모바일교환권': '00010094',
            '모바일교환권': '00010095',
            '모바일교환권': '00010096',
            '모바일교환권': '00010097',
            '모바일교환권': '00010098',
            '모바일교환권': '00010099',
            '모바일교환권': '00010100',
            '모바일교환권': '00010101',
            '모바일교환권': '00010102',
            '모바일교환권': '00010103',
            '모바일교환권': '00010104',
            '모바일교환권': '00010105',
            '모바일교환권': '00010106',
            '모바일교환권': '00010107',
            '모바일교환권': '00010108',
            '모바일교환권': '00010109',
            '모바일교환권': '00010110',
            '모바일교환권': '00010111',
            '모바일교환권': '00010112',
            '모바일교환권': '00010113',
            '모바일교환권': '00010114',
            '모바일교환권': '00010115',
            '모바일교환권': '00010116',
            '모바일교환권': '00010117',
            '모바일교환권': '00010118',
            '모바일교환권': '00010119',
            '모바일교환권': '00010120',
            '모바일교환권': '00010121',
            '모바일교환권': '00010122',
            '모바일교환권': '00010123',
            '모바일교환권': '00010124',
            '모바일교환권': '00010125',
            '모바일교환권': '00010126',
            '모바일교환권': '00010127',
            '모바일교환권': '00010128',
            '모바일교환권': '00010129',
            '모바일교환권': '00010130',
            '모바일교환권': '00010131',
            '모바일교환권': '00010132',
            '모바일교환권': '00010133',
            '모바일교환권': '00010134',
            '모바일교환권': '00010135',
            '모바일교환권': '00010136',
            '모바일교환권': '00010137',
            '모바일교환권': '00010138',
            '모바일교환권': '00010139',
            '모바일교환권': '00010140',
            '모바일교환권': '00010141',
            '모바일교환권': '00010142',
            '모바일교환권': '00010143',
            '모바일교환권': '00010144',
            '모바일교환권': '00010145',
            '모바일교환권': '00010146',
            '모바일교환권': '00010147',
            '모바일교환권': '00010148',
            '모바일교환권': '00010149',
            '모바일교환권': '00010150',
            '모바일교환권': '00010151',
            '모바일교환권': '00010152',
            '모바일교환권': '00010153',
            '모바일교환권': '00010154',
            '모바일교환권': '00010155',
            '모바일교환권': '00010156',
            '모바일교환권': '00010157',
            '모바일교환권': '00010158',
            '모바일교환권': '00010159',
            '모바일교환권': '00010160',
            '모바일교환권': '00010161',
            '모바일교환권': '00010162',
            '모바일교환권': '00010163',
            '모바일교환권': '00010164',
            '모바일교환권': '00010165',
            '모바일교환권': '00010166',
            '모바일교환권': '00010167',
            '모바일교환권': '00010168',
            '모바일교환권': '00010169',
            '모바일교환권': '00010170',
            '모바일교환권': '00010171',
            '모바일교환권': '00010172',
            '모바일교환권': '00010173',
            '모바일교환권': '00010174',
            '모바일교환권': '00010175',
            '모바일교환권': '00010176',
            '모바일교환권': '00010177',
            '모바일교환권': '00010178',
            '모바일교환권': '00010179',
            '모바일교환권': '00010180',
            '모바일교환권': '00010181',
            '모바일교환권': '00010182',
            '모바일교환권': '00010183',
            '모바일교환권': '00010184',
            '모바일교환권': '00010185',
            '모바일교환권': '00010186',
            '모바일교환권': '00010187',
            '모바일교환권': '00010188',
            '모바일교환권': '00010189',
            '모바일교환권': '00010190',
            '모바일교환권': '00010191',
            '모바일교환권': '00010192',
            '모바일교환권': '00010193',
            '모바일교환권': '00010194',
            '모바일교환권': '00010195',
            '모바일교환권': '00010196',
            '모바일교환권': '00010197',
            '모바일교환권': '00010198',
            '모바일교환권': '00010199',
            '모바일교환권': '00010200',
            '모바일교환권': '00010201',
            '모바일교환권': '00010202',
            '모바일교환권': '00010203',
            '모바일교환권': '00010204',
            '모바일교환권': '00010205',
            '모바일교환권': '00010206',
            '모바일교환권': '00010207',
            '모바일교환권': '00010208',
            '모바일교환권': '00010209',
            '모바일교환권': '00010210',
            '모바일교환권': '00010211',
            '모바일교환권': '00010212',
            '모바일교환권': '00010213',
            '모바일교환권': '00010214',
            '모바일교환권': '00010215',
            '모바일교환권': '00010216',
            '모바일교환권': '00010217',
            '모바일교환권': '00010218',
            '모바일교환권': '00010219',
            '모바일교환권': '00010220',
            '모바일교환권': '00010221',
            '모바일교환권': '00010222',
            '모바일교환권': '00010223',
            '모바일교환권': '00010224',
            '모바일교환권': '00010225',
            '모바일교환권': '00010226',
            '모바일교환권': '00010227',
            '모바일교환권': '00010228',
            '모바일교환권': '00010229',
            '모바일교환권': '00010230',
            '모바일교환권': '00010231',
            '모바일교환권': '00010232',
            '모바일교환권': '00010233',
            '모바일교환권': '00010234',
            '모바일교환권': '00010235',
            '모바일교환권': '00010236',
            '모바일교환권': '00010237',
            '모바일교환권': '00010238',
            '모바일교환권': '00010239',
            '모바일교환권': '00010240',
            '모바일교환권': '00010241',
            '모바일교환권': '00010242',
            '모바일교환권': '00010243',
            '모바일교환권': '00010244',
            '모바일교환권': '00010245',
            '모바일교환권': '00010246',
            '모바일교환권': '00010247',
            '모바일교환권': '00010248',
            '모바일교환권': '00010249',
            '모바일교환권': '00010250',
            '모바일교환권': '00010251',
            '모바일교환권': '00010252',
            '모바일교환권': '00010253',
            '모바일교환권': '00010254',
            '모바일교환권': '00010255',
            '모바일교환권': '00010256',
            '모바일교환권': '00010257',
            '모바일교환권': '00010258',
            '모바일교환권': '00010259',
            '모바일교환권': '00010260',
            '모바일교환권': '00010261',
            '모바일교환권': '00010262',
            '모바일교환권': '00010263',
            '모바일교환권': '00010264',
            '모바일교환권': '00010265',
            '모바일교환권': '00010266',
            '모바일교환권': '00010267',
            '모바일교환권': '00010268',
            '모바일교환권': '00010269',
            '모바일교환권': '00010270',
            '모바일교환권': '00010271',
            '모바일교환권': '00010272',
            '모바일교환권': '00010273',
            '모바일교환권': '00010274',
            '모바일교환권': '00010275',
            '모바일교환권': '00010276',
            '모바일교환권': '00010277',
            '모바일교환권': '00010278',
            '모바일교환권': '00010279',
            '모바일교환권': '00010280',
            '모바일교환권': '00010281',
            '모바일교환권': '00010282',
            '모바일교환권': '00010283',
            '모바일교환권': '00010284',
            '모바일교환권': '00010285',
            '모바일교환권': '00010286',
            '모바일교환권': '00010287',
            '모바일교환권': '00010288',
            '모바일교환권': '00010289',
            '모바일교환권': '00010290',
            '모바일교환권': '00010291',
            '모바일교환권': '00010292',
            '모바일교환권': '00010293',
            '모바일교환권': '00010294',
            '모바일교환권': '00010295',
            '모바일교환권': '00010296',
            '모바일교환권': '00010297',
            '모바일교환권': '00010298',
            '모바일교환권': '00010299',
            '모바일교환권': '00010300',
            '모바일교환권': '00010301',
            '모바일교환권': '00010302',
            '모바일교환권': '00010303',
            '모바일교환권': '00010304',
            '모바일교환권': '00010305',
            '모바일교환권': '00010306',
            '모바일교환권': '00010307',
            '모바일교환권': '00010308',
            '모바일교환권': '00010309',
            '모바일교환권': '00010310',
            '모바일교환권': '00010311',
            '모바일교환권': '00010312',
            '모바일교환권': '00010313',
            '모바일교환권': '00010314',
            '모바일교환권': '00010315',
            '모바일교환권': '00010316',
            '모바일교환권': '00010317',
            '모바일교환권': '00010318',
            '모바일교환권': '00010319',
            '모바일교환권': '00010320',
            '모바일교환권': '00010321',
            '모바일교환권': '00010322',
            '모바일교환권': '00010323',
            '모바일교환권': '00010324',
            '모바일교환권': '00010325',
            '모바일교환권': '00010326',
            '모바일교환권': '00010327',
            '모바일교환권': '00010328',
            '모바일교환권': '00010329',
            '모바일교환권': '00010330',
            '모바일교환권': '00010331',
            '모바일교환권': '00010332',
            '모바일교환권': '00010333',
            '모바일교환권': '00010334',
            '모바일교환권': '00010335',
            '모바일교환권': '00010336',
            '모바일교환권': '00010337',
            '모바일교환권': '00010338',
            '모바일교환권': '00010339',
            '모바일교환권': '00010340',
            '모바일교환권': '00010341',
            '모바일교환권': '00010342',
            '모바일교환권': '00010343',
            '모바일교환권': '00010344',
            '모바일교환권': '00010345',
            '모바일교환권': '00010346',
            '모바일교환권': '00010347',
            '모바일교환권': '00010348',
            '모바일교환권': '00010349',
            '모바일교환권': '00010350',
            '모바일교환권': '00010351',
            '모바일교환권': '00010352',
            '모바일교환권': '00010353',
            '모바일교환권': '00010354',
            '모바일교환권': '00010355',
            '모바일교환권': '00010356',
            '모바일교환권': '00010357',
            '모바일교환권': '00010358',
            '모바일교환권': '00010359',
            '모바일교환권': '00010360',
            '모바일교환권': '00010361',
            '모바일교환권': '00010362',
            '모바일교환권': '00010363',
            '모바일교환권': '00010364',
            '모바일교환권': '00010365',
            '모바일교환권': '00010366',
            '모바일교환권': '00010367',
            '모바일교환권': '00010368',
            '모바일교환권': '00010369',
            '모바일교환권': '00010370',
            '모바일교환권': '00010371',
            '모바일교환권': '00010372',
            '모바일교환권': '00010373',
            '모바일교환권': '00010374',
            '모바일교환권': '00010375',
            '모바일교환권': '00010376',
            '모바일교환권': '00010377',
            '모바일교환권': '00010378',
            '모바일교환권': '00010379',
            '모바일교환권': '00010380',
            '모바일교환권': '00010381',
            '모바일교환권': '00010382',
            '모바일교환권': '00010383',
            '모바일교환권': '00010384',
            '모바일교환권': '00010385',
            '모바일교환권': '00010386',
            '모바일교환권': '00010387',
            '모바일교환권': '00010388',
            '모바일교환권': '00010389',
            '모바일교환권': '00010390',
            '모바일교환권': '00010391',
            '모바일교환권': '00010392',
            '모바일교환권': '00010393',
            '모바일교환권': '00010394',
            '모바일교환권': '00010395',
            '모바일교환권': '00010396',
            '모바일교환권': '00010397',
            '모바일교환권': '00010398',
            '모바일교환권': '00010399',
            '모바일교환권': '00010400',
            '모바일교환권': '00010401',
            '모바일교환권': '00010402',
            '모바일교환권': '00010403',
            '모바일교환권': '00010404',
            '모바일교환권': '00010405',
            '모바일교환권': '00010406',
            '모바일교환권': '00010407',
            '모바일교환권': '00010408',
            '모바일교환권': '00010409',
            '모바일교환권': '00010410',
            '모바일교환권': '00010411',
            '모바일교환권': '00010412',
            '모바일교환권': '00010413',
            '모바일교환권': '00010414',
            '모바일교환권': '00010415',
            '모바일교환권': '00010416',
            '모바일교환권': '00010417',
            '모바일교환권': '00010418',
            '모바일교환권': '00010419',
            '모바일교환권': '00010420',
            '모바일교환권': '00010421',
            '모바일교환권': '00010422',
            '모바일교환권': '00010423',
            '모바일교환권': '00010424',
            '모바일교환권': '00010425',
            '모바일교환권': '00010426',
            '모바일교환권': '00010427',
            '모바일교환권': '00010428',
            '모바일교환권': '00010429',
            '모바일교환권': '00010430',
            '모바일교환권': '00010431',
            '모바일교환권': '00010432',
            '모바일교환권': '00010433',
            '모바일교환권': '00010434',
            '모바일교환권': '00010435',
            '모바일교환권': '00010436',
            '모바일교환권': '00010437',
            '모바일교환권': '00010438',
            '모바일교환권': '00010439',
            '모바일교환권': '00010440',
            '모바일교환권': '00010441',
            '모바일교환권': '00010442',
            '모바일교환권': '00010443',
            '모바일교환권': '00010444',
            '모바일교환권': '00010445',
            '모바일교환권': '00010446',
            '모바일교환권': '00010447',
            '모바일교환권': '00010448',
            '모바일교환권': '00010449',
            '모바일교환권': '00010450',
            '모바일교환권': '00010451',
            '모바일교환권': '00010452',
            '모바일교환권': '00010453',
            '모바일교환권': '00010454',
            '모바일교환권': '00010455',
            '모바일교환권': '00010456',
            '모바일교환권': '00010457',
            '모바일교환권': '00010458',
            '모바일교환권': '00010459',
            '모바일교환권': '00010460',
            '모바일교환권': '00010461',
            '모바일교환권': '00010462',
            '모바일교환권': '00010463',
            '모바일교환권': '00010464',
            '모바일교환권': '00010465',
            '모바일교환권': '00010466',
            '모바일교환권': '00010467',
            '모바일교환권': '00010468',
            '모바일교환권': '00010469',
            '모바일교환권': '00010470',
            '모바일교환권': '00010471',
            '모바일교환권': '00010472',
            '모바일교환권': '00010473',
            '모바일교환권': '00010474',
            '모바일교환권': '00010475',
            '모바일교환권': '00010476',
            '모바일교환권': '00010477',
            '모바일교환권': '00010478',
            '모바일교환권': '00010479',
            '모바일교환권': '00010480',
            '모바일교환권': '00010481',
            '모바일교환권': '00010482',
            '모바일교환권': '00010483',
            '모바일교환권': '00010484',
            '모바일교환권': '00010485',
            '모바일교환권': '00010486',
            '모바일교환권': '00010487',
            '모바일교환권': '00010488',
            '모바일교환권': '00010489',
            '모바일교환권': '00010490',
            '모바일교환권': '00010491',
            '모바일교환권': '00010492',
            '모바일교환권': '00010493',
            '모바일교환권': '00010494',
            '모바일교환권': '00010495',
            '모바일교환권': '00010496',
            '모바일교환권': '00010497',
            '모바일교환권': '00010498',
            '모바일교환권': '00010499',
            '모바일교환권': '00010500',
            '모바일교환권': '00010501',
            '모바일교환권': '00010502',
            '모바일교환권': '00010503',
            '모바일교환권': '00010504',
            '모바일교환권': '00010505',
            '모바일교환권': '00010506',
            '모바일교환권': '00010507',
            '모바일교환권': '00010508',
            '모바일교환권': '00010509',
            '모바일교환권': '00010510',
            '모바일교환권': '00010511',
            '모바일교환권': '00010512',
            '모바일교환권': '00010513',
            '모바일교환권': '00010514',
            '모바일교환권': '00010515',
            '모바일교환권': '00010516',
            '모바일교환권': '00010517',
            '모바일교환권': '00010518',
            '모바일교환권': '00010519',
            '모바일교환권': '00010520',
            '모바일교환권': '00010521',
            '모바일교환권': '00010522',
            '모바일교환권': '00010523',
            '모바일교환권': '00010524',
            '모바일교환권': '00010525',
            '모바일교환권': '00010526',
            '모바일교환권': '00010527',
            '모바일교환권': '00010528',
            '모바일교환권': '00010529',
            '모바일교환권': '00010530',
            '모바일교환권': '00010531',
            '모바일교환권': '00010532',
            '모바일교환권': '00010533',
            '모바일교환권': '00010534',
            '모바일교환권': '00010535',
            '모바일교환권': '00010536',
            '모바일교환권': '00010537',
            '모바일교환권': '00010538',
            '모바일교환권': '00010539',
            '모바일교환권': '00010540',
            '모바일교환권': '00010541',
            '모바일교환권': '00010542',
            '모바일교환권': '00010543',
            '모바일교환권': '00010544',
            '모바일교환권': '00010545',
            '모바일교환권': '00010546',
            '모바일교환권': '00010547',
            '모바일교환권': '00010548',
            '모바일교환권': '00010549',
            '모바일교환권': '00010550',
            '모바일교환권': '00010551',
            '모바일교환권': '00010552',
            '모바일교환권': '00010553',
            '모바일교환권': '00010554',
            '모바일교환권': '00010555',
            '모바일교환권': '00010556',
            '모바일교환권': '00010557',
            '모바일교환권': '00010558',
            '모바일교환권': '00010559',
            '모바일교환권': '00010560',
            '모바일교환권': '00010561',
            '모바일교환권': '00010562',
            '모바일교환권': '00010563',
            '모바일교환권': '00010564',
            '모바일교환권': '00010565',
            '모바일교환권': '00010566',
            '모바일교환권': '00010567',
            '모바일교환권': '00010568',
            '모바일교환권': '00010569',
            '모바일교환권': '00010570',
            '모바일교환권': '00010571',
            '모바일교환권': '00010572',
            '모바일교환권': '00010573',
            '모바일교환권': '00010574',
            '모바일교환권': '00010575',
            '모바일교환권': '00010576',
            '모바일교환권': '00010577',
            '모바일교환권': '00010578',
            '모바일교환권': '00010579',
            '모바일교환권': '00010580',
            '모바일교환권': '00010581',
            '모바일교환권': '00010582',
            '모바일교환권': '00010583',
            '모바일교환권': '00010584',
            '모바일교환권': '00010585',
            '모바일교환권': '00010586',
            '모바일교환권': '00010587',
            '모바일교환권': '00010588',
            '모바일교환권': '00010589',
            '모바일교환권': '00010590',
            '모바일교환권': '00010591',
            '모바일교환권': '00010592',
            '모바일교환권': '00010593',
            '모바일교환권': '00010594',
            '모바일교환권': '00010595',
            '모바일교환권': '00010596',
            '모바일교환권': '00010597',
            '모바일교환권': '00010598',
            '모바일교환권': '00010599',
            '모바일교환권': '00010600',
            '모바일교환권': '00010601',
            '모바일교환권': '00010602',
            '모바일교환권': '00010603',
            '모바일교환권': '00010604',
            '모바일교환권': '00010605',
            '모바일교환권': '00010606',
            '모바일교환권': '00010607',
            '모바일교환권': '00010608',
            '모바일교환권': '00010609',
            '모바일교환권': '00010610',
            '모바일교환권': '00010611',
            '모바일교환권': '00010612',
            '모바일교환권': '00010613',
            '모바일교환권': '00010614',
            '모바일교환권': '00010615',
            '모바일교환권': '00010616',
            '모바일교환권': '00010617',
            '모바일교환권': '00010618',
            '모바일교환권': '00010619',
            '모바일교환권': '00010620',
            '모바일교환권': '00010621',
            '모바일교환권': '00010622',
            '모바일교환권': '00010623',
            '모바일교환권': '00010624',
            '모바일교환권': '00010625',
            '모바일교환권': '00010626',
            '모바일교환권': '00010627',
            '모바일교환권': '00010628',
            '모바일교환권': '00010629',
            '모바일교환권': '00010630',
            '모바일교환권': '00010631',
            '모바일교환권': '00010632',
            '모바일교환권': '00010633',
            '모바일교환권': '00010634',
            '모바일교환권': '00010635',
            '모바일교환권': '00010636',
            '모바일교환권': '00010637',
            '모바일교환권': '00010638',
            '모바일교환권': '00010639',
            '모바일교환권': '00010640',
            '모바일교환권': '00010641',
            '모바일교환권': '00010642',
            '모바일교환권': '00010643',
            '모바일교환권': '00010644',
            '모바일교환권': '00010645',
            '모바일교환권': '00010646',
            '모바일교환권': '00010647',
            '모바일교환권': '00010648',
            '모바일교환권': '00010649',
            '모바일교환권': '00010650',
            '모바일교환권': '00010651',
            '모바일교환권': '00010652',
            '모바일교환권': '00010653',
            '모바일교환권': '00010654',
            '모바일교환권': '00010655',
            '모바일교환권': '00010656',
            '모바일교환권': '00010657',
            '모바일교환권': '00010658',
            '모바일교환권': '00010659',
            '모바일교환권': '00010660',
            '모바일교환권': '00010661',
            '모바일교환권': '00010662',
            '모바일교환권': '00010663',
            '모바일교환권': '00010664',
            '모바일교환권': '00010665',
            '모바일교환권': '00010666',
            '모바일교환권': '00010667',
            '모바일교환권': '00010668',
            '모바일교환권': '00010669',
            '모바일교환권': '00010670',
            '모바일교환권': '00010671',
            '모바일교환권': '00010672',
            '모바일교환권': '00010673',
            '모바일교환권': '00010674',
            '모바일교환권': '00010675',
            '모바일교환권': '00010676',
            '모바일교환권': '00010677',
            '모바일교환권': '00010678',
            '모바일교환권': '00010679',
            '모바일교환권': '00010680',
            '모바일교환권': '00010681',
            '모바일교환권': '00010682',
            '모바일교환권': '00010683',
            '모바일교환권': '00010684',
            '모바일교환권': '00010685',
            '모바일교환권': '00010686',
            '모바일교환권': '00010687',
            '모바일교환권': '00010688',
            '모바일교환권': '00010689',
            '모바일교환권': '00010690',
            '모바일교환권': '00010691',
            '모바일교환권': '00010692',
            '모바일교환권': '00010693',
            '모바일교환권': '00010694',
            '모바일교환권': '00010695',
            '모바일교환권': '00010696',
            '모바일교환권': '00010697',
            '모바일교환권': '00010698',
            '모바일교환권': '00010699',
            '모바일교환권': '00010700',
            '모바일교환권': '00010701',
            '모바일교환권': '00010702',
            '모바일교환권': '00010703',
            '모바일교환권': '00010704',
            '모바일교환권': '00010705',
            '모바일교환권': '00010706',
            '모바일교환권': '00010707',
            '모바일교환권': '00010708',
            '모바일교환권': '00010709',
            '모바일교환권': '00010710',
            '모바일교환권': '00010711',
            '모바일교환권': '00010712',
            '모바일교환권': '00010713',
            '모바일교환권': '00010714',
            '모바일교환권': '00010715',
            '모바일교환권': '00010716',
            '모바일교환권': '00010717',
            '모바일교환권': '00010718',
            '모바일교환권': '00010719',
            '모바일교환권': '00010720',
            '모바일교환권': '00010721',
            '모바일교환권': '00010722',
            '모바일교환권': '00010723',
            '모바일교환권': '00010724',
            '모바일교환권': '00010725',
            '모바일교환권': '00010726',
            '모바일교환권': '00010727',
            '모바일교환권': '00010728',
            '모바일교환권': '00010729',
            '모바일교환권': '00010730',
            '모바일교환권': '00010731',
            '모바일교환권': '00010732',
            '모바일교환권': '00010733',
            '모바일교환권': '00010734',
            '모바일교환권': '00010735',
            '모바일교환권': '00010736',
            '모바일교환권': '00010737',
            '모바일교환권': '00010738',
            '모바일교환권': '00010739',
            '모바일교환권': '00010740',
            '모바일교환권': '00010741',
            '모바일교환권': '00010742',
            '모바일교환권': '00010743',
            '모바일교환권': '00010744',
            '모바일교환권': '00010745',
            '모바일교환권': '00010746',
            '모바일교환권': '00010747',
            '모바일교환권': '00010748',
            '모바일교환권': '00010749',
            '모바일교환권': '00010750',
            '모바일교환권': '00010751',
            '모바일교환권': '00010752',
            '모바일교환권': '00010753',
            '모바일교환권': '00010754',
            '모바일교환권': '00010755',
            '모바일교환권': '00010756',
            '모바일교환권': '00010757',
            '모바일교환권': '00010758',
            '모바일교환권': '00010759',
            '모바일교환권': '00010760',
            '모바일교환권': '00010761',
            '모바일교환권': '00010762',
            '모바일교환권': '00010763',
            '모바일교환권': '00010764',
            '모바일교환권': '00010765',
            '모바일교환권': '00010766',
            '모바일교환권': '00010767',
            '모바일교환권': '00010768',
            '모바일교환권': '00010769',
            '모바일교환권': '00010770',
            '모바일교환권': '00010771',
            '모바일교환권': '00010772',
            '모바일교환권': '00010773',
            '모바일교환권': '00010774',
            '모바일교환권': '00010775',
            '모바일교환권': '00010776',
            '모바일교환권': '00010777',
            '모바일교환권': '00010778',
            '모바일교환권': '00010779',
            '모바일교환권': '00010780',
            '모바일교환권': '00010781',
            '모바일교환권': '00010782',
            '모바일교환권': '00010783',
            '모바일교환권': '00010784',
            '모바일교환권': '00010785',
            '모바일교환권': '00010786',
            '모바일교환권': '00010787',
            '모바일교환권': '00010788',
            '모바일교환권': '00010789',
            '모바일교환권': '00010790',
            '모바일교환권': '00010791',
            '모바일교환권': '00010792',
            '모바일교환권': '00010793',
            '모바일교환권': '00010794',
            '모바일교환권': '00010795',
            '모바일교환권': '00010796',
            '모바일교환권': '00010797',
            '모바일교환권': '00010798',
            '모바일교환권': '00010799',
            '모바일교환권': '00010800',
            '모바일교환권': '00010801',
            '모바일교환권': '00010802',
            '모바일교환권': '00010803',
            '모바일교환권': '00010804',
            '모바일교환권': '00010805',
            '모바일교환권': '00010806',
            '모바일교환권': '00010807',
            '모바일교환권': '00010808',
            '모바일교환권': '00010809',
            '모바일교환권': '00010810',
            '모바일교환권': '00010811',
            '모바일교환권': '00010812',
            '모바일교환권': '00010813',
            '모바일교환권': '00010814',
            '모바일교환권': '00010815',
            '모바일교환권': '00010816',
            '모바일교환권': '00010817',
            '모바일교환권': '00010818',
            '모바일교환권': '00010819',
            '모바일교환권': '00010820',
            '모바일교환권': '00010821',
            '모바일교환권': '00010822',
            '모바일교환권': '00010823',
            '모바일교환권': '00010824',
            '모바일교환권': '00010825',
            '모바일교환권': '00010826',
            '모바일교환권': '00010827',
            '모바일교환권': '00010828',
            '모바일교환권': '00010829',
            '모바일교환권': '00010830',
            '모바일교환권': '00010831',
            '모바일교환권': '00010832',
            '모바일교환권': '00010833',
            '모바일교환권': '00010834',
            '모바일교환권': '00010835',
            '모바일교환권': '00010836',
            '모바일교환권': '00010837',
            '모바일교환권': '00010838',
            '모바일교환권': '00010839',
            '모바일교환권': '00010840',
            '모바일교환권': '00010841',
            '모바일교환권': '00010842',
            '모바일교환권': '00010843',
            '모바일교환권': '00010844',
            '모바일교환권': '00010845',
            '모바일교환권': '00010846',
            '모바일교환권': '00010847',
            '모바일교환권': '00010848',
            '모바일교환권': '00010849',
            '모바일교환권': '00010850',
            '모바일교환권': '00010851',
            '모바일교환권': '00010852',
            '모바일교환권': '00010853',
            '모바일교환권': '00010854',
            '모바일교환권': '00010855',
            '모바일교환권': '00010856',
            '모바일교환권': '00010857',
            '모바일교환권': '00010858',
            '모바일교환권': '00010859',
            '모바일교환권': '00010860',
            '모바일교환권': '00010861',
            '모바일교환권': '00010862',
            '모바일교환권': '00010863',
            '모바일교환권': '00010864',
            '모바일교환권': '00010865',
            '모바일교환권': '00010866',
            '모바일교환권': '00010867',
            '모바일교환권': '00010868',
            '모바일교환권': '00010869',
            '모바일교환권': '00010870',
            '모바일교환권': '00010871',
            '모바일교환권': '00010872',
            '모바일교환권': '00010873',
            '모바일교환권': '00010874',
            '모바일교환권': '00010875',
            '모바일교환권': '00010876',
            '모바일교환권': '00010877',
            '모바일교환권': '00010878',
            '모바일교환권': '00010879',
            '모바일교환권': '00010880',
            '모바일교환권': '00010881',
            '모바일교환권': '00010882',
            '모바일교환권': '00010883',
            '모바일교환권': '00010884',
            '모바일교환권': '00010885',
            '모바일교환권': '00010886',
            '모바일교환권': '00010887',
            '모바일교환권': '00010888',
            '모바일교환권': '00010889',
            '모바일교환권': '00010890',
            '모바일교환권': '00010891',
            '모바일교환권': '00010892',
            '모바일교환권': '00010893',
            '모바일교환권': '00010894',
            '모바일교환권': '00010895',
            '모바일교환권': '00010896',
            '모바일교환권': '00010897',
            '모바일교환권': '00010898',
            '모바일교환권': '00010899',
            '모바일교환권': '00010900',
            '모바일교환권': '00010901',
            '모바일교환권': '00010902',
            '모바일교환권': '00010903',
            '모바일교환권': '00010904',
            '모바일교환권': '00010905',
            '모바일교환권': '00010906',
            '모바일교환권': '00010907',
            '모바일교환권': '00010908',
            '모바일교환권': '00010909',
            '모바일교환권': '00010910',
            '모바일교환권': '00010911',
            '모바일교환권': '00010912',
            '모바일교환권': '00010913',
            '모바일교환권': '00010914',
            '모바일교환권': '00010915',
            '모바일교환권': '00010916',
            '모바일교환권': '00010917',
            '모바일교환권': '00010918',
            '모바일교환권': '00010919',
            '모바일교환권': '00010920',
            '모바일교환권': '00010921',
            '모바일교환권': '00010922',
            '모바일교환권': '00010923',
            '모바일교환권': '00010924',
            '모바일교환권': '00010925',
            '모바일교환권': '00010926',
            '모바일교환권': '00010927',
            '모바일교환권': '00010928',
            '모바일교환권': '00010929',
            '모바일교환권': '00010930',
            '모바일교환권': '00010931',
            '모바일교환권': '00010932',
            '모바일교환권': '00010933',
            '모바일교환권': '00010934',
            '모바일교환권': '00010935',
            '모바일교환권': '00010936',
            '모바일교환권': '00010937',
            '모바일교환권': '00010938',
            '모바일교환권': '00010939',
            '모바일교환권': '00010940',
            '모바일교환권': '00010941',
            '모바일교환권': '00010942',
            '모바일교환권': '00010943',
            '모바일교환권': '00010944',
            '모바일교환권': '00010945',
            '모바일교환권': '00010946',
            '모바일교환권': '00010947',
            '모바일교환권': '00010948',
            '모바일교환권': '00010949',
            '모바일교환권': '00010950',
            '모바일교환권': '00010951',
            '모바일교환권': '00010952',
            '모바일교환권': '00010953',
            '모바일교환권': '00010954',
            '모바일교환권': '00010955',
            '모바일교환권': '00010956',
            '모바일교환권': '00010957',
            '모바일교환권': '00010958',
            '모바일교환권': '00010959',
            '모바일교환권': '00010960',
            '모바일교환권': '00010961',
            '모바일교환권': '00010962',
            '모바일교환권': '00010963',
            '모바일교환권': '00010964',
            '모바일교환권': '00010965',
            '모바일교환권': '00010966',
            '모바일교환권': '00010967',
            '모바일교환권': '00010968',
            '모바일교환권': '00010969',
            '모바일교환권': '00010970',
            '모바일교환권': '00010971',
            '모바일교환권': '00010972',
            '모바일교환권': '00010973',
            '모바일교환권': '00010974',
            '모바일교환권': '00010975',
            '모바일교환권': '00010976',
            '모바일교환권': '00010977',
            '모바일교환권': '00010978',
            '모바일교환권': '00010979',
            '모바일교환권': '00010980',
            '모바일교환권': '00010981',
            '모바일교환권': '00010982',
            '모바일교환권': '00010983',
            '모바일교환권': '00010984',
            '모바일교환권': '00010985',
            '모바일교환권': '00010986',
            '모바일교환권': '00010987',
            '모바일교환권': '00010988',
            '모바일교환권': '00010989',
            '모바일교환권': '00010990',
            '모바일교환권': '00010991',
            '모바일교환권': '00010992',
            '모바일교환권': '00010993',
            '모바일교환권': '00010994',
            '모바일교환권': '00010995',
            '모바일교환권': '00010996',
            '모바일교환권': '00010997',
            '모바일교환권': '00010998',
            '모바일교환권': '00010999',
            '모바일교환권': '00011000',
            '비니스트 25 마일드 10개입': '00011001',
            '비니스트 25 마일드 5개입': '00011002',
            '비니스트 25 마일드 밸류팩': '00011003',
            '비니스트 25 오리지널 10개입': '00011004',
            '비니스트 25 오리지널 5개입': '00011005',
            '비니스트 25 오리지널 밸류팩': '00011006',
            '비니스트 25 마일드 점보팩': '00011007',
            '비니스트 25 오리지널 점보팩': '00011008',
            '리얼시리얼라떼': '00011009',
            '블랙시리얼라떼': '00011010',
            '리얼시리얼플랫치노': '00011011',
            '블랙시리얼플랫치노': '00011012',
            'S-리얼 시리얼 라떼': '00011013',
            'S-블랙 시리얼 라떼': '00011014',
            'S-리얼 시리얼 플랫치노': '00011015',
            'S-블랙 시리얼 플랫치노': '00011016',
            '홍시주스': '00011017',
            '골드키위주스': '00011018',
            '체리베리 모카(HOT)': '00011019',
            '체리베리 초코렛(HOT)': '00011020',
            '체리베리 모카(서비스)': '00011021',
            '체리베리 초코렛(서비스)': '00011022',
            '스텐텀블러_블루': '00011023',
            '스텐텀블러_화이트': '00011024',
            '비니스트25(시럽세트_오리지널)': '00011025',
            '비니스트25(시럽세트_마일드)': '00011026',
            '비니스트25(교환권세트_오리지널)': '00011027',
            '비니스트25(교환권세트_마일드)': '00011028',
            '비니스트25(다이어리세트_오리지널)': '00011029',
            '비니스트25(다이어리세트_마일드)': '00011030',
            '비니스트25(블루머그세트)': '00011031',
            '리얼 팥빙수': '00011032',
            '리얼 녹차빙수': '00011033',
            '치킨 샌드위치세트A': '00011034',
            '치킨 샌드위치세트L': '00011035',
            '치즈 샌드위치세트A': '00011036',
            '치즈 샌드위치세트L': '00011037',
            '비니스트25 세라믹머그세트': '00011038',
            '비니스트25 미니팟 텀블러세트': '00011039',
            '크로크 무슈': '00011040',
            '시나몬 롤': '00011041',
            '(EX)ice 카라멜마끼아또': '00011042',
            '(EX)ice 카페모카': '00011043',
            '(EX)복숭아아이스티': '00011044',
            '(EX)레몬아이스티': '00011045',
            'S-(EX)ICED 카라멜 마끼아또': '00011046',
            'S-(EX)ICED 카페모카': '00011047',
            'S-(EX)복숭아 아이스티': '00011048',
            'S-(EX)레몬 아이스티': '00011049',
            '팥 (추가)': '00011050',
            '떡 (추가)': '00011051',
            '비니스트25 아이스 쿨 텀블러 세트': '00011052',
            '아이스 쿨 텀블러': '00011053',
            '비니스트25 추석 선물세트': '00011054',
            '비니스트25 추석 선물세트(B)': '00011055',
            'ice 루이보스티 라떼': '00011056',
            'ice 밀크티': '00011057',
            'S-ICED 루이보스 티 라떼': '00011058',
            'S-ICED 밀크티': '00011059',
            '비니스트25 설 선물세트': '00011060',
            '모바일교환권': '00011082',
            '모바일교환권': '00011083',
            '모바일교환권': '00011084',
            '모바일교환권': '00011085',
            '모바일교환권': '00011086',
            '딸기주스': '00011087',
            '비니스트 미니 오리지널 6개입': '00011088',
            '비니스트 미니 마일드 6개입': '00011089',
            '비니스트 미니 오리지널 15개입': '00011090',
            '비니스트 미니 마일드 15개입': '00011091',
            '비니스트 미니 오리지널 50개입': '00011092',
            '비니스트 미니 마일드 50개입': '00011093',
            '애플망고빙수': '00011094',
            '베리베리빙수': '00011095',
            '민트초코빙수': '00011096',
            '팥빙수': '00011097',
            '녹차빙수': '00011098',
            '이디야 데일리 너츠': '00011099',
            '진동벨1': '00011100',
            '진동벨2': '00011101',
            '진동벨3': '00011102',
            '진동벨4': '00011103',
            '진동벨5': '00011104',
            '진동벨6': '00011105',
            '진동벨7': '00011106',
            '진동벨8': '00011107',
            '진동벨9': '00011108',
            '진동벨10': '00011109',
            '진동벨11': '00011110',
            '진동벨12': '00011111',
            '진동벨13': '00011112',
            '진동벨14': '00011113',
            '진동벨15': '00011114',
            '진동벨16': '00011115',
            '진동벨17': '00011116',
            '진동벨18': '00011117',
            '진동벨19': '00011118',
            '진동벨20': '00011119',
            '진동벨21': '00011120',
            '진동벨22': '00011121',
            '진동벨23': '00011122',
            '진동벨24': '00011123',
            '진동벨25': '00011124',
            '진동벨26': '00011125',
            '진동벨27': '00011126',
            '진동벨28': '00011127',
            '진동벨29': '00011128',
            '진동벨30': '00011129',
            '아메리카노': '00011130',
            '카페라떼': '00011131',
            '카푸치노': '00011132',
            '카페모카': '00011133',
            '카라멜 카페모카': '00011134',
            '카라멜 마끼아또': '00011135',
            '화이트 초콜릿 모카': '00011136',
            '화이트 카라멜 모카': '00011137',
            '민트 모카': '00011138',
            '시나몬 모카': '00011139',
            '바닐라 라떼': '00011140',
            '스위트 카라멜넛 라떼': '00011141',
            '오렌지 카페라떼': '00011142',
            '(EX)아메리카노': '00011143',
            '(EX)카페라떼': '00011144',
            '에스프레소': '00011145',
            '에스프레소 마끼아또': '00011146',
            '에스프레소 콘파냐': '00011147',
            'ICED 아메리카노': '00011148',
            'ICED 카페라떼': '00011149',
            'ICED 카푸치노': '00011150',
            'ICED 카페모카': '00011151',
            'ICED 카라멜 카페모카': '00011152',
            'ICED 카라멜 마끼아또': '00011153',
            'ICED 화이트 초콜릿 모카': '00011154',
            'ICED 화이트 카라멜 모카': '00011155',
            'ICED 민트 모카': '00011156',
            'ICED 시나몬 모카': '00011157',
            'ICED 바닐라 라떼': '00011158',
            'ICED 스위트 카라멜넛 라떼': '00011159',
            'ICED 오렌지 카페라떼': '00011160',
            '(EX)ICED 아메리카노': '00011161',
            '(EX)ICED 카페라떼': '00011162',
            '(EX)ICED 카페모카': '00011163',
            '(EX)ICED 카라멜 마끼아또': '00011164',
            '우유': '00011165',
            '초콜릿': '00011166',
            '화이트 초콜릿': '00011167',
            '민트 초콜릿': '00011168',
            '시나몬 초콜릿': '00011169',
            '오가닉 초콜릿': '00011170',
            '녹차 라떼': '00011171',
            '차이티 라떼': '00011172',
            '토피넛 라떼': '00011173',
            '카라멜 크림넛 라떼': '00011174',
            '리얼 시리얼 라떼': '00011175',
            '블랙 시리얼 라떼': '00011176',
            '체리베리 초콜릿': '00011177',
            'ICED 우유': '00011178',
            'ICED 초콜릿': '00011179',
            'ICED 화이트 초콜릿': '00011180',
            'ICED 민트 초콜릿': '00011181',
            'ICED 시나몬 초콜릿': '00011182',
            'ICED 오가닉 초콜릿': '00011183',
            'ICED 녹차 라떼': '00011184',
            'ICED 차이티 라떼': '00011185',
            'ICED 토피넛 라떼': '00011186',
            'ICED 카라멜 크림넛 라떼': '00011187',
            '복숭아 아이스티': '00011188',
            '레몬 아이스티': '00011189',
            'ICED 체리베리 초콜릿': '00011190',
            '(EX)복숭아 아이스티': '00011191',
            '(EX)레몬 아이스티': '00011192',
            '망고 플랫치노': '00011193',
            '자몽 플랫치노': '00011194',
            '유자 플랫치노': '00011195',
            '커피 플랫치노': '00011196',
            '모카 플랫치노': '00011197',
            '카라멜 플랫치노': '00011198',
            '녹차 플랫치노': '00011199',
            '그린애플 플랫치노': '00011200',
            '초콜릿 칩 플랫치노': '00011201',
            '민트 초콜릿 칩 플랫치노': '00011202',
            '리얼 시리얼 플랫치노': '00011203',
            '블랙 시리얼 플랫치노': '00011204',
            '피스타치오 매직 팝 플랫치노': '00011205',
            '체리 매직 팝 플랫치노': '00011206',
            '블루베리 요거트 플랫치노': '00011207',
            '딸기 요거트 플랫치노': '00011208',
            '플레인 요거트 플랫치노': '00011209',
            '녹차 요거트 플랫치노': '00011210',
            '그린애플 요거트 플랫치노': '00011211',
            '얼그레이홍차(블랙티)': '00011212',
            '로즈 쟈스민 티': '00011213',
            '보이차(디카페인)': '00011214',
            '어린잎녹차(작설차)': '00011215',
            '페퍼민트 티': '00011216',
            '캐모마일 레드 티': '00011217',
            '핑크 로즈 티': '00011218',
            '루이보스 티': '00011219',
            '밀크티': '00011220',
            '루이보스 티 라떼': '00011221',
            'ICED 얼그레이홍차(블랙티)': '00011222',
            'ICED 로즈 쟈스민 티': '00011223',
            'ICED 보이차(디카페인)': '00011224',
            'ICED 어린잎녹차(작설차)': '00011225',
            'ICED 페퍼민트 티': '00011226',
            'ICED 캐모마일 레드 티': '00011227',
            'ICED 핑크 로즈 티': '00011228',
            'ICED 루이보스 티': '00011229',
            'ICED 밀크티': '00011230',
            'ICED 루이보스 티 라떼': '00011231',
            '자몽 에이드': '00011232',
            '레몬 에이드': '00011233',
            '블루레몬 에이드': '00011234',
            '(EX)자몽 에이드': '00011235',
            '(EX)레몬 에이드': '00011236',
            '(EX)블루레몬 에이드': '00011237',
            '밀크 버블티': '00011238',
            '타로 버블티': '00011239',
            '초코 버블티': '00011240',
            '카페 버블티': '00011241',
            '토피넛 버블티': '00011242',
            '(EX)밀크 버블티': '00011243',
            '(EX)타로 버블티': '00011244',
            '(EX)초코 버블티': '00011245',
            '(EX)카페 버블티': '00011246',
            '(EX)토피넛 버블티': '00011247',
            '+(버블티)펄': '00011248',
            '개인컵 할인 (-100원)': '00011249',
            '세트메뉴 할인 (-500원)': '00011250',
            '+에스프레소샷': '00011251',
            '+휘핑 크림': '00011252',
            '+(시럽)카라멜': '00011253',
            '+(시럽)바닐라': '00011254',
            '+(시럽)헤이즐넛': '00011255',
            '고구마 라떼': '00011256',
            'ICED 고구마 라떼': '00011257',
            '홍시주스': '00011258',
            '골드키위주스': '00011259',
            '딸기주스': '00011260',
            '레드빈 플랫치노': '00011261',
            '(병)골드메달 애플': '00011262',
            '(병)랭거스 망고': '00011263',
            '(병)랭거스 자몽': '00011264',
            '(병)로리나 핑크레몬에이드': '00011265',
            '(병)로리나 레몬에이드': '00011266',
            '(병)로리나 오렌지에이드': '00011267',
            '(병)피지워터': '00011268',
            '(병)페리에 라임': '00011269',
            '(병)페리에 레몬': '00011270',
            '(병)섬씽내츄럴 스파클링': '00011271',
            '(병)스토리 레드': '00011272',
            '(병)스토리 화이트': '00011273',
            '팥빙수': '00011274',
            '녹차빙수': '00011275',
            '애플망고빙수': '00011276',
            '베리베리빙수': '00011277',
            '민트초코빙수': '00011278',
            '+(빙수)팥': '00011279',
            '+(빙수)떡': '00011280',
            '(브레드)허니카라멜': '00011281',
            '(브레드)메이플넛': '00011282',
            '(브레드)애플시나몬': '00011283',
            '(브레드)오렌지 미니': '00011284',
            '(와플)크림치즈': '00011285',
            '(와플)플레인': '00011286',
            '(와플)생크림': '00011287',
            '(와플)메이플': '00011288',
            '프레즐': '00011289',
            '(번)오리지널': '00011290',
            '(번)치즈': '00011291',
            '(베이글)플레인': '00011292',
            '(베이글)블루베리': '00011293',
            '(베이글)어니언': '00011294',
            '(베이글)시나몬&레이즌': '00011295',
            '(치아바타)치킨': '00011296',
            '(치아바타)불고기': '00011297',
            '크로크 무슈': '00011298',
            '시나몬 롤': '00011299',
            '(팟파이)치킨': '00011300',
            '(팟파이)베이컨': '00011301',
            '(팟파이)미트': '00011302',
            '크림치즈(포션)': '00011303',
            '(쿠키)초콜릿칩청크': '00011304',
            '(쿠키)마카다미아넛': '00011305',
            '(쿠키)로키로드': '00011306',
            '(쿠키)오트밀레이즌': '00011307',
            '(스틱케익)플레인': '00011308',
            '(스틱케익)초콜릿': '00011309',
            '(스틱케익)고구마': '00011310',
            '(스틱케익)블루베리': '00011311',
            '(머핀)초코칩': '00011312',
            '(머핀)치즈': '00011313',
            '(머핀)블루베리': '00011314',
            '(조각케익)초코티라미스': '00011315',
            '(조각케익)수플레': '00011316',
            '(조각케익)필라델피아치즈': '00011317',
            '(조각케익)초코무스': '00011318',
            '(조각케익)티라미스': '00011319',
            '(조각케익)아메리칸 치즈': '00011320',
            '(조각케익)블루베리요거트': '00011321',
            '(조각케익)고구마': '00011322',
            '이디야 이너츠': '00011323',
            '초콜릿 마카롱(시즌)': '00011324',
            '피스타치오 마카롱(시즌)': '00011325',
            '바닐라 마카롱(시즌)': '00011326',
            '(미니)오리지널 6T': '00011327',
            '(미니)마일드 6T': '00011328',
            '(미니)오리지널 15T': '00011329',
            '(미니)마일드 15T': '00011330',
            '(미니)오리지널 50T': '00011331',
            '(미니)마일드 50T': '00011332',
            '(25)오리지널 5T': '00011333',
            '(25)마일드 5T': '00011334',
            '(25)오리지널 10T': '00011335',
            '(25)마일드 10T': '00011336',
            '(25)오리지널 50T': '00011337',
            '(25)마일드 50T': '00011338',
            '(25)오리지널 100T': '00011339',
            '(25)마일드 100T': '00011340',
            '13-아이스 쿨 텀블러 세트': '00011341',
            '13-추석선물세트': '00011342',
            '13-추석선물세트(B)': '00011343',
            '14-설선물세트': '00011344',
            '(200g)이디야 로스트': '00011345',
            '(200g)이디야 다크블루': '00011346',
            '(200g)이디야 바이올렛': '00011347',
            '(800g)이디야 로스트': '00011348',
            '(세트)이디야 로스트X2': '00011349',
            '(세트)이디야 다크블루X2': '00011350',
            '(세트)이디야 바이올렛X2': '00011351',
            '(세트)다크블루+로스트': '00011352',
            '(세트)바이올렛+로스트': '00011353',
            '(세트)다크블루+바이올렛': '00011354',
            '(차)얼그레이 홍차': '00011355',
            '(차)로즈쟈스민티': '00011356',
            '(차)보이차': '00011357',
            '(차)어린잎 녹차': '00011358',
            '(차)페퍼민트티': '00011359',
            '(차)캐모마일 레드티': '00011360',
            '(차)핑크로즈': '00011361',
            '(차)잉글리쉬블랙퍼스트': '00011362',
            '(차)루이보스': '00011363',
            '(시럽)바닐라': '00011364',
            '(시럽)카라멜': '00011365',
            '(파우더)바닐라': '00011366',
            '(파우더)토피넛': '00011367',
            '(파우더)프랄린': '00011368',
            '(상품권)5천원': '00011369',
            '(상품권)1만원': '00011370',
            '(도서)커피 그 블랙의 행복': '00011371',
            '(머그)13oz머그': '00011372',
            '(머그)유리머그': '00011373',
            '(머그)카푸치노': '00011374',
            '(머그)데미타세': '00011375',
            '(머그)도피오': '00011376',
            '(머그)베이직 그레이': '00011377',
            '(머그)베이직 블랙': '00011378',
            '(머그)베이직 화이트': '00011379',
            '(머그)빈티지 브라운': '00011380',
            '(머그)빈티지 블루': '00011381',
            '(머그)블루머그': '00011382',
            '(텀블러)젤리 로즈': '00011383',
            '(텀블러)젤리 퍼플': '00011384',
            '(텀블러)젤리 핑크': '00011385',
            '(텀블러)스탠 블루': '00011386',
            '(텀블러)스탠 화이트': '00011387',
            '(텀블러)아이스 로고': '00011388',
            '(텀블러)아이스 영문': '00011389',
            '(드립)웨이브드리퍼': '00011390',
            '(드립)웨이브스타일': '00011391',
            '(드립)웨이브필터': '00011392',
            '(드립)서버': '00011393',
            '(드립)핸드밀': '00011394',
            '(드립)티포트': '00011395',
            '(머그)10주년': '00011396',
            '(머그)아이리쉬': '00011397',
            '(머그)오토': '00011398',
            '(텀블러)10주년': '00011399',
            '(텀블러)베이직': '00011400',
            '(텀블러)피크닉': '00011401',
            '(텀블러)캡슐 그린': '00011402',
            '(텀블러)캡슐 블루': '00011403',
            '(텀블러)캡슐 화이트': '00011404',
            '(드립)그라인더드리퍼': '00011405',
            '(드립)도자기드리퍼': '00011406',
            '(드립)플라스틱드리퍼': '00011407',
            '(드립)여과지': '00011408',
            '(드립)하리오핸드밀': '00011409',
            '(드립)전동그라인더': '00011410',
            '(컵받침)열매': '00011411',
            '(컵받침)컵': '00011412',
            '(-)유자차': '00011413',
            '(-)레몬차': '00011414',
            '(-)자몽차': '00011415',
            '(-)키위': '00011416',
            '(-)토마토': '00011417',
            '(-)바나나': '00011418',
            '(-)키위+바나나': '00011419',
            '(-)딸기+바나나': '00011420',
            '(-)과일주스1': '00011421',
            '(-)과일주스2': '00011422',
            '(-)과일주스3': '00011423',
            '(-)과일주스4': '00011424',
            '(-)기타1': '00011425',
            '(-)기타2': '00011426',
            '(-)기타3': '00011427',
            '(-)기타4': '00011428',
            '싱글컵': '00011429',
            '더블': '00011430',
            '미디움': '00011431',
            '아포가토': '00011432',
            '젤라또쉐이크': '00011433',
            '젤라또와플': '00011434',
            '(G)요거트': '00011435',
            '(G)스트로베리': '00011436',
            '(G)비스초코': '00011437',
            '(G)뚜또보스코': '00011438',
            '(G)바닐라': '00011439',
            '(G)그린티': '00011440',
            '(G)치즈케익': '00011441',
            '(G)로세': '00011442',
            '(G)쿠즈코초코': '00011443',
            '(G)커피바리가또': '00011444',
            '(G)티라미슈': '00011445',
            '(G)페스카란차': '00011446',
            '(G)스트라치아텔라': '00011447',
            '(G)체리바리가또': '00011448',
            '(-)제주 팥빙수(2인)': '00011449',
            '(-)제주 팥빙수(4인)': '00011450',
            '(-)제주 녹차빙수(2인)': '00011451',
            '(-)제주 녹차빙수(4인)': '00011452',
            '(-)제주 요거트빙수(1인)': '00011453',
            '(-)제주 요거트빙수(2인)': '00011454',
            '(-)제주 요거트빙수(3인)': '00011455',
            '(-)제주 요거트빙수(4인)': '00011456',
            '(-)제주 토핑추가': '00011457',
            '(-)제주 플레인요거트아이스': '00011458',
            '(-)제주 커플요거트아이스(2인)': '00011459',
            '(-)제주 라즈베리요거트아이스': '00011460',
            '(-)제주 블루베리요거트아이스': '00011461',
            '(-)제주 망고요거트아이스': '00011462',
            '14-추석선물세트': '00011484',
            '(-)기타0': '00011487',
            '+(소스)카라멜': '00011489',
            '+(소스)초콜릿': '00011490',
            '+초콜릿 칩': '00011491',
            '(도서)왜 이디야에 열광하는가': '00011492',
            '이곡 라떼': '00011618',
            'ICED 이곡 라떼': '00011619',
            '(병)에비앙': '00011781',
            '수플레 치즈케익 라떼 시즌': '00011783',
            'ICED수플레치즈케익라떼시즌': '00011784',
            'S-ICED수플레치즈케익라떼': '00011785',
            'S-수플레 치즈케익 라떼 시즌': '00011786',
            '유자 티': '00011787',
            '자몽 티': '00011788',
            'S-유자 티': '00011789',
            'S-자몽 티': '00011790',
            '누텔라 초코브레드': '00011791',
            '체다 치즈브레드': '00011792',
            '까로짜': '00011793',
            '라자냐': '00011794',
            '소세지롤': '00011795',
            '치즈롤': '00011796',
            '잉글리쉬 머핀': '00011797',
            '브라우니': '00011798',
            '코요타': '00011799',
            '아몬드 비스코티': '00011800',
            '초코 비스코티': '00011801',
            '롤케익': '00011802',
            '플레인요거트': '00011803',
            '클럽 샌드위치': '00011804',
            '카프레제 샌드위치': '00011805',
            '치킨바베큐 샌드위치': '00011806',
            '15-비니스트 선물세트': '00011807',
            '오리진 쉐이크': '00011808',
            '이디야 커피 쉐이크': '00011809',
            '초코쿠키 쉐이크': '00011810',
            '단팥빵': '00011811',
            '오리진쉐이크 교환권': '00011812',
            '잉글리쉬 머핀': '00011813',
            '소시지롤': '00011814',
            '치즈롤': '00011815',
            '(100g)               하와이안코나   엑스트라팬시': '00011816',
            '밀크커피빙수': '00011817',
            '치즈딸기빙수': '00011818',
            '(기본)페르니고띠 아모르 와퍼': '00011819',
            '(다크)페르니고띠 아모르 와퍼': '00011820',
            '(100g)               에티오피아   예가체프': '00011821',
            '모바일교환권(교육용)': '00011822',
            '(병)이디야워터': '00011823',
            '(100g) 파나마 에스메랄다 게이샤': '00011824',
            '키슈(베이컨)': '00011825',
            '키슈(시금치)': '00011826',
            '키슈(버섯)': '00011827',
            '커브 크로와상': '00011828',
            '아몬드 크로와상': '00011829',
            '뱅오쇼콜라': '00011830',
            '타르트(치즈)': '00011831',
            '타르트(초코)': '00011832',
            '다쿠아즈': '00011833',
            '컵과일': '00011834',
            '샐러드&드레싱': '00011835',
            '(100g)케냐테구': '00011836',
            '레몬뷰티': '00011837',
            '(100g)코스타리카 라스라하즈': '00011838',
            '15-추석 선물세트': '00011839',
            '(머그)스키니 화이트': '00011840',
            '(머그)스키니 블루': '00011841',
            '(머그)라이트 화이트': '00011842',
            '(머그)라이트 블루': '00011843',
            '(머그)라이트 오렌지': '00011844',
            '(텀블러)슬림 블루': '00011845',
            '(텀블러)슬림 그레이': '00011846',
            '레몬티 교환권': '00011847',
            '(EX)아메리카노_행사': '00011848',
            '(EX)ICED 아메리카노_행사': '00011849',
            '(100g)브라질 사오 주다스': '00011850',
            '아메리카노_테스트': '00011851',
            '피칸초콜릿': '00011852',
            'ICED 피칸초콜릿': '00011853',
            '로즈쟈스민티': '00011860',
            '캐모마일티': '00011861',
            'ICE로즈쟈스민티': '00011866',
            'ICE캐모마일티': '00011870',
            '(100g)크리스마스 블렌드': '00011873',
            '(머그)15-윈터빌리지': '00011874',
            '(머그)15-윈터레드': '00011875',
            '(머그)15-윈터실버링': '00011876',
            '(다이어리)16-블루': '00011877',
            '(다이어리)16-베이지': '00011878',
            'ICE 아메리카노 무료사이즈업': '00011879',
            '아메리카노 2잔 + 블루머그': '00011880',
            '카페라떼 2잔 + 블루머그': '00011881',
            '녹차라떼&nbsp;2잔 + 블루머그': '00011882',
            '아메리카노_이마트행사': '00011883',
            'ICED 아메리카노_이마트행사': '00011884',
            '(NEW)이디야이너츠': '00011885',
            '(행사용)다이어리 16': '00011886',
            '아메리카노_테스트': '00011887',
            '플레인 치즈 케이크(스틱) + 아메리카노': '00011888',
            '플레인 치즈 케이크(스틱)+ 카페라떼': '00011889',
            '허니카라멜브레드 + 아메리카노 2': '00011890',
            '바닐라라떼 + 토피넛라떼': '00011891',
            '16-설 선물세트': '00011892',
            '(미니)오리지널 100T_기획': '00011893',
            '(미니)마일드 100T_기획': '00011894',
            '유기농 스퀴즈 오렌지': '00011895',
            '유기농 과일야채': '00011896',
            '이디야커피_든든 세트': '00011897',
            '이디야커피_러블리 세트': '00011898',
            '이디야커피_커플 세트': '00011899',
            '이디야커피_생일 축하해': '00011900',
            '이디야커피_감사의 마음': '00011901',
            '(드립커피) 다크블루': '00011902',
            '(드립커피) 바이올렛': '00011903',
            '아메리카노-할인 HOT': '00011904',
            '아메리카노-할인 ICE': '00011905',
            '바나나 쉐이크': '00011906',
            '딸기 쉐이크': '00011907',
            '로얄밀크티 쉐이크': '00011908',
            '오션스프레이(루비레드)': '00011909',
            '오션스프레이(크랜베리)': '00011910',
            '눈꽃팥빙수': '00011911',
            '눈꽃흑임자빙수': '00011912',
            '눈꽃애플망고빙수': '00011913',
            '눈꽃블루베리빙수': '00011914',
            '눈꽃초코빙수': '00011915',
            '리얼흑임자빙수': '00011916',
            '블루베리빙수': '00011917',
            '리얼초코빙수': '00011918',
            '라임 에이드': '00011919',
            '청포도 에이드': '00011920',
            '(EX) 라임 에이드': '00011921',
            '(EX) 청포도 에이드': '00011922',
            '썸머세트(자몽플랫치노+망고플랫치노)': '00011923',
            '에이드세트(자몽에이드+레몬에이드)': '00011924',
            '(EX) 카페모카': '00011925',
            '(EX) 카라멜마끼아또': '00011926',
            '(EX) 바닐라 라떼': '00011927',
            '(EX)ICED 바닐라 라떼': '00011928',
            '(EX) 초콜릿': '00011929',
            '(EX) 녹차라떼': '00011930',
            '(EX) 토피넛 라떼': '00011931',
            '(EX)ICED 초콜릿': '00011932',
            '(EX)ICED 녹차라떼': '00011933',
            '(EX)ICED 토피넛 라떼': '00011934',
            '복숭아 플랫치노': '00011935',
            '(EX) 복숭아 플랫치노': '00011936',
            '자두 플랫치노': '00011937',
            '(EX) 자두 플랫치노': '00011938',
            '행사_복숭아 플랫치노': '00011939',
            '행사_(EX) 복숭아 플랫치노': '00011940',
            '행사_자두 플랫치노': '00011941',
            '행사_(EX) 자두 플랫치노': '00011942',
            '전주행사 아메리카노': '00011943',
            '전주행사 ICED 아메리카노': '00011944',
            '고구마 라떼': '00011945',
            'ICED 고구마 라떼': '00011946',
            '레드빈 플랫치노': '00011947',
            '행사_아메리카노': '00011948',
            '행사_ICED 아메리카노': '00011949',
            '(NEW)크림치즈머핀': '00011950',
            '(NEW)초코칩머핀': '00011951',
            '(NEW)블루베리머핀': '00011952',
            '행사_자몽에이드': '00011953',
            '행사_레몬에이드': '00011954',
            '행사_블루레몬 에이드': '00011955',
            '행사_복숭아 아이스티': '00011956',
            '행사_레몬 아이스티': '00011957',
            '1+1행사 자두 플랫치노': '00011958',
            '1+1행사 복숭아 플랫치노': '00011959',
            '스노우쿠키슈': '00011960',
            '(EX)행사_아메리카노': '00011961',
            '(EX)행사_ICED 아메리카노': '00011962',
            '행사_초콜릿 칩 플랫치노': '00011963',
            '행사_ICED 초콜릿': '00011964',
            '행사_초콜릿': '00011965',
            '비니스트 선물세트 1호': '00011966',
            '비니스트 선물세트 2호': '00011967',
            '돗자리': '00011968',
            '비치타월': '00011969',
            '50%_망고 플랫치노': '00011971',
            '50%_그린애플 플랫치노': '00011972',
            '50%_딸기요거트 플랫치노': '00011973',
            '50%_초콜렛칩 플랫치노': '00011974',
            '50%_복숭아 플랫치노': '00011975',
            '1+1행사 초콜릿 칩 플랫치노': '00011976',
            '1+1행사 망고 플랫치노': '00011977',
            '에끌레어': '00011978',
            '페스트리 슈': '00011979',
            '행사_옥스포드 트럭세트': '00011980',
            '행사_옥스포드 매장세트': '00011981',
            '행사_옥스포드 트럭단품': '00011982',
            '행사_옥스포드 매장단품': '00011983',
            '추석선물패키지1(트럭형)': '00011984',
            '추석선물패키지2(매장형)': '00011985',
            '행사_피크닉세트': '00011986',
            '행사_피크닉세트(50%)': '00011987',
            '행사_이디야 보틀': '00011988',
            '행사_이디야 돗자리': '00011989',
            '리얼니트로커피': '00011990',
            '커피_에끌레어 SET': '00011991',
            '커피_페스트리슈 SET': '00011992',
            '아이스커피_에끌레어 SET': '00011993',
            '아이스커피_페스트리슈 SET': '00011994',
            '오징어먹물핫도그': '00011995',
            '피자바게트': '00011996',
            '하와이안피자바게트': '00011997',
            '먹물 감자 베이글': '00011998',
            '프리미엄소시지베이글': '00011999',
            '마늘바게트': '00012000',
            '데니쉬 식빵': '00012001',
            '먹물 고다 식빵': '00012002',
            '먹물연유바게트': '00012003',
            '브리오슈 식빵': '00012004',
            '쇼콜라데니쉬': '00012005',
            '스틱앙버터': '00012006',
            '아몬드크로와상': '00012007',
            '에멘탈큐브': '00012008',
            '자색고구마 밤 식빵': '00012009',
            '진한 초콜릿 식빵': '00012010',
            '초코스콘': '00012011',
            '치즈 치아바타': '00012012',
            '크로와상': '00012013',
            '크림브륄레': '00012014',
            '크림치즈데니쉬': '00012015',
            '트레디션 바게트': '00012016',
            '페스트리 슈': '00012017',
            '플레인스콘': '00012018',
            '피칸호두스틱': '00012019',
            '매콤햄치즈': '00012020',
            '스틱초코': '00012021',
            '이디야슈크림': '00012022',
            '초코데니쉬식빵': '00012023',
            '치즈데니쉬식빵': '00012024',
            '토마토치즈치아바타': '00012025',
            '피칸사워도우': '00012026',
            '크리스피 에끌레어(바닐라)': '00012027',
            '크리스피 에끌레어(초코)': '00012028',
            '브라우니': '00012029',
            '더블치즈케익': '00012030',
            '데블스 케이크': '00012031',
            '딸기요거트 생크림(조각)': '00012032',
            '딸기요거트생크림(2호)': '00012033',
            '딸기타르트': '00012034',
            '마스카포네티라미스롤': '00012035',
            '메론생크림케익(2호)': '00012036',
            '바나나초코 생크림케이크': '00012037',
            '바나나타르트': '00012038',
            '블루베리 타르트(조각)': '00012039',
            '사워치즈케이크': '00012040',
            '캐롯 케이크': '00012041',
            '티라미수': '00012042',
            '플레인 롤': '00012043',
            '화이트 치즈무스(조각)': '00012044',
            '치즈먹물구제르': '00012045',
            '아몬드치킨샌드위치': '00012046',
            '아보카도 모짜렐라 샌드위치': '00012047',
            '바게트샌드위치': '00012048',
            '햄치즈크로아상': '00012049',
            '(미니)다크 6T': '00012050',
            '(미니)다크 15T': '00012051',
            '(미니)다크 50T': '00012052',
            '(미니)다크 100T_기획': '00012053',
            '포카치아 샌드위치': '00012054',
            '아메리카노+ICE아메리카노': '00012055',
            '딸기요거트생크림 1호': '00012056',
            '카라멜치즈케이크 1호': '00012057',
            '행사_크로와상': '00012058',
            '호두범벅': '00012059',
            '우유식빵': '00012060',
            '크림치즈 호두빵': '00012061',
            '미니치즈롤': '00012062',
            '크라상': '00012063',
            '아몬드크라상': '00012064',
            '초코 트위스트': '00012065',
            '소시지 데니쉬': '00012066',
            '햄치즈 데니쉬': '00012067',
            '블루베리 파이': '00012068',
            '크림브릴뤠': '00012069',
            '마늘바게트': '00012070',
            '크림치즈 데니쉬': '00012071',
            '아몬드 데니쉬': '00012072',
            '스틱초코': '00012073',
            '먹물연유바게트': '00012074',
            '먹물고다식빵': '00012075',
            '브리오슈식빵': '00012076',
            '브리오슈 계란빵': '00012077',
            '야채빵': '00012078',
            '이디야 슈크림빵': '00012079',
            '바게트피자': '00012080',
            '하와이안피자': '00012081',
            '오징어 핫도그': '00012082',
            '에멘탈큐브': '00012083',
            '플레인스콘': '00012084',
            '초코스콘': '00012085',
            '딸기 요거트 생크림': '00012086',
            '카라멜 치즈케이크': '00012087',
            '바나나 초코생크림 케이크': '00012088',
            '딸기 요거트 생크림': '00012089',
            '딸기타르트': '00012090',
            '사워치즈케이크': '00012091',
            '데블스 케이크': '00012092',
            '포레누아': '00012093',
            '생크림 롤': '00012094',
            '마스카포네 티라미수롤': '00012095',
            '브라우니': '00012096',
            '블루베리타르트': '00012097',
            '슈': '00012098',
            '슈게뜨': '00012099',
            '햄치즈 크라상': '00012100',
            '바게트샌드위치': '00012101',
            '아몬드 치킨샌드위치': '00012102',
            '티라미수 라떼': '00012103',
            'ICED 티라미수 라떼': '00012104',
            '수능쿠키': '00012105',
            '데블스케이크': '00012106',
            '(분쇄원두)다크블렌드': '00012107',
            '(분쇄원두)라이트블렌드': '00012108',
            '1+1행사 티라미수 라떼': '00012109',
            '1+1행사 ICED 티라미수 라떼': '00012110',
            '1+1행사 피칸초콜릿': '00012111',
            '1+1행사 ICED 피칸초콜릿': '00012112',
            '시크릿쉬폰케이크': '00012113',
            '크리스마스통나무케이크': '00012114',
            '스트로베리몽블랑': '00012115',
            '행사_크리스마스옥스포드 단품': '00012116',
            '행사_크리스마스옥스포드 셋트': '00012117',
            '유자피나콜라다': '00012118',
            '자몽네이블오렌지': '00012119',
            '레몬스윗플럼': '00012120',
            '유자차': '00012121',
            '자몽차': '00012122',
            '레몬차': '00012123',
            '드립커피선물세트': '00012124',
            '딥 초콜릿': '00012125',
            'ICED 딥 초콜릿': '00012126',
            '생크림 초코쿠키빵': '00012127',
            '생크림 팥빵': '00012128',
            '생크림 티라미수': '00012129',
            '오픈_아메리카노': '00012130',
            '오픈_ICED 아메리카노': '00012131',
            '오픈_바닐라라떼': '00012132',
            '오픈_ICED 바닐라라떼': '00012133',
            '오픈_카페라떼': '00012134',
            '오픈_ICED 카페라떼': '00012135',
            '오픈_화이트초코렛모카': '00012136',
            '오픈_ICED 화이트초코렛모카': '00012137',
            '오픈_카푸치노': '00012138',
            '오픈_ICED 카푸치노': '00012139',
            '오픈_민트모카': '00012140',
            '오픈_ICED 민트모카': '00012141',
            '오픈_카페모카': '00012142',
            '오픈_ICED 카페모카': '00012143',
            '오픈_시나몬모카': '00012144',
            '오픈_ICED 시나몬모카': '00012145',
            '오픈_카라멜마끼아또': '00012146',
            '오픈_ICED 카라멜마끼아또': '00012147',
            '오픈_초코렛': '00012148',
            '오픈_ICED 초코렛': '00012149',
            '오픈_녹차라떼': '00012150',
            '오픈_ICED 녹차라떼': '00012151',
            '오픈_화이트초코렛': '00012152',
            '오픈_ICED 화이트초코렛': '00012153',
            '오픈_이곡라떼': '00012154',
            '오픈_ICED 이곡라떼': '00012155',
            '오픈_민트초코렛': '00012156',
            '오픈_ICED 민트초코렛': '00012157',
            '오픈_토피넛라떼': '00012158',
            '오픈_ICED 토피넛라떼': '00012159',
            '오픈_시나몬초코렛': '00012160',
            '오픈_ICED 시나몬초코렛': '00012161',
            '오픈_차이티라떼': '00012162',
            '오픈_ICED 차이티 라떼': '00012163',
            '오픈_유자차': '00012164',
            '오픈_유자피콜라다': '00012165',
            '오픈_자몽차': '00012166',
            '오픈_자몽네이블오렌지': '00012167',
            '오픈_레몬차': '00012168',
            '오픈_레몬스윗플럼': '00012169',
            '오픈_에스프레소': '00012170',
            '오픈_에스프레소 마끼아또': '00012171',
            '오픈_에스프레소 콘파냐': '00012172',
            '오픈_아이스티 복숭아': '00012173',
            '오픈_아이스티 레몬': '00012174',
            '오픈_딥초콜릿': '00012175',
            '오픈_ICED딥초콜릿': '00012176',
            '플레인 에끌레어': '00012178',
            '딸기 에끌레어': '00012179',
            '[행사]생크림쿠키빵': '00012180',
            '오픈_고구마라떼': '00012181',
            '오픈_ICED 고구마라떼': '00012182',
            '오픈_니트로커피': '00012183',
            '오리지널 80+20T': '00012184',
            '마일드 80+20T': '00012185',
            '다크 80+20T': '00012186',
            '니트로 SET1': '00012187',
            '니트로 SET2': '00012188',
            'ICED 유자': '00012189',
            'ICED 자몽': '00012190',
            '행사_리얼니트로': '00012191',
            '500원추가니트로': '00012192',
            '행사_니트로커피_슈_에끌레어': '00012193',
            '(스틱케익)케롯': '00012194',
            '(스틱케익)오렌지쇼콜라': '00012195',
            '식권': '00012196',
            '니트로패키지': '00012197',
            '(도서) 커피드림': '00012198',
            '샷추가': '00012199',
            '(N)에스프레소': '00012512',
            '(N)에스프레소 마끼아또': '00012513',
            '(N)에스프레소 콘파냐': '00012514',
            '(N)녹차라떼': '00012515',
            '(N)(EX)녹차라떼': '00012516',
            '(N)ICED 녹차라떼': '00012517',
            '(N)(EX)ICED 녹차라떼': '00012518',
            '(N)바닐라라떼': '00012519',
            '(N)(EX)바닐라라떼': '00012520',
            '(N)ICED 바닐라라떼': '00012521',
            '(N)(EX)ICED 바닐라라떼': '00012522',
            '(N)화이트 초콜릿': '00012523',
            '(N)토피넛 라떼': '00012524',
            '(N)초콜릿': '00012525',
            '(N)민트 초콜릿': '00012526',
            '(N)(EX)토피넛 라떼': '00012527',
            '(N)(EX)초콜릿': '00012528',
            '(N)화이트 초콜릿 모카': '00012529',
            '(N)카푸치노': '00012530',
            '(N)카페모카': '00012531',
            '(N)카페라떼': '00012532',
            '(N)카라멜 마끼아또': '00012533',
            '(N)아메리카노': '00012534',
            '(N)민트 모카': '00012535',
            '(N)(EX)카페라떼': '00012536',
            '(N)(EX)아메리카노': '00012537',
            '(N)(EX)카페모카': '00012538',
            '(N)(EX)카라멜 마끼아또': '00012539',
            '(N)ICED 화이트 초콜릿': '00012540',
            '(N)ICED 토피넛 라떼': '00012541',
            '(N)ICED 초콜릿': '00012542',
            '(N)ICED 민트 초콜릿': '00012543',
            '(N)(EX)ICED 토피넛 라떼': '00012544',
            '(N)(EX)ICED 초콜릿': '00012545',
            '(N)ICED 화이트 초콜릿 모카': '00012546',
            '(N)ICED 카푸치노': '00012547',
            '(N)ICED 카페모카': '00012548',
            '(N)ICED 카페라떼': '00012549',
            '(N)ICED 카라멜 마끼아또': '00012550',
            '(N)ICED 아메리카노': '00012551',
            '(N)ICED 민트 모카': '00012552',
            '(N)(EX)ICED 카페모카': '00012553',
            '(N)(EX)ICED 카페라떼': '00012554',
            '(N)(EX)ICED 카라멜 마끼아또': '00012555',
            '(N)(EX)ICED 아메리카노': '00012556',
            'N)허니카라멜브레드   아메리카노 2': '00012562',
            'N)이디야커피_ 세트(베이글 아메리카노)': '00012563',
            'N)이디야커피_ 세트(프레즐 아메리카노)': '00012564',
            'N)이디야커피_ 세트(모카번 아메리카노)': '00012565',
            'N)이디야커피_ 세트(생크림초코쿠키빵 아메리카노)': '00012566',
            'N)이디야커피_ 세트(쿠키 아메리카노)': '00012567',
            'N)이디야커피_ 세트(스틱케익 아메리카노)': '00012568',
            'N)이디야커피_ 세트(스노우쿠키슈 아메리카노)': '00012569',
            'N)이디야커피_ 세트(이너츠 아메리카노)': '00012570',
            '시나몬': '00012571',
            '휘핑': '00012572',
            '시럽': '00012573',
            '카라멜시럽': '00012574',
            '헤이즐넛시럽': '00012575',
            '메이플시럽': '00012576',
            '초코시럽': '00012577',
            'HOT': '00012578',
            'ICED': '00012579'
         }
        a = str(b[input])
        return a


    def custom_code(self, input):
        b = {
            '휘핑크림 추가': '0001',
            '휘핑크림 빼고': '0002',
            '휘핑크림 적당히': '0003',
            '휘핑크림 많이': '0004',
            '시럽 추가': '0005',
            '시럽 빼고': '0006',
            '시럽 적당히': '0007',
            '미지근하게': '0008',
            '진하게': '0009',
            '연하게': '0010',
            '얼음 조금': '0011',
            '얼음 많이': '0012',
            '덜 달게': '0013',
            '머그잔': '0014',
            '포장': '0015',
            '드리즐': '0016',
            '토핑': '0017'
         }
        a = str(b[input])
        return a


    # 1~999
    def readNumberMinute(self,n):

        n = int(n)
        units = '일,십,백,천'.split(',')
        nums = '일,이,삼,사,오,육,칠,팔,구'.split(',')
        result = []
        i = 0
        while n > 0:
            n, r = divmod(n, 10)
            if r == 1:
                result.append(str(units[i]))
            elif i == 0 and r > 0:
                result.append(nums[r - 1])
            elif r > 0:
                result.append(nums[r - 1] + str(units[i]))
            i += 1
        if len(result) == 0:
            result.append("영")
        return ''.join(result[::-1])

    def readNumberHour(self,n):
        n = int(n)
        units = '한,열,백,천'.split(',')
        nums = '한,두,세,네,다섯,여섯,일곱,어덟,아홉'.split(',')
        #    units = list('일십백천')
        #    nums = list('일이삼사오육칠팔구')
        #    units = ['일', '십', '백', '천']
        #    nums = ['일', '이' ,'삼' ,'사' ,'오' ,'육' ,'칠' ,'팔' ,'구']
        result = []
        i = 0
        while n > 0:
            n, r = divmod(n, 10)
            if r == 1:
                result.append(str(units[i]))
            elif i == 0 and r > 0:
                result.append(nums[r - 1])
            elif r > 0:
                result.append(nums[r - 1] + str(units[i]))
            i += 1
        if len(result) == 0:
            result.append("영")
        return ''.join(result[::-1])
    def change_bu(self,input):
        b = {
            '한': '1',
            '두': '2',
            '세': '3',
            '네': '4',
            '다섯': '5',
            '여섯': '6',
            '일곱': '7',
            '여덟': '8',
            '아홉': '9',
            '열': '10',
            '열한': '11',
            '열두': '12',
            '열세': '13',
            '열네': '14',
            '열다섯': '15',
            '열여섯': '16',
            '열일곱': '17',
            '열여덟': '18',
            '열아홉': '19',
            '스물': '20',
            '스물한': '21',
            '스물두': '22',
            '스물세': '23',
            '스물네': '24'
        }
        text = {'스물네', '스물세', '스물두', '스물한', '스물', '열아홉', '열여덟', '열일곱', '열여섯', '열다섯', '열네', '열세', '열두', '열한', '열', '아홉','여덟', '일곱', '여섯', '다섯', '네', '세', '두', '한'}
        for t in text:
            if input.find(t) >= 0:
                input = input.replace(t, b[t])
            else:
                pass
        return input
    def change(self,input):
        kortext = '영일이삼사오육칠팔구'
        dic = {'십': 10, '백': 100, '천': 1000, '만': 10000, '억': 100000000, '조': 1000000000000}
        result = 0
        tmpResult = 0
        num = 0
        for i in range(0,len(input)):
            token = input[i]
            check = kortext.find(input[i])

            if check == -1:
                if '만억조'.find(token) == -1:
                    if num != 0:
                        tmpResult = tmpResult + num * dic[token]
                    else:
                        tmpResult = tmpResult + 1 * dic[token]

                else:
                    tmpResult = tmpResult + num
                    if tmpResult != 0:
                        result = result + tmpResult * dic[token]
                    else:
                        result = result + 1 * dic[token]
                    tmpResult = 0

                num = 0
            else:
                num = check
        return result + tmpResult + num


    def unknown_answer(self):
        """
        리스트에서 랜덤으로 Unknown 답변 출력.
        """
        unknown_text = ['죄송해요, 제가 잘 못 알아 들었어요. 키워드 위주로 다시 질문해주시겠어요?',
                        '답변을 찾을 수 없습니다. 다른 질문을 해주시면 성실히 답변해 드리겠습니다. ']
        return random.choice(unknown_text)


    def Close(self, req, context):
        print 'V3 ', 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        return talk_stat

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()

    def Open(self, req, context):
        print 'V3 ', 'Open', 'called'
        # req = talk_pb2.OpenRequest()
        event_res = talk_pb2.OpenResponse()
        event_res.code = 1000000
        event_res.reason = 'success'
        answer_meta = struct.Struct()

        answer_meta["play1"] = "play1"
        answer_meta["play2"] = "play2"
        answer_meta["play3"] = "play3"

        answer_meta.get_or_create_struct("audio1")["name"] = "media_play1"
        answer_meta.get_or_create_struct("audio1")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio1")["duration"] = "00:10:12"

        answer_meta.get_or_create_struct("audio2")["name"] = "media_play2"
        answer_meta.get_or_create_struct("audio2")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio2")["duration"] = "00:00:15"

        event_res.meta.CopyFrom(answer_meta)
        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        # DO NOTHING
        return event_res

    def Event(self, req, context):
        print 'V3 ', 'Event', 'called'
        # req = talk_pb2.EventRequest()

        event_res = talk_pb2.EventResponse()
        event_res.code = 10
        event_res.reason = 'success'

        answer_meta = struct.Struct()
        answer_meta["meta1"] = "meta_body_1"
        answer_meta["meta2"] = "meta_body_2"
        event_res.meta.CopyFrom(answer_meta)

        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        return event_res

def serve():
    parser = argparse.ArgumentParser(description='CMS DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(
        EchoDa(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
