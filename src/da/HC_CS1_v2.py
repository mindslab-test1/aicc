#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from concurrent import futures
import argparse
import grpc
import os
import random
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
import time
import syslog
import pymysql
import datetime
import re
import json 
import websocket
from websocket import create_connection
import pdb
import traceback
from lib import logger

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da.v3 import talk_pb2
from maum.m2u.facade import front_pb2

# Custom import
#from qa.util import *
from qa import basicQA
from custom_hs.sds import SDS
from custom_hs.HC_Func import FUNC
from qa.util import Util
from cfg import config

# echo_simple_classifier = {
#    "echo_test": {
#        "regex": [
#            "."
#        ]
#    }

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
Session_Value = []
_NOTHING_FOUND_INTENT_ = "의도를 찾지 못했습니다."
model = 'HC_CS1'

#def setSessionValue(session_id, key, value):
#    try:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        findIndex = sessionList.index(session_id)
#        Session_Value[findIndex][key] = value
#    except Exception as err:
#        pass
#
#def getSessionValue(session_id, key):
#    keyValue = None
#    isNew = False
#
#    if len(Session_Value) > 0:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        try:
#            findIndex = sessionList.index(session_id)
#            keyValue = Session_Value[findIndex][key]
#        except Exception as err:
#            isNew = True
#    else:
#        isNew = True
#
#    if isNew:
#        Session_Value.append({"session_id" : session_id, "model" : ""})
#        keyValue = Session_Value[len(Session_Value)-1][key]
#
#    return keyValue
#
#def RegEx(self, session_id):
#    print("RegEx call!!")
#
#    print("get model : " + str(getSessionValue(session_id, "model")))
#    modelname = str(getSessionValue(session_id, "model"))
#    if modelname == "":
#        intent = "Happy_Call_HH"
#        setSessionValue(session_id, "model", intent)
#    elif modelname == "2":
#        intent = "privacy"
#        setSessionValue(session_id, "model", intent)
#
#    #intent = str(getSessionValue(session_id, "model"))
#    return intent

class EchoDa(talk_pb2_grpc.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'control'
    provider.description = 'control intention return DA'
    provider.version = '0.1'
    provider.single_turn = True
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # PARAMETER

    def __init__(self):
        syslog.syslog('init')
        self.state = provider_pb2.DIAG_STATE_IDLE
        syslog.syslog(str(self.state))
        self.qa_util = Util()
        self.Sds = SDS()
        self.func = FUNC()
        self.logger = logger.get_timed_rotating_logger(
            logger_name='DA',
            log_dir_path='/srv/maum/logs/',
            log_file_name='HC_CS1.log',
            backup_count=3,
            log_level='debug'
        )
        self.logger.debug("="*100)
        self.logger.debug("1. DA V3 Server Constructor")
        try:
            self.ws = create_connection("ws://10.122.64.152:13254/callsocket")
        except:
            self.logger.error('error occur message :: {}'.format(traceback.format_exc()))
    #
    # INIT or TERM METHODS
    #

    def IsReady(self, empty, context):
        print 'V3 ', 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'V3 ', 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'V3 ', 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, empty, context):
        print 'V3 ', 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = userattr_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        loc.title = '기본 지역'
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '기본으로 조회할 지역을 지정해주세요.'
        attrs.append(loc)

        device = userattr_pb2.UserAttribute()
        device.name = 'device'
        device.title = '기본 디바이스'
        device.type = userattr_pb2.DATA_TYPE_STRING
        device.desc = '기본으로 사용할 디바이스를 지정해주세요.'
        attrs.append(device)

        country = userattr_pb2.UserAttribute()
        country.name = 'time'
        country.title = '기준 국가 설정'
        country.type = userattr_pb2.DATA_TYPE_STRING
        country.desc = '기본으로 조회할 국가를 지정해주세요.'
        attrs.append(country)

        result.attrs.extend(attrs)
        return result

    #
    # PROPERTY METHODS
    #

    def GetProviderParameter(self, empty, context):
        print 'V3 ', 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'V3 ', 'GetRuntimeParameters', 'called'
        result = provider_pb2.RuntimeParameterList()
        params = []

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '171.64.122.134'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_INT
        db_port.desc = 'Database Port'
        db_port.default_value = '7701'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'minds'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_AUTH
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'minds67~'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'ascar'
        db_database.required = True
        params.append(db_database)

        result.params.extend(params)
        return result
    def OpenSession(self, request, context):
        print "openSession"

        #param = {}
        #bizRes = {}
        lectureInfo = {}
        resMessage = 'success'
        meta = ''
        lectureNum = ''
        session_id = request.session.id
        #self.showKeyValue(context.invocation_metadata())

        if 'meta' in request.utter.meta:
             #meta = eval(request.utter.meta['meta'].replace('null','\"\"'))
             meta = request.utter.meta['meta']

             if 'intent' in meta:
                slots = meta['intent']['slots']
                if 'lectureNumber' in slots:
                    lectureNum = slots['lectureNumber']['value']

        #requestParam = Common.setMetaToParamMap(lectureNum=lectureNum,userTalk=' ' ,request=request, isopenRequest=True,session_id=session_id)
        localSessionObj = session_id
        print 'OpenSession id: '+str(request.session.id)

        result = talk_pb2.TalkResponse()
        res_meta = struct.Struct()
        #res_meta['response'] = bizRes
        result.response.meta.CopyFrom(res_meta)

        #session 정보 ,session data 10k
        result.response.session_update.id = session_id
        res_context = struct.Struct()
        res_context['session_data'] = str(lectureInfo)
        result.response.session_update.context.CopyFrom(res_context)

##############################################세션 카운트 지정
        session_data = struct.Struct()
        session_data['id'] = session_id
        session_data['db_count'] = '0'
        session_data['seq_id'] = '0'
        session_data['task1'] = '0'
        session_data['task2'] = '0'
        session_data['task3'] = '0'
        session_data['task4'] = '0'
        session_data['task5'] = '0'
        session_data['task6'] = '0'
        session_data['task7'] = '0'
        session_data['task8'] = '0'
        session_data['task9'] = '0'
        session_data['task9_1'] = '0'
        session_data['time'] = '0'
        session_data['timeaffirm'] = '0'
        session_data['PRIVACY1'] = '0'
        session_data['privacy1_1'] = '0'
        session_data['privacy1_1affirm'] = '0'
        session_data['PRIVACY2'] = '0'
        session_data['call_status'] = '0'
        result.response.session_update.context.CopyFrom(session_data)

        print 'OpenSession_'
        return result

    def OpenSkill(self, request, context):
        print "OpenSkill start"
        print 'Open request: '+str(request)
        session_id = request.session.id
        print 'open_session_data: '+ str(session_id)+', '+str(context)
        result = talk_pb2.TalkResponse()
        print 'OpenSkill end'
        return result


    def CloseSkill(self, request, context):

        result = talk_pb2.CloseSkillResponse()
        return result

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()


    def DBConnect(self, query):
        conn = pymysql.connect(user=config.DATABASE_CONFIG['user'],
                               password=config.DATABASE_CONFIG['passwd'],
                               host=config.DATABASE_CONFIG['host'],
                               database=config.DATABASE_CONFIG['name'],
                               charset=config.DATABASE_CONFIG['charset'],
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        curs.execute(query)
        print("query good!")
        rows = curs.fetchall()

        print(rows)
        curs.execute("commit;")
        curs.close()
        conn.close()
        return rows
    def db_dml(self, query, sql_args):
        conn = pymysql.connect(user=config.DATABASE_CONFIG['user'],
                               password=config.DATABASE_CONFIG['passwd'],
                               host=config.DATABASE_CONFIG['host'],
                               database=config.DATABASE_CONFIG['name'],
                               charset=config.DATABASE_CONFIG['charset'],
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        try:
            curs.executemany(query, sql_args)
        except:
            conn.rollback()
        else:
            conn.commit()
        finally:
            print("query good!")
            curs.close()
            conn.close()

    def send_ws(self, json_data, call_id):
        try:
            self.ws.send(json_data)
            self.logger.debug('send to socket server data :: {}, call id :: {} '.format(json_data, call_id))
        except:
            self.logger.error('error occur message :: {}, call id :: {}'.format(traceback.format_exc(), call_id))


    def Talk(self, talk, context):
        ########################################### 콜 status 체크 및 첫 시작할때 정보 전달 ###########
        self.logger.debug('called talk function, request data :: {}'.format(talk))

        try:
            call_status = talk.utter.meta['status'] 
            seq = talk.utter.meta['contract_no']
            call_id = talk.utter.meta['call_id'] 
            user_utter = talk.utter.utter
        except:
            call_status = ''
            seq = '66' 
            call_id =  '1368'
            user_utter = talk.utter.utter
            self.logger.error('error occur message :: {}'.format(traceback.format_exc()))

        talk_res = talk_pb2.TalkResponse()
        time_pre = time.time()

        
        self.logger.debug('set utter {}, seq :: {}, call_id :: {}, call_status :: {}'.format(user_utter, 
                            seq, call_id, call_status))
        print("talk : ", talk.utter.utter)
        print("", str(seq) + "," + str(call_id))

        ################################################################################################### 시작할 때 정보 전달
        session_data = talk.session.context
        if call_status == 'CS0000':
        
            json_test = {
                "EventType":"CALL",
                "Event":"mntresult",
                "call_id":"" + call_id,
                "call_status":"MR0003",
                "contract_no":""+ seq,
                "taskNo_name":"",
                }
            jsonString = json.dumps(json_test)
            print(jsonString)
    #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
            self.send_ws(jsonString, call_id)

            self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0003', mnt_status_name ='진행중' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")

        #########################################################################################################

        db_count2 = self.DBConnect("select count(seq_num) from HC_HH_CAMPAIGN_SCORE where contract_no = '" + seq + "' and call_id = '"+call_id+"';")
        db_count2 = db_count2[0]['count(seq_num)']

        session_data = talk.session.context
        db_count = int(session_data['db_count'])
        print('하하잉' + str(db_count))
        if db_count == 0 and db_count2 == 0:
            task_list = ['task1','task2','PRIVACY1','PRIVACY2','time','task3','task4','task5','task6','task7','task8','task9','task9_1']
            sql_args = list()
            for i in task_list:
                seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id=3 and task = '" + i + "';")
                #print (camp_id_db)
                seq_id = str(seq_id_db[0]['seq'])
                sql_args.append((call_id, seq, seq_id, i))
                #self.DBConnect("insert into HC_HH_CAMPAIGN_SCORE (call_id, contract_no, info_seq, info_task, task_value) values ('"+call_id+"','"+seq+"','"+seq_id+"','"+i +"',null);")
            print("*" * 80)
            print(sql_args)
            print("*" * 80)
            self.db_dml("insert into HC_HH_CAMPAIGN_SCORE (call_id, contract_no, info_seq, info_task, task_value, review_coment) values ( %s, %s, %s, %s, null, null);", sql_args)
            self.logger.debug('done update campaign score table, call id :: {}'.format(call_id))
            session_data['db_count'] = 1
            talk_res.response.session_update.context.CopyFrom(session_data)
        elif db_count == 1:
            pass

        
        a = {}
       
        session_id = talk.session.id
        dbsession = self.DBConnect("select \
        cust_tel_no,\
        cust_nm,\
        create_dt,\
        insured_contractor,\
        insured_person,\
        addr_main,\
        addr_sub,\
        addr_code,\
        jumin_no, \
        prod_name \
        from MNT_TARGET_MNG where contract_no = '" + seq + "';")
        # cust_nm 통화자(가입자)
        # join_time 전체 시간 호출
        # join_month 가입 월
        # join_day 가입 일
        # insurance_contractor 가입 계약자
        # insurance_insured 가입 피보험자
        # insurance_closeproduct 무해지상품 가입여부
        # address_main 메인주소
        # address_sub 세부주소
        
        phone = dbsession[0]['cust_tel_no']
        user_name = dbsession[0]['cust_nm']

        join_time = dbsession[0]['create_dt']
        print ("join_time : " + str(join_time))
        join_strp = datetime.datetime.strptime(str(join_time), '%Y-%m-%d %H:%M:%S')
        join_month = join_strp.month
        join_day = join_strp.day


        insured_contractor = dbsession[0]['insured_contractor']
        insured_person = dbsession[0]['insured_person']
        privacy_add1 = dbsession[0]['addr_main']
        privacy_add2 = dbsession[0]['addr_sub']
        privacy_add3 = dbsession[0]['addr_code']
#        product_code1 = dbsession[0]['product_code1']
#        product_code2 = dbsession[0]['product_code2']
#        product_code3 = dbsession[0]['product_code3']
        prod_name = dbsession[0]['prod_name']
        prod_name = '주택화재상해보험'
        cust_ssn = dbsession[0]['jumin_no']
        cust_ssn = cust_ssn[0:6]


#       talk_res = talk_pb2.TalkResponse()

        ##초기 모델 설정
#        dbsession = self.DBConnect("select model from MNT_TARGET_MNG where contract_no = '" + seq + "';")
#        if dbsession[0]['model'] is None:
#            model = "Happy_Call_HH"
#        else:
#            model = dbsession[0]['model']

        #question = talk.utter.utter
        meta = dict()
        #meta['seq_id'] = util.time_check(0)
        meta['log_type'] = 'SVC'
        meta['svc_name'] = 'DA'

        #output = ""
        original_answer = ""
        engine_path = list()
        answer_engine = "None"
        status_code = ""
        status_message = ""
        flag = False
        weight = 0
        code = 'None'
        sds_intent = ""
        unknown_count = 0
                
###############################################################prelogic
        # SDS
        dbtask = self.DBConnect("select task from MNT_TARGET_MNG where contract_no = '" + seq + "';")
        task = dbtask[0]['task']
        print(len(re.findall(u'[\u3130-\u318F\uAC00-\uD7A3]+', talk.utter.utter.decode('utf-8'))))
        if task == 'PRIVACY1' and len(re.findall(u'[\u3130-\u318F\uAC00-\uD7A3]+', talk.utter.utter.decode('utf-8'))) == 0:
            print("당첨")
            talk.utter.utter = talk.utter.utter + '번호'
            print(talk.utter.utter)
            sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)

        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
        #슬롯값 입력받아서 처리하는 로직
        a['nextweek'] = ""
        a['morae'] = ""
        a['tomorrow'] = ""
        a['today'] = ""
        a['day'] = ""
        a['part'] = ""
        a['hour'] = ""
        a['minute'] = ""
        a['input_month'] = ""
        a['input_day'] = ""
        a['input_year'] = ""
        a['input_six'] = ""
        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
#정상적으로 입력이 되었는지 확인하는 로직      
        if sds_res['current_task'] == 'PRIVACY2':

            items = sds_res['intent.filled_slots.items']
            print(items)
            for id in items:
                a[id[0]] = id[1]
                print(id)
            if a['input_year'] == "" : input_year = ""
            else: input_year = a['input_year']
            if a['input_month'] == "" : input_month = ""
            else: input_month = a['input_month']
            if a['input_day'] == "" : input_day = ""
            else: input_day = a['input_day']
            if a['input_six'] == "" : input_six = ""
            else: input_six = a['input_six']
            print ("input_six : " + str(input_six))
            print ("input_year : " + str(input_year))
            print ("input_month : " + str(input_month))
            print ("input_day : " + str(input_day))
            
            try:
                if input_month.find('월') >= 0 and input_month.find('일') >= 0:
                    input_day = input_month[input_month.find('월')+1:input_month.find('일')]
                    input_month = input_month[0:input_month.find('월')]
            except:
                pass
            try:
                if input_day.find('월') >= 0 and input_day.find('일') >= 0:
                    input_month = input_day[0:input_day.find('월')]
                    input_day = input_day[input_day.find('월')+1:input_day.find('일')]
            except:
                pass

            if len(input_year) != 0 and len(input_month) != 0 and len(input_day) !=0: 
              ##### 한글 년 확인 
                if ord(input_year[-1]) >= 48 and ord(input_year[-1]) <= 57:
                    input_year = self.digit2txt(input_year)
                re_input = str(input_year)
                re_input = re_input.replace(' ','')
                re_input = re_input.replace('년','')
                input_year = self.func.convert_time(re_input)
              ##### 한글 월 확인
                if ord(input_month[-1]) >= 48 and ord(input_month[-1]) <= 57:
                    input_month= self.digit2txt(input_month)
                re_input = str(input_month)
                re_input = re_input.replace(' ','')
                re_input = re_input.replace('시','십')
                re_input = re_input.replace('유','육')
                re_input = re_input.replace('월','')
                input_month = self.func.convert_time(re_input)

              ##### 한글 일 확인 
                if ord(input_day[-1]) >= 48 and ord(input_day[-1]) <= 57:
                    input_day= self.digit2txt(input_day)
                re_input = str(input_day)
                re_input = re_input.replace(' ','')
                re_input = re_input.replace('이십일일','이십일')
                re_input = re_input.replace('일일','일')
                re_input = re_input.replace('이일','이')
                re_input = re_input.replace('삼일','삼')
                re_input = re_input.replace('사일','사')
                re_input = re_input.replace('오일','오')
                re_input = re_input.replace('육일','육')
                re_input = re_input.replace('칠일','칠')
                re_input = re_input.replace('팔일','팔')
                re_input = re_input.replace('구일','구')
                input_day = self.func.convert_time(re_input)
                
                try:
                    if int(input_month) >= 0 and int(input_month) <= 9:
                        input_month = '0' + str(input_month)
                    if int(input_day) >= 0 and int(input_day) <= 9:
                        input_day = '0' + str(input_day)
                except:
                    pass
                

                input_six = str(input_six)
                input_year = str(input_year)
                input_month = str(input_month)
                input_day = str(input_day)

            if sds_intent == 'privacy':
            
                if len(input_six) <= 0 and (len(input_year) <= 0 or len(input_month) <= 0 or len(input_day) <= 0):
                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                    session_data = talk.session.context
                    privacy1_1 = int(session_data['privacy1_1'])
                    privacy1_1 = privacy1_1 + 1
                    session_data['privacy1_1'] = privacy1_1
                    talk_res.response.session_update.context.CopyFrom(session_data)
                    
                    if int(session_count) >= 3:
                        talk.utter.utter = "$privacy1$"
                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                    elif int(session_count) <= 2:
                        talk.utter.utter = "$privacy1_1back$"
                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                else:
                    session_data = talk.session.context
                    privacy1_1 = int(session_data['privacy1_1'])
                    privacy1_1affirm = int(session_data['privacy1_1affirm'])
                    if len(input_six) >= 1:
                        input_print = input_six
                    elif len(input_year) >= 1:
                        input_year = input_year[-2:]
                        input_print = input_year + input_month + input_day
                    print("입력하신 번호는" + input_print)
                    privacy1_1affirm = input_print
                    session_data['privacy1_1affirm'] = privacy1_1affirm
                    print("privacy1_1affirm" + privacy1_1affirm)
                    talk_res.response.session_update.context.CopyFrom(session_data)
                    print ("slot_input_six : " + str(input_six))
                    print ("slot_input_year : " + str(input_year))
                    print ("slot_input_month : " + str(input_month))
                    print ("slot_input_day : " + str(input_day))
                    print ("slot_input_print : " + str(input_print))
        
                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 3 and task = '" + task + "';")
                    seq_id = str(seq_id_db[0]['seq'])
                    if cust_ssn !=  privacy1_1affirm:
                        privacy1_1 = int(privacy1_1) + 1
                        session_data['privacy1_1'] = privacy1_1
                        print(privacy1_1)
                        #print('여기 진입')
                        if int(privacy1_1) >= 3:
                            #print('여기 진입2')
                            talk.utter.utter = "$privacy1$"
                            sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                        elif int(privacy1_1) <= 2:
                            #print('여기 진입3 : '+str(privacy1_1))
                            talk.utter.utter = "$privacy1_1back$"
                            sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                    else:
                        talk.utter.utter = "$privacy1$"
                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)


###########################  넘어가는 로직 설정
        
        task_list = ['task1','task2','task3','task4','task5','task6','task7','task8','task9_1','PRIVACY2']
        print("$" * 70)
        print(talk.session.context)
        print("$" * 70)
        
        answer_value = ''

        for i in task_list:
            if sds_res['current_task'] == i:
                session_data = talk.session.context
                sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
                session_count = int(session_data[i])
                if sds_intent == 'unknown' or sds_intent == 'again' or sds_intent == 'noproportion' or sds_intent == 'proportion' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'req':
                    session_count = session_count + 1
                    session_data[i] = session_count
                    talk_res.response.session_update.context.CopyFrom(session_data)
                    if int(session_count) >= 2:
                        seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO camp_id = 3 and where task = '" + sds_res['current_task'] + "';")
                        seq_id = str(seq_id_db[0]['seq'])
                        talk.utter.utter = "$"+i+"$"
                        self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '모름' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                        answer_value = '모름'
                        self.DBConnect("update MNT_TARGET_MNG set task='"+sds_res['current_task']+"' where contract_no = '" + seq + "';")
                        unknown_count = 1
                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)


        if sds_res['current_task'] == 'task9':
            sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
            session_data = talk.session.context
            session_count = int(session_data['task9'])
            if sds_intent == 'affirm' or sds_intent == 'negate':
                session_count = session_count + 1
                session_data['task9'] = session_count
                talk_res.response.session_update.context.CopyFrom(session_data)
                if int(session_count) >= 2:
                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 3 and task = '" + sds_res['current_task'] + "';")
                    seq_id = str(seq_id_db[0]['seq'])
                    talk.utter.utter = "$task9$"
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '모름' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                    answer_value = '모름'
                    self.DBConnect("update MNT_TARGET_MNG set task='"+sds_res['current_task']+"' where contract_no = '" + seq + "';")
                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)


        if sds_res['current_task'] == 'PRIVACY1':
            sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
            session_data = talk.session.context
            session_count = int(session_data['privacy1_1'])
            if sds_intent != 'privacy' and sds_intent != 'privacy1' and sds_intent != 'privacy1_1back':
                session_count = session_count + 1
                session_data['privacy1_1'] = session_count
                talk_res.response.session_update.context.CopyFrom(session_data)
                if int(session_count) >= 3:
                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 3 and task = '" + sds_res['current_task'] + "';")
                     #print (camp_id_db)
                    seq_id = str(seq_id_db[0]['seq'])
                    talk.utter.utter = "$privacy$"
                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                elif int(session_count) <= 2:
       #             talk.utter.utter = "$privacy1_1back$"
                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                 
        time_cen = time.time()
        print("시간의 결과는 : "+ str(time_cen - time_pre))


        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
        print("sds_intent의 값 : " + str(sds_intent))

        # 결과값 DB에 저장하는 방식
        b = []
        b.append("time")
        b.append("timeAffirm")
        b.append("taskEnd") 
        b.append("timeEnd")
        for i in range(1, 15):
            b.append("task" + str(i))
        for i in range(1, 4):
            b.append("PRIVACY" + str(i))
        b.append("PRIVACY1_1" + str(i))
        b.append("task9_1" + str(i))


        if sds_res['current_task'] in b:
            seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 3 and task = '" + sds_res['current_task']  + "';")
         #   print (camp_id_db)
#            print(seq_id_db, task)
            seq_id = str(seq_id_db[0]['seq'])
            print('여기넘어왔다1')
            if sds_intent == 'affirm':
                print('여기넘어왔다2')
                if task != 'task9':
                    print('여기넘어왔다3')
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'Y' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
                    answer_value = 'Y'
                else:
                    pass
            elif sds_intent == 'negate':
                if task != 'task9':
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'N' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
                    answer_value = 'N'
                else:
                    pass
            elif sds_intent == 'overlap' or sds_intent == 'noproportion':
                if task == 'task9':
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '중복' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
                    answer_value = '중복'
                else:
                    pass
            elif sds_intent == 'nooverlap' or sds_intent == 'proportion':
                if task == 'task9':
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '비례' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
                    answer_value = '비례'
                else:
                    pass
            elif sds_intent == 'unknown' or sds_intent == 'again':
                pass
            elif unknown_count == 1:
                pass
            else:
                if task == 'PRIVACY1' or task == 'time':
                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '입력' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
                    answer_value = '입력'


        print("SDS Start!")


        #그렇다면 넘어가는 로직을 여기다가 잡아놔야 할 것 같음
        # 값을 append에 포함되지 않을 경우, 해당로직을 건너 뛰는 것으로...

        # SDS
        dambo = ''
        damboCount = 0
        task3_print = ''
        task8_print = ''
        dbtask = self.DBConnect("select task from MNT_TARGET_MNG where contract_no = '" + seq + "';")
        task = dbtask[0]['task']
        if task == 'task4' and insured_contractor == insured_person:
            talk.utter.utter = "$task4$"
            self.DBConnect("update MNT_TARGET_MNG set task='task5' where contract_no = '" + seq + "';")
            sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
        
#        if product_code1 == 'Y':
#            damboCount += 1
#        if product_code2 == 'Y':
#            damboCount += 1
#        if product_code3 == 'Y':
#            damboCount += 1

        if task == 'task9' or task == 'task9_1':
#            if product_code1 == 'Y':
#                dambo = dambo + "화재벌금 또는 과실치사상 벌금담보,"
#            if product_code2 == 'Y':
#                dambo = dambo + "일상생활 배상책임담보,"
#            if product_code3 == 'Y':
#                dambo = dambo + "법률비용담보,"
                dambo = dambo + "화재보험 또는 과실치사상 벌금담보, 일상생활 배상책임담보, 법률비용담보,"
            
#            if task == 'task9' and damboCount == 0:
#                talk.utter.utter = "$task9$"
#                self.DBConnect("update MNT_TARGET_MNG set task='taskEnd' where contract_no = '" + seq + "';")
#                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
        if task == 'task3':
            if  damboCount == 1:
               # task3_print = task3_print + "이어서 질문드리겠습니다. 질문은 총 다섯가지입니다."
                task3_print = task3_print + "이어서 질문드리겠습니다. 질문은 총 여섯가지입니다."
            if  damboCount >= 2:
                #task3_print = task3_print + "이어서 질문드리겠습니다. 가입하신 총 "+ self.readNumberHour(damboCount)+"건에 대해 모두 확인해 드리겠습니다. 질문은 총 여섯가지입니다."
                task3_print = task3_print + "이어서 질문드리겠습니다. 질문은 총 여섯가지입니다."
#        if task == 'task8':
#            if  damboCount == 0:
#                task8_print = task8_print + "마지막 질문입니다,"
#            if  damboCount >= 1:
#                pass
        

        #print("===============test=============")

        original_answer = sds_res['response']
        answer_engine = "SDS"
        engine_path.append("SDS")

        #original_answer = self.unknown_answer()
        #첫 SDS 답변 입력사항
        original_answer = sds_res['response']
        talk_res.response.speech.utter = original_answer


        # 현재시간 입력
        next_month = ""
        next_day = ""
        next_part = ""
        next_hour = ""
        next_minute = ""
        #next_time = talk_time
        next_time = datetime.datetime.now()


        #시간 컨펌
        a['nextweek'] = ""
        a['morae'] = ""
        a['tomorrow'] = ""
        a['today'] = ""
        a['day'] = ""
        a['part'] = ""
        a['hour'] = ""
        a['minute'] = ""
        a['input_month'] = ""
        a['input_day'] = ""
        a['input_year'] = ""
        a['input_six'] = ""
        
        
        if sds_res['current_task'] == 'timeAffirm':
            items = sds_res['intent.filled_slots.items']
            print(items)
            for id in items:
                a[id[0]] = id[1]
                print(id)
            if a['input_year'] == "" : input_year = ""
            else: input_year = a['input_year']
            if a['nextweek'] == "" : slot_nextweek = ""
            elif a['nextweek'] == '돌아오는' : slot_nextweek = "다음" 
            else: slot_nextweek = a['nextweek']
            if a['morae'] == "" : slot_morae = ""
            else: slot_morae = a['morae']
            if a['input_month'] == "" : slot_input_month = ""
            else: slot_input_month = a['input_month']
            if a['input_day'] == "" : slot_input_day = ""
            else: slot_input_day = a['input_day']
            if a['tomorrow'] == "" : slot_tomorrow = ""
            else: slot_tomorrow = a['tomorrow']
            if a['today'] == "" : slot_today = ""
            else: slot_today = a['today']
            if a['day'] == "" : slot_day = ""
            else: slot_day = a['day']
            if a['part'] == "" : slot_part = ""
            elif a['part'] == '저녁' or  a['part'] == '밤': slot_part = "오후" 
            elif a['part'] == '아침' or  a['part'] == '새벽': slot_part = "오전" 
            else: slot_part = a['part']
            if a['hour'] == "" : slot_hour = "0"
            else: slot_hour = a['hour']
            if a['minute'] == "" : slot_minute = "0"
            elif a['minute'] == "반": slot_minute = "30"
            else: slot_minute = a['minute']
            ### 시를 수정하는 로직
            print ('첫번쨰 시 출력' + str(slot_hour))
            re_input = str(slot_hour)
            re_input = re_input.replace(' ','')
            re_input = re_input.replace('시','')
            if re_input == "0":
                slot_hour = 0
            else:
#           sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
                slot_hour = self.func.convert_time(re_input)


            print ('두번쨰 시 출력' + str(slot_hour))
#            slot_hour = int(slot_hour)
            ### 분를 수정하는 로직
            re_input = str(slot_minute)
            print ('들어온 분은 : '+ str(slot_minute))
            re_input = re_input.replace(' ','')
            re_input = re_input.replace('분','')
            if re_input == "0":
                slot_minute = 0
            else:
                print('여기에 도착을 했다')
                slot_minute = self.func.convert_time(re_input)
            
            ############ 결과 값 받기
            (next_time, next_month, next_day, next_part, next_hour, next_minute) = \
            self.func.next_call_schedule(slot_nextweek, slot_morae, slot_input_month,\
            slot_input_day, slot_tomorrow, slot_today, slot_day, slot_part, slot_hour, slot_minute)

                
        if sds_res['current_task'] == 'timeAffirm':
            if talk.utter.utter == "$hourMiss$" or talk.utter.utter == "$dayMiss$":
                print("dcdddddddddddddddddddddddddddddddddddd")
                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
            else:
                print("===============test=============")
                print("next_month : " + str(next_month))
                print("next_day : " + str(next_day))
                print("next_part : " + str(next_part))
                print("next_hour : " + str(next_hour))
                print("next_minute : " + str(next_minute))
                print("===============test=============")
                if int(next_minute) == 0 or next_minute == '':
                    talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + self.func.readNumberMinute(next_month) +"월"+ self.func.readNumberMinute(next_day) + "일 " +str(next_part) +", " +  self.func.readNumberHour(next_hour) + "시가 맞습니까?"
                else:
                    talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + self.func.readNumberMinute(next_month) +"월" + self.func.readNumberMinute(next_day) + "일 "+str(next_part) +", " + self.func.readNumberHour(next_hour) + "시 " + self.func.readNumberMinute(next_minute) + "분이 맞습니까?"
                self.DBConnect("update MNT_TARGET_MNG set callback_dt='"+str(next_time)+"' where contract_no = '" + seq + "';")





        #질문 수정사항, 답변 문구 정리 * 예외처리사항에 대해서 모두 정리를 해줘야함
        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
        print ("죄종답변 intent: "+ sds_intent)
        #task1
        if sds_res['current_task'] == 'task1':
            if (sds_intent != 'affirm' and sds_intent != 'negate') and len(talk.utter.utter) >= 15:
                talk_res.response.speech.utter = "통화종료."
                talk_res.response.meta['status'] = 'unConnected'
            elif sds_intent == "again":
                talk_res.response.speech.utter = "다시 말씀드리겠습니다. 마인즈보험 로봇상담원입니다. "+ user_name + ". 고객님 되시나요?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'negate':
                talk_res.response.speech.utter = "죄송합니다, 잘못 연락드렸습니다. 오늘도 행복한 하루 보내세요."
                talk_res.response.meta['status'] = 'unConnected'
            elif sds_intent == 'task1':
                talk_res.response.speech.utter = "상담사를 통해 다시 연락드리겠습니다, 마인즈보험 고객센터 였습니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "안녕하십니까?, 마인즈보험 로봇상담원입니다. " + user_name + ". 고객님 되십니까?;"
            
        #task2
        if sds_res['current_task'] == 'task2':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 가입하신 계약이 정확하게 체결되었는지, 안내해 드리고자 하는데요. 잠시 통화 가능하십니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'task2':
                talk_res.response.speech.utter = "상담사를 통해 다시 연락드리겠습니다, 마인즈보험 고객센터 였습니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "네 고객님, 반갑습니다, 지난 " + self.func.readNumberMinute(join_month) + "월" + self.func.readNumberMinute(join_day) + "일, 저희 마인즈보험 "+prod_name+"을 가입해 주셔서. 진심으로 감사드립니다, 가입하신 계약이 정확하게 체결되었는지, 안내해 드리고자 하는데요 소요시간은 약 삼분정도인데 잠시 통화 가능하십니까?"


        #PRIVACY1
        if sds_res['current_task'] == 'PRIVACY1':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 본인확인을 위해 생년월일을, 년과 월, 일로 말씀해주세요"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "생년월일을, 년과 월. 일로 말씀해주세요"
            elif int(session_count) == 2:
                talk_res.response.speech.utter = "다시 한번 생년월일을, 년과 월. 일로 또박또박 말씀해주세요."
            elif int(session_count) >= 3:
                pass
            elif sds_intent == 'privacy1':
                talk_res.response.speech.utter = "네 죄송합니다 고객님, 상담사를 통해 다시 연락드리겠습니다, 마인즈보험 고객센터 였습니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다. 먼저, 본인확인을 위해 생년월일을,  년과 월, 일로 말씀해주세요"


        if sds_res['current_task'] == 'PRIVACY1_1':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 말씀하신 주민번호는 " + self.func.readNumberMinute(int(input_year))+ "년" + self.func.readNumberMinute(int(input_month))+ "월" + self.func.readNumberMinute(int(input_day))+ "일이,  맞으신가요?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "말씀하신 주민번호는 " + self.func.readNumberMinute(int(input_year))+ "년" + self.func.readNumberMinute(int(input_month))+ "월" + self.func.readNumberMinute(int(input_day))+ "일이, 맞으신가요?"
                
        #PRIVACY2
        if sds_res['current_task'] == 'PRIVACY2':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 고객님의 주소는 " + str(privacy_add1) + ", " + str(privacy_add2) + ". 으로 확인 되는데. 맞으십니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "말씀해주셔서 감사합니다, 고객님의 주소는 " + str(privacy_add1) + ", " + str(privacy_add2) + ". 으로 확인 되는데. 맞으십니까?"
        #PRIVACY3
        if sds_res['current_task'] == 'PRIVACY3':
            talk_res.response.speech.utter = "말씀해주셔서 감사합니다, 고객님의 주소는 " + str(privacy_add1) + " 으로 확인되는데요, 나머지 주소는 어떻게 되십니까?"
        # ask3
        if sds_res['current_task'] == 'task3':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 계약하실 때 계약자 " + insured_contractor + "님께서 청약서, 상품설명서, 개인정보처리 동의서에 직접 서명하셨습니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "확인 감사드립니다," + task3_print +" 계약하실 때 계약자 " + insured_contractor + "님께서 청약서, 상품설명서, 개인정보처리 동의서에 직접 서명하셨습니까?"
        # task4
        if sds_res['current_task'] == 'task4':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 타인의 사망을 보장 해주는 계약의 경우 보험대상자도 반드시 서면동의를해주셔야 하는데요, 피보험자 "+ insured_person +" 님도 직접 서명하셨습니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "타인의 사망을 보장 해주는 계약의 경우 보험대상자도 반드시 서면동의를 해주셔야 하는데요, 피보험자 "+ insured_person +" 님도 직접 서명하셨습니까?"
        if sds_res['current_task'] == 'task5':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 청약서, 약관, 상품설명서를 받으시고, 약관의 중요내용을 충분히 설명 받으셨습니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "청약서, 약관, 상품설명서를 받으시고, 약관의 중요내용을 충분히 설명 받으셨습니까?"
        if sds_res['current_task'] == 'task6':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 약관과 청약서 부본을 전달받지 못 하셨거나, 자필서명을 하지 않은 경우, 그리고 중요사항을 제대로 설명받지 못한 경우에는, 청약일로부터 삼개월 이내에 계약취소를 요청하실수 있다는 내용을 알고 계십니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "그러셨군요, 세번째 질문입니다, 약관과 청약서 부본을 전달받지 못 하셨거나, 자필서명을 하지 않은 경우, 그리고 중요사항을 제대로 설명받지 못한 경우에는. 청약일로부터 삼개월 이내에 계약취소를 요청하실수 있다는 내용을 알고 계십니까?;"
        if sds_res['current_task'] == 'task7':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 직업, 건강상태 등 계약전 알릴의무 사항을 제대로 알리지 않으면 , 향후 보험금 지급이 제한될 수 있는데요, 고객님께서 해당 내용을 정확히 확인하고 작성하셨습니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "직업, 건강상태 등 계약전 알릴의무 사항을 제대로 알리지 않으면 , 향후 보험금 지급이 제한될 수 있는데요, 고객님께서 해당 내용을 정확히 확인하고 작성하셨습니까?"
        # task8
        if sds_res['current_task'] == 'task8':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 중도해지 또는 만기시, 환급금이 납입한 보험료보다 적을 수 있다는 설명을 들으셨습니까?"
            elif sds_intent == 'unknown' or sds_intent == 'nooverlap' or sds_intent == 'overlap' or sds_intent == 'proportion' or sds_intent == 'noproportion':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "중도해지 또는 만기시, 환급금이 납입한 보험료보다 적을 수 있다는 설명을 들으셨습니까?"
        # task9
        if sds_res['current_task'] == 'task9':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 고객님께서 가입하신 담보는 "+ dambo +"으로. 가입한 모든 담보에 대해 중복가입시 보험금을 중복해서 받으실 수 있다고 설명들으신 경우, 중복보상으로 답변을. 실제손해액을 한도로 비례보상 된다고 설명들으신 경우, 비례보상으로 답변주시기 바랍니다"
            elif session_count == 2:
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 고객님께서 가입하신 담보는 "+ dambo +"으로. 가입한 모든 담보에 대해 중복가입시 보험금을 중복해서 받으실 수 있다고 설명들으신 경우, 중복보상으로 답변을. 실제손해액을 한도로 비례보상 된다고 설명들으신 경우, 비례보상으로 답변주시기 바랍니다"
            elif sds_intent == 'unknown' or ((sds_intent == 'affirm' or sds_intent == 'negate') and session_count >= 2):
                talk_res.response.speech.utter = "중복보상, 또는 비례보상으로 답변해주세요."
            else:
                talk_res.response.speech.utter = "마지막 질문입니다, 고객님께서 가입하신 담보는 "+ dambo +"으로. 가입한 모든 담보에 대해 중복가입시 보험금을 중복해서 받으실 수 있다고 설명들으신 경우, 중복보상으로 답변을. 실제손해액을 한도로 비례보상 된다고 설명들으신 경우, 비례보상으로 답변주시기 바랍니다"
        # task9_1
        if sds_res['current_task'] == 'task9_1':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 가입하신 "+ dambo+"은 중복가입시 모두 비례보상됩니다. 한번 더 확인하겠습니다. 담보를 중복으로 가입하셨을 경우, 실제손해액을 한도로 비례보상 된다고 설명 들으셨나요?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "가입하신 "+ dambo+"은 중복가입시 모두 비례보상됩니다. 한번 더 확인하겠습니다. 담보를 중복으로 가입하셨을 경우, 실제손해액을 한도로 비례보상 된다고 설명 들으셨나요?"
        #time        
        if sds_res['current_task'] == 'time':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "통화 가능하신 요일과 시를 말씀해주세요"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "통화 가능하신 요일과 시를 말씀해주세요"
            elif sds_intent == "hourmiss":
                talk_res.response.speech.utter = "통화 가능 요일과 시를 다시 말씀해주세요"
            elif sds_intent == "daymiss":
                talk_res.response.speech.utter = "통화 가능 요일을 말씀해주시지 않았습니다. 통화가능 요일을 말씀해주세요."
            else:
                talk_res.response.speech.utter = "그럼 가능하신 시간을 알려주시면 다시 연락드리겠습니다, 편하신 요일과 시를, 말씀해주세요"
        #time_end       
        if sds_res['current_task'] == 'timeEnd':
            talk_res.response.speech.utter = "네 고객님, 말씀하신 시간에 다시 연락을 드리겠습니다, 마인즈보험 로봇상담원 였습니다, 감사합니다."
            talk_res.response.meta['status'] = 'callback'
        #taskEnd    
        if sds_res['current_task'] == 'taskEnd':
            talk_res.response.speech.utter = "소중한 시간 내주셔서 감사드립니다, 향후 불편하시거나 궁금하신점 있으시면, 담당자나 고객콜센터로 언제든지, 연락주시기 바랍니다, 오늘도 행복한 하루 보내세요."
            talk_res.response.meta['status'] = 'complete'
#

################################################################ 위치 고정 시키기
        self.DBConnect("update MNT_TARGET_MNG set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")
        print("[ANSWER]: " + original_answer)
        self.logger.debug('user utter :: {}, system utter :: {}, call id :: {}, task :: {}'.format(user_utter,
                            talk_res.response.speech.utter, call_id, sds_res['current_task']))
        #print("[SESSION_KEY] :" + )
        print("[ENGINE]: " + answer_engine)

################################################################ 웹 소켓 데이터 전달
        
        
        
        ######## 통화가 갑자기 끊겼을 때, 데이터 전달        
        if call_status == '(null)':
            ####### 중간단계 데이터 전달
            if session_data['seq_id'] == "":
                print("Json 보낼게 없음")
            else:
                json_test = {
                    "EventType":"DETECT",
                    "Event":"result",
                    "No":"" + session_data['seq_id'],
                    "Result":""+ answer_value,
                    "contract_no":""+ seq,
                    }
                print("Json 보낼게 없음")
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
            ######## 종료일떄 데이터 전달
            if sds_res['current_task'] == 'timeEnd' or sds_res['current_task'] == 'taskEnd':
                
                json_test = {
                    "EventType":"DETECT",
                    "Event":"result",
                    "No":"" + seq_id,
                    "Result":"Y",
                    "contract_no":""+ seq,
                    }
                print("Json 보낼게 없음")
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
        else:
            if call_status == 'CS0003':
                sds_res['current_task'] = sds_res['current_task'].replace('Greet','task1')
                seq_id_db = self.DBConnect("select task_info from HC_HH_CAMPAIGN_INFO where camp_id = 3 and task = '" + sds_res['current_task']  + "';")
             #   print (camp_id_db)
                taskNo_name = str(seq_id_db[0]['task_info'])
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0005_"+session_data['seq_id'],
                    "contract_no":""+ seq,
                    "taskNo_name":""+ taskNo_name,
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0005_"+session_data['seq_id']+"', mnt_status_name ='확인필요_"+taskNo_name+"' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0005':
                
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0001",
                    "contract_no":""+ seq,
                    "taskNo_name":"",
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0001', mnt_status_name ='전체완료' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0007':
            
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0004",
                    "contract_no":""+ seq,
                    "taskNo_name":"",
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0004', mnt_status_name ='콜백요청' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0000':
                pass
            else:
                print('콜 값을 전달할 게 없어용')
            if call_status == 'CS0003' or call_status == 'CS0005' or call_status == 'CS0007':
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
################################################################

        
################################################################ 현재 위치 저장
        session_data = talk.session.context
        session_data['seq_id'] = str(seq_id)
        print('**************************' + str(session_data['seq_id']))
        talk_res.response.session_update.context.CopyFrom(session_data)

        #위치 전송
        #self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")

################################################################

        time_post = time.time()
        print("시간의 결과는 : " + str(time_post - time_pre))
        self.logger.debug('end talk function call id :: {}'.format(call_id))
#     
        return talk_res


    def unknown_answer(self):
        """
        리스트에서 랜덤으로 Unknown 답변 출력.
        """
        unknown_text = ['죄송해요, 제가 잘 못 알아 들었어요. 키워드 위주로 다시 질문해주시겠어요?',
                        '답변을 찾을 수 없습니다. 다른 질문을 해주시면 성실히 답변해 드리겠습니다. ']
        return random.choice(unknown_text)


    def Close(self, req, context):
        print 'V3 ', 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        return talk_stat

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()

    def Open(self, req, context):
        print 'V3 ', 'Open', 'called'
        # req = talk_pb2.OpenRequest()
        event_res = talk_pb2.OpenResponse()
        event_res.code = 1000000
        event_res.reason = 'success'
        answer_meta = struct.Struct()

        answer_meta["play1"] = "play1"
        answer_meta["play2"] = "play2"
        answer_meta["play3"] = "play3"

        answer_meta.get_or_create_struct("audio1")["name"] = "media_play1"
        answer_meta.get_or_create_struct("audio1")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio1")["duration"] = "00:10:12"

        answer_meta.get_or_create_struct("audio2")["name"] = "media_play2"
        answer_meta.get_or_create_struct("audio2")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio2")["duration"] = "00:00:15"

        event_res.meta.CopyFrom(answer_meta)
        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        # DO NOTHING
        return event_res

    def Event(self, req, context):
        print 'V3 ', 'Event', 'called'
        # req = talk_pb2.EventRequest()

        event_res = talk_pb2.EventResponse()
        event_res.code = 10
        event_res.reason = 'success'

        answer_meta = struct.Struct()
        answer_meta["meta1"] = "meta_body_1"
        answer_meta["meta2"] = "meta_body_2"
        event_res.meta.CopyFrom(answer_meta)

        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        return event_res

    def digit2txt(self, strNum):
        # 만 단위 자릿수
        tenThousandPos = 4
        # 억 단위 자릿수
        hundredMillionPos = 9
        txtDigit = ['', '십', '백', '천', '만', '억']
        txtNumber = ['', '일', '이', '삼', '사', '오', '육', '칠', '팔', '구']
        txtPoint = '쩜 '
        resultStr = ''
        digitCount = 0
        print(strNum)
        #자릿수 카운트
        for ch in strNum:
            # ',' 무시
            if ch == ',':
                continue
            #소숫점 까지
            elif ch == '.':
                break
            digitCount = digitCount + 1


        digitCount = digitCount-1
        index = 0

        while True:
            notShowDigit = False
            ch = strNum[index]
            #print(str(index) + ' ' + ch + ' ' +str(digitCount))
            # ',' 무시
            if ch == ',':
                index = index + 1
                if index >= len(strNum):
                    break;
                continue

            if ch == '.':
                resultStr = resultStr + txtPoint
            else:
                #자릿수가 2자리이고 1이면 '일'은 표시 안함.
                # 단 '만' '억'에서는 표시 함
                if(digitCount > 1) and (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos) and int(ch) == 1:
                    resultStr = resultStr + ''
                elif int(ch) == 0:
                    resultStr = resultStr + ''
                    # 단 '만' '억'에서는 표시 함
                    if (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos):
                        notShowDigit = True
                else:
                    resultStr = resultStr + txtNumber[int(ch)]


            # 1억 이상
            if digitCount > hundredMillionPos:
                if not notShowDigit:
                    resultStr = resultStr + txtDigit[digitCount-hundredMillionPos]
            # 1만 이상
            elif digitCount > tenThousandPos:
                if not notShowDigit:
                    resultStr = resultStr + txtDigit[digitCount-tenThousandPos]
            else:
                if not notShowDigit:
                    resultStr = resultStr + txtDigit[digitCount]

            if digitCount <= 0:
                digitCount = 0
            else:
                digitCount = digitCount - 1
            index = index + 1
            if index >= len(strNum):
                break;
        return resultStr

def serve():
    parser = argparse.ArgumentParser(description='CMS DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(
        EchoDa(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
