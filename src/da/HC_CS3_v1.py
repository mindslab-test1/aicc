#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from concurrent import futures
import argparse
import grpc
import os
import random
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
from google.protobuf.json_format import MessageToJson
import time
import syslog
import pymysql
import datetime
import re
import json
import websocket
from websocket import create_connection
import pdb
import traceback
from lib import logger

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da.v3 import talk_pb2
from maum.m2u.facade import front_pb2

# Custom import
#from qa.util import *
from qa import basicQA
from custom_hs.sds import SDS
from qa.util import Util
from custom_hs.HC_Func import FUNC
from cfg import config

# echo_simple_classifier = {
#    "echo_test": {
#        "regex": [
#            "."
#        ]
#    }

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
Session_Value = []
_NOTHING_FOUND_INTENT_ = "의도를 찾지 못했습니다."
model = 'HC_CS3'

#def setSessionValue(session_id, key, value):
#    try:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        findIndex = sessionList.index(session_id)
#        Session_Value[findIndex][key] = value
#    except Exception as err:
#        pass
#
#def getSessionValue(session_id, key):
#    keyValue = None
#    isNew = False
#
#    if len(Session_Value) > 0:
#        sessionList = [x.get("session_id") for x in Session_Value]
#        try:
#            findIndex = sessionList.index(session_id)
#            keyValue = Session_Value[findIndex][key]
#        except Exception as err:
#            isNew = True
#    else:
#        isNew = True
#
#    if isNew:
#        Session_Value.append({"session_id" : session_id, "model" : ""})
#        keyValue = Session_Value[len(Session_Value)-1][key]
#
#    return keyValue
#

class EchoDa(talk_pb2_grpc.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'control'
    provider.description = 'control intention return DA'
    provider.version = '0.1'
    provider.single_turn = True
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # PARAMETER

    def __init__(self):
        syslog.syslog('init')
        self.state = provider_pb2.DIAG_STATE_IDLE
        syslog.syslog(str(self.state))
        self.qa_util = Util()
        self.Sds = SDS()
        self.func = FUNC()
        self.logger = logger.get_timed_rotating_logger(
            logger_name='DA',
            log_dir_path='/srv/maum/logs/',
            log_file_name='HC_CS3.log',
            backup_count=3,
            log_level='debug'
        )
        self.logger.debug("="*100)
        self.logger.debug("1. DA V3 Server Constructor")
        try:
            self.ws = create_connection("ws://10.122.64.152:13254/callsocket")
        except:
            self.logger.error('error occur message :: {}'.format(traceback.format_exc()))

    #
    # INIT or TERM METHODS
    #

    def IsReady(self, empty, context):
        print 'V3 ', 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'V3 ', 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'V3 ', 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, empty, context):
        print 'V3 ', 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = userattr_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        loc.title = '기본 지역'
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '기본으로 조회할 지역을 지정해주세요.'
        attrs.append(loc)

        device = userattr_pb2.UserAttribute()
        device.name = 'device'
        device.title = '기본 디바이스'
        device.type = userattr_pb2.DATA_TYPE_STRING
        device.desc = '기본으로 사용할 디바이스를 지정해주세요.'
        attrs.append(device)

        country = userattr_pb2.UserAttribute()
        country.name = 'time'
        country.title = '기준 국가 설정'
        country.type = userattr_pb2.DATA_TYPE_STRING
        country.desc = '기본으로 조회할 국가를 지정해주세요.'
        attrs.append(country)

        result.attrs.extend(attrs)
        return result

    #
    # PROPERTY METHODS
    #

    def GetProviderParameter(self, empty, context):
        print 'V3 ', 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'V3 ', 'GetRuntimeParameters', 'called'
        result = provider_pb2.RuntimeParameterList()
        params = []

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '171.64.122.134'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_INT
        db_port.desc = 'Database Port'
        db_port.default_value = '7701'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'minds'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_AUTH
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'minds67~'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'ascar'
        db_database.required = True
        params.append(db_database)

        result.params.extend(params)
        return result
    def OpenSession(self, request, context):
        print "openSession"

        #param = {}
        #bizRes = {}
        lectureInfo = {}
        resMessage = 'success'
        meta = ''
        lectureNum = ''
        session_id = request.session.id
        #self.showKeyValue(context.invocation_metadata())

        if 'meta' in request.utter.meta:
             #meta = eval(request.utter.meta['meta'].replace('null','\"\"'))
             meta = request.utter.meta['meta']

             if 'intent' in meta:
                slots = meta['intent']['slots']
                if 'lectureNumber' in slots:
                    lectureNum = slots['lectureNumber']['value']

        #requestParam = Common.setMetaToParamMap(lectureNum=lectureNum,userTalk=' ' ,request=request, isopenRequest=True,session_id=session_id)
        localSessionObj = session_id
        print 'OpenSession id: '+str(request.session.id)

        result = talk_pb2.TalkResponse()

        res_meta = struct.Struct()
        #res_meta['response'] = bizRes
        result.response.meta.CopyFrom(res_meta)

        #session 정보 ,session data 10k
        result.response.session_update.id = session_id
        res_context = struct.Struct()
        res_context['session_data'] = str(lectureInfo)
        result.response.session_update.context.CopyFrom(res_context)

        ######### 세션 카운트 지정
#        logic_count = (
#                {
#                    "task1" : "0",
#                    "task2" : "0",
#                    "task3" : "0",
#                    "task4" : "0",
#                    "task5" : "0",
#                    "task6" : "0",
#                    "task7" : "0",
#                    "task8" : "0",
#                    "task9" : "0",
#                    "task9_1" : "0",
#                    "time" : "0",
#                    "timeaffirm" : "0",
#                    "PRIVACY1" : "0",
#                    "PRIVACY1_1" : "0",
#                    "privacy1_1affirm" : "0",
#                    "PRIVACY2" : "0"
#                }
#            )
        
        session_data = struct.Struct()
        session_data['id'] = session_id
        session_data['db_count'] = '0'
        session_data['seq_id'] = ''
        session_data['task1'] = '0'
        session_data['task2'] = '0'
        session_data['task3'] = '0'
        session_data['task4'] = '0'
        session_data['task4_1'] = '0'
        session_data['task5'] = '0'
        session_data['task6'] = '0'
        session_data['task7'] = '0'
        session_data['task7_1'] = '0'
        session_data['task8'] = '0'
        session_data['task9'] = '0'
        session_data['task9_1'] = '0'
        session_data['time'] = '0'
        session_data['timeaffirm'] = '0'
        session_data['PRIVACY1'] = '0'
        session_data['privacy1_1'] = '0'
        session_data['privacy1_1affirm'] = '0'
        session_data['PRIVACY2'] = '0'
        session_data['money'] = '0'
        session_data['bank_name'] = ''
        session_data['bank_acc_num'] = ''
        session_data['interest_rate'] = '0'
        session_data['interest_rate_han'] = ''
        session_data['bank_acc_han'] = ''
        session_data['price_han'] = ''
        session_data['transfer'] = '0'
        session_data['jumin_han'] = ''
        session_data['call_status'] = '0'
        result.response.session_update.context.CopyFrom(session_data)

        print 'OpenSession_'
        return result

    def OpenSkill(self, request, context):
        print "OpenSkill start"
        print 'Open request: '+str(request)
        session_id = request.session.id
        print 'open_session_data: '+ str(session_id)+', '+str(context)
        result = talk_pb2.TalkResponse()
        print 'OpenSkill end'
        return result


    def CloseSkill(self, request, context):

        result = talk_pb2.CloseSkillResponse()
        return result

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()


    def DBConnect(self, query):
        conn = pymysql.connect(user=config.DATABASE_CONFIG['user'],
                               password=config.DATABASE_CONFIG['passwd'],
                               host=config.DATABASE_CONFIG['host'],
                               database=config.DATABASE_CONFIG['name'],
                               charset=config.DATABASE_CONFIG['charset'],
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        curs.execute(query)
        print("query good!")
        rows = curs.fetchall()

        print(rows)
        curs.execute("commit;")
        curs.close()
        conn.close()
        return rows

    def db_dml(self, query, sql_args):
        conn = pymysql.connect(user=config.DATABASE_CONFIG['user'],
                               password=config.DATABASE_CONFIG['passwd'],
                               host=config.DATABASE_CONFIG['host'],
                               database=config.DATABASE_CONFIG['name'],
                               charset=config.DATABASE_CONFIG['charset'],
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)
        #query = "select * from test;"
        print(query)
        try:
            curs.executemany(query, sql_args)
        except:
            conn.rollback()
        else:
            conn.commit()
        finally:
            print("query good!")
            curs.close()
            conn.close()

    def send_ws(self, json_data, call_id):
        try:
            self.ws.send(json_data)
            self.logger.debug('send to socket server data :: {}, call id :: {} '.format(json_data, call_id))
        except:
            self.logger.error('error occur message :: {}, call id :: {}'.format(traceback.format_exc(), call_id))


    def Talk(self, talk, context):
        ########################################### 콜 status 체크 및 첫 시작할때 정보 전달 ###########
        self.logger.debug('called talk function, request data :: {}'.format(talk))
        try:
            call_status = talk.utter.meta['status'] 
            seq = talk.utter.meta['contract_no']
            call_id = talk.utter.meta['call_id'] 
            user_utter = talk.utter.utter
        except:
            call_status = ''
            seq = '66' 
            call_id =  '1368'
            user_utter = talk.utter.utter
            self.logger.error('error occur message :: {}'.format(traceback.format_exc()))

        talk_res = talk_pb2.TalkResponse()
        a = {}

        self.logger.debug('set utter {}, seq :: {}, call_id :: {}, call_status :: {}'.format(user_utter, 
                            seq, call_id, call_status))
        print("talk : ", talk.utter.utter)
        print("seq : ,", str(seq) + "callid : ," + str(call_id))

        ################################################################################################### 시작할 때 정보 전달
        session_data = talk.session.context
        if call_status == 'CS0000':
        
            json_test = {
                "EventType":"CALL",
                "Event":"mntresult",
                "call_id":"" + call_id,
                "call_status":"MR0003",
                "contract_no":""+ seq,
                "taskNo_name":"",
                }
            jsonString = json.dumps(json_test)
            print(jsonString)
    #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
            print("Json send")
            self.send_ws(jsonString, call_id)
            print("send complete")


    #        result =  ws.recv()
    #        print("Received '%s'" % result)
    #        ws.closeo()
            #pdb.set_trace()
            self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0003', mnt_status_name ='진행중' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")





        #########################################################################################################


        db_count2 = self.DBConnect("select count(seq_num) from HC_HH_CAMPAIGN_SCORE where contract_no = '" + seq + "' and call_id = '"+call_id+"';")
        db_count2 = db_count2[0]['count(seq_num)']



        session_data = talk.session.context
        db_count = int(session_data['db_count'])
        print('하하잉' + str(db_count))
        if db_count == 0 and db_count2 == 0:
            task_list = ['task1','task2','task3','task4','task4_1', 'task5', 'task6', 'task7','task7_1', 'task8', 'task9','taskEnd']
            sql_args = list()
            for i in task_list:
                seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id=5 and task = '" + i + "';")
                #print (camp_id_db)
                seq_id = str(seq_id_db[0]['seq'])
                sql_args.append((call_id, seq, seq_id, i))
            print("*" * 80)
            print(sql_args)
            print("*" * 80)
            self.db_dml("insert into HC_HH_CAMPAIGN_SCORE (call_id, contract_no, info_seq, info_task, task_value, review_coment) values ( %s, %s, %s, %s, null, null);", sql_args)
            self.logger.debug('done update campaign score table, call id :: {}'.format(call_id))
            session_data['db_count'] = 1
            talk_res.response.session_update.context.CopyFrom(session_data)
        elif db_count == 1:
            pass
            

        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        print("talk : ", talk.utter.utter)
        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
       
        # SDS.dp.slots["product_code"] = "A"
        session_id = talk.session.id
        dbsession = self.DBConnect("select \
        cust_tel_no,\
        cust_nm,\
        create_dt,\
        insured_contractor,\
        insured_person,\
        addr_main,\
        addr_sub,\
        addr_code,\
        jumin_no, \
        prod_name \
        from MNT_TARGET_MNG where contract_no = '" + seq + "';")
        # cust_nm 통화자(가입자)
        # join_time 전체 시간 호출
        # join_month 가입 월
        # join_day 가입 일
        # insurance_contractor 가입 계약자
        # insurance_insured 가입 피보험자
        # insurance_closeproduct 무해지상품 가입여부
        # address_main 메인주소
        # address_sub 세부주소
        
        phone = dbsession[0]['cust_tel_no']
        user_name = dbsession[0]['cust_nm']

        join_time = dbsession[0]['create_dt']
        print ("join_time : " + str(join_time))
        join_strp = datetime.datetime.strptime(str(join_time), '%Y-%m-%d %H:%M:%S')
        join_month = join_strp.month
        join_day = join_strp.day


        insured_contractor = dbsession[0]['insured_contractor']
        insured_person = dbsession[0]['insured_person']
        privacy_add1 = dbsession[0]['addr_main']
        privacy_add2 = dbsession[0]['addr_sub']
        privacy_add3 = dbsession[0]['addr_code']
#        product_code1 = dbsession[0]['product_code1']
#        product_code2 = dbsession[0]['product_code2']
#        product_code3 = dbsession[0]['product_code3']
        prod_name = dbsession[0]['prod_name']
        prod_name = '무배당 게속받는 암보험'
        cust_ssn = dbsession[0]['jumin_no']
#        camp_id = dbsession[0]['camp_id']
        cust_ssn = cust_ssn[0:6]




        #question = talk.utter.utter
        meta = dict()
        #meta['seq_id'] = util.time_check(0)
        meta['log_type'] = 'SVC'
        meta['svc_name'] = 'DA'

        #output = ""
        original_answer = ""
        engine_path = list()
        answer_engine = "None"
        status_code = ""
        status_message = ""
        flag = False
        weight = 0
        code = 'None'
        sds_intent = ""
        unknown_count = 0

#        # SDS
        dbtask = self.DBConnect("select task from MNT_TARGET_MNG where contract_no = '" + seq + "';")
        task = dbtask[0]['task']
        talk.utter.utter = talk.utter.utter.replace(' ','') 
        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
#        if sds_res['current_task'] == 'task2':
#            dic = dict(talk.session.context)
#            dic['task1'] = '5555'
#
#            print(dic)
#            session_data = struct.Struct()
#            session_data['id'] = talk.session.id
#            session_data = dic
#        if sds_res['current_task'] == 'task2':
#            session_data = talk.session.context
#            session_data['id'] = talk.session.id
#            session_data['task1'] = '5555'
#            print(session_data)
#            talk_res.response.session_update.context.CopyFrom(session_data)
#        elif sds_res['current_task'] == 'task3':
#            print("$" * 70)
#            print(dict(talk.session.context))
#            print("$" * 70)
##            count_dic = json.loads(dict(talk.session.context).get('count'))
#            dic = json.loads(MessageToJson(talk.session.context))
#            print(dic)
#            count_dic = dic.get('count')[0].get('task1')
##            print(count_dic[0].get('task1'))
#            print(count_dic)
        a['eogwon'] = ""
        a['manwon'] = ""
        a['input_day'] = ""
        

        if sds_res['current_task'] == 'task1':
            bank_acc_num_replace = ''
            interest_rate_replace = ''
            price_replace = ''
            jumin_no = ''

            bank_info = self.DBConnect("select \
            bank_name,\
            bank_acc_num \
            from MNT_TARGET_MNG where contract_no = '" + seq + "';")
            
            bank_name = bank_info[0]['bank_name']
            bank_acc_num = bank_info[0]['bank_acc_num']

            loan_info = self.DBConnect("select \
            loan_name,\
            price, \
            interest_rate \
            from HC_HH_LOAN_LIST where contract_no = '" + seq + "';")
            
            loan_name = loan_info[0]['loan_name']
            price = loan_info[0]['price']
            interest_rate = loan_info[0]['interest_rate']
            interest_rate = str(interest_rate)
            
            
            target_info = self.DBConnect("select \
            jumin_no\
            from MNT_TARGET_MNG where contract_no = '" + seq + "';")
            jumin_no = target_info[0]['jumin_no']
            jumin1 = self.func.readNumberMinute(jumin_no[0:2])
            jumin2 = self.func.readNumberMinute(jumin_no[2:4])
            jumin3 = self.func.readNumberMinute(jumin_no[4:6])
            session_data['jumin_han'] = ""+jumin1+"년,"+jumin2+"월,"+jumin3+"일"
            

            print('interest_rate : ' + interest_rate)

            session_data = talk.session.context
            session_data['bank_name'] = bank_name
            session_data['bank_acc_num'] = bank_acc_num
            session_data['interest_rate'] = str(interest_rate)
            
            print('@' * 30)
            print('bank_name : ' + bank_name)
            print('bank_acc_num : ' + bank_acc_num)
            print('interest_rate : ' + interest_rate)
            
            for i in bank_acc_num:
                if i == '-':
                    i = i.replace('-',',다시,')
                    bank_acc_num_replace += i
                else:
                    bank_acc_num_replace += self.func.readNumberMinute(i)
                    
            session_data['bank_acc_han'] = bank_acc_num_replace

            for i in interest_rate:
                if i == '.':
                    i = i.replace('.',',쩜,')
                    interest_rate_replace += i
                else:
                    interest_rate_replace += self.func.readNumberMinute(i)
                    

            session_data['interest_rate_han'] = interest_rate_replace
            session_data['price_han'] = self.func.readNumberMinute(price)

            print('interrest_rate_han : ' + interest_rate_replace)
            print('bank_acc_han : ' + bank_acc_num_replace)
            print('price_han : ' + session_data['price_han'])
            print('@' * 30)
            talk_res.response.session_update.context.CopyFrom(session_data)

        answer_value = ''
##################################################task2 로직
           
        if sds_res['current_task'] == 'task2':
            seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
                #print (camp_id_db)
            seq_id = str(seq_id_db[0]['seq'])

            slot_eogwon = 0
            slot_manwon = 0
            items = sds_res['intent.filled_slots.items']
            print(items)
            for id in items:
                a[id[0]] = id[1]
                print(id)
            if a['eogwon'] == "": pass
            else: slot_eogwon = a['eogwon']
            if a['manwon'] == "" : pass
            else: slot_manwon = a['manwon']

            print('inputengwon:' + str(slot_eogwon))
            print('inputmanwon:' + str(slot_manwon))
            talk.utter.utter = talk.utter.utter.replace(' ','') 


            sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
            session_data = talk.session.context
#            session_count = int(session_data['task3'])
            session_money = int(session_data['money'])
## eog - 억, manwon - 만원, Variance 이율변동 문의, 
            if sds_intent == 'money':
                try:
                    if slot_eogwon != 0:
                        slot_eogwon = int(self.func.convert_time(slot_eogwon)) * 10000
                    if slot_manwon != 0:
                        slot_manwon = int(self.func.convert_time(slot_manwon))
                    total_money = slot_eogwon + slot_manwon
                except Exception as e:
                    import traceback as tb; tb.print_exc()
                    print (e)
                print(type(total_money))
                print('금액은? : ' + str(total_money))
                
#                session_count = session_count + 1
                session_data['money'] = total_money
                print('**************************' + str(session_data['money']))
                talk_res.response.session_update.context.CopyFrom(session_data)


                print('금액통과')
                if total_money > 0 and total_money < 101:
                    print('금액통과')
                    talk.utter.utter = "$task2$"
                    answer_value = '입력'
#                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
#                    #print (camp_id_db)
#                    seq_id = str(seq_id_db[0]['seq'])
                else:
                    print('금액불통과')
#                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
#                    #print (camp_id_db)
#                    seq_id = str(seq_id_db[0]['seq'])
                    sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
                    session_data = talk.session.context
                    session_count = int(session_data['task2'])
                    session_count = session_count + 1
                    session_data['task2'] = session_count
                    print('**************************1+' + str(session_data['task2']))
                    talk_res.response.session_update.context.CopyFrom(session_data)
                    if int(session_count) >= 2:
                        talk.utter.utter = '$callback$'
                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
            else:
#                seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
#                #print (camp_id_db)
#                seq_id = str(seq_id_db[0]['seq'])
                sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
                session_data = talk.session.context
                session_count = int(session_data['task2'])
                session_count = session_count + 1
                session_data['task2'] = session_count
                print('**************************2+' + str(session_data['task2']))
                talk_res.response.session_update.context.CopyFrom(session_data)
                if int(session_count) >= 2:
                    talk.utter.utter = '$callback$'
                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                    
        if sds_res['current_task'] == 'task5':
            seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
            seq_id = str(seq_id_db[0]['seq'])
            talk.utter.utter = talk.utter.utter.replace(' ','') 
            slot_transfer = 0
            slot_manwon = 0
            items = sds_res['intent.filled_slots.items']
            print(items)
            for id in items:
                a[id[0]] = id[1]
                print(id)
            if a['input_day'] == "":
                pass
            else:
                slot_transfer = a['input_day']
                slot_transfer = slot_transfer.replace('일','')

            print('inputransfer:' + str(slot_transfer))


            sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
            session_data = talk.session.context
            session_money = int(session_data['transfer'])
            if sds_intent == 'time':
                try:
                    if slot_transfer != 0:
                        slot_transfer = int(self.func.convert_time(slot_transfer))
                except Exception as e:
                    import traceback as tb; tb.print_exc()
                    print (e)
                print(type(slot_transfer))
                print('입력 일자는? : ' + str(slot_transfer))
                
#                session_count = session_count + 1
                session_data['transfer'] = int(slot_transfer)
                print('**************************' + str(session_data['transfer']))
                talk_res.response.session_update.context.CopyFrom(session_data)


                if session_data['transfer'] != 5 and session_data['transfer'] != 10 and session_data['transfer'] != 20 and session_data['transfer'] != 25:
                    talk.utter.utter = "모르겠어요"
                else:
                    talk.utter.utter = "$task5$"
                    answer_value = '입력'
            else:
                talk.utter.utter = "모르겠어요"
                


        if sds_res['current_task'] == 'task3':
            print('**************************' + str(session_data))

        task_list = ['task1','task3','task4','task4_1','task5','PRIVACY1','PRIVACY2', 'task6', 'task7','task7_1', 'task8', 'task9','task10' ,'task11', 'time', 'timeAffirm']
        for i in task_list:
            if sds_res['current_task'] == i:
                seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
                seq_id = str(seq_id_db[0]['seq'])
                sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
                session_data = talk.session.context
                session_count = int(session_data[i])
                if sds_intent == 'unknown' or sds_intent == 'again':
                    session_count = session_count + 1
                    session_data[i] = session_count
                    print('**************************' + str(session_data[i]))
                    talk_res.response.session_update.context.CopyFrom(session_data)
                    if int(session_count) >= 2:
                        talk.utter.utter = "$callback$"
                        self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '모름' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                        answer_value = '모름'
                        self.DBConnect("update MNT_TARGET_MNG set task='"+sds_res['current_task']+"' where contract_no = '" + seq + "';")
                   
                        sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
                else:
                    print('여기로 들어왔음')
                    seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task'] + "';")
                    #print (camp_id_db)
                    seq_id = str(seq_id_db[0]['seq'])
                    if sds_intent == 'affirm':
                        if i == 'task4_1':
                            talk.utter.utter = "$callback$"
                        else:
                            talk.utter.utter = "$"+ sds_res['current_task'] +"$"
                            
                        self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'Y' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                        answer_value = 'Y'
                    elif sds_intent == 'negate':
                        if i == 'task4':
                            talk.utter.utter = "$task4negate$"
                        elif i == 'task7':
                            talk.utter.utter = "$task7negate$"
                        elif i == 'task4_1':
                            talk.utter.utter = "$task4_1$"
                        self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'N' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                        answer_value = 'N'
                    else:
                        if i == 'time':
                            self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '입력' where contract_no = '" +seq+ "' and info_task = '"+ sds_res['current_task'] +"';")
                            answer_value = '입력'
                    sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
            

        


        #sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)

        #self.DBConnect("update test set task='" + sds_res['current_task'] + "' where contract_no = '" + seq + "';")
        sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
        print("sds_intent의 값 : " + str(sds_intent))

        # 결과값 DB에 저장하는 방식
#        b = []
#        b.append("time")
#        b.append("timeAffirm")
#        b.append("timeEnd")
#        for i in range(1, 15):
#            b.append("task" + str(i))
#        for i in range(1, 4):
#            b.append("PRIVACY" + str(i))
#
#
#        if task in b:
#            seq_id_db = self.DBConnect("select seq from HC_HH_CAMPAIGN_INFO where camp_id = 2 and task = '" + task  + "';")
#            print (camp_id_db)
#            seq_id = str(seq_id_db[0]['seq'])
#            if sds_intent == 'affirm':
#                self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'Y' where contract_no = '" +seq+ "' and info_task = '+ task +"';")
#            elif sds_intent == 'negate':
#                self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = 'N' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
#            elif sds_intent == 'overlap' or sds_intent == 'noproportion':
#                self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '중복' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
#            elif sds_intent == 'nooverlap' or sds_intent == 'proportion':
#                self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '비례' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
#            elif sds_intent == 'unknown' or sds_intent == 'again':
#                pass
#            elif unknown_count == 1:
#                pass
#            else:
#                if task == 'PRIVACY1' or task == 'time':
#                    self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task_value = '입력' where contract_no = '" +seq+ "' and info_task = '"+ task +"';")
#
#        
#        ##위치 고정 시키기
#        self.DBConnect("update MNT_TARGET_MNG set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")

        print("SDS Start!")

        original_answer = sds_res['response']
        answer_engine = "SDS"
        engine_path.append("SDS")

        #original_answer = self.unknown_answer()
        #첫 SDS 답변 입력사항
        original_answer = sds_res['response']
        talk_res.response.speech.utter = original_answer

        #시간 테스트
        #talk_time = dbsession[0]['talk_time']
        #talk_strp = datetime.datetime.strptime(talk_time, '%Y-%m-%d %H:%M:%S')
        #talk_month = talk_strp.month
        #talk_day = talk_strp.day
        #talk_hour = talk_strp.hour
        #talk_minute = talk_strp.minute



       #시간 컨펌
#        a['nextweek'] = ""
#        a['morae'] = ""
#        a['tomorrow'] = ""
#        a['today'] = ""
#        a['day'] = ""
#        a['part'] = ""
#        a['hour'] = ""
#        a['minute'] = ""
#        a['input_month'] = ""
#        a['input_day'] = ""
#        a['input_year'] = ""
#        a['input_six'] = ""
#
#        if sds_res['current_task'] == 'timeAffirm':
#            items = sds_res['intent.filled_slots.items']
#            print(items)
#            for id in items:
#                a[id[0]] = id[1]
#                print(id)
#            if a['input_year'] == "" : input_year = ""
#            else: input_year = a['input_year']
#            if a['nextweek'] == "" : slot_nextweek = ""
#            elif a['nextweek'] == '돌아오는' : slot_nextweek = "다음" 
#            else: slot_nextweek = a['nextweek']
#            if a['morae'] == "" : slot_morae = ""
#            else: slot_morae = a['morae']
#            if a['input_month'] == "" : slot_input_month = ""
#            else: slot_input_month = a['input_month']
#            if a['input_day'] == "" : slot_input_day = ""
#            else: slot_input_day = a['input_day']
#            if a['tomorrow'] == "" : slot_tomorrow = ""
#            else: slot_tomorrow = a['tomorrow']
#            if a['today'] == "" : slot_today = ""
#            else: slot_today = a['today']
#            if a['day'] == "" : slot_day = ""
#            else: slot_day = a['day']
#            if a['part'] == "" : slot_part = ""
#            elif a['part'] == '저녁' or  a['part'] == '밤': slot_part = "오후" 
#            elif a['part'] == '아침' or  a['part'] == '새벽': slot_part = "오전" 
#            else: slot_part = a['part']
#            if a['hour'] == "" : slot_hour = "0"
#            else: slot_hour = a['hour']
#            if a['minute'] == "" : slot_minute = "0"
#            elif a['minute'] == "반": slot_minute = "30"
#            else: slot_minute = a['minute']
#            ### 시를 수정하는 로직
#            print ('첫번쨰 시 출력' + str(slot_hour))
#            re_input = str(slot_hour)
#            re_input = re_input.replace(' ','')
#            re_input = re_input.replace('시','')
##           sds_intent = self.Sds.GetIntent(talk.utter.utter, model)
#            if int(re_input) == 0:
#                slot_hour = 0
#            else:
#                slot_hour = self.func.convert_time(re_input)
#
#
#            print ('두번쨰 시 출력' + str(slot_hour))
##            slot_hour = int(slot_hour)
#            ### 분를 수정하는 로직
#            re_input = str(slot_minute)
#            print ('들어온 분은 : '+ str(slot_minute))
#            re_input = re_input.replace(' ','')
#            re_input = re_input.replace('분','')
#            if int(re_input) == 0:
#                slot_minute = 0
#            else:
#                slot_minute = self.func.convert_time(re_input)
#            
#            ############ 결과 값 받기
#            (next_time, next_month, next_day, next_part, next_hour, next_minute) = \
#            self.func.next_call_schedule(slot_nextweek, slot_morae, slot_input_month,\
#            slot_input_day, slot_tomorrow, slot_today, slot_day, slot_part,slot_hour, slot_minute)
#
#
#
#        if sds_res['current_task'] == 'timeAffirm':
#            if talk.utter.utter == "$hourMiss$" or talk.utter.utter == "$dayMiss$":
#                print("dcdddddddddddddddddddddddddddddddddddd")
#                sds_res = self.Sds.Talk(talk.utter.utter, session_id, model)
#            else:
#                print("===============test=============")
#                print("next_month : " + str(next_month))
#                print("next_day : " + str(next_day))
#                print("next_part : " + str(next_part))
#                print("next_hour : " + str(next_hour))
#                print("next_minute : " + str(next_minute))
##                print("===============test=============")
#                if int(next_minute) == 0 or next_minute == '':
#                    talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + self.func.readNumberMinute(next_month) +"월"+ self.func.readNumberMinute(next_day) + "일 "+str(next_part) +", " +  self.func.readNumberHour(next_hour) + "시가 맞습니까?"
#                else:
#                    talk_res.response.speech.utter = "말씀하신 통화가능 시간이 " + self.func.readNumberMinute(next_month) +"월"+ self.func.readNumberMinute(next_day) + "일 "+str(next_part) +", " + self.func.readNumberHour(next_hour) + "시 "+ self.func.readNumberMinute(next_minute) + "분이 맞습니까?"
#                    
#                self.DBConnect("update MNT_TARGET_MNG set collback_dt='"+str(next_time)+"' where contract_no = '" + seq + "';")

        print ("죄종답변 intent: "+ sds_intent)
        #task1
        if sds_res['current_task'] == 'task1':
            if (sds_intent != 'affirm' and sds_intent != 'negate') and len(talk.utter.utter) >= 15:
                talk_res.response.speech.utter = "통화종료."
                talk_res.response.meta['status'] = 'unConnected'
            elif sds_intent == "again":
                talk_res.response.speech.utter = " 마인즈 보험 AI 상담사입니다, 보험 계약 대출 진행해드릴까요?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요"
            elif sds_intent == 'negate':
                talk_res.response.speech.utter = "말씀 감사합니다, 오늘도 행복한 하루 보내세요."
                talk_res.response.meta['status'] = 'unConnected'
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "상담사를 통해 다시 연락드리겠습니다. 마인즈보험 고객센터 였습니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "연결해주셔서 감사합니다, 마인즈 보험 AI 상담사입니다. 보험 계약 대출 진행해드릴까요?"
            
        #task2
        if sds_res['current_task'] == 'task2':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "대출 신청 금액을 말씀해주세요. 백만원 이하까지 가능합니다."
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "대출 신청 금액을 말씀해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "상담사를 통해 다시 연락드리겠습니다. 마인즈보험 고객센터 였습니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "고객님께서 대출 가능한 금액은 총 한개. 마인즈 보험에서 연이율  "+session_data['interest_rate_han']+"퍼센트로 "+session_data['price_han']+"원 신청가능하며. 이율은 변동될수 있습니다. 대출 신청 금액은 얼마입니까?"
                

#        #PRIVACY1
#        if sds_res['current_task'] == 'PRIVACY1':
#            if sds_intent == 'again':
#                talk_res.response.speech.utter = "고객님의 성함이, "+ user_name + "고객님 맞으십니까?"
#            elif sds_intent == 'unknown':
#                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
#            elif sds_intent == 'negate':
#                talk_res.response.speech.utter = "죄송합니다만, 담당자와 통화 후, \
#                다시한번 연락을 드리겠습니다, 번거롭게 해드려 죄송합니다. $callback$"
#            elif sds_intent == 'privacy':
#                talk_res.response.speech.utter = "죄송합니다만, 담당자와 통화 후, \
#                다시한번 연락을 드리겠습니다, 번거롭게 해드려 죄송합니다. $callback$"
#            else:
#                talk_res.response.speech.utter = "지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다. "\
#                + user_name + "고객님 맞으십니까"


        # task3
        if sds_res['current_task'] == 'task3':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "대출 금액"+self.func.readNumberMinute(int(session_data['money']))+" 만원으로 신청하는게 맞으시죠?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'negate':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 일반상담으로 연결해드리겠습니다."
                talk_res.response.meta['status'] = 'callback'
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "네, 확인하겠습니다. 대출 금액 "+ self.func.readNumberMinute(int(session_data['money'])) + "만원으로 신청하시는게 맞으시죠?."

        # task4
        if sds_res['current_task'] == 'task4':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "입금 받으실 방법은 고객님 거래계좌로 가능하며. "+session_data['bank_name']+"은행 "+session_data['bank_acc_han']+" 계좌로 입금가능합니다. 괜찮으십니까?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            else:
                talk_res.response.speech.utter = "네 확인되었습니다. 입금 받으실 방법은 고객님 거래계좌로 가능하며. "+session_data['bank_name']+"은행 "+session_data['bank_acc_han']+" 계좌로 입금가능합니다. 괜찮으십니까?"
        # task4_1
        if sds_res['current_task'] == 'task4_1':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "전화연결을 통한 대출진행의 경우. 최근 1년간 당사와 입출금 거래가 확인된 계좌정보로 대출진행이 가능합니다. 대출 신청을 중단하시겠습니까?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다.ack$"
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "고객님, 전화연결을 통한 대출진행의 경우. 최근 1년간 당사와 입출금 거래가 확인된 계좌정보로 대출진행이 가능합니다. 대출 신청을 중단하시겠습니까?"
        # task5
        if sds_res['current_task'] == 'task5':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "보험계약 대출에 매월 발생되는 이자는. 같은 은행 계좌 납부해야 주셔야 합니다.이자 납입이 가능한 이체일은 매달 오일, 십일, 이십일, 이십오일이 있습니다. 몇일로 신청해드릴까요"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "오일, 십일, 이십일, 이십오일 중 몇 일로 신청해드릴까요?"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "네 확인되었습니다. 보험계약 대출에 매월 발생되는 이자는. 같은 은행 계좌 납부해야 주셔야 합니다.이자 납입이 가능한 이체일은 매달 오일, 십일, 이십일, 이십오일이 있습니다. 몇일로 신청해드릴까요?;"
        # task6
        if sds_res['current_task'] == 'task6':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. "+str(self.func.readNumberMinute(session_data['transfer']))+"로 신청해드려도 될까요?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = ""+str(self.func.readNumberMinute(session_data['transfer']))+"일로 신청해드리겠습니다. 맞으십니까?"
        # task7
        if sds_res['current_task'] == 'task7':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 보험계약 대출 약정서를 문자로 발송해드리고 있는데. 사용하시는 휴대폰이 스마트폰이세요?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "네 알겠습니다. 이자 미납시 보험계약대출 원금에 납입일까지 발생한 이자를 합산한 금액 기준으로 정상이율을 적용하되. 연체이율은 적용되지 않습니다. 상환은 최소 일만원 부터 신청이 가능하며. 고객님 명의의 계좌에 입금하시고 연락주시면 즉시이체 해드립니다. 그리고 보험계약 대출 약정서를 문자로 발송해드리고 있는데. 사용하시는 휴대폰이 스마트폰이세요?;"
        # task7_1
        if sds_res['current_task'] == 'task7_1':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 보험계약대출 약정서 및 필수안내사항의 내용에 동의하십니까?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            elif sds_intent == 'negate':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "그러시군요. 보험계약대출 약정서 및 필수안내사항을 말씀드리겠습니다. 내용에 동의하십니까?"
        # task8
        if sds_res['current_task'] == 'task8':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "다시 한번 말씀드리겠습니다. 고객님께서 안내주신 내용에 따라 마인즈보험에 요청하신 자동이체에 대한 출금동의를 진행하겠습니다. 예금주인 마인즈님의 생년월일은 "+session_data['jumin_han']+"이며.  "+session_data['bank_name']+"은행 "+session_data['bank_acc_han']+" 계좌번호로." + str(self.func.readNumberMinute(session_data['money'])) +" 만원 대출 등록해드리겠습니다. 동의하시겠습니까?"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
            elif sds_intent == 'callback':
                talk_res.response.speech.utter = "AI상담사에 의한 안내도중 정확한 확인이 되지 않아 전화예약을 남겨드리겠습니다. 불편을 끼쳐 죄송합니다."
                talk_res.response.meta['status'] = 'callback'
            else:
                talk_res.response.speech.utter = "네, 바로 발송해드리겠습니다. 약정서의 내용을 꼭 확인하여 주시기 바랍니다. 방금 안내드린 내용은 당사 홈페이지를 통해서 언제든지 확인 가능합니다. 네 그럼 고객님께서 안내주신 내용에 따라 마인즈보험에 요청하신 자동이체에 대한 출금동의를 진행하겠습니다. 예금주인 마인즈님의 생년월일은 "+session_data['jumin_han']+"이며. "+session_data['bank_name']+"은행 "+session_data['bank_acc_han']+"계좌번호로.  "+str(self.func.readNumberMinute(session_data['money']))+"만원 대출 등록해드리겠습니다. 동의하시겠습니까?"
        # task9
        if sds_res['current_task'] == 'task9':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "접수 중이니 잠시만 기다려 주세요."
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "네라고 말씀해주세요"
            else:
                talk_res.response.speech.utter = "감사합니다. 요청하신 "+session_data['bank_name']+"은행 계좌로 "+str(self.func.readNumberMinute(session_data['money']))+"만원 바로 입금진행하도록 하겠습니다. 접수중이니 잠시만 기다려 주세요." 
#        # task11
#        if sds_res['current_task'] == 'task11':
#            if sds_intent == 'again':
#                talk_res.response.speech.utter = "입금 받으실 방법은 고객님 거래계좌로 가능하며, 하나은행 일이삼사,오육칠팔,구영일이, 계좌로 입금가능합니다. 괜찮으십니까?"
#            elif sds_intent == 'unknown':
#                talk_res.response.speech.utter = "네, 또는 아니오로 답변해주세요,"
#            else:
#                talk_res.response.speech.utter = "기다려주셔서 감사합니다. 지급 완료 되었습니다. 입금 내용 확인해보시기 바랍니다. 이용해주셔서 감사합니다. 문의사항은 언제든 홈페이지 및 고객삼당실로 연락 부탁드립니다.;$detectNo=11$"

        #time
        if sds_res['current_task'] == 'time':
            if sds_intent == 'again':
                talk_res.response.speech.utter = "통화 가능하신 요일과 시를 말씀해주세요"
            elif sds_intent == 'unknown':
                talk_res.response.speech.utter = "통화 가능하신 요일과 시를 말씀해주세요"
            elif sds_intent == "hourmiss":
                talk_res.response.speech.utter = "통화 가능 시를 말씀해주시지 않았습니다. 통화가능 시를 말씀해주세요."
            elif sds_intent == "daymiss":
                talk_res.response.speech.utter = "통화 가능 요일을 말씀해주시지 않았습니다. 통화가능 요일을 말씀해주세요."
            else:
                talk_res.response.speech.utter = "그럼 가능하신 시간을 알려주시면 다시 연락드리겠습니다, 편하신 요일과 시를, 말씀해주세요"

        #time_end       
        if sds_res['current_task'] == 'timeEnd':
            talk_res.response.speech.utter = "네 고객님. 말씀하신 시간에 다시 연락을 드리겠습니다. 마인즈보험 에이아이상담원 였습니다. 감사합니다."
            talk_res.response.meta['status'] = 'callback'
        if sds_res['current_task'] == 'taskEnd':
            #talk_res.response.speech.utter = "네 고객님, 소중한시간 내주셔서 감사합니다. 마인즈보험 고객센터였습니다. $complete$"
            talk_res.response.speech.utter = "이용해주셔서 감사합니다. 문의사항은 언제든 홈페이지 및 고객상담실로 연락 부탁드립니다."
            talk_res.response.meta['status'] = 'complete'



################################################################ 위치 고정 시키기
        
        self.DBConnect("update MNT_TARGET_MNG set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")
        self.logger.debug('user utter :: {}, system utter :: {}, call id :: {} task :: {}'.format(user_utter,
                            talk_res.response.speech.utter, call_id, sds_res['current_task']))

################################################################ 웹 소켓 데이터 전달
        
        
        
        ######## 통화가 갑자기 끊겼을 때, 데이터 전달        
        if call_status == '(null)':
            ####### 중간단계 데이터 전달
            if session_data['seq_id'] == "":
                print("Json 보낼게 없음")
            else:
                json_test = {
                    "EventType":"DETECT",
                    "Event":"result",
                    "No":"" + session_data['seq_id'],
                    "Result":""+ answer_value,
                    "contract_no":""+ seq,
                    }
                print("Json 보낼게 없음")
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
            ######## 종료일떄 데이터 전달
            if sds_res['current_task'] == 'timeEnd' or sds_res['current_task'] == 'taskEnd':
                
                json_test = {
                    "EventType":"DETECT",
                    "Event":"result",
                    "No":"" + seq_id,
                    "Result":"Y",
                    "contract_no":""+ seq,
                    }
                print("Json 보낼게 없음")
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
        else:
            if call_status == 'CS0003':
                sds_res['current_task'] = sds_res['current_task'].replace('Greet','task1')
                seq_id_db = self.DBConnect("select task_info from HC_HH_CAMPAIGN_INFO where camp_id = 5 and task = '" + sds_res['current_task']  + "';")
             #   print (camp_id_db)
                taskNo_name = str(seq_id_db[0]['task_info'])
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0005_"+session_data['seq_id'],
                    "contract_no":""+ seq,
                    "taskNo_name":""+ taskNo_name,
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0005_"+session_data['seq_id']+"', mnt_status_name ='확인필요_"+taskNo_name+"' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0005':
                
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0001",
                    "contract_no":""+ seq,
                    "taskNo_name":"",
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0001', mnt_status_name ='전체완료' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0007':
            
                json_test = {
                    "EventType":"CALL",
                    "Event":"mntresult",
                    "call_id":"" + call_id,
                    "call_status":"MR0004",
                    "contract_no":""+ seq,
                    "taskNo_name":"",
                    }
                self.DBConnect("update CALL_HISTORY set mnt_status = 'MR0004', mnt_status_name ='콜백요청' where contract_no = '" +seq+ "'  and call_id = '"+call_id+"';")
            elif call_status == 'CS0000':
                pass
            else:
                print('콜 값을 전달할 게 없어용')
            if call_status == 'CS0003' or call_status == 'CS0005' or call_status == 'CS0007':
                jsonString = json.dumps(json_test)
                print(jsonString)
        #        ws = create_connection("ws://10.122.64.152:13254/callsocket")
                print("Json send")
                self.send_ws(jsonString, call_id)
                print("send complete")
        #        result =  ws.recv()
        #        print("Received '%s'" % result)
        #        ws.closeo()
################################################################

        print("[ANSWER]: " + original_answer)
        #print("[SESSION_KEY] :" + )
        print("[ENGINE]: " + answer_engine)

################################################################ 현재 위치 저장
        session_data = talk.session.context
        session_data['seq_id'] = str(seq_id)
        print('**************************' + str(session_data['seq_id']))
        talk_res.response.session_update.context.CopyFrom(session_data)

        #위치 전송
        #self.DBConnect("update HC_HH_CAMPAIGN_SCORE set task='"+ sds_res['current_task'] + "' where contract_no = '" + seq + "';")

################################################################

        self.logger.debug('end talk function call id :: {}'.format(call_id))

        return talk_res


    def unknown_answer(self):
        """
        리스트에서 랜덤으로 Unknown 답변 출력.
        """
        unknown_text = ['죄송해요, 제가 잘 못 알아 들었어요. 키워드 위주로 다시 질문해주시겠어요?',
                        '답변을 찾을 수 없습니다. 다른 질문을 해주시면 성실히 답변해 드리겠습니다. ']
        return random.choice(unknown_text)


    def Close(self, req, context):
        print 'V3 ', 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        return talk_stat

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()

    def Open(self, req, context):
        print 'V3 ', 'Open', 'called'
        # req = talk_pb2.OpenRequest()
        event_res = talk_pb2.OpenResponse()
        event_res.code = 1000000
        event_res.reason = 'success'
        answer_meta = struct.Struct()

        answer_meta["play1"] = "play1"
        answer_meta["play2"] = "play2"
        answer_meta["play3"] = "play3"

        answer_meta.get_or_create_struct("audio1")["name"] = "media_play1"
        answer_meta.get_or_create_struct("audio1")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio1")["duration"] = "00:10:12"

        answer_meta.get_or_create_struct("audio2")["name"] = "media_play2"
        answer_meta.get_or_create_struct("audio2")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio2")["duration"] = "00:00:15"

        event_res.meta.CopyFrom(answer_meta)
        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        # DO NOTHING
        return event_res

    def Event(self, req, context):
        print 'V3 ', 'Event', 'called'
        # req = talk_pb2.EventRequest()

        event_res = talk_pb2.EventResponse()
        event_res.code = 10
        event_res.reason = 'success'

        answer_meta = struct.Struct()
        answer_meta["meta1"] = "meta_body_1"
        answer_meta["meta2"] = "meta_body_2"
        event_res.meta.CopyFrom(answer_meta)

        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        return event_res

def serve():
    parser = argparse.ArgumentParser(description='CMS DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(
        EchoDa(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
