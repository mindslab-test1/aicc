< 개발 / 운영 DA 세팅 >

1. cfg/config.py 에 PROD, DEV 로 주석되어 있는 부분을 확인하고 수정한다.
2. 수정할 부분은 크게 log path, websocket 정보, DB 정보 이다.
3. 운영(187번 서버)에서 소스 적용 시 # PROD 아래 소스를 사용한다.
4. 개발(152번 서버)에서 소스 적용 시 # DEV 아래 소스를 사용한다.


< DA daemon 실행 >

* DA 실행 방법은 2가지가 있다.
1. ./start.sh 을 실행하는 방법
2. svctl -> start SULLYBOT 로 실행하는 방법

< batch 관련 >
1. token.pickle : oauth 인증 token 파일
2. lib/google_spread/credentials.json : client-secret.json 파일

< 기타 참고문서 >
confluence: https://pms.maum.ai/confluence/pages/viewpage.action?pageId=14320209
