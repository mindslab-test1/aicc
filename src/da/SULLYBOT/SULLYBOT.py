#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import grpc
import time
import argparse
import traceback
from lib import logger
from lib.talk import DA_Talk
from datetime import datetime
from cfg.config import DaConfig
from concurrent import futures
from google.protobuf import empty_pb2, struct_pb2 as struct
from google.protobuf.json_format import MessageToDict
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'da'))
from qa.util import Util
from custom_hs.sds import SDS
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
from maum.m2u.da import provider_pb2
from maum.m2u.da.v3 import talk_pb2_grpc, talk_pb2
from maum.m2u.facade import userattr_pb2

###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')

#############
# constants #
#############
__ONE_DAY_IN_SECONDS__ = 60 * 60 * 24


#########
# class #
#########
class EchoDa(talk_pb2_grpc.DialogAgentProviderServicer):
    def __init__(self):
        # logger setting
        self.logger = logger.get_timed_rotating_logger(
            logger_name=DaConfig.logger_name,
            log_dir_path=DaConfig.log_dir_path,
            log_file_name=DaConfig.log_file_name,
            backup_count=DaConfig.log_backup_count,
            log_level=DaConfig.log_level
        )
        print 'log path: {0}'.format(DaConfig.log_dir_path)
        self.logger.debug("="*100)
        self.logger.debug("1. DA V3 Server Constructor")
        # init parameter
        self.init_param = provider_pb2.InitParameter()
        # state
        self.state = provider_pb2.DIAG_STATE_IDLE
        self.logger.debug('DialogAgentState: {0}'.format(self.state))
        # library
        self.qa_util = Util()
        self.Sds = SDS()
        # PROVIDER
        self.provider = provider_pb2.DialogAgentProviderParam()
        self.provider.name = 'control'
        self.provider.description = 'control intention return DA'
        self.provider.version = '0.1'
        self.provider.single_turn = True
        self.provider.agent_kind = provider_pb2.AGENT_SDS
        self.provider.require_user_privacy = True

    def IsReady(self, empty, context):
        """
        현재 Dialog Agent Instance의 상태를 확인할 수 있는 함수
        Dialog Agent Instance  동작에 따라 상태 변경 필요 생성자에서 DIAG_STATE_IDLE 상태로 초기화
        :param      empty:
        :param      context:
        :return:                        status
        """
        self.logger.info("DA V3 Server IsReady")
        try:
            status = provider_pb2.DialogAgentStatus()
            # DA instance 상태 저장 (준비 상태)
            status.state = self.state
            self.logger.debug("IsReady response: {0}".format(status))
            return status
        except Exception:
            self.logger.error("{0}".format(traceback.format_exc()))

    def Init(self, init_param, context):
        """
        Web Console을 통해 DA를 Dialog Agent Instance의 형태로 실행할 때, 관련 설정값을 이용하여 Instance를 초기화하도록 실행
        :param      init_param:
        :param      context:
        :return:                        result
        """
        self.logger.info("DA V3 Server Init")
        self.logger.debug("Init parameter: {0}".format(init_param))
        self.logger.debug("context: {0}".format(context))
        try:
            # DIAG_STATE_INITIALIZING 로 상태 변경 (초기화중)
            self.state = provider_pb2.DIAG_STATE_INITIALIZING
            # provider를 result로 변경하여 return
            result = provider_pb2.DialogAgentProviderParam()
            result.CopyFrom(self.provider)
            # 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장
            self.init_param.CopyFrom(init_param)
            # DIAG_STATE_RUNNING 상태로 변경 (동작중)
            self.state = provider_pb2.DIAG_STATE_RUNNING
            self.logger.debug("Init response : {0}".format(result))
            return result
        except Exception:
            # 예외처리
            self.logger.error(traceback.format_exc())

    def Terminate(self, empty, context):
        """
        동작중인 DA Instance를 종료시키는 함수
        Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경
        :param      empty:
        :param      context:
        :return:
        """
        self.logger.info("DA V3 Server Terminate")
        try:
            # DA instance state 변경
            self.state = provider_pb2.DIAG_STATE_TERMINATED
            return empty_pb2.Empty()
        except Exception:
            self.logger.error(traceback.format_exc())

    def GetUserAttributes(self, empty, context):
        print 'V3 ', 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = userattr_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        loc.title = '기본 지역'
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '기본으로 조회할 지역을 지정해주세요.'
        attrs.append(loc)

        device = userattr_pb2.UserAttribute()
        device.name = 'device'
        device.title = '기본 디바이스'
        device.type = userattr_pb2.DATA_TYPE_STRING
        device.desc = '기본으로 사용할 디바이스를 지정해주세요.'
        attrs.append(device)

        country = userattr_pb2.UserAttribute()
        country.name = 'time'
        country.title = '기준 국가 설정'
        country.type = userattr_pb2.DATA_TYPE_STRING
        country.desc = '기본으로 조회할 국가를 지정해주세요.'
        attrs.append(country)

        result.attrs.extend(attrs)
        return result

    def GetProviderParameter(self, empty, context):
        """
        DA가 동작하는데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
        :param      empty:
        :param      context:
        :return:
        """
        self.logger.info("DA V3 Server GetProviderParameter")
        try:
            self.logger.debug("GetProviderParameter response: {0}".format(self.provider))
            result = provider_pb2.DialogAgentProviderParam()
            result.CopyFrom(self.provider)
            return result
        except Exception:
            self.logger.error(traceback.format_exc())

    def set_runtime_parameter(self, **kwargs):
        """
        Set Runtime parameter
        :param      kwargs:         arguments
        :return:                    runtime parameter
        """
        item = provider_pb2.RuntimeParameter()
        item.name = kwargs.get('name')
        item.type = kwargs.get('type')
        item.desc = kwargs.get('desc')
        item.default_value = kwargs.get('default_value')
        item.required = True
        return item

    def GetRuntimeParameters(self, empty, context):
        """
        실제로 DA Instance가 돌아가기 위해 필요한 parameters를 정의
        DB 정보 정의
        :param      empty:
        :param      context:
        :return:                        result
        """
        self.logger.info("DA V3 Server GetRuntimeParameters")
        try:
            result = provider_pb2.RuntimeParameterList()
            params = list()
            params.append(self.set_runtime_parameter(
                name='db_host',
                type=userattr_pb2.DATA_TYPE_STRING,
                desc='Database Host',
                default_value='171.64.122.134'
            ))
            params.append(self.set_runtime_parameter(
                name='db_port',
                type=userattr_pb2.DATA_TYPE_INT,
                desc='Database Port',
                default_value='7701'
            ))
            params.append(self.set_runtime_parameter(
                name='db_user',
                type=userattr_pb2.DATA_TYPE_STRING,
                desc='Database User',
                default_value='minds'
            ))
            params.append(self.set_runtime_parameter(
                name='db_pwd',
                type=userattr_pb2.DATA_TYPE_AUTH,
                desc='Database Password',
                default_value='minds67~'
            ))
            params.append(self.set_runtime_parameter(
                name='db_database',
                type=userattr_pb2.DATA_TYPE_STRING,
                desc='Database Database name',
                default_value='ascar'
            ))
            result.params.extend(params)
            return result
        except Exception:
            self.logger.error(traceback.format_exc())

    def OpenSession(self, request, context):
        """
        세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있음.
        이 호출은 세션 전체를 시작할 때 들어오는 요청이며, 모든 DA가 이 요청을 받지는 않음.
        :param      request:
        :param      context:
        :return:                        result
        """
        self.logger.info("DA V3 Server OpenSession")
        # self.logger.debug("OpenSession request: {0}".format(request))
        try:
            # result 설정
            session_id = request.session.id
            result = talk_pb2.TalkResponse()
            res_meta = struct.Struct()
            result.response.meta.CopyFrom(res_meta)
            # session 정보 ,session data 10k
            result.response.session_update.id = session_id
            # session 정보 등록
            session_data = struct.Struct()

            # SDS를 위한 session data
            session_data['current_task'] = ''               # 현재 task 위치
            session_data['slot_items'] = ''                 # 현재 task slot item
            session_data['intent'] = 'inform'               # 현재 task 의도
            session_data['last_sds_question'] = ''          # SDS 마지막 질문

            # session_data['task2_count'] = '0'               # task2 중복실행 횟수
            # session_data['task10_count'] = '0'              # task10 중복실행 횟수
            # session_data['ivr_call'] = ''                   # ivr 호출 신호
            # session_data['jumin_number'] = ''               # 고객 주민등록번호
            # session_data['avail_money'] = ''                # 대출 한도
            # session_data['recent_deal_yn'] = ''             # 최근 1년간 거래 유무
            # session_data['cust_nm'] = ''                    # 고객 이름
            # session_data['prod_nm'] = ''                    # 상품 명
            # session_data['prod_avail_money'] = ''           # 상품별 대출 한도
            # session_data['user_avail_money'] = ''           # 고객별 해당 상품 대출 한도
            # session_data['manwon'] = '0'                    # n만원
            # session_data['eogwon'] = ''                     # n억원
            # session_data['cust_tel_comp'] = str()           # 통신사
            # session_data['certifi_no'] = str()              # 인증번호
            # session_data['tel_comp_save_yn'] = str()        # 알뜰폰 여부
            # session_data['interest'] = ''                   # 이자
            # session_data['bank_info'] = '{}'                  # 은행 정보
            # session_data['get_contract'] = '일'              # 은행 선택 번호
            # session_data['pay_day'] = ''                    # 이체일 선택

            # for sully bot
            session_data['exit_count'] = '0'                # exit count
            session_data['exit_check_task'] = ''            # exit check task
            session_data['exit_check_intent'] = ''            # exit check intent
            session_data['slot_key'] = ''                   # slot name in sds_res['best_slu']
            session_data['slot_val'] = ''                   # slot value in sds_res['best_slu']
            session_data['cust_name'] = ''                  # 고객 이름
            session_data['cust_tel_no'] = ''                # 고객 전화번호
            session_data['pretreatment_utter'] = ''         # pre_processing 하기 이전의 origin utter
            session_data['confirmed_working_time'] = False       # 최초 1회에 한해서 업무시간 체크를 했는지 확인을 위한 Flag 데이터 -> True/False
            session_data['is_working_time'] = False              # 현재 업무시간인지 확인 데이터 -> True/False
            session_data['is_lunch_time'] = False                # 현재 점심시간인지 확인 데이터 -> True/False

            # session_data['new_cust_yn'] = 'N'               # 신규고객 여부
            # session_data['current_loan_info'] = '{}'        # 현재 대출 상황 정보
            # session_data['seq'] = '0'                       # db current task seq
            session_data['answer_value'] = ''               # answer value
            session_data['web_socket_task'] = ''            # web socket 을 위한 task 저장
            # session_data['previous_loan_info'] = '{}'               # 기존 대출 정보
            # session_data['previous_loan_detail_info'] = '{}'        # 기존 대출 상품 정보
            # virtual task를 위한 session data
            # session_data['virtual_process_yn'] = 'N'    # 가상 task 처리중 y/n
            # session_data['virtual_current_task'] = ''   # 가상 현재 task 위치
            # session_data['virtual_exit_count'] = '0'    # 가상 exit count
            # NQA를 위한 session data
            # session_data['nqa_process_yn'] = 'N'        # nqa 처리중 y/n
            # session_data['nqa_count'] = '0'             # nqa count
            # session_data['answer_id_list'] = ''         # nqa answer id 리스트
            # session_data['before_layer_option'] = ''    # nqa 이전 계층 후보
            # session_data['current_layer_list'] = ''     # 현재 계층 리스트
            result.response.session_update.context.CopyFrom(session_data)
            # self.logger.debug("OpenSession result: {0}".format(result))
            return result
        except Exception:
            self.logger.error(traceback.format_exc())

    def OpenSkill(self, request, context):
        """
        해당하는 스킬을 최초로 호출 할 때 호출
        싱글턴일 경우 호출되지 않음
        대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우 호출
        :param      request:
        :param      context:
        :return:                    result
        """
        self.logger.info("DA V3 Server OpenSkill")
        # self.logger.info("OpenSkill request: {0}".format(request))
        result = talk_pb2.TalkResponse()
        print 'OpenSkill end'
        return result

    def CloseSkill(self, request, context):
        result = talk_pb2.CloseSkillResponse()
        return result

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()

    def Talk(self, talk, context):
        """
        Front 에서 대화를 위해 호출되는 함수
        한번 답변이 완료된 경우 새로운 세션으로 대화를 시작
        :param      talk:       이전 계층 정보
        :param      context:    현재 사용되는 부분 없음
        :return:                이후 생성된 계층 정보
        """

        # self.logger.info("utter: {0}".format(talk.utter.utter))
        # if talk.utter.utter in ['NULL']:
        #     return None

        try:
            self.logger.info("1. SULLYBOT.Talk() start")
            # self.logger.info("status: {0}".format(talk.utter.meta['status']))
            # self.logger.info("\ttalk: {0}".format(talk))

            # talk.py 객체 생성 (qna)
            qna = DA_Talk(talk=talk, logger=self.logger)
            qna.model = DaConfig.model

            # 대화 진행 순서
            qna.call_sds()  # talk.call_sds() -> task_worker

            # Input Setting
            output = qna.answer                             # 챗봇 답변 받아와서 저장
            talk = qna.talk                                 # da_info_dict['talk'] 값

            #### retun talk_res 정의 시작 ####
            talk_res = talk_pb2.TalkResponse()
            talk_res.response.speech.utter = output

            self.logger.info(output)

            # phone 기능에 대한 data 할당
            if qna.status:
                talk_res.response.meta['status'] = qna.status            # 상태 설정
                self.logger.info('\tqna.status: {0}'.format(talk_res.response.meta['status']))
            if qna.extra:
                talk_res.response.meta['extra'] = qna.extra              # 키패드 on/off 여부 설정
                self.logger.info('\tqna.extra: {0}'.format(talk_res.response.meta['extra']))
            if qna.transfer:
                talk_res.response.meta['transfer'] = qna.transfer        # 콜 연결 직원 설정
                self.logger.info('\tqna.transfer: {0}'.format(talk_res.response.meta['transfer']))
            if qna.tts:
                talk_res.response.meta['tts'] = qna.tts                  # tts 발화 중지 여부 설정
                self.logger.info('\tqna.tts: {0}'.format(talk_res.response.meta['tts']))

            for key, value in qna.meta.items():
                talk_res.response.meta[key] = value
            talk_res.response.session_update.context.CopyFrom(talk.session.context)

            #### retun talk_res 정의 끝 ####

            # qna.chat_log(
            #     chatbot=talk.chatbot,
            #     device=talk.system_context.device.id
            # )   # CHAT_LOG(채팅로그)
            qna.print_result()  # 결과 콘솔 출력
            self.logger.info("1. SULLYBOT.Talk() end")
            return talk_res

        except Exception:
            print('** SULLYBOT.talk() Exception')
            self.logger.error(traceback.format_exc())

    def Close(self, req, context):
        print 'V3 ', 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        return talk_stat

    def EventT(self, empty, context):
        print 'V3 ', 'DA Version 3 EventT', 'called'

        # DO NOTHING
        return empty_pb2.Empty()

    def Open(self, req, context):
        print 'V3 ', 'Open', 'called'
        # req = talk_pb2.OpenRequest()
        event_res = talk_pb2.OpenResponse()
        event_res.code = 1000000
        event_res.reason = 'success'
        answer_meta = struct.Struct()

        answer_meta["play1"] = "play1"
        answer_meta["play2"] = "play2"
        answer_meta["play3"] = "play3"

        answer_meta.get_or_create_struct("audio1")["name"] = "media_play1"
        answer_meta.get_or_create_struct("audio1")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio1")["duration"] = "00:10:12"

        answer_meta.get_or_create_struct("audio2")["name"] = "media_play2"
        answer_meta.get_or_create_struct("audio2")["url"] = "htpp://101.123.212.321:232/media/player_1.mp3"
        answer_meta.get_or_create_struct("audio2")["duration"] = "00:00:15"

        event_res.meta.CopyFrom(answer_meta)
        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        # DO NOTHING
        return event_res

    def Event(self, req, context):
        print 'V3 ', 'Event', 'called'
        # req = talk_pb2.EventRequest()

        event_res = talk_pb2.EventResponse()
        event_res.code = 10
        event_res.reason = 'success'

        answer_meta = struct.Struct()
        answer_meta["meta1"] = "meta_body_1"
        answer_meta["meta2"] = "meta_body_2"
        event_res.meta.CopyFrom(answer_meta)

        answer_context = struct.Struct()
        answer_context["context1"] = "context_body1"
        answer_context["context2"] = "context_body2"
        answer_context["context3"] = "context_body3"
        event_res.context.CopyFrom(answer_context)

        return event_res


#######
# def #
#######
def get_session_data(session):
    session_dict = dict()
    try:
        session_dict = MessageToDict(session)
    except Exception as e:
        print("-" * 100)
        print(e)
        # session_data = list()
        print("-" * 100)
    return session_dict


def serve(args):
    """
    This is a program that HC_CS_LOAN
    :param      args:       Arguments
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    try:
        talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(EchoDa(), server)
        listen = '[::]:{0}'.format(args.port)
        server.add_insecure_port(listen)
        server.start()
        while True:
            time.sleep(__ONE_DAY_IN_SECONDS__)
    except KeyboardInterrupt:
        server.stop(0)
    except Exception:
        print traceback.format_exc()


########
# main #
########
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CMS DA')
    parser.add_argument('-p', '--port', nargs='?', action='store', dest='port', type=int, default=False, required=True,
                        help="port to access server")
    argument = parser.parse_args()
    serve(argument)
