#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import traceback
from datetime import datetime
from cfg import config
from lib import logger, utility, google_spread


#######
# def #
#######
def setup():

    # Log setting
    date = datetime.strftime(datetime.now(), '%Y/%m')
    day = datetime.strftime(datetime.now(), '%d')
    log = logger.set_logger(
        logger_name=day,
        log_dir_path=os.path.join(config.DaConfig.log_dir_path, 'batch/', date),
        log_file_name='{0}.log'.format(day),
        log_level=config.DaConfig.log_level
    )

    # Connect DB
    try:
        mysql = utility.db_connect()
    except Exception:
        time.sleep(3)
        mysql = utility.db_connect()

    # G-Spread Employee info sheet
    g_spread_dict = dict()
    # If modifying these scopes, delete the file token.pickle.
    g_spread_dict['scopes'] = ['https://www.googleapis.com/auth/spreadsheets.readonly']
    g_spread_dict['spreadsheet_id'] = '133f1wt0iT7zCe1srflNyrgbDir5YEKzysX6L676Ju6o'
    g_spread_dict['range_name'] = 'Employee_info!A2:G'
    g_spread_dict['client_secrets_file'] = 'lib/google_spread/credentials.json'
    # scopes = ['https://www.googleapis.com/auth/spreadsheets.readonly']
    # spreadsheet_id = '133f1wt0iT7zCe1srflNyrgbDir5YEKzysX6L676Ju6o'
    # range_name = 'Employee_info!A2:E'

    log.info("1. setup")

    return log, mysql, g_spread_dict


def unsetup(log, mysql):
    log.info("3. unsetup")
    for handler in log.handlers:
        handler.close()
        log.removeHandler(handler)
    mysql.disconnect()


def start_batch(log, mysql, g_spread_dict):
    log.info("2. start_batch")

    # google spread 직원 정보 조회 (열 순서: No/이름/직급/전화번호/소속/부서/계약형태)
    creds = google_spread.get_credentials(g_spread_dict)
    service = google_spread.get_service(creds)
    emp_info = google_spread.read(service, g_spread_dict)

    # spread sheet 에서 range 로 설정한 마지막 열은 빈값이면 안됨 (중간에 빈값들은 value가 없어짐)
    emp_info_tuple = tuple()
    for row in emp_info:
        if len(row) == 7 and row[0] and row[1] and row[3]: # row[0],[1],[3] 은 PK/이름/연락처로 DB 에서 Not Null 임
            log.info('%s, %s, %s, %s, %s, %s (len: %s) ' % (row[0], row[1], row[2], row[3], row[4], row[5], len(row)))
            for i in range(0, len(row)):
                if row[i] == u'':
                    row[i] = None
                if row[3]:
                    row[3] = row[3].replace("-", "")
            row = tuple(row[:6])
            emp_info_tuple += (row,)

    # delete -> insert emp info
    if emp_info_tuple:
        utility.delete_insert_employee_info(log, mysql, emp_info_tuple)


if __name__ == '__main__':
    log, mysql, g_spread_dict = setup()
    start_batch(log, mysql, g_spread_dict)
    unsetup(log, mysql)




