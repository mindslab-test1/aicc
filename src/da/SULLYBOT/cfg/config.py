#!/usr/bin/env python
# -*- coding: utf-8 -*-

###########
# imports #
###########
import os
import sys
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
from maum.brain.qa.basicqa import basicqa_pb2


class DaConfig:
    model = 'sully_ver02'
    logger_name = 'SULLYBOT'
    log_file_name = 'sullybot.log'
    log_backup_count = 3
    log_level = 'debug'
    
    # PROD
    log_dir_path = '/srv/maum/logs/SULLYBOT'
    web_socket_ip = '15.164.116.26'
    web_socket_port = '13254'
    
    # DEV
    # log_dir_path = '/home/happy/maum/logs/SULLYBOT'
    # web_socket_ip = '10.122.64.152'
    # web_socket_port = '13254'


class NqaConfig:
    ntop = 5                        # 가져올 질문의 결과 갯수
    score = 0.0                     # 점수 filter
    max_size = 10.0                 # 사이즈 filter
    category = 'ECA'                # 카테고리
    query_type = basicqa_pb2.ALL    # 쿼리 타입(ALL, AND, OR)
    query_target_list = [           # 쿼리 종류
        basicqa_pb2.Q,              # 0
        basicqa_pb2.GRAMQ,          # 1
        basicqa_pb2.A,              # 3
        basicqa_pb2.GRAMA           # 6
    ]


class MysqlConfig:
    # PROD
    host = '10.122.64.141'
    port = 3306
    user = 'aicc'
    passwd = 'Aicc12#$'
    db = 'HAPPYCALL4'
    
    # DEV
    # host = '10.122.64.152'
    # port = 3306
    # user = 'minds'
    # passwd = 'msl1234~'
    # db = 'HAPPYCALL3'    

    charset = 'utf8'

    
    
