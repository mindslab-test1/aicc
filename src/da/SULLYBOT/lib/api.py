#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib2 import Request, urlopen
from datetime import datetime
import time
import xml.etree.ElementTree as ET
import sys

###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


# 공공데이터 open api 를 통한 공휴일 데이터를 조회하여, 업무시간 및 점심시간 체크
# API 사용기간: 2019-08-30 ~ 2021-08-30
def get_working_time(log):

    log.info('* api.get_working_time()')

    # setting common input
    now = datetime.now()    # 2019-09-12 10:37:13.771378
    currentYear = now.strftime('%Y')
    currentMonth = now.strftime('%m')
    log.info('\tyear: {0}, month: {1}'.format(currentYear, currentMonth))

    # setting input for API
    url = 'http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getRestDeInfo'
    serviceKey = '?serviceKey='+'6MIfp1uU0gBaqkHiGV2PJvXLN60ZxrSWApUWFncc4sNNVLzRwSneFJ0%2BI3vrFF2BzmXerv3gliF40M%2BZqPeYLw%3D%3D'
    solYear = '&solYear=' + currentYear
    solMonth = '&solMonth=' + currentMonth

    queryParams = serviceKey + solYear + solMonth
    request = Request(url + queryParams)
    request.get_method = lambda: 'GET'
    response_body = urlopen(request).read()

    # parse xml
    tree = ET.fromstring(response_body)
    items = tree.find('./body/items')
    totalCount = tree.find('./body/totalCount').text    # 월 공휴일 개수

    holidayList = list()
    for child in items.iter("locdate"):
        holidayList.append(child.text)

    # setting input for 평일, 업무시간
    day = ['월', '화', '수', '목', '금', '토', '일']
    today = time.localtime().tm_wday

    morningTime = now.replace(hour=9, minute=30, second=0)
    startLunchTime = now.replace(hour=12, minute=0, second=0)
    endLunchTime = now.replace(hour=13, minute=0, second=0)
    afternoonTime = now.replace(hour=18, minute=30, second=0)

    # log
    log.info('\tnow: {0}'.format(now))
    log.info('\tday[today]: {0}'.format(day[today]))
    log.info('\tholidayList: {0}'.format(holidayList))

    # 평일, 업무시간 check and output
    is_working_time = True
    is_lunch_time = False
    formatToday = now.strftime('%Y%m%d')
    if day[today] in ['토', '일']:
        is_working_time = False
    elif now < morningTime or now > afternoonTime:
        is_working_time = False
    elif formatToday in holidayList:
        is_working_time = False
    if now >= startLunchTime and now < endLunchTime:
        is_lunch_time = True

    # 업무시간이 아니더라도 통화 연결을 위한 코드 (불필요시 주석)
    # is_working_time = True
    # is_lunch_time = False

    return is_working_time, is_lunch_time

