#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import re
import sys
import datetime
import traceback
#import logger
###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


#############
# constants #
#############

#########
# class #
#########
# class = Util, Reg, Replace, Convert, Slot

class Util():
    def __init__(self):
        pass

    def convert_number(self, var):
        var = var.replace('1', '일').decode('utf-8')
        #        global convart_var
        hanCount = len(re.findall(u'[\u3130-\u318F\uAC00-\uD7A3]+', var))
        if hanCount >= 1:
            try:
                convart_var = self.change_bu(var)
                print('convart_var : ' + convart_var)
                hanCount = len(re.findall(u'[\u3130-\u318F\uAC00-\uD7A3]+', convart_var.decode('utf-8')))
                if hanCount >= 1:
                    convart_var = convart_var
                    convart_var = self.change(convart_var)
                    print('convert_var : ' + str(convart_var))
                else:
                    print('1차의 한글 더이상 없음1')
            except:
                convart_var = ''
        else:
            convart_var = var
            print('2차의 한글 더이상 없음2')

        #        convart_var = convart_var.encode('utf-8')

        return str(convart_var)

    def change_bu(self, input_var):
        input_var = input_var.decode('utf-8')
        b = {
            '한': '1',
            '두': '2',
            '세': '3',
            '네': '4',
            '다섯': '5',
            '여섯': '6',
            '일곱': '7',
            '여덟': '8',
            '아홉': '9',
            '열': '10',
            '열한': '11',
            '열두': '12',
            '열세': '13',
            '열네': '14',
            '열다섯': '15',
            '열여섯': '16',
            '열일곱': '17',
            '열여덟': '18',
            '열아홉': '19',
            '스물': '20',
            '스물한': '21',
            '스물두': '22',
            '스물세': '23',
            '스물네': '24'
        }

        text = ['스물네', '스물세', '스물두', '스물한', '스물', '열아홉', '열여덟', '열일곱', '열여섯', '열다섯', '열네', '열세', '열두', '열한', '열', '아홉',
                '여덟', '일곱', '여섯', '다섯', '네', '세', '두', '한']
        for t in text:
            if input_var.find(t.decode('utf-8')) >= 0:
                input_var = input_var.replace(t, b[t])
            else:
                pass
        input_var = input_var.encode('utf-8')
        return input_var


    def change(self, input_var):
        input_var = input_var.decode('utf-8')
        kortext = '영일이삼사오육칠팔구1'.decode('utf-8')
        korman = '만억조'.decode('utf-8')
        dic = {'시': 10, '십': 10, '백': 100, '천': 1000, '만': 10000, '억': 100000000, '조': 1000000000000}
        result = 0
        tmpResult = 0
        num = 0
        for i in range(0, len(input_var)):
            token = input_var[i]
            check = kortext.find(input_var[i])
            if check == -1:
                try:
                    if korman.find(token) == -1:
                        if num != 0:
                            tmpResult = tmpResult + num * dic[token.encode('utf-8')]
                        else:
                            tmpResult = tmpResult + 1 * dic[token.encode('utf-8')]

                    else:
                        print("else")
                        tmpResult = tmpResult + num
                        if tmpResult != 0:
                            result = result + tmpResult * dic[token.encode('utf-8')]
                        else:
                            result = result + 1 * dic[token.encode('utf-8')]
                        tmpResult = 0
                except Exception as e:
                    import traceback as tb;
                    tb.print_exc()
                    print(e)
                num = 0
            else:
                num = check
        return result + tmpResult + num




class Reg(object):
    """
    Regular expression
    """
    def __init__(self, util_obj=None):
        """
        Init
        :param util_obj:
        """
        self.util_obj = util_obj
        self.year_reg = re.compile(r'(0|1|2|3|4|5|6|7|8|9|일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이){1,4}년'.decode('utf8'))
        self.month_reg = re.compile(
            r'(0|1|2|3|4|5|6|7|8|9|10|11|12|일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이|시){1,2}월'.decode('utf8'))
        self.day_reg = re.compile(r'(0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|\
                                          일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이){1,4}일'.decode('utf8'))
        self.hour_reg = re.compile(r'(0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|\
                                          일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이){1,3}시'.decode('utf8'))
        self.manwon_reg = re.compile(r'(0|1|2|3|4|5|6|7|8|9|일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이|백|천|만|억|조|경|해){1,20}원'.decode('utf8'))
        self.day_week_reg = re.compile(r'(월|화|수|목|금|토|일){,1}요일'.decode('utf8'))
        self.week_reg = re.compile(r'(이번|다음|다다음|차|금){1,3}주'.decode('utf8'))
        self.day_week = {'월': 0, '화': 1, '수': 2, '목': 3, '금': 4, '토': 5, '일': 6}
        self.contract_reg = re.compile(r'(0|1|2|3|4|5|6|7|8|9|일|하나|둘|셋|삼|사|오|육|륙|칠|팔|구|공|넷|영|십|이){1,2}번'.decode('utf8'))

    def get_day(self, text):
        """
        Get Day
        :param      text:       Text
        :return:
        """
        slots = {}
        text = text.replace(' ', '').decode('utf8')
        iter_result = list()
        for flag in range(1, 7):
            if flag == 1:
                iter_result = self.month_reg.finditer(text)
            elif flag == 2:
                iter_result = self.day_reg.finditer(text)
            elif flag == 3:
                iter_result = self.hour_reg.finditer(text)
            elif flag == 4:
                iter_result = self.day_week_reg.finditer(text)
            elif flag == 5:
                iter_result = self.week_reg.finditer(text)
            elif flag == 6:
                iter_result = self.year_reg.finditer(text)
            for val in iter_result:
                start = val.start()
                end = val.end()
                end = end-1 if flag in [1, 2, 5, 6] else (end-2)
                slot = text[start:end].encode('utf8')
                if flag in [1, 2, 3, 6]:
                    print 'target: {0}'.format(slot)
                    slot = self.util_obj.convert_number(slot)
                if flag == 1:
                    slots['month'] = slot
                elif flag == 2:
                    slots['day'] = slot
                elif flag == 3:
                    slots['hour'] = slot
                elif flag == 4:
                    slots['weekday'] = slot
                elif flag == 5:
                    slots['week'] = slot
                elif flag == 6:
                    slots['year'] = slot
        if 'weekday' in slots.keys() and 'week' in slots.keys():
            slots = self.cal_day(slots['weekday'], slots['week'])
        elif 'weekday' in slots.keys():
            slots = self.cal_day(slots['weekday'])
        # if 'year' not in slots.keys():
        #     now = datetime.datetime.now()
        #     slots['year'] = str(now.year)
        return slots

    def cal_day(self, weekday, week='이번'):
        """
        Calculate Date
        :param      weekday:        Week day
        :param      week:           Week Count
        :return:                    day Dictionary
        """

        def get_day(time):
            """
            Get Day
            :param      time:       Time
            :return:                Year, Month, Day
            """
            return time.year, time.month, time.day

        def get_target_day(temp_compare_day, mul):
            """
            Get Target Day
            :param      temp_compare_day:        Compare Day
            :param      mul:                Week Count
            :return:
            """
            temp_week = 7 * mul
            if temp_compare_day < 0:
                temp_target_day = temp_week - now_weekday
            elif temp_compare_day > 0:
                temp_target_day = temp_week + temp_compare_day
            else:
                temp_target_day = temp_week
            return temp_target_day

        now = datetime.datetime.now()
        now_weekday = now.weekday()
        target_day = self.day_week[weekday]
        if week == '이번' or week == '금':
            if weekday == '오늘':
                target_day = now_weekday
            elif weekday == '내일':
                target_day = now_weekday + 1
            elif weekday == '내일모레' or weekday == '모레':
                target_day = now_weekday + 2
            if target_day > 6:
                compare_day = target_day - 6
                target_day = get_target_day(compare_day, 1)
            target_day -= now_weekday
            target_now = now + datetime.timedelta(days=target_day)
            result = get_day(target_now)
        elif week == '다음' or week == '차':
            compare_day = target_day - now_weekday
            target_day = get_target_day(compare_day, 1)
            target_now = now + datetime.timedelta(days=target_day)
            result = get_day(target_now)
        elif week == '다다음':
            compare_day = target_day - now_weekday
            target_day = get_target_day(compare_day, 2)
            target_now = now + datetime.timedelta(days=target_day)
            result = get_day(target_now)
        else:
            return -1
        dic = {'year': result[0], 'month': result[1], 'day': result[2]}
        return dic

    def get_money(self, text):
        """
        Get Money
        :param      text:       Text
        :return:
        """
        slots = dict()
        try:
            # print self.util_obj.change(text)
            text = text.replace(' ', '').decode('utf8')
            iter_result = self.manwon_reg.finditer(text)
            print iter_result
            for val in iter_result:
                start = val.start()
                end = val.end()
                end = end - 1
                slot = text[start:end].encode('utf8')
                slot = self.util_obj.change(slot)
                slots['manwon'] = slot
        except Exception:
            pass
        return slots

    def get_pay_day(self, text):
        """
        Get Pay day
        :param      text:       Text
        :return:
        """
        slots = dict()
        try:
            # print self.util_obj.change(text)
            text = text.replace(' ', '').decode('utf8')
            iter_result = self.day_reg.finditer(text)
            print iter_result
            for val in iter_result:
                start = val.start()
                end = val.end()
                end = end - 1
                slot = text[start:end].encode('utf8')
                slot = self.util_obj.change(slot)
                slots['pay_day'] = slot
        except Exception:
            pass
        return slots

    def get_contract(self, text):
        """
        Get Contract
        :param      text:       Text
        :return:
        """
        slots = dict()
        try:
            # print self.util_obj.change(text)
            text = text.replace(' ', '').decode('utf8')
            iter_result = self.contract_reg.finditer(text)
            print iter_result
            for val in iter_result:
                start = val.start()
                end = val.end()
                end = end - 1
                slot = text[start:end].encode('utf8')
                slot = self.util_obj.change(slot)
                slots['get_contract'] = slot
        except Exception:
            pass
        return slots


class Replace():

    def basic_replace(self, log, da_info_dict):
        """
        replace Function
        :param      da_info_dict:      DA Information dictionary
        :return:                       modify DA Information Dictionary
        """

        log.info('4. convert.basic_replace()')

        # setting data
        talk = da_info_dict['talk']
        utter = talk.utter.utter
        # replace process
        replace_dict = dict()
        replace_dict['만원'] = [' 만원']
        for i in ['일', '이', '삼', '사', '오', '유', '육', '칠', '팔', '구', '십', '시']:
            replace_dict['{0}일'.format(i)] = ['{0} 일'.format(i)]
            replace_dict['{0}월'.format(i)] = ['{0} 월'.format(i)]
            replace_dict['{0}년'.format(i)] = ['{0} 년'.format(i)]
        # TODO STT 학습 필요 부분
        replace_dict['음성'] = ['이상', '삼성', '음상', '감사', '인상', '오사', '엠성', '신성']
        replace_dict['알뜰폰'] = ['아 이틀 전', '알일 번']
        replace_dict['백만원'] = ['100만원', '백만년']
        replace_dict['문자'] = ['혼자']
        replace_dict['네'] = ['내', '네에', '네네', 'ㄴ네', 'ㄴ 네', '음네', '아네', '아 네']
        replace_dict['에스케이티'] = ['에스케이']
        replace_dict['케이티'] = ['칠칠', '게이트']
        replace_dict['유플러스'] = ['육 플러스', '이 플러스']
        replace_dict['읽어주세요'] = ['일 거 주세요', '일 꺼 주세요', '일거 주세요', '일구어 주세요']
        for key, value_list in replace_dict.items():
            for value in value_list:
                if value in utter:
                    utter = utter.replace(value, key)
        # output setting data
        talk.utter.utter = utter
        da_info_dict['talk'] = talk

        return da_info_dict


    def front_check_replace(self, log, da_info_dict):

        log.info("** convert.front_check_replace()")

        # input setting
        task = da_info_dict['session_data']['current_task']
        utter = da_info_dict['talk'].utter.utter

        # data setting
        first = utter[0:1]
        second = utter[1:2]
        replace_dict = {
            'Category1': ['에스티티', '티티에스', '챗봇', '음성봇', '콜센터'],
        }

        # input log
        # log.info("\ttalk.utter.utter: {0}".format(utter))
        log.info("\tfirst: {0}, second: {1}".format(first, second))

        # process
        if second == ' ':
            word = first
        else:
            word = first + second
            if word in ['고객']:
                word = u'콜센'

        log.info("\tword : {0}, len(word): {1}".format(word, len(word)))

        for val in replace_dict[task]:
            log.info("\t@val: {0}".format(val[0:len(word)*3]))
            if word == val[0:len(word)*3]:
                log.info("\tval: {0}".format(val[0:len(word)*3]))
                utter = val

        # output log
        log.info("\tword: {0}".format(word))
        log.info("\ttalk.utter.utter: {0}".format(utter))

        # setting output
        da_info_dict['talk'].utter.utter = utter

        return da_info_dict


class Convert():

    # name=str()

    def __init__(self):
        self.name = '$name'  # 이름 slot 변환 포인트
        self.phone = '$phone'

    # 숫자를 한글로 변환
    def convert_tel_no(self, answer):
        # 숫자 -> 한글 변환용 dict
        number_info = {
            u'0':u'공', u'1':u'일', u'2':u'이', u'3':u'삼', u'4':u'사', u'5':u'오', u'6':u'육', u'7':u'칠', u'8':u'팔', u'9':u'구'    
        }

        try:
            change_answer = u''
            for target in answer:
                if target in number_info:
                    target = target.replace(target, number_info[target])
                change_answer += target
            
            return change_answer

        except Exception:
            print(traceback.format_exc())

    # slot 변환 (ex: $phone => 01011112222)
    def change_slot(self, answer, utter, phone_num):
        # slot replace
        answer = answer.replace(self.name, utter)
        answer = answer.replace(self.phone, phone_num)

        return answer


class Slot():

    # slot name 추출
    def get_slot(self, text):
        # param: text
        # param ex: "SLU1:  담당자	 #request(manager) (0.723646)"

        # slot 추출
        start = text.find('(')
        end = text.find(')')
        slot = text[start+1:end]

        # value 추출
        if slot.find('=') != -1:
            start = slot.find('"')
            end = slot.find('"')
            slot = slot[0:start-1]
            value = slot[start+1:end]
            return slot, value

        return slot, None


if __name__ == '__main__':
    service = Util()
    reg = Reg(service)
    # result = reg.get_day('다음주화요일')
    result = reg.get_money('구십팔만원')
    # result = reg.get_pay_day('이십오일')
    # result = reg.get_contract('이번')
    print result
