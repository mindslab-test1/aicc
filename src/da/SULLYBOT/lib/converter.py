#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import re
import sys
import datetime
import traceback
#import logger
###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


#############
# constants #
#############

#########
# class #
#########
# class = Util, Reg, Replace, Convert, Slot

# class Util():
#     def __init__(self):
#         pass

# class Reg():



class Replace():

    def __init__(self):

        ###################
        # def change_slot #
        ###################
        self.name = '$name'  # 이름 slot 변환 포인트
        self.phone = '$phone'

        ######################
        # def convert_tel_no #
        ######################
        # 숫자 -> 한글 변환용 dict
        self.number_info = {
            u'0':u'공', u'1':u'일', u'2':u'이', u'3':u'삼', u'4':u'사', u'5':u'오', u'6':u'육', u'7':u'칠', u'8':u'팔', u'9':u'구'
        }

        ################################
        # def remove_unnecessary_words #
        ################################
        # utter 에서 제거할 단어 리스트 (사람 이름에 포함될만한 음절은 넣으면 안됨)
        # 회사명 리스트
        self.corporate_list = [
            '마인즈랩', '마음커넥트', '마음컨설턴트', '마음컴퍼니'
        ]
        # 부서 리스트
        self.department_list = [
            '인사', '총무', '영업', '제품', '홍보', '마케팅', '광고', '재무', '회계', '아이알', '플랫폼', '브레인', '마음에이아이', '기획',
            '개발', '디자인',
            '신사업', '비즈'
        ]
        # 부서 수식어 리스트
        self.department2_list = [
            '팀', '부서'
        ]
        # 직급 리스트
        self.corporate_title_list = [
            '인턴', '매니저', '팀장', '이사', '상무', '전무', '대표',
            '박사'
        ]
        # 기타 명사 리스트
        self.noun_list = [
            '연결', '교환', '바꿔', '기타'
        ]
        # 조사 리스트
        self.postposition_list = [
            '은', '는', '이', '가', '에', '의', '에게', '을', '를', '으로', '로', '와', '과', '아', '야', '만', '뿐'
        ]
        # 연결어 리스트
        self.connection_word_list = [
            '있으면', '있어', '보이면', '보여', '알면', '아시면'
        ]
        # 동사 리스트
        self.verb_list = [
            '해줘', '해주세요', '있어', '있어요', '있나요', '있습니다',
            '알아요', '아시나요', '바꿔줘', '바꿔주세요',
            '있지', '할수', '해줄수'
        ]
        # 존칭어 리스트
        self.honorific_word_list = [
            '님', '씨'
        ]
        # 사람 성 리스트
        self.last_name_list = "(강|고|곽|권|김|남|류|박|배|백|서|선|손|송|신|심|안|오|옥|우|원|유|윤|이|임|장|정|조|진|최|한|현|형|홍|황)"

        #######################
        # def division_korean #
        #######################
        # 초성 리스트. 00 ~ 18
        self.choseong_list = [
            'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'
        ]
        self.jungseong_list = [
            'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ', 'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ', 'ㅗ', 'ㅘ',
            'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ', 'ㅣ'
        ]
        self.jongseong_list = [
            ' ', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ','ㄽ', 'ㄾ',
            'ㄿ', 'ㅀ', 'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'
        ]

    # 숫자를 한글로 변환
    def convert_tel_no(self, log, answer):

        log.info("** converter.convert_tel_no()")

        try:
            change_answer = u''
            for target in answer:
                if target in self.number_info:
                    target = target.replace(target, self.number_info[target])
                change_answer += target

            return change_answer

        except Exception:
            print(traceback.format_exc())

    # slot 변환 (ex: $phone => 01011112222)
    def change_slot(self, log, da_info_dict, total_answer, phone_num):

        log.info("** converter.change_slot()")

        if self.name in total_answer and da_info_dict.has_key('emp_name'):
            total_answer = total_answer.replace(self.name, da_info_dict['emp_name'])
        if self.phone in total_answer:
            total_answer = total_answer.replace(self.phone, phone_num)

        return total_answer

    # utter 에서 직원명 빼고 다 제거
    def remove_unnecessary_words(self, utter, log):
        # parameter 로 받는 utter 는 이미 공백제거하고 받은 utter 임

        # (회사명), (회사명+조사) 제거
        for corporate in self.corporate_list:
            if corporate in utter:
                for postposition in self.postposition_list:
                    if corporate + postposition in utter:
                        utter = utter.replace(corporate + postposition, '')
                if corporate in utter:
                    utter = utter.replace(corporate, '')

        # (부서명), (부서명+수식어), (부서명+조사), (부서명+수식어+조사) 제거
        for department in self.department_list:
            if department in utter:
                for department2 in self.department2_list:    # (부서명+수식어)
                    if department + department2 in utter:
                        for postposition in self.postposition_list:  # (부서명+수식어+조사)
                            if department + department2 + postposition in utter:
                                utter = utter.replace(department + department2 + postposition, '')  # (부서명+수식어+조사) 제거
                        if department + department2 in utter:
                            utter = utter.replace(department + department2, '') # (부서명+수식어) 제거
                for postposition in self.postposition_list:  # (부서명+조사)
                    if department + postposition in utter:
                        utter = utter.replace(department + postposition, '')    # (부서명+조사) 제거
                if department in utter:
                    utter = utter.replace(department, '')   # (부서명) 제거

        # 기타 명사 제거
        for noun in self.noun_list:
            if noun in utter:
                utter = utter.replace(noun, '')

        # 직급 제거
        for corporate_title in self.corporate_title_list:
            if corporate_title in utter:
                utter = utter.replace(corporate_title, '')

        # 연결어 제거
        for connection_word in self.connection_word_list:
            if connection_word in utter:
                utter = utter.replace(connection_word, '')

        # 동사 제거
        for verb in self.verb_list:
            if verb in utter:
                utter = utter.replace(verb, '')

        # 존칭어 제거
        for honorific_word in self.honorific_word_list:
            if honorific_word in utter:
                utter = utter.replace(honorific_word, '')

        # 정규식으로 이름 3글자까지 추출 (19.12.03 기준 4글자 이름없음, 2글자는 '백훈'만 있음)
        # pattern = re.compile(r'[가-힣]{2,4}')    # 한글 2~4자
        # pattern = re.compile(r'[가-힣]{2,3}')    # 한글 2~4자
        pattern = re.compile(r'' + self.last_name_list + '[가-힣]{6}')  # 성 포함 3글자 추출 패턴
        if re.search(pattern, utter):
            log.info("\t*** Regular expresion ***")
            match = re.search(pattern, utter)
            utter = match.group()

        # 정규식으로 이름 추출해서 예외 이름에 대한 처리  (ex: 2글자 이름 or 4글자 이름 등)
        # exception_name = ['백훈']
        # for name in exception_name:
        #     if name in utter:

        return utter

    # 한글을 자,모음으로 분리하여 list type 으로 return (consonant vowel)
    def division_korean(self, korean_word):

        korean_word = korean_word.replace(' ', '')
        result_list = list()
        for word in list(korean_word.strip()):
            if u'가' <= word <= u'힣':
                ## 588개 마다 초성이 바뀜.
                choseong_idx = (ord(word) - ord(u'가'))//588
                ## 중성은 총 28가지 종류
                jungseong_idx = ((ord(word) - ord(u'가')) - (588*choseong_idx)) // 28
                jongseong_idx = (ord(word) - ord(u'가')) - (588*choseong_idx) - 28 * jungseong_idx
                # LOGGER.debug('choseong_idx: {0}\tjungseong_idx: {1}\tjongseong_idx: {2}\tword: {3}'.format(choseong_idx, jungseong_idx, jongseong_idx, word))
                result_list.append(
                    [self.choseong_list[choseong_idx], self.jungseong_list[jungseong_idx], self.jongseong_list[jongseong_idx]])
            else:
                result_list.append([word, word, word])
        return result_list


class Slot():

    # slot name 추출
    def get_slot(self, text):
        # param: text
        # param ex: "SLU1:  담당자	 #request(manager) (0.723646)"

        # slot 추출
        start = text.find('(')
        end = text.find(')')
        slot = text[start+1:end]

        # value 추출
        if slot.find('=') != -1:
            start = slot.find('"')
            end = slot.find('"')
            slot = slot[0:start-1]
            value = slot[start+1:end]
            return slot, value

        return slot, None

class Analysis():

    def similar(self, a, b, log):

        # log.info("** converter.similar()")
        # log.info("\ta word: {0}".format(a))
        # log.info("\tb word: {0}".format(b))

        a_list = Replace().division_korean(a)
        b_list = Replace().division_korean(b)
        total_count = 0
        true_count = 0
        b_word_idx = 0
        a_word_idx = 0
        # 글자 수 많은 것을 a에 저장
        if len(a_list) < len(b_list):
            temp = b_list
            b_list = list(a_list)
            a_list = list(temp)
        # 두 단어 비교 시작
        while True:
            # 두 단어의 남은 글자수가 없는 경우
            if len(a_list) <= a_word_idx and len(b_list) <= b_word_idx:
                break
            total_count += 3
            save_word_idx = a_word_idx
            jamo_true_count_dict = dict()
            # b의 대상과 a 리스트 비교 시작
            while True:
                # 두 음절 비교 시작 (두 단어의 idx 체크하여 몇개 음절까지 비교를 할지 로직도 있음)
                for jamo_idx in range(0, 3):
                    if len(a_list) > b_word_idx and len(b_list) > b_word_idx:
                        # if a_list[save_word_idx][jamo_idx] == '':
                        #     continue
                        if a_list[save_word_idx][jamo_idx] == b_list[b_word_idx][jamo_idx]: # a_list와 b_list에 동일한 idx의 음절에 일치하는 자모음이 있으면
                            if save_word_idx not in jamo_true_count_dict:
                                jamo_true_count_dict[save_word_idx] = 0 # a와 b가 일치하는 자모음이 없으면 jamo_true_count_dict['idx'] = 'score' => ({0: 0})
                            jamo_true_count_dict[save_word_idx] += 1    # a와 b가 일치하는 자모음이 있으면 jamo_true_count_dict['idx'] = 'score' => ({0: 0+1})
                if len(a_list) - save_word_idx != len(b_list) - b_word_idx: # b_list 의 비교할 음절 idx 부터 음절 length 만큼 a_list 에서 비교
                    save_word_idx += 1  # (ex: a_list: '가나다라', b_list: '나마바' 이면 b에서 '나'를 비교하면 a에서 '가'~'나'까지만 비교 , 일치한 음절을 찾으면 break)
                else:
                    break
            final_true_count = 0
            final_word_idx = b_word_idx

            # log.info('-' * 10)
            # log.info('jamo_true_count_dict: {0}'.format(jamo_true_count_dict))

            # b 대상과 a 리스트 중 가장 점수가 높은 a 선택
            for save_word_idx, save_true_count in jamo_true_count_dict.items():
                if final_true_count < save_true_count:
                    final_true_count = save_true_count
                    final_word_idx = save_word_idx

            # log.info('final_true_count: {0}, final_word_idx: {1}'.format(final_true_count, final_word_idx))

            true_count += final_true_count  # 결과 합산
            # 선택된 a 리스트 위치 저장 및 b의 다음대상 시작 준비
            a_word_idx = final_word_idx
            a_word_idx += 1
            b_word_idx += 1
        # 최종 점수 계산
        total_count += (a_word_idx - b_word_idx) * 3
        ratio = float(true_count) / total_count
        return ratio


if __name__ == '__main__':
    service = Util()
    reg = Reg(service)
    result = 'converter.py success'
    # result = reg.get_day('다음주화요일')
    # result = reg.get_money('구십팔만원')
    # result = reg.get_pay_day('이십오일')
    # result = reg.get_contract('이번')
    print result
