#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import traceback


#########
# class #
#########
class MySQL(object):
    def __init__(self, conf):
        import pymysql
        self.conf = conf
        self.conn = pymysql.connect(
            host=self.conf.host,
            port=self.conf.port,
            user=self.conf.user,
            passwd=self.conf.passwd,
            db=self.conf.db,
            charset=self.conf.charset
        )
        self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)

    def disconnect(self):
        try:
            self.cursor.close()
            self.conn.close()
        except Exception:
            raise Exception(traceback.format_exc())
