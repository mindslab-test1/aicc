#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import pdb
import os
import re
import sys
import grpc
import time
import traceback
import ConfigParser
from sds import SDS
from datetime import datetime
from concurrent import futures
sys.path.append('..')
from task_worker import task_worker
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'da'))
#from custom.ngram_classifier import NgramClassifier
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'da/qa'))
from hmd import HmdClient
from util import Util
from basicQA import BasicQA
# from sds.sds import SDS
from wiseQA_2 import WiseQA
from kbqa_client import KbqaClient
from custom.minds_gspread import GS

###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


#########
# class #
#########
class DA_Talk():
    __CONFIG_FILE__ = "/srv/maum/da/qa/qa.cfg"
    _config = ConfigParser.ConfigParser()
    _config.read(__CONFIG_FILE__)

    def __init__(self, question="", session_id="", model="", skill_id=0, com_id=0, lang='kor', talk=None, logger=None):
        # Q Args
        self.lang = lang                # 언어
        self.skill_id = skill_id        # Skill ID
        self.com_id = str(com_id)       # Company ID
        # self.question = question        # 질문
        self.question = talk.utter.utter    # 질문
        self.talk = talk
        # self.session_id = session_id    # Session ID
        self.session_id = talk.session.id   # Session ID
        self.model = model              # SDS 모델명
        self.logger = logger            # Logger
        print 'hi1'
        # A Args
        self.flag = False               # 엔진 답변 여부
        self.answer = ""                # 답변
        self.rcm_list = list()          # 질문추천 리스트
        self.code = ""                  # 답변 CODE
        self.engine = "None"            # 답변 엔진
        self.path = list()              # 엔진이 거쳐온 경로
        self.weight = 0                 # 답변확률 or 가중치
        self.sds_res = dict()           # SDS 응답
        self.status = False
        self.meta = dict()
        self.extra = False              # 키패드 on/off 설정 값
        self.transfer = ""              # 콜 연결 직원 값
        self.tts = str()                # tts 발화 중지 여부 결정 값
        print 'hi'

    def __repr__(self):
        return self.answer
    
    def call_bqa_exact(self, skill_id=-1):
        """ BQA Exact Maching """
        if self.flag == False:
            self.path.append("BasicQA_0")
            self.answer = self.bqa(0, skill_id=skill_id).strip()
            if self.flag == True:
                self.engine = "BasicQA_0"
        return self.answer

    def call_bqa_1st(self, skill_id=-1):
        """ BQA 1차 검색 """
        if self.flag == False:
            self.path.append("BasicQA_1")
            self.answer = self.bqa(1, skill_id=skill_id).strip()
            if self.flag == True:
                self.engine = "BasicQA_1"
        return self.answer

    def call_bqa_2nd(self, skill_id=-1):
        """ BQA 2차 검색(질문추천) 중 첫 번째 답변 """
        if self.flag == False:
            self.path.append("BasicQA_2")
            self.answer = self.bqa(2, skill_id=skill_id).strip()
            if self.flag == True:
                self.engine = "BasicQA_2"
        return self.answer

    def call_recommend(self, skill_id=-1, num=3):
        """ 질문추천 """
        if self.flag == False:
            self.path.append("BasicQA_3")
            self.answer = self.bqa(qa_type=3, skill_id=skill_id, num=3)\
                .strip()
            if self.flag == True:
                self.engine = "BasicQA_3"
        return self.answer

    def call_recommend_button(self, comment=None, skill_id=-1, num=3):
        """ 추천질문 버튼 구분자로 추가"""
        if comment == None:
            comment = '혹시 이런 질문을 찾으시나요?'
        if self.flag == False:
            self.call_recommend(skill_id=skill_id, num=num)
            button = "$$VB_S$$"
            for i in self.rcm_list:
                button += "{},".format(i)
            button += "$$VB_F$$"
            self.answer = comment.strip()
            self.answer += button
        return self.answer

    def bqa(self, qa_type, skill_id=-1, num=3):
        """ BQA 답변 받아오기 """
        if skill_id != -1:
            self.skill_id = skill_id
        if self.skill_id == -1:
            raise
        bq = BasicQA(self._config.get("URL", "BASICQA"))
        meta = dict()
        self.flag = False
        res = ""
        try:
            if qa_type == 0:
                res, self.flag = bq.exact_run(
                    self.question, 10, meta, self.skill_id, self.lang
                ) # Exact Matching
            elif qa_type == 1:
                res, self.flag = bq.run(
                    self.question, 10, meta, self.skill_id, self.lang
                ) # 1차 검색
            elif qa_type == 2:
                res, self.flag = bq.run_2nd(
                    self.question, 10, meta, self.skill_id, self.lang
                ) # 2차 검색
            else:
                self.rcm_list, self.flag = bq.recommend(
                    self.question, 10, meta, self.skill_id, self.lang, 
                    num=num
                ) # 질문 추천
        except grpc.RpcError as e:
            import traceback as tb; tb.print_exc()
            print(e)
            res = ""
            self.flag = False
            if e.code().value[0] == 4:
                print("Time out in basic qa")
            else:
                print("Fail to connect basic qa")
        except Exception as e:
            import traceback as tb; tb.print_exc()
            print(e)
            res = ""
        if qa_type != 3:
            res = self.validate_check(res)
        return res

    def call_kbqa(self):
        """ KBQA """
        if self.flag == False:
            self.path.append("KBQA")
            kb = KbqaClient(self._config.get("URL", "KBQA"))
            self.answer, self.flag = kb.run(self.question.strip())
            if self.flag == True:
                self.engine = "KBQA"
        return self.answer

    def call_irqa(self):
        """ EXO Brain """
        if self.flag == False:
            self.path.append("EXO")
            if len(self.question) == 0:
                return "정보를 찾을 수 없습니다."
            self.__TIMEOUT__ = int(self._config.get("TIMEOUT", "TIMEOUT"))
            res = ''
            nlqa_result = ''
            try:
                wise_QA = WiseQA(self._config.get("URL", "WISEQA"))
                res, self.flag, self.weight, _ = \
                    wise_QA.run(
                        question=self.question, timeout=self.__TIMEOUT__
                    )
                print("[EXO res]: '%s'" % (res))
                print("[EXO flag]: '%s'" % (self.flag))
            except grpc.RpcError as e:
                import traceback as tb; tb.print_exc()
                res = ''
                if e.code().value[0] == 4:
                    print("Time out in irqa")
                else:
                    print("Fail to connect irqa")
            except Exception as e:
                import traceback as tb; tb.print_exc()
                res = ''
                print(e)
                print("Fail to get irqa answer")
            else:
                if self.flag:
                    self.engine = 'EXO'
            self.answer = self.validate_check(res).strip()
        return self.answer

    def call_sds(self, session_id=-1, model=""):
        self.logger.info("* talk.call_sds() start")
        try:
            """ SDS """
            if session_id != -1:
                self.session_id = session_id
            if model != "":
                self.model = model
            self.sds_res = dict()
            if not self.flag:
                try:
                    self.path.append("SDS")
                    sds = SDS()
                    # if self.talk.session.context['nqa_process_yn'] == 'N':
                    #     self.sds_res = sds.Talk(self.question, self.session_id, self.model)
                    try:
                        contract_no = self.talk.utter.meta['contract_no']
                    except ValueError:
                        contract_no = 'chatbot'
                    self.logger.info("\tcontract_no: {0}".format(contract_no))
                    system_answer, talk, self.sds_res, self.status, self.meta, self.extra, self.transfer, self.tts = task_worker(
                        self.sds_res, self.talk, sds, self.model)
                    # print(type(self.sds_res))
                    # print(self.sds_res)
                    if len(self.sds_res['response'].strip().replace(' ', '')) > 1:
                        self.flag = True
                        #self.answer = self.sds_res['response'].strip()
                        self.answer = system_answer
                        self.talk = talk
                        self.engine = "SDS"
                except Exception as e:
                    import traceback as tb; tb.print_exc()
                    # print('** talk.call_sds() Exception 1')
                    self.logger.error(e)
                    print(e)
                    self.sds_res = dict()
                    system_answer = ''
                    self.answer = system_answer
                    talk = self.talk
            self.logger.info("* talk.call_sds() end")
            return (system_answer, talk)
        except Exception:
            print('** talk.call_sds() Exception 2')
            self.logger.error(traceback.format_exc())
            raise Exception(traceback.format_exc())

    def close_sds(self, session_id=-1, model=""):
        if session_id != -1:
            self.session_id = session_id
        if model != "":
            self.model = model
        sds = SDS()
        sds.GetSdsServer(self.model)
        sds.CloseTalk(self.session_id, self.model)
    
    def call_ngram(self, text='', com_id=-1):
        """ NGRAM """
        if com_id != -1:
            self.com_id = com_id
        if self.flag == False:
            self.path.append("NGRAM")
            nc = NgramClassifier()
            if text == '':
                text = self.answer
            print("[NGRAM]: <{}> <{}>".format(
                text, self.com_id
            ))
            self.flag, self.answer, self.weight = nc.ngram_classify(
                text, self.com_id
            )
            if self.flag == True:
                self.engine = "NGRAM"
        return self.answer

    def validate_check(self, res):
        """ 유효한 답변인지 체크 """
        if self.engine != "BasicQA_3" and len(res.strip()\
                                                 .replace(' ', '')) < 1:
            self.flag = False
            res = ""
        return res

    def call_hmd(self, filepath):
        """ hmd 파일 경로를 입력 hmd 파일에 정의된 intent를 출력 """
        if self.flag == False:
            hmd = HmdClient(filepath)
            hmd.set_model()
            hmd_intent = hmd.get_class_bytext(self.question)
            print("[HMD_RESULT]: {}".format(hmd_intent))
            if hmd_intent == 'notfoundIntents':
                hmd_intent = ''
            self.hmd_intent = hmd_intent
            return self.hmd_intent
        return ''

    def code_to_text(self, com_id=0):
        """ self.answer BQA 코드-> 답변 """
        if com_id == 0:
            com_id = self.com_id
        if str(self.answer.upper().strip()) != 'UNKNOWN':
            self.code = self.answer
            gs = GS() 
            res = gs.convert_code(self.code, com_id)
            if res.upper() != 'UNKNOWN':
                self.answer = res
            return self.answer

    def print_result(self):
        """ 변수 출력 """
        print("[QUESTION]: %s" %(self.question))
        print("[PATH]: %s" %(self.path))
        print("[ENGINE]: %s" %(self.engine))
        print("[FLAG]: %s" %(self.flag))
        print("[RECOMMENDATION]")
        for i in self.rcm_list:
            print("    -> %s" % (str(i)))
        print("[CODE]: %s" % (self.code))
        print("[ANSWER]: %s" %(self.answer))
    #
    # def chat_log(self, chatbot, device):
    #     """ 채팅로그 기록 """
    #     self.chat_logger = CHAT_LOG()
    #     req_time = datetime.today().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]
    #     self.chat_logger.write(
    #         model=chatbot,
    #         question=self.question,
    #         answer=self.answer,
    #         src=self.engine, # 답변엔진
    #         weight=self.weight,
    #         date=req_time, # 답변 시간
    #         code=self.code, # 답변 CODE
    #         session_id=self.session_id, # 세션ID
    #         device=device # 장치
    #     )


class NoneSkillID(Exception):
    def __str__(self):
        return "Basic QA에 skill_id 가 존재하지 않습니다."


########
# main #
########
if __name__ == "__main__":
    filepath = "/srv/maum/samples/testdd/hmd/HumanCareHmd.txt"
    qna = DA_Talk(question = "회사소개서 줘 봐봐봐봐요")
    # answer = qna.call_ngram(com_id='7_kakao')
    answer = qna.call_hmd(filepath=filepath)
    print(answer)
