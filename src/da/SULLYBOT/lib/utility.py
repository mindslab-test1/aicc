#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 2019-05-28, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import pymysql
import traceback
import db_connection
# sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
sys.path.append('/home/happy/maum/da/SULLYBOT/')
from cfg import config


#######
# def #
#######
'''
def update_mnt_target_mng(log, mysql, info_dict):
    """
    UPDATE MNT_TARGET_MNG
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:
    """
    try:
        query = """
            UPDATE
                MNT_TARGET_MNG
            SET
                task = %s
            WHERE 1=1
                AND contract_no = %s
        """
        bind = (
            info_dict['task'],
            info_dict['contract_no'],
        )
        mysql.cursor.execute(query, bind)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        raise Exception(traceback.format_exc())
'''


def update_call_history(log, mysql, info_dict):
    """
    UPDATE CALL_HISTORY
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    bool
    """
    query = str()
    bind = tuple()
    try:
        query = """
            UPDATE
                CALL_HISTORY
            SET
                mnt_status = %s,
                mnt_status_name = (SELECT cd_desc FROM CM_COMMON_CD WHERE 1=1 AND code = substr(%s, 1, 6))
            WHERE 1=1
                AND contract_no = %s
                AND call_id = %s
        """
        bind = (
            info_dict['call_status'],
            info_dict['call_status'],
            info_dict['contract_no'],
            info_dict['call_id'],
        )

        mysql.cursor.execute(query, bind)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        log.error(query)
        log.error(bind)
        raise Exception(traceback.format_exc())


def select_mnt_target_mng(log, mysql, info_dict):
    """
    Select Cust meta
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    Cust Meta
    """
    log.debug("Select MNT_TARGET_MNG")
    try:
        query = """
            SELECT  /* select_mnt_target_mng */
                tel_no
            FROM
                CM_CONTRACT
            WHERE 1=1
                AND contract_no = %s
        """
        bind = (
            info_dict['contract_no'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())


def select_phone_book(log, mysql, talk):
    """
    Select Cust meta
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    Cust Meta
    """
    log.debug("Select PHONE_BOOK")
    try:
        query = """
            SELECT  /* select_phone_book */
                tel_no
            FROM
                CM_PHONE_BOOK
            WHERE 1=1
                AND department = %s
        """
        bind = (
            talk,
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results[0]
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())


def select_employee_info(log, mysql):
    """
    select employee info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:
    """
    log.debug("Select CM_EMPLOYEE_INFO")
    try:
        query = """
            SELECT  /* select_cm_employee_info */
                employee_id, employee_name, corporate_name, department, corporate_title, phone_number
            FROM
                CM_EMPLOYEE_INFO
        """

        mysql.cursor.execute(query)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())


def delete_employee_info(log, mysql):
    """
    delete emplyee info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:
    """
    query = str()
    bind = tuple()
    log.debug("Delete CM_EMPLOYEE_INFO")
    try:
        query = """
            DELETE
            FROM CM_EMPLOYEE_INFO
        """
        mysql.cursor.execute(query)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        log.error(query)
        log.error(bind)
        raise Exception(traceback.format_exc())


def delete_insert_employee_info(log, mysql, values):
    """
    Insert emplyee info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:
    """
    delete_query = str()
    insert_query = str()
    log.debug("Delete -> Insert CM_EMPLOYEE_INFO")
    try:
        delete_query = """
            DELETE
            FROM CM_EMPLOYEE_INFO
        """

        insert_query = """
            INSERT INTO CM_EMPLOYEE_INFO (EMPLOYEE_ID, EMPLOYEE_NAME, CORPORATE_TITLE, PHONE_NUMBER, CORPORATE_NAME, DEPARTMENT)
            VALUES (%s, %s, %s, %s, %s, %s)
        """
        mysql.cursor.execute(delete_query)
        mysql.cursor.executemany(insert_query, values)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        log.error(delete_query)
        log.error(insert_query)
        log.error(values)
        raise Exception(traceback.format_exc())


# def select_employee_phone_number(log, mysql, emp_name):
#     """
#     select employee phone number
#     :param      log:            Logger
#     :param      mysql:          MySQL
#     :param      info_dict:      Information Dictionary
#     :return:
#     """
#     log.debug("select_employee_phone_number")
#     try:
#         query = """
#             SELECT  /* select_employee_phone_number */
#                 phone_number
#             FROM
#                 CM_EMPLOYEE_INFO
#             WHERE
#                 employee_name = %s
#         """
#         bind = (
#             emp_name,
#         )
#         mysql.cursor.execute(query, bind)
#         results = mysql.cursor.fetchall()
#         if results is bool:
#             return list()
#         if not results:
#             return list()
#         return results
#     except Exception:
#         mysql.conn.rollback()
#         raise Exception(traceback.format_exc())

def db_connect():
    """
    Execute DB connect
    :return:                DB object
    """
    cfg = config.MysqlConfig
    try:
        mysql = db_connection.MySQL(cfg)
        return mysql
    except pymysql.DatabaseError as e:
        exc_info = traceback.format_exc()
        raise Exception(exc_info)
    except Exception:
        exc_info = traceback.format_exc()
        raise Exception(exc_info)




'''
# 현대해상 음성봇 관련 쿼리
def insert_hc_hh_campaign_score(log, mysql, info_dict):
    """
    Insert HC_HH_CAMPAIGN_SCORE
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    bool
    """
    try:
        query = """
            INSERT INTO HC_HH_CAMPAIGN_SCORE (  /* insert_hc_hh_campaign_score */
                call_id,
                contract_no,
                info_seq,
                info_task,
                task_value,
                review_coment
            )
            VALUES (
                %s, %s, %s, %s, null, null
            )
        """
        bind = (
            info_dict['call_id'],
            info_dict['contract_no'],
            info_dict['seq'],
            info_dict['task'],
        )
        mysql.cursor.execute(query, bind)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        raise Exception(traceback.format_exc())
        

def select_hc_hh_campaign_info(log, mysql, task):
    """
    Select HC_HH_CAMPAIGN_INFO
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      task:           Task name
    :return:                    HC_HH_CAMPAIGN_INFO Information list
    """
    try:
        query = """
            SELECT
                seq,
                task_info
            FROM
                HC_HH_CAMPAIGN_INFO
            WHERE 1=1
                AND camp_id = '6'
                AND task = %s
        """
        bind = (
            task,
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())
        
        
def select_hc_hh_campaign_score(log, mysql, info_dict):
    """
    Select HC_HH_CAMPAIGN_SCORE
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    HC_HH_CAMPAIGN_SCORE Information
    """
    try:
        query = """
            SELECT  /* select_hc_hh_campaign_score */
                seq_num
            FROM 
                HC_HH_CAMPAIGN_SCORE
            WHERE 1=1
                AND contract_no = %s
                AND call_id = %s
        """
        bind = (
            info_dict['contract_no'],
            info_dict['call_id'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())


def select_cust_loan_info(log, mysql, info_dict):
    """
    Select Cust_loan_info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      information dictionary
    :return:                    Customer loan information
    """
    log.debug("Select cust loan info")
    try:
        query = """
            SELECT  /* select_cust_loan_info */
                seq,
                loan_month_inter_price,
                create_dtm,
                calc_inter_price,
                loan_price
            FROM
                CUST_LOAN_INFO
            WHERE 1=1
                AND cust_id = %s
        """
        bind = (
            info_dict['cust_id'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())
        
def update_hc_hh_campaign_score(log, mysql, info_dict):
    """
    Update HC_HH_CAMPAIGN_SCORE
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    bool
    """
    try:
        query = """
            UPDATE
                HC_HH_CAMPAIGN_SCORE
            SET
                task_value = %s
            WHERE 1=1
                AND contract_no = %s
                AND info_seq = %s
        """
        bind = (
            info_dict['Result'],
            info_dict['contract_no'],
            info_dict['No'],
        )
        mysql.cursor.execute(query, bind)
        if mysql.cursor.rowcount > 0:
            mysql.conn.commit()
            return True
        else:
            mysql.conn.rollback()
            return False
    except Exception:
        mysql.conn.rollback()
        mysql.disconnect()
        raise Exception(traceback.format_exc())





def select_prod_info(log, mysql, info_dict):
    """
    Select prod_info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      information dictionary
    :return:                    Product infomation list
    """
    log.debug("select_prod_info")
    try:
        query = """
            SELECT  /* select_loan_meta */
                prod_id,
                prod_nm
            FROM
                PROD_INFO
            WHERE 1=1
                AND prod_id = %s
        """
        bind = (
            info_dict['prod_id'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())
        
def select_cust_loan_detail_info(log, mysql, info_dict):
    """
    Select Cust_loan_detail_info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      information dictionary
    :return:                    Customer loan detail information
    """
    log.debug("Select cust loan detail info")
    try:
        query = """
            SELECT  /* select_cust_loan_detail_info */
                prod_id,
                prod_loan_price,
                prod_interest_rate
            FROM
                CUST_LOAN_DETAIL_INFO
            WHERE 1=1
                AND cust_id = %s
        """
        bind = (
            info_dict['cust_id'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())


def select_cust_ins_info(log, mysql, info_dict):
    """
    Select Cust_ins_info
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    customer ins information
    """
    log.debug("Select loan meta")
    try:
        query = """
            SELECT  /* select_cust_ins_info */
                loan_avail_money,
                recent_payment_date,
                prod_id,
                bank_name,
                bank_acc_num,
                loan_interest_rate,
                cancel_refund_rate,
                cancel_refund,
                maturity_refund
            FROM
                CUST_INS_INFO
            WHERE 1=1
                AND cust_id = %s
            ORDER BY
                loan_interest_rate
        """
        bind = (
            info_dict['cust_id'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())
        
        
def select_cust_base_info(log, mysql, info_dict):
    """
    Select CUST_BASE_INFO
    :param      log:            Logger
    :param      mysql:          MySQL
    :param      info_dict:      Information Dictionary
    :return:                    Cust Meta
    """
    log.debug("Select CUST_BASE_INFO")
    try:
        query = """
            SELECT  /* select_cust_base_info */
                cust_id,
                cust_tel_no,
                cust_nm,
                cust_tel_comp,
                tel_comp_save_yn,
                certifi_no
            FROM
                CUST_BASE_INFO
            WHERE 1=1
                AND cust_tel_no = %s
                AND SUBSTR(jumin_no, 1, 6) = %s
        """
        bind = (
            info_dict['cust_tel_no'],
            info_dict['jumin_no'],
        )
        mysql.cursor.execute(query, bind)
        results = mysql.cursor.fetchall()
        if results is bool:
            return list()
        if not results:
            return list()
        return results
    except Exception:
        mysql.conn.rollback()
        raise Exception(traceback.format_exc())                                
'''
