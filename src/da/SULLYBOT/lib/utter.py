#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import sys
import json
from collections import OrderedDict

###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


###########
# session #
###########

#############
# constants #
#############
__COMPLETE__ = 'complete'               # 'status': 통화종료를 뜻하는 상태값?
__TRANSFERRED__ = 'transferred'         # 'status': 다른 phone 으로 연결 상태값?
__AGENT__ = 'agent'                     # 'transfer': '고객전화번호' or 'agent'  => 고객전화번호 연결 or PC Agent 연결
__STOP__ = 'stop'                       # 'tts': 현재 AI 발화중지요청, silence 항목이 없을경우 기본 1초 묵음이 추가됨
# __CALL_TRANSFER__ = 'transferred'
# __CALLBACK__ = 'callback'


#######
# def #
#######

def check_task_group(task, detect_yn):
    """
    Task 별 성공여부에 따른 task 그룹 설정
    :param      task:           Task
    :param      detect_yn:      Detect Result
    :return:                    Task Group
    """
    group_dict = dict()
    group_dict['Y'] = {
        'task_group_name': ['task_name1', 'task_name2'],
    }
    group_dict['N'] = {
        'task_group_name': ['task_name1', 'task_name2'],
    }
    task_group = str()
    if detect_yn in group_dict:
        for group_name, task_member_list in group_dict[detect_yn].items():
            if task in task_member_list:
                task_group = group_name
                break
    return task_group


def check_detect_yn(task, intent):
    """
    Task 별 의도에 따른 탐지 결과 결정 및 반환
    :param      task:           Task
    :param      intent:         Intent
    :return:                    Detect Result
    """
    check_dict = dict()
    check_dict['Y'] = {
        'task_name': ['intent1', 'intent2'],
    }
    check_dict['N'] = {
        'task_name': ['intent1', 'intent2'],
    }
    detect_result = str()
    for answer, task_dict in check_dict.items():
        if task in task_dict:
            if intent in task_dict[task]:
                detect_result = answer
    return detect_result


def ignore_check(task, intent, slot):
    """
    TTS 발화 도중 task 별 의도 확인하여 return 이 True 이면 무시, False 이면 SDS 호출하여 답변
    :param      task:           Task
    :param      intent:         Intent
    :return:                    Bool
    """
    intent_dict = {
        # 'Greet': ['sales', 'other_key1', 'other_key2', 'other_key3', 'other_key4', 'request'],
        'Greet': ['request', 'affirm', 'negate'],
        'Callback': ['affirm', 'negate'],
        'CallbackResult': ['negate'],

        # 'Category1': ['other_key1', 'other_key2', 'other_key3', 'other_key4', 'product', 'request'],
        # 'Category2': ['negate', 'request', 'unknown'],
        # 'Inform': ['affirm', 'negate'],
    }

    slot_dict = {
        'Greet': ['agent', 'marketing', 'humanResources', 'finance', 'flatform', 'employeeNotFind', 'employeeFind'],
        # 'Category1': ['keyword', 'product', 'manager'],
        # 'Category2': ['manager'],
        # 'Agent': ['manager'],
    }

    # val_dict = {
    #     'Category1': ['기타'],
    # }

    # return 이 True 이면 사용자 질문을 무시, False 이면 SDS 호출하여 답변을 return 함
    if task in intent_dict:
        if intent in intent_dict[task]:
            if slot == '':
                return False
            elif slot in slot_dict[task]:
                return False
            else:
                return True
        else:
            return True
    else:
        return True


def Greet(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Task1 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    if not is_working_time:
        utter = """
        죄송합니다.
        지금은 업무시간이 아닙니다.
        업무시간은 평일 오전 아홉 시 삼십 분 부터 오후 여섯 시 삼심 분 까지 입니다.
        가능한 시간에 담당자가 연락드리길 원하시면 네 라고 말씀해주세요.
        """
    elif is_lunch_time:
        utter = """
        열두 시 부터 한 시 까지는 점심식사시간 입니다.
        가능한 시간에 담당자가 연락드리길 원하시면 네 라고 말씀해주세요.
        """
    else:
        utter += """
        """

    return utter, sds_flag, status


def Employee(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Employee 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def Department(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Department 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def Escape(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Escape 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def Agent(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Agent 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def Callback(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Task1 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def CallbackResult(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    CallbackResult 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def PhoneNumKeypad(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    PhoneNumKeypad 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def CallWait(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    CallWait 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def CallWaitResult(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    CallWaitResult 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    return utter, sds_flag, status


def end(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time):
    """
    Task1 답변 문구 정의
    :param      sds_res             sds_res ( 의도 추출 )
    :param      session_data        세션 정보
    :param      sds_flag            sds_flag ( SDS 재처리 작업 시 insert 문장 )
    :param      status              status
    :return:                        utter ( 답변 문구 )
    """
    utter = str()
    status = False
    intent = sds_res['intent']
    utter = sds_res['response']

    # if intent in ['other_key1', 'other_key2', 'other_key3', 'other_key4', 'request']:
    if intent in ['request']:
        status = __TRANSFERRED__
    else:
        status = __COMPLETE__

    return utter, sds_flag, status


def call_forward_guide(task, intent, exit_count):
    """
    호전환 안내 발화
    :param    intent:    intent
    :return:
    """
    utter = str()

    if exit_count == 1:
        utter = """
        말씀하신 내용을 제가 잘 알아듣지 못하였습니다.
        한번더 정확하게 말씀해주시겠어요?
        """
    elif exit_count >= 2:
        utter = """
        죄송합니다.
        말씀하신 내용을 제가 잘 알아듣지 못하였습니다. 
        상담 직원에게 전화를 연결해 드리겠습니다. 
        잠시만 기다려주시기 바랍니다.
        """

    return utter


def no_call_guide(task, intent, exit_count):
    """
    업무시간 외에 '네', '아니오' 외에 학습되지 않은 질문 시 답변
    :param    intent:    intent
    :return:
    """
    utter = str()

    if exit_count == 1:
        utter = """
        가능한 시간에 담당자가 연락드리길 원하시면 네, 아니오로 말씀해 주세요.
        """
    elif exit_count >= 2:
        utter = """
        죄송합니다.  업무시간에 다시 연락주시기 바랍니다.
        """

    return utter




