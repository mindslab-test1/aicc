#!/usr/bin/python
# -*- coding: utf-8 -*-

"""program"""
__author__ = "MINDsLAB"
__date__ = "creation: 0000-00-00, modification: 0000-00-00"

###########
# imports #
###########
import os
import sys
import json
import time
import datetime
import traceback
from cfg import config
#from lib.nqa import NQA
from lib import logger, utility, utter, converter, api
# from lib.talk import DA_Talk
from datetime import datetime
from websocket import create_connection
from collections import OrderedDict

###########
# options #
###########
reload(sys)
sys.setdefaultencoding('utf-8')


#############
# constants #
#############
__TASK_LIST__ = ['Greet', 'Employee', 'Department', 'Escape', 'Agent', 'Callback', 'CallbackResult', 'PhoneNumKeypad', 'CallWait', 'CallWaitResult', 'end']
# CONVERT = convert.Reg(convert.Util())
SLOT = converter.Slot()
REPLACE = converter.Replace()
ANALYSIS = converter.Analysis()

#######
# def #
#######
def convert(log, mysql, current_task, sds_res, da_info_dict):

    log.info('* task_worker.convert()')

    # setting input
    intent = sds_res['intent']
    session_data = da_info_dict['session_data']
    call_meta = da_info_dict['call_meta']
    talk = da_info_dict['talk']
    total_answer = da_info_dict['total_answer']

    # log
    log.info('\tcurrent_task: {0}'.format(current_task))
    log.info('\tintent: {0}'.format(intent))
    log.info('\texit_check_task: {0}'.format(session_data['exit_check_task']))

    # 고객 연락처 조회
    # -> 고객명과 연락처를 slot tagging
    # -> 연락처는 한글로 변환
    if current_task in ['Callback']:
        total_answer = sds_res['response']

        # select phone_num
        cust_meta_list = utility.select_mnt_target_mng(log, mysql, call_meta)
        call_meta['tel_no'] = str()
        for cust_meta in cust_meta_list:
            call_meta['tel_no'] = cust_meta['tel_no']
        session_data['cust_tel_no'] = call_meta['tel_no']
        # session_data['cust_name'] = talk.utter.utter
        total_answer = REPLACE.change_slot(log, da_info_dict, total_answer, call_meta['tel_no']) # slot 변환
        total_answer = REPLACE.convert_tel_no(log, total_answer) # num to kor
    elif current_task in ['end']:
        session_data['cust_tel_no'] = session_data['pretreatment_utter']
        total_answer = REPLACE.change_slot(log, da_info_dict, total_answer, session_data['cust_tel_no']) # slot 변환
        total_answer = REPLACE.convert_tel_no(log, total_answer) # num to kor

    # setting output
    da_info_dict['session_data'] = session_data
    da_info_dict['total_answer'] = total_answer

    return da_info_dict


def simple_utter(log, current_task, sds_res, da_info_dict):

    log.info('* task_worker.simple_utter()')

    # setting input
    intent = sds_res['intent']
    session_data = da_info_dict['session_data']
    total_answer = da_info_dict['total_answer']
    if current_task not in ['']:
        request_utter_flag = True
    else:
        request_utter_flag = False

    # log
    log.info('\tcurrent_task: {0}'.format(current_task))
    log.info('\texit_check_task: {0}'.format(session_data['exit_check_task']))
    log.info('\tintent: {0}'.format(intent))
    log.info('\texit_check_intent: {0}'.format(session_data['exit_check_intent']))

    # 같은 답변 2번 이상 발화할 시 작업
    if session_data['exit_check_task'] == current_task and session_data['exit_check_intent'] != intent:
        session_data['exit_count'] = str(int(session_data['exit_count']) + 1)

        log.info("\texit_count: {0}".format(session_data['exit_count']))
        log.info('\tintent: {0}'.format(intent))

        if int(session_data['exit_count']) == 1:
            # no_utter_proc_flag = True
            # log.info("\texit! {0}".format(no_utter_proc_flag))

            request_utter_flag = False

            # 안내 가이드 답변 호출
            total_answer = utter.call_forward_guide(current_task, intent, int(session_data['exit_count']))
        elif int(session_data['exit_count']) >= 2:
            # no_utter_proc_flag = True
            # log.info("\texit! {0}".format(no_utter_proc_flag))

            request_utter_flag = False

            # 안내 가이드 답변 호출
            total_answer = utter.call_forward_guide(current_task, intent, int(session_data['exit_count']))

            # talk.DA_Talk.close_sds('123', 'sully_ver02') # 호출하여 sds 종료 필요
    else:
        session_data['exit_count'] = '0'
        session_data['exit_check_task'] = current_task
        session_data['exit_check_intent'] = intent

    # setting output
    da_info_dict['session_data'] = session_data
    da_info_dict['total_answer'] = total_answer

    return da_info_dict, request_utter_flag


def keypad_setting(log, da_info_dict):
    """
    KeyPad Setting
    :param      log:                Logger
    :param      da_info_dict:       DA Information Dictionary
    :return:                        extra value
    """
    log.info("8. Keypad On / Off Setting")
    
    # 키패드 구동 조건
    log.info("키패드 테스크 : {0}".format(da_info_dict['session_data']['current_task']))
    log.info("키패드 인텐트 : {0}".format(da_info_dict['session_data']['intent']))
    extra = 'dtmf_on' if da_info_dict['session_data']['current_task'] == 'PhoneNumKeypad' and da_info_dict['session_data']['intent'] == 'negate' else 'dtmf_off'
    
    log.info("\textra: {0}".format(extra))
    return extra


def connect_department(log, mysql, da_info_dict, sds_res, transfer):
    """
    Connect Department
    :param      log:                Logger
    :param      da_info_dict:       DA Information Dictionary
    :return:                        transfer value
    """
    log.info("9. Connect Department")

    log.info('\tcurrent_task: {0}'.format(da_info_dict['session_data']['current_task']))
    log.info('\tintent_only: {0}, intent: {1}'.format(sds_res['intent_only'], da_info_dict['session_data']['intent']))

    # setting input
    session_data = da_info_dict['session_data']
    intent = sds_res['intent']
    slot_key = session_data['slot_key']
    # slot_val = session_data['slot_val']
    talk = str()
    # slot, value = SLOT.get_slot(sds_res['best_slu'])

    # 홍보/광고 팀 연결
    if da_info_dict['session_data']['intent'] in ['request'] or int(da_info_dict['session_data']['exit_count']) >= 2:
        if slot_key == 'marketing':
            talk = '홍보/마케팅/광고'
            department_info = utility.select_phone_book(log, mysql, talk)
            transfer = department_info['tel_no']
        elif slot_key == 'humanResources':
            talk = '인사/총무/기타'
            department_info = utility.select_phone_book(log, mysql, talk)
            transfer = department_info['tel_no']
        elif slot_key == 'finance':
            talk = '재무/회계/IR'
            department_info = utility.select_phone_book(log, mysql, talk)
            transfer = department_info['tel_no']
        elif slot_key == 'flatform':
            talk = '플랫폼'
            department_info = utility.select_phone_book(log, mysql, talk)
            transfer = department_info['tel_no']
        elif slot_key == 'employeeFind':
            pass
        else:   # 상담원 연결 (2번 연속 미인식시, 상담원 연결 요청시), slot_key == employeeNotFind 도 포함
            transfer = utter.__AGENT__
    log.info('\ttransfer: {0}'.format(transfer))

    return transfer


def data_post_processing(log, mysql, sds_res, da_info_dict):
    """
    Current task worker
    :param      log:                    Logger
    :param      mysql:                  MySQL
    :param      sds_res:                SDS Result
    :param      da_info_dict:           DA Information Dictionary
    :return:                            Repeat flag, Meta, DA Information Dictionary
    """

    log.info("7. task_worker.data_post_processing()")
    log.info("\tsds_res['response']: {0}".format(sds_res['response']))


    # setting input 1
    da_info_dict['sds_flag'] = False
    sds_flag = False
    current_task = str()
    meta = dict()
    answer = ''
    current_task = sds_res['current_task']
    # if sds_res['current_task']:
    #     current_task = sds_res['current_task']
    # elif sds_res['intent_only']:
    #     current_task = sds_res['intent_only']
    # setting_input 2 - 고객 발화 미인식의 경우 처리 (두 번 미인식시 상담사 연결)
    da_info_dict, request_utter_flag = simple_utter(log, current_task, sds_res, da_info_dict)
    session_data = da_info_dict['session_data']
    status = da_info_dict['status']
    func_nm = 'utter.{0}'.format(current_task)
    is_working_time = session_data['is_working_time']
    is_lunch_time = session_data['is_lunch_time']

    # input log
    log.info("\tfunc_nm: {0}, sds_intent: {1}".format(func_nm, sds_res['intent']))

    # utter.py process
    if request_utter_flag:
        log.info('\tutter.{0}()'.format(func_nm))
        answer, sds_flag, status = eval(func_nm)(sds_res, session_data, sds_flag, status, is_working_time, is_lunch_time)
        total_answer = answer
        da_info_dict['total_answer'] = total_answer
        log.info('\tstatus: {0}'.format(status))
    # total_answer = '{0} {1}'.format(total_answer, answer)

    # 답변 변환 처리
    da_info_dict = convert(log, mysql, current_task, sds_res, da_info_dict)
    total_answer = da_info_dict['total_answer']

    # output log
    log.info('\ttotal utter: {0}'.format(da_info_dict['total_answer']))
    log.info("\tslot_key: {0}, slot_value: {1}".format(session_data['slot_key'], session_data['slot_val']))

    # setting output
    da_info_dict['total_answer'] = total_answer
    da_info_dict['sds_flag'] = sds_flag
    da_info_dict['status'] = status
    repeat_flag = True if sds_flag else False

    return repeat_flag, meta, da_info_dict


def execute_sds(log, mysql, sds, model, da_info_dict):
    """
    Execute SDS
    :param      log:                Logger
    :param      mysql:              MySQL
    :param      sds:                SDS Instance
    :param      model:              SDS Model
    :param      da_info_dict:       DA Information Dictionary
    :return:                        SDS Result, modify DA Information Dictionary
    """

    log.info("6. task_worker.execute_sds()")

    # setting data
    sds_flag = da_info_dict['sds_flag']
    talk = da_info_dict['talk']
    call_meta = da_info_dict['call_meta']
    session_data = da_info_dict['session_data']

    # process start (target_mng 에 정보 저장)
    if not sds_flag:
        sds_flag = talk.utter.utter
    sds_res = sds.Talk(sds_flag, talk.session.id, model) # sds 호출
    # Greet 일 경우 intent 가 unknown 이어서, Greet 에서 연속 인식불가한 질문에 대한 처리를 위해 greet 로 넣음
    if sds_res['current_task'] in ['Greet']:
        session_data['exit_check_intent'] = 'greet'
    info_dict = dict(call_meta)
    info_dict['task'] = sds_res['current_task']
    # utility.update_mnt_target_mng(log, mysql, info_dict)
    # hc_hh_campaign_info_list = utility.select_hc_hh_campaign_info(log, mysql, info_dict['task'])
    seq = str()
    # for hc_hh_campaign_info in hc_hh_campaign_info_list:
    #    seq = hc_hh_campaign_info['seq']
    # session_data['seq'] = seq

    #
    if sds_res['current_task']:
        session_data['current_task'] = sds_res['current_task']
    elif sds_res['intent_only']:
        session_data['current_task'] = sds_res['intent_only']
        sds_res['current_task'] = sds_res['intent_only']
    #

    # session_data['current_task'] = sds_res['current_task']
    session_data['slot_items'] = ','.join(str(v) for v in sds_res['intent.filled_slots.items'])
    session_data['intent'] = sds_res['intent']
    session_data['response'] = sds_res['response']
    session_data['slot_key'], session_data['slot_val'] = SLOT.get_slot(sds_res['best_slu'])
    if sds_res['intent'] != 'unknown' and \
            'virtual' not in sds_res['intent'] and not sds_res['intent'].startswith('nqa'):
        session_data['last_sds_question'] = sds_flag
    log.info('\ttalk: {0}\tlast_sds_question: {1}'.format(sds_flag, session_data['last_sds_question']))

    # setting output
    da_info_dict['session_data'] = session_data

    return sds_res, da_info_dict


def data_pre_processing(log, da_info_dict, sds, mysql):
    """
    Ready to Process
    :param      log:                    Logger
    :param      mysql:                  MySQL
    :param      sds:                    SDS Instance
    :param      model:                  SDS Model
    :param      sds_res:                SDS Result
    :param      da_info_dict:           DA Informatino Dictionary
    :return:                            SDS Result, Session data, virtual Intent
    """
    log.info("5. task_worker.data_pre_processing() - start")

    # common input
    session_data = da_info_dict['session_data']
    # current_task = da_info_dict['current_task'] if 'current_task' in da_info_dict.keys() else u'Greet'
    # total_answer = da_info_dict['total_answer'] if 'total_answer' in da_info_dict.keys() else ''
    current_task = session_data['current_task'] if 'current_task' in session_data.keys() else u'Greet'
    total_answer = session_data['total_answer'] if 'total_answer' in session_data.keys() else ''
    input_utter = da_info_dict['talk'].utter.utter

    # 업무시간 관련
    is_working_time = session_data['is_working_time']
    is_lunch_time = session_data['is_lunch_time']

    # 일치하는 단어 관련
    total_department_list = REPLACE.department_list # 전체 부서 리스트 (내선번호 있던, 없던)
    call_department_list = ['인사', '총무',
                            '홍보', '마케팅', '광고',
                            '재무', '회계', '아이알',
                            '플랫폼', '마음에이아이', '기획']  # 내선번호가 있는 부서 리스트

    # similar 관련
    space_removed_utter = input_utter.replace(" ", "")
    first_similar_list = list()

    # max_similar_score = 0
    second_similar_score = 0
    second_similar_name = ''

    # 기타
    will_stop_da_working = False    # DA 중지 여부
    transfer = ''   # 상담원 or 부서 or 직원 연결을 위한 meta data (ex: 'agent' or '연락처')

    # input log
    log.info("\tcurrent_task: {0}".format(current_task))

    if current_task == 'Greet':
        intent = sds.get_intent(input_utter, config.DaConfig.model).intent
        log.info("\tintent: {0}".format(intent))
        if not is_working_time or is_lunch_time:    # 업무시간이 아닐때 '네', '아니오' 외에는 SDS 호출안하고 DA 중단하는 로직
            if not (intent in ['affirm', 'negate']):
                will_stop_da_working = True
                session_data['exit_count'] = str(int(session_data['exit_count']) + 1)
                total_answer = utter.no_call_guide(current_task, intent, int(session_data['exit_count']))
            else:
                session_data['exit_count'] = '0'
                # session_data['exit_check_task'] = current_task
                # session_data['exit_check_intent'] = intent
        else:   # 업무시간일 때 '네', '아니오' 일경우 SDS 호출안하고 DA 중단
            if intent in ['affirm', 'negate']:
                will_stop_da_working = True
                session_data['exit_count'] = str(int(session_data['exit_count']) + 1)
                total_answer = utter.call_forward_guide(current_task, intent, int(session_data['exit_count']))
            else:
                # session_data['exit_count'] = '0'
                # 불필요한 단어를 제거한 utter (즉, 고객 발화에서 직원명만 남겨서 유사도 체크만 함)
                refined_utter = REPLACE.remove_unnecessary_words(space_removed_utter, log)
                # utter에 직원 이름이 있는지 체크하여 있으면 직원 번호로 연결하도록 함
                emp_info_list = utility.select_employee_info(log, mysql)
                log.info("\tutter: {0}".format(input_utter))
                log.info("\tspace_removed_utter: {0}".format(space_removed_utter))
                log.info("\trefined_utter: {0}".format(refined_utter))
                for emp_info in emp_info_list:
                    if emp_info['employee_name'] in space_removed_utter:    # input_utter 의 공백을 모두 제거하였을 때 DB에서 조회해온 직원명과 동일한 경우
                        # 해당 직원의 연락처를 할당
                        transfer = emp_info['phone_number']
                        log.info("\tPerfect included emp_name: {0}, emp_phone_number: {1}".format(emp_info['employee_name'], emp_info['phone_number']))
                        da_info_dict['emp_name'] = emp_info['employee_name'] + ' ' + emp_info['corporate_title']
                        da_info_dict['emp_phone_number'] = emp_info['phone_number']
                        input_utter = '임플로이 파인드'
                        break
                    else:
                        similar_score = ANALYSIS.similar(emp_info['employee_name'], refined_utter, log)

                        if similar_score >= 0.88:   # DB에서 조회한 직원명과 초,중,종성 중에 1자리가 다른경우를 체크해서 list 에 append
                            first_similar_list.append({'score': similar_score,
                                                       'emp_name': emp_info['employee_name'],
                                                       'emp_phone_number': emp_info['phone_number'],
                                                       'corporate_title': emp_info['corporate_title']
                                                       })
                        else:   # similar score 가 0.88 이하이면서, 가장 높은 score 저장
                            if second_similar_score <= similar_score:
                                second_similar_score = similar_score
                                second_similar_name = emp_info['employee_name']

                # 유사도 체크 결과 0.88 이상인 data list 에서 score 만 추출
                first_score_list = list()
                for first_similar_info in first_similar_list:
                    first_score_list.append(first_similar_info['score'])

                # 이미 연결할 대상을 찾았으면 pass
                if transfer:
                    pass
                # 연결할 대상을 못찾았고, 유사도 체크 결과 0.88 이상이면
                elif first_score_list:
                    log.debug("\tSuccess to pass first_similar_list count: {0}".format(len(first_similar_list)))
                    idx = first_score_list.index(max(first_score_list))
                    max_score = first_similar_list[idx]['score']
                    max_score_name = first_similar_list[idx]['emp_name']
                    max_score_phone_num = first_similar_list[idx]['emp_phone_number']
                    max_score_corporate_title = first_similar_list[idx]['corporate_title']
                    # 유사도 점수에서 가장 높은 점수를 가진 직원이 1명일 경우
                    if first_score_list.count(max(first_score_list)) == 1:
                        log.info("\tOne with the highest score")
                        log.info("\tscore: {0}, emp_name: {1}, emp_phone_number: {2}".format(max_score, max_score_name, max_score_phone_num))
                        da_info_dict['emp_name'] = max_score_name + ' ' + max_score_corporate_title
                        da_info_dict['emp_phone_number'] = max_score_phone_num
                        input_utter = '임플로이 파인드'
                        transfer = max_score_phone_num
                    else:   # 유사도 점수에서 가장 높은 점수를 가진 직원이 2명 이상일 경우
                        log.info("\tHighest score overlap")
                        for similar_info in first_similar_list:
                            log.info("\tscore: {0}, emp_name: {1}, emp_phone_number: {2}".format(similar_info['score'], similar_info['emp_name'], similar_info['emp_phone_number']))
                        # 이후 키패드로 고객이 선택가능하도록 하려면 여기에 로직 추가하면 됨
                        input_utter = '임플로이 낫 파인드'
                # 연결할 대상을 못찾았고, 유사도 체크 결과 0.88 이하이면
                else:
                    log.debug("\tFail to pass second_similar_name: {0}, second_similar_score: {1}".format(second_similar_name, second_similar_score))
                    # 공백제거한 input_utter 에서 부서명 추출
                    for department in total_department_list:
                        if department in space_removed_utter:
                            log.info("\tHave department in utter, department: {0}".format(department))
                            if department in call_department_list:  # 내선번호가 있는 부서는 해당 부서연결
                                input_utter = department + '팀 연결해주세요'
                                break
                            else:   # 내선번호가 없는 부서는 영업팀(상담원) 연결
                                input_utter = '영업팀 연결해주세요'
                                break

    # 키패드 처리 (sds 에서 폰번호 학습을 '01011112222' 하나만 해둬서 sds 호출 전에 input_utter 를 전처리 함)
    if current_task == 'PhoneNumKeypad':
        session_data['pretreatment_utter'] = input_utter
        input_utter = '01011112222'
        # output value debugging log
        log.info("\tchanged utter: {0}".format(input_utter))

    # setting output
    da_info_dict['session_data'] = session_data
    da_info_dict['total_answer'] = total_answer
    da_info_dict['talk'].utter.utter = input_utter

    log.info("* task_worker.data_pre_processing() - end")

    return da_info_dict, will_stop_da_working, transfer


def web_socket_send(log, mysql, sds, model, da_info_dict):
    """
    Web Socket Sending
    :param      log:                Logger
    :param      mysql:              MySQL
    :param      sds:                SDS
    :param      model:              Model
    :param      da_info_dict:       DA Information Dictionary
    :return:                        modify DA Information Dictionary
    """
    log.info("5. task_worker.web_socket_send()")

    # setting data
    session_data = da_info_dict['session_data']
    call_meta = da_info_dict['call_meta']
    talk = da_info_dict['talk'].utter.utter
    web_socket = da_info_dict['web_socket']

    # process start
    # log.info("\tseq: {0}".format(session_data['seq']))
    status = call_meta['status']
    call_id = call_meta['call_id']
    web_socket_task = session_data['web_socket_task']

    # group seq 조회
    task_group = utter.check_task_group(web_socket_task, 'N')
    # hc_hh_campaign_info_list = utility.select_hc_hh_campaign_info(log, mysql, task_group)
    # seq = str()
    # for hc_hh_campaign_info in hc_hh_campaign_info_list:
    #    seq = hc_hh_campaign_info['seq']
    # log.info("\tseq: {0}".format(seq))
    json_dict = dict()

    # 통화가 갑자기 끊긴 경우
    # 마지막 테스크인 경우 status 정지 신호를 전체 완료 신호로 작업
    log.info('\tstatus: {0}'.format(status))
    # if web_socket_task == 'task30' and status == 'CS0003':
    #     status = 'CS0007'
    #     log.info('\tstatus change CS0003 -> CS0007')
    if status == 'CS0003':

        # 중간단계 데이터 전달
        if session_data['current_task'] != 'END':
            json_dict["EventType"] = 'DETECT'
            json_dict["Event"] = 'result'
            json_dict["Result"] = str()
            json_dict["contract_no"] = call_meta['contract_no']
            if session_data['current_task'] == 'END':
                json_dict["Result"] = 'Y'
            # elif seq != "":
            #     intent = sds.get_intent(talk, model).intent
            #     log.info("\ttask: {0}, intent: {1}".format(web_socket_task, intent))
            #     json_dict["Result"] = utter.check_detect_yn(web_socket_task, intent)

            # task group 조회
            log.info("\tResult: {0}".format(json_dict["Result"]))
            task_group = utter.check_task_group(web_socket_task, json_dict["Result"])
            log.info("\tTask Group: {0}".format(task_group))
            # hc_hh_campaign_info_list = utility.select_hc_hh_campaign_info(log, mysql, task_group)
            # no = str()
            # for hc_hh_campaign_info in hc_hh_campaign_info_list:
            #    no = hc_hh_campaign_info['seq']
            # json_dict["No"] = no
        else:
            log.info("web socket : no json")
        json_str = json.dumps(json_dict)
        try:
            if len(json_dict["Result"]) > 0:
                web_socket.send(json_str)
                # utility.update_hc_hh_campaign_score(log, mysql, json_dict)
                log.debug("web socket sending success, call_id: {0}".format(call_id))
                log.info("[web meta] json_str: {0}".format(json_str))
            else:
                log.error("web socket sending failed, Result is None")
        except Exception:
            log.error("web socket sending error, call_id: {0}".format(call_id))
            log.error(traceback.format_exc())
    if call_meta['status'] in ['CS0000', 'CS0003', 'CS0005', 'CS0007']:
        json_dict['EventType'] = 'CALL'
        json_dict['Event'] = 'mntresult'
        json_dict['call_id'] = call_id
        json_dict['contract_no'] = call_meta['contract_no']
        json_dict['taskNo_name'] = str()
        if call_meta['status'] == 'CS0000':
            json_dict['call_status'] = 'MR0003'
        elif call_meta['status'] == 'CS0003':
            current_task = session_data['current_task'].replace('Greet', 'task1')
            # hc_hh_campaign_info_list = utility.select_hc_hh_campaign_info(log, mysql, current_task)
            # task_name = str()
            # for hc_hh_campaign_info in hc_hh_campaign_info_list:
            #    task_name = hc_hh_campaign_info['task_info']
            json_dict['call_status'] = 'MR0005'
            # json_dict['taskNo_name'] = task_name
        elif call_meta['status'] == 'CS0005':
            json_dict['call_status'] = 'MR0001'
        elif call_meta['status'] == 'CS0007':
            json_dict['call_status'] = 'MR0004'
        call_meta['call_status'] = json_dict['call_status']
        utility.update_call_history(log, mysql, call_meta)
        json_str = json.dumps(json_dict)
        try:
            web_socket.send(json_str)
            log.debug("web socket sending success, call_id: {0}".format(call_id))
            log.info("[web meta] json_str: {0}".format(json_str))
        except Exception:
            log.error("web socket sending error, call_id: {0}".format(call_id))
            log.error(traceback.format_exc())
    session_data['answer_value'] = ''

    # setting output data
    da_info_dict['session_data'] = session_data

    return da_info_dict


def setting(talk):
    """
    Setting log
    :param      talk:       Talk Instance
    :return:                Log, Total Answer, Session data, SDS flag, virtual intent, status, call_meta
    """

    # Setting Call Id
    call_meta = dict()
    try:
        call_meta['status'] = talk.utter.meta['status']
        call_meta['contract_no'] = talk.utter.meta['contract_no']
        call_meta['call_id'] = talk.utter.meta['call_id']
    except ValueError:
        # chat bot 구동시 contract no 하드코딩 설정
        call_meta['status'] = 'chatbot'
        call_meta['contract_no'] = '61'
        call_meta['call_id'] = '61'

    # Setting Log
    start_date = datetime.strftime(datetime.now(), '%Y/%m/%d')
    log = logger.set_logger(
        logger_name=call_meta['contract_no'],
        log_dir_path=os.path.join(config.DaConfig.log_dir_path, start_date),
        log_file_name='{0}.log'.format(call_meta['contract_no']),               # 로그 파일 규칙 설정
        log_level=config.DaConfig.log_level
    )
    log.info('1. task_worker.setting()')
    log.info("\t[TARGET] call_id : {0}, contract_no : {1}".format(call_meta['call_id'], call_meta['contract_no']))

    total_answer = str()

    # Session Data Load
    session_data = talk.session.context

    sds_flag = False
    status = False

    # Web Socket Client Setting
    web_socket = object()
    try:
        ip = config.DaConfig.web_socket_ip
        port = config.DaConfig.web_socket_port
        web_socket = create_connection("ws://{0}:{1}/callsocket".format(ip, port))
    except Exception:
        log.error("** Web Socket create connection error: {0}".format(traceback.format_exc()))

    # 업무시간 및 점심시간 확인
    if not session_data['confirmed_working_time']:
        session_data['confirmed_working_time'] = True
        session_data['is_working_time'], session_data['is_lunch_time'] = api.get_working_time(log)
    log.info("\tis_working_time: {0}, is_lunch_time: {1}".format(session_data['is_working_time'], session_data['is_lunch_time']))

    # task_worker 에서 meta data 를 통합하여 사용하는 dictionary (매번 DA 호출될때 마다 reset 되는 1회성 dictionary)
    da_info_dict = {
        'talk': talk,
        'total_answer': total_answer,
        'session_data': session_data,
        'sds_flag': sds_flag,
        'status': status,
        'call_meta': call_meta,
        'web_socket': web_socket
    }
    return log, da_info_dict


def da_process_ignore(log, da_info_dict, sds):
    """
    DA process ignore True/False Check
    :param      log:                Logger
    :param      da_info_dict:       DA Information Dictionary
    :param      sds:                SDS Instance
    :return:                Bool
    """

    log.info('2. task_worker.da_process_ignore()')

    # setting input
    talk = da_info_dict['talk']
    session_data = da_info_dict['session_data']
    current_task = session_data['current_task']
    is_working_time = session_data['is_working_time']
    is_lunch_time = session_data['is_lunch_time']
    try:
        is_tts_working = True if talk.utter.meta['doing_tts'] == 'Y' else False
    except Exception:
        log.info("-- Exception -- da_process_ignore")
        is_tts_working = False

    # input log
    log.info("\ttts_working_status: {0}".format(is_tts_working))
    log.info("\tutter: {0}".format(talk.utter.utter))

    if is_tts_working:  # tts 중 일때
        # 데이터 전처리 (19.11.29 에 주석처리 함, 이유는 기존에 Category1 일때 앞글자 2자리를 체크해서 변환해줬던 로직이 필요없어져서 by 김대현)
        # da_info_dict = data_pre_processing(log, da_info_dict)
        # talk = da_info_dict['talk']

        # sds.get_intent 호출
        intent = sds.get_intent(talk.utter.utter, config.DaConfig.model)

        # get slot_key, get slot_value in intent
        slot, value = SLOT.get_slot(intent.origin_best_slu)
        log.info("\tcurrent_task: {0}, intent: {1}".format(current_task, intent.intent))
        log.info("\tslot: {0}, value: {1}".format(slot, value))

        # 업무시간이 아닐때 && intent in ['affirm', 'negate'] 아니면 다 무시
        if current_task in ['Greet'] and (not is_working_time) and is_lunch_time:
            is_ignore = False if intent in ['affirm', 'negate'] else True
        else:  # utter.ignore_check 호출
            is_ignore = utter.ignore_check(current_task, intent.intent, slot)
    else:
        is_ignore = False
    log.info("\tUser question is ignore: {0}".format(is_ignore))

    return is_ignore


def db_connect(log):

    log.info('3. task_worker.db_connect()')

    try:
        mysql = utility.db_connect()
    except Exception:
        time.sleep(3)
        mysql = utility.db_connect()

    return mysql


# def processing_by_task(log, da_info_dict, sds):
#
#     log.info("4. task_worker.processing_by_task()")
#
#     # setting input
#     session_data = da_info_dict['session_data']
#     current_task = da_info_dict['current_task'] if 'current_task' in da_info_dict.keys() else ''
#     total_answer = da_info_dict['total_answer'] if 'total_answer' in da_info_dict.keys() else ''
#     is_working_time = session_data['is_working_time']
#     is_lunch_time = session_data['is_lunch_time']
#     will_stop_da_working = False
#
#     # input log
#     log.info("\tcurrent_task: {0}".format(current_task))
#
#     # processing
#     if current_task == 'Greet':
#         intent = sds.get_intent(utter, config.DaConfig.model).intent
#         log.info("\tintent: {0}".format(intent))
#         if not is_working_time or is_lunch_time:
#             if not (intent in ['affirm', 'negate']):
#                 will_stop_da_working = True
#                 session_data['exit_count'] = str(int(session_data['exit_count']) + 1)
#                 total_answer = utter.no_call_guide(current_task, intent, session_data['exit_count'])
#             else:
#                 session_data['exit_count'] = '0'
#
#     # setting output
#     da_info_dict['total_answer'] = total_answer
#
#     return da_info_dict, will_stop_da_working



def task_worker(sds_res, talk, sds, model):
    """
    Each Task Worker
    :param      sds_res:            Response Instance
    :param      talk:               Talk Instance
    :param      sds:                SDS Instance
    :param      model:              MODEL
    :return:                        answer, talk, sds_result, status, extra
    """

    meta = dict()
    da_info_dict = dict()
    extra = str()
    try:
        # 1. setting
        log, da_info_dict = setting(talk)

        # 2. DA processing ignore check
        # TTS 중 사용자 발화시 SDS에 학습되어 있는 내용이 아닌경우 DA처리 안하고 무시 (데이터 선처리 -> sds.get_intent)
        if da_process_ignore(log, da_info_dict, sds):
            sds_res = dict()
            sds_res['response'] = ''
            log.info('TASK WORKER END \n')
            for handler in log.handlers:
                handler.close()
                log.removeHandler(handler)
            return '', talk, sds_res, da_info_dict['status'], dict(), 'dtmf_off', '', ''

        # 3. db connect
        mysql = db_connect(log)

        # 4. web socket sending (웹에서 실시간으로 task 정보, 콜 상태 등을 보기 위해)
        da_info_dict['session_data']['web_socket_task'] = da_info_dict['session_data']['current_task']
        da_info_dict = web_socket_send(log, mysql, sds, model, da_info_dict)

        # 5. task 별로 처리하는 함수
        # da_info_dict, will_stop_da_working = processing_by_task(log, da_info_dict, sds)
        # if will_stop_da_working:
        #     return da_info_dict['total_answer'], talk, sds_res, da_info_dict['status'], dict(), 'dtmf_off', '', utter.__STOP__

        # task worker start
        # log.info("Processing Repeat")
        while True:
            # 5. data_pre_processing
            da_info_dict, will_stop_da_working, transfer = data_pre_processing(log, da_info_dict, sds, mysql)

            # 전처리 이후 SDS 호출이 필요없는 경우 DA return 하고 종료
            if will_stop_da_working:
                sds_res = dict()
                sds_res['response'] = da_info_dict['total_answer']
                log.info('TASK WORKER END \n')
                for handler in log.handlers:
                    handler.close()
                log.removeHandler(handler)
                return da_info_dict['total_answer'], talk, sds_res, da_info_dict['status'], dict(), 'dtmf_off', '', utter.__STOP__

            # 6. SDS 호출
            sds_res, da_info_dict = execute_sds(log, mysql, sds, model, da_info_dict)

            # 7. data_post_processing
            repeat_flag, meta, da_info_dict = data_post_processing(log, mysql, sds_res, da_info_dict)
            
            # sds 재실행 (현재 무조건 break 상태)
            log.info("\trepeat_flag: {0}".format(repeat_flag))
            if not repeat_flag:
                break

            # websocket send
            da_info_dict = web_socket_send(log, mysql, sds, model, da_info_dict)
            time.sleep(0.1)

        # 8. keypad on/off setting
        extra = keypad_setting(log, da_info_dict)

        # 9. 담당자 또는 agent 연결
        transfer = connect_department(log, mysql, da_info_dict, sds_res, transfer)

        # 10. last talk ignored
        if talk.utter.utter == 'NULL':
            da_info_dict['total_answer'] = str()
            log.info("")
            log.info("############")
            log.info("# 통화 종료 #")
            log.info("############")

    except Exception:
        log.error('-- DA Exception --')
        log.error(traceback.format_exc())
        for handler in log.handlers:
            handler.close()
            log.removeHandler(handler)
        mysql.disconnect()
    log.info('TASK WORKER END \n')
    for handler in log.handlers:
        handler.close()
        log.removeHandler(handler)
    return da_info_dict['total_answer'], da_info_dict['talk'], sds_res, da_info_dict['status'], meta, extra, transfer, utter.__STOP__

