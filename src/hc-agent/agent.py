#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import struct
import sys
import socketserver
import socket
import time
import pyaudio
import threading
import select
import logging
import datetime as dt
import requests
import configparser
import wave

# from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import QtWidgets

from agent_message import *

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 8000

AGENT_ID = ''
IS_CALLING = False
IS_LISTENING = False

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception as e:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


class MyFormatter(logging.Formatter):
    converter = dt.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


# https://stackoverflow.com/questions/28655198/best-way-to-display-logs-in-pyqt
class QTextEditLogger(QObject, logging.Handler):
    new_record = pyqtSignal(object)

    def __init__(self, parent):
        super().__init__(parent)
        super(logging.Handler).__init__()

        # formatter = Formatter('%(asctime)s|%(levelname)s|%(message)s|', '%d/%m/%Y %H:%M:%S')
        formatter = MyFormatter(fmt='%(asctime)s %(message)s')
        self.setFormatter(formatter)

    def emit(self, record):
        msg = self.format(record)
        self.new_record.emit(msg) # <---- emit signal here


class MyDialog(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        handler = QTextEditLogger(self)
        log_text_box = QPlainTextEdit(self)
        log_text_box.setReadOnly(True)
        log_text_box.setMaximumBlockCount(1000)

        logging.getLogger().addHandler(handler)
        logging.getLogger().setLevel(logging.INFO)
        handler.new_record.connect(log_text_box.appendPlainText) # <---- connect QPlainTextEdit.appendPlainText slot

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(log_text_box)

        self.gif = QtWidgets.QLabel()
        self.movie = QMovie("phone-ringing-gif-36.gif")
        self.gif.setMovie(self.movie)
        self.gif.hide()
        self.movie.start()
        layout.addWidget(self.gif)

        self.setLayout(layout)

    def test(self):
        logging.debug('damn, a bug')
        logging.info('something to remember')
        logging.warning('that\'s not right')
        logging.error('foobar')

    @pyqtSlot(str)
    def display_gif(self, msg):
        if msg == 'on':
            self.gif.show()
        elif msg == 'off':
            self.gif.hide()


class Login(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Login, self).__init__(parent)
        # self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        # self.setWindowFlags(Qt.FramelessWindowHint)

        self.textName = QtWidgets.QLineEdit(self)
        self.textPass = QtWidgets.QLineEdit(self)
        self.textPass.setEchoMode(QLineEdit.Password)
        self.buttonLogin = QtWidgets.QPushButton('Login', self)
        self.buttonLogin.clicked.connect(self.handleLogin)
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.textName)
        layout.addWidget(self.textPass)
        layout.addWidget(self.buttonLogin)

        self.addr = ''
        self.agent_id = ''

        config = configparser.ConfigParser()
        try:
            with open('agent.ini') as f:
                config.read_file(f)
                if 'user' in config['LOGIN']:
                    print('find user')
                    self.textName.setText(config['LOGIN']['user'])
                    self.textPass.setText(config['LOGIN']['passwd'])
        except IOError:
            logging.error("Can't open agent.ini file")
            QtWidgets.QMessageBox.warning(self, 'Error', "Can't open agent.ini file")

    def get_login_addr(self):
        config = configparser.ConfigParser()
        try:
            with open('agent.ini') as f:
                config.read_file(f)
        except IOError:
            logging.error("Can't open agent.ini file")
            QtWidgets.QMessageBox.warning(self, 'Error', "Can't open agent.ini file")
            return None
        # 'http://10.122.66.70:5000/call/login'
        url = config['LOGIN']['url']
        return url

    def handleLogin(self):
        global AGENT_ID
        AGENT_ID = self.textName.text()
        self.addr = get_ip()
        self.url = self.get_login_addr()
        if self.url is None:
            return
        data = '{"User": "%s", "Password":"%s", "Addr":"%s"}' % (
            self.textName.text(),
            self.textPass.text(),
            self.addr)
        response = requests.post(self.url, data=data)
        print(data)
        print(response.json())

        if 'code' in response.json():
            if response.json()['code'] == 0:
                self.accept()
                self.agent_id = self.textName.text()
        else:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Bad user or password')

        config = configparser.ConfigParser()
        try:
            with open('agent.ini') as f:
                config.read_file(f)
                config['LOGIN']['user'] = self.textName.text()
                config['LOGIN']['passwd'] = self.textPass.text()
                with open('agent.ini', 'w') as configfile:
                    config.write(configfile)
        except IOError:
            logging.error("Can't open agent.ini file")
            QtWidgets.QMessageBox.warning(self, 'Error', "Can't open agent.ini file")
            return None

class WinApplication():
    def __init__(self):
        self.connected = False
        self.tcpHandler = None
        self.app = QApplication(sys.argv)

        self.w = QWidget()
        self.w.resize(600, 300)
        self.w.move(1920 - 600, 1080 - 380)
        # bottom-right
        #ph = self.app.desktop().screenGeometry().height()
        #self.w.showMaximized()
        #ph = self.w.geometry()
        #print(ph)

        self.w.setWindowTitle('HappyCall Agent 0.1')
        self.w.setWindowFlags(Qt.WindowStaysOnTopHint)

        # (int left, int top, int right, int bottom)
        margin = self.w.getContentsMargins()
        # resize(int w, int h)

        self.exitBtn = QPushButton('통화 시작', self.w)
        #self.exitBtn.move(10, self.w.height() - 50)
        self.exitBtn.clicked.connect(self.on_click)
        # "QLineEdit { background-color: yellow }"
        self.exitBtn.setStyleSheet('QPushButton:disabled { color: gray }')
        self.exitBtn.setFixedWidth(100)
        self.exitBtn.setEnabled(True)

        self.listenBtn = QPushButton('청취 시작', self.w)
        #self.listenBtn.move(10, self.w.height() - 50)
        self.listenBtn.clicked.connect(self.on_listen)
        # "QLineEdit { background-color: yellow }"
        self.listenBtn.setStyleSheet('QPushButton:disabled { color: gray }')
        self.listenBtn.setFixedWidth(100)

        self.dlg = MyDialog(self.w)
        # self.dlg.setFixedHeight(100)
        # self.dlg.resize(self.w.width() - margin[3] - 20, 100)
        # self.dlg.move(10, 150)
        # self.dlg.show()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.dlg)
        self.layout2 = QHBoxLayout()
        self.layout2.addWidget(self.exitBtn)
        self.layout2.addWidget(self.listenBtn)
        self.layout2.setAlignment(Qt.AlignLeft)
        self.layout.addLayout(self.layout2)

#        self.layout = QGridLayout()
#        self.layout.addWidget(self.dlg, 0, 0, 1, 2)
#        self.layout.setColumnStretch(0, 20)
#        self.layout.addWidget(self.exitBtn, 1, 0)
#        self.layout.addWidget(self.listenBtn, 1, 1)

        self.w.setLayout(self.layout)

        logging.info('연결을 기다리고 있습니다')

    def on_click(self):
        print('dn is {}, agent_id is {}'.format(self.t.dn, self.agent_id))
        global IS_CALLING
        IS_CALLING = not IS_CALLING
        if IS_CALLING:
            send_register(self.t.sock, SystemType.PC_AGENT, TransferType.RECV_SEND, self.agent_id, self.t.dn)
            self.exitBtn.setText('통화 종료')
        else:
            # To Call-Manager
            data = '{"dialNo": "%s", "agentId": "%s"}' % (
                    self.t.dn,
                    self.agent_id)
            url = self.url.replace('login', 'hangup')
            response = requests.post(url, data=data)

            # To Relay Server
            send_regi(self.t.sock, SystemType.PC_AGENT, self.agent_id)
            self.exitBtn.setText('통화 시작')

    def on_listen(self):
        global IS_LISTENING
        IS_LISTENING = not IS_LISTENING
        if IS_LISTENING:
            send_regi(self.t.sock, SystemType.PC_AGENT, AGENT_ID)
            self.listenBtn.setText('청취 종료')
        else:
            self.listenBtn.setText('청취 시작')

    def close(self):
        logging.info("연결이 종료되었습니다")

        # self.exitBtn.setEnabled(False)
        # self.tcpHandler = None

    def start(self):
        self.w.show()
        login = Login(self.w)
        # login.setModal(True)
        # login.setWindowFlags(Qt.Window)
        # login = Login()

        if login.exec_() == QtWidgets.QDialog.Accepted:
            self.agent_id = login.agent_id
            self.url = login.url
            self.t = BizAudioHandler()
            self.t.gif.connect(self.dlg.display_gif)
            self.t.dn = ''
            self.t.start()
            time.sleep(1)
            print(self.agent_id)
            print(self.t.dn)

            rc = self.app.exec_()


APP = WinApplication()


class BizAudioHandler(QThread):
    gif = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.sock = None
        self.t = None
        self.is_connected = False
        self.is_ringing = False
        self.setup()

    def get_addr(self):
        config = configparser.ConfigParser()
        try:
            with open('agent.ini') as f:
                config.read_file(f)
        except IOError:
            logging.error("Can't open agent.ini file")
            QtWidgets.QMessageBox.warning(self, 'Error', "Can't open agent.ini file")
            return None, None
        host = config['LOGIN']['relay_host']
        port = int(config['LOGIN']['relay_port'])
        return host, port

    def connect(self):
        HOST, PORT = self.get_addr()
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
            self.sock.connect((HOST, PORT))
            self.is_connected = True
            print(HOST, PORT)
            send_regi(self.sock, SystemType.PC_AGENT, AGENT_ID)
        except socket.error as e:
            self.sock.close()
            self.is_connected = False
            print(str(e))

    def run(self):
        while True:
            self.connect()
            if not self.is_connected:
                time.sleep(0.5)
                continue

            self.t = threading.Thread(target=self.sendto_customer, args=())
            self.t.daemon = True
            self.t.start()

            data = bytes()
            while True:
                try:
                    data += self.sock.recv(1024)
                    data = self.process_msg(data)
                except socket.error as e:
                    if e.errno == 32:
                        # disconnected
                        break
            self.finish()
        pass

    def ringing(self, filename):
        self.is_ringing = True
        self.gif.emit('on')
        chunk = 1024
        wf = wave.open(filename, 'rb')

        # create an audio object
        p = pyaudio.PyAudio()

        # open stream based on the wave object which has been input.
        stream = p.open(format =
                        p.get_format_from_width(wf.getsampwidth()),
                        channels = wf.getnchannels(),
                        rate = wf.getframerate(),
                        output = True)

        # read data (based on the chunk size)
        data = wf.readframes(chunk)

        # play stream (looping from beginning of file to the end)
        while data != '' and IS_CALLING is False:
            # writing to the stream is what *actually* plays the sound.
            stream.write(data)
            data = wf.readframes(chunk)

        # cleanup stuff.
        stream.close()    
        p.terminate()
        self.gif.emit('off')
        self.is_ringing = False

    def setup(self):
        self.p2 = pyaudio.PyAudio()
        self.mic_stream = self.p2.open(format=FORMAT,
                                       channels=CHANNELS,
                                       rate=RATE,
                                       input=True,
                                       stream_callback=self.send_agent_voice)
        if self.mic_stream.is_active():
            logging.info("마이크가 활성화 되었습니다")
        else:
            logging.info("ERROR - 마이크가 활성화되지 않았습니다")

        self.p = pyaudio.PyAudio()
        self.c_stream = self.p.open(format=FORMAT,
                                    channels=CHANNELS,
                                    rate=8000,
                                    output=True)
        if self.c_stream.is_active():
            logging.info("스피커가 활성화 되었습니다")
        else:
            logging.info("ERROR - 스피커가 활성화되지 않았습니다")

        # self.is_connected = True

    def send_agent_voice(self, in_data, frame_count, time_info, status):
        if in_data and self.is_connected is True:
            try:
                if IS_CALLING:
                    send_pcm(self.sock, SystemType.PC_AGENT, in_data)
            except socket.error as msg:
                pass
                # print('request.sendall()', msg)

        return (None, pyaudio.paContinue)

    def sendto_customer(self):
        print('start sendto_customer')

        self.mic_stream.start_stream()
        while self.mic_stream.is_active() and self.is_connected:
            time.sleep(0.1)
        print('try to stop stream')
        # while True:
        #     data = self.mic_stream.read(CHUNK)
        #     # data = '1234'.encode()
        #     try:
        #         if data:
        #             self.request.sendall(data)
        #         else:
        #             print('no data')
        #     except socket.error as msg:
        #         print('request.sendall()', msg)
        #         break
        self.mic_stream.stop_stream()
        self.mic_stream.close()
        self.p2.terminate()

        print('end sendto_customer')

    def disconnect(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def process_msg(self, data):
        global IS_CALLING
        global IS_LISTENING
        while len(data) >= 8:
            ver, sys_type, opcode, length = struct.unpack('!2shhh', data[:8])
            if ver != b'ML':
                self.sock.close()
                data = bytes()
                break
            if len(data) >= length:
                if opcode == OpCode.TRANSFER:
                    body = data[8:length]
                    # Do send
                    if IS_CALLING or IS_LISTENING:
                        self.c_stream.write(body)

                    # elif not self.is_ringing:
                    #     self.is_ringing = True
                    #     self.ring = threading.Thread(target=self.ringing, args=('telephone-ring-04.wav',))
                    #     self.ring.daemon = True
                    #     self.ring.start()
                if opcode == OpCode.RINGING:
                    body = data[8:length]
                    contract_no, agent_id, self.dn, trans_type = struct.unpack('!i20s20si', body)
                    self.dn = self.dn.split(b'\x00')[0].decode('utf-8')

                    # Do send
                    if not self.is_ringing:
                        self.is_ringing = True
                        self.ring = threading.Thread(target=self.ringing, args=('telephone-ring-04.wav',))
                        self.ring.daemon = True
                        self.ring.start()
                if opcode == OpCode.DISCONNECT:
                    if IS_CALLING:
                        IS_CALLING = False;
                        APP.exitBtn.setText('통화 시작')
                        logging.info('고객이 통화를 종료하였습니다')
                    if IS_LISTENING:
                        IS_LISTENING = False;
                        APP.listenBtn.setText('청취 시작')
                        logging.info('고객이 통화를 종료하였습니다')

                data = data[length:]
            else:
                # print('not yet all data')
                break
        return data

    def finish(self):
        self.c_stream.stop_stream()
        self.c_stream.close()
        self.p.terminate()

        self.t.join()

        print('finished')


def process_msg(s, data):
    while len(data) >= 8:
        hdr = struct.unpack('!2shhh', data[:8])
        if hdr[0] != b'ML':
            s.close()
            break
        if len(data) >= hdr[3]:
            print(hdr, len(data[8:]))
            data = data[hdr[3]:]
        else:
            print('not yet all data')
            break
    return data


def recv_msg(s):
    data = ''
    try:
        data = s.recv(1024, socket.MSG_DONTWAIT)
    except socket.timeout as e:
        pass
    except socket.error as e:
        if e.errno == 11:
            pass
        else:
            print(str(e))
    return data


if __name__ == "__main__":
    # t = BizAudioHandler()
    # t.daemon = True
    # t.start()

    APP.start()

    # sys.exit()
    # t.join()

