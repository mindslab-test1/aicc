import struct
import socket


class SystemType:
    AI_AGENT = 0
    PC_AGENT = 1

class TransferType:
    RECV_ONLY = 0
    SEND_ONLY = 1
    RECV_SEND = 2


class OpCode:
    REGISTER  = 0
    TRANSFER  = 1
    PUBLISH   = 2
    SUBSCRIBE = 3
    RINGING   = 4
    NOTIFY    = 5
    CONNECT   = 6
    DISCONNECT = 7


class MsgHeader:
    def __init__(self, raw):
        self.version, self.system_type, self.opcode, self.length = raw

def send_register(conn, system_type, trans_type, agent_id, dn):
    # ContractNo, AgentID, DialNo, TransType
    contract_no = 0
    body = struct.pack('!i20s20si', contract_no, str.encode(agent_id), str.encode(dn), trans_type)
    # Version, System-type, OpCode, Length
    hdr = struct.pack('!2shhh', b'ML', system_type, OpCode.REGISTER, 8 + len(body))
    #print msg.encode('hex')
    conn.sendall(hdr + body)

def send_regi(conn, system_type, agent_id):
    # ContractNo, AgentID, DialNo, TransType
    contract_no = 0
    dial_no = b'0'
    trans_type = TransferType.RECV_ONLY
    body = struct.pack('!i20s20si', contract_no, str.encode(agent_id), dial_no, trans_type)
    # Version, System-type, OpCode, Length
    hdr = struct.pack('!2shhh', b'ML', system_type, OpCode.REGISTER, 8 + len(body))
    #print msg.encode('hex')
    conn.sendall(hdr + body)

def send_pcm(conn, system_type, pcm):
    hdr = struct.pack('!2shhh', b'ML', system_type, OpCode.TRANSFER, 8 + len(pcm))
    conn.sendall(hdr + pcm)
    # LOGGER.debug('sent transfer message')

def recv_msg(s):
    data = ''
    try:
        data = s.recv(1024, socket.MSG_DONTWAIT)
    except socket.timeout as e:
        pass
    except socket.error as e:
        if e.errno == 11:
            pass
        else:
            print(str(e), e)
    return data
