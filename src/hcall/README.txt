0. Dependencies
   - For CentOS:
      libuuid
      libuuid-devel
      alsa-lib-devel
      mariadb-devel
      boost-devel
      zeromq-devel
      # for MSSQL
      freetds freetds-devel

1. BUILD pjproject
   - sip protocol library

   $ tar xvfj pjproject-2.7.2.tar.bz2
   $ cd pjproject-2.7.2
   $ patch -p1 < ../rtp_pt_telephone.patch
   $ ./configure
   $ make dep
   $ make -j
   $ sudo make install


2. soci 설치
   - Ubuntu
   $ cd soci
   # sample for oracle
   $ cmake -DWITH_ORACLE=ON -DORACLE_HOME=/opt/oracle/instantclient_12_2 \
           -DORACLE_OCCI_LIBRARY=/opt/oracle/instantclient_12_2/libocci.so.12.1 \
           -DORACLE_OCI_LIBRARY=/opt/oracle/instantclient_12_2/libclntsh.so.12.1 \
           -DSOCI_LIBDIR=${MAUM_ROOT}/lib \
           -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
           ..


   - CentOS / RHEL
   $ cd soci
   $ mkdir build && cd build
   $ cmake3 -DSOCI_LIBDIR=${MAUM_ROOT}/lib \
            -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
            ..

   # 오라클 사용시 -DWITH_ORACLE=ON -DORACLE_HOME=/Product/oracle/product/11g 추가
   $ make -j
   $ make install

   * 오라클 접속하는 client 환경 설정
   $ export NLS_LANG=KOREAN_KOREA.UTF8


3. BUILD happy-call 
   - library
   $ tar xvfz spdlog-1.4.1.tar.gz
   $ unzip ../third-party/rapidjson-master.zip
   $ mv rapidjson-master/include/rapidjson .
   $ rm -rf rapidjson-master

   $ cd tts
   $ sh build_proto.sh
   $ cd ..
   $ mkdir build
   $ cd build
   # 사용 Database 명시 (USE_MYSQL, USE_ORACLE, USE_MSSQL)
   $ cmake3 -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
            -DUSE_MSSQL=ON \
            ..


OPTION: Use user build boost library
4. BUILD boost (Ubuntu gcc4 와 같이 사용 시 다음 작업이 필요, boost 1.67.0 기준)

   $ echo "using gcc : 4.8 : /usr/bin/g++-4.8 ; " >> tools/build/src/user-config.jam
   $ ./bootstrap.sh  # b2 실행파일이 생성됨
   $ ./b2 --toolset=gcc-4.8 -j
   $ sudo ./b2 --prefix=/opt/boost_1_67_0 install

   * cmake
   $ cmake -DBOOST_ROOT=/opt/boost_1_67_0 ..
   $ cmake  -DCMAKE_BUILD_TYPE=Debug \
            -DCMAKE_C_COMPILER=/usr/bin/gcc-4.8 \
            -DCMAKE_CXX_COMPILER=/usr/bin/g++-4.8  \
            -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
            -DBOOST_ROOT=/opt/boost_1_67_0 \
            ..

< 참고사항 >
* TTS proto 파일 위치
https://github.com/mindslab-ai/brain-tts/tree/master/javasrc/spring-boot/proto/src/main/proto/maum/brain/tts

* pjsip 패치
- PJMEDIA_RTP_PT_TELEPHONE_EVENTS 값 101로 변경
- pjsip/src/pjsua-lib/pjsua_media.c:604 attach option 수정 (방화벽 문제로 판명, 패치 불필요)

* 패치 파일 만드는 법
- 원본 디렉토리를 그대로 놔두고 원본 디렉토리를 복사하여 작업 디렉토리를 만들고 작업
# sample
$ diff -u pjproject-2.7.2/pjmedia/include/pjmedia/config.h pjproject-2.7.2.new/pjmedia/include/pjmedia/config.h > rtp_pt_telephone.patch

* 패치 파일 적용법
$ cd pjproject-2.7.2
$ patch -p1 < ../rtp_pt_telephone.patch # p옵션은 patch파일에 나와있는 경로 중에 N단계 만큼 생략을 의미
