#include "agent-stream.h"
#include "app-variable.h"

#include "agent_message.h"
#include "tts-port.h"

#include <grpc++/grpc++.h>
#include "tts/ng_tts.grpc.pb.h"
#include "tts/ng_tts.pb.h"

using namespace maum::brain::tts;
using namespace std::chrono;

int do_tts(std::string text, SafeQueue<SoundFrame> *result_q, int utter_no, int sent_no) {
  auto channel = grpc::CreateChannel(g_var.sdn_addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<NgTtsService::Stub> stub(NgTtsService::NewStub(channel));

  ClientContext ctx;

  TtsRequest req;
  req.set_lang(maum::common::Lang::ko_KR);
  // NO EFFECT ! Not implemented yet?
  // req.set_samplerate(16000);
  req.set_text(text);

  ctx.AddMetadata("tempo", std::to_string(g_var.tts_tempo));
  ctx.AddMetadata("campaign", g_var.campaign_id);
  ctx.AddMetadata("samplerate", std::to_string(g_var.clock_rate));
  auto deadline = system_clock::now() + seconds(g_var.tts_deadline);
  ctx.set_deadline(deadline);

  auto stream = stub->SpeakWav(&ctx, req);

  size_t duration = 0;
  bool remove_header = false;
  TtsMediaResponse resp;
  std::string remained;
  bool wait_a_moment = false;

  while (stream->Read(&resp)) {
    std::string bytes = resp.mediadata();
    if (!remove_header && bytes.size() >= 44) {
      bytes = bytes.substr(44);
      remove_header = true;
    }
    remained += bytes;
    duration += bytes.size();
    // if (remained.size() >= (16000 * 500 / 1000 * 2) /* 500ms */) {
    //   wait_a_moment = false;
    // }
    if (!wait_a_moment && !remained.empty()) {
      push_sound_stream(remained, result_q, utter_no, sent_no);
    }
  }
  Status status = stream->Finish();

  if (!status.ok()) {
    g_logger->debug("SDN error message: {}", status.error_message());
  }

  if (!remained.empty()) {
    push_sound_stream(remained, result_q, utter_no, sent_no, true);
  }

  g_logger->debug("TTS Done");
  return (duration * 100) / (g_var.clock_rate * 2);
}

AgentStream::AgentStream() : timer_(g_var.service) {
  connected_ = false;
  is_done_ = false;
}

AgentStream::AgentStream(
    // boost::asio::io_service& io_service,
    SafeQueue<SoundFrame>* result_q,
    std::shared_ptr<WebsocketClient> wsock,
    VoiceDialog *dialog,
    pjsua_call_id call_id)
    : websock_(wsock), app_call_id_(call_id), timer_(g_var.service) {
  result_q_ = result_q;
  connected_ = false;
  is_done_ = false;
  dialog_ = dialog;
}

AgentStream::~AgentStream() {
  Close();

  if (read_thrd_.joinable()) {
    g_var.logger->trace("AgentStream read thread joined - start");
    read_thrd_.join();
    g_var.logger->trace("AgentStream read thread joined - end");
    close(socket_.get_handle());
  }

  if (stt_thrd_.joinable()) {
    g_var.logger->trace("AgentStream stt thread joined - start");
    stt_thrd_.join();
    g_var.logger->trace("AgentStream stt thread joined - end");
  }
}

void AgentStream::Run() {
  while (!is_done_) {
    char   buf[1024];
    size_t received;

    received = socket_.Recv(buf, sizeof(buf));
    if (received > 0) {
      agent_stream_buf_.append(buf, received);
      // Agent ---> Customer
      push_sound_stream(agent_stream_buf_, result_q_, 0, 0);
    } else {
      // error
      break;
    }
  }
}

void AgentStream::send_register(const char *agent_id, int trans_type) {
  if (!connected_) return;

  AgentMessage message;
  RegisterBody *body = (RegisterBody *)message.Body;
  int msg_len = sizeof(AgentMessageHeader) + sizeof(RegisterBody);

  message.SetVersion();
  message.SetSystemType(AI_AGENT);
  message.SetOpCode(REGISTER);
  message.SetLength(msg_len);

  body->SetContractNo(0);
  body->SetTransType(trans_type);
  snprintf(body->AgentID, sizeof(body->AgentID), "%s", agent_id);
  snprintf(body->DialNo,  sizeof(body->DialNo),  "%s", g_var.sip_user.c_str());

  if (socket_.SendAll((char *)&message, msg_len) < 0) {
    // TODO: error
  }
}

void AgentStream::send_transfer(char *buf, int size) {
  if (!connected_) return;

  AgentMessage message;
  int msg_len = sizeof(AgentMessageHeader) + size;
  message.SetVersion();
  message.SetSystemType(AI_AGENT);
  message.SetOpCode(TRANSFER);
  message.SetLength(msg_len);
  memcpy(message.Body, buf, size);

  if (socket_.SendAll((char *)&message, msg_len) < 0) {
    // TODO: error
  }
}

void AgentStream::process_msg(char *buf, int size)
{
  AgentMessage* message = (AgentMessage*)buf;

  switch (message->GetOpCode()) {
    case REGISTER: {
      break;
    }
    case TRANSFER: {
      int body_length = size - sizeof(AgentMessageHeader);
      agent_stream_buf_.append(message->Body, body_length);
      // Agent ---> Customer
      push_sound_stream(agent_stream_buf_, result_q_, 0, 0);
      if (agent_stt_) {
        Speech speech;
        speech.set_bin(message->Body, body_length);
        agent_stt_->Write(speech);
      }
      break;
    }
    default:
      break;
  }
}

void AgentStream::process_no_agent(const boost::system::error_code& e) {
  if (e == boost::asio::error::operation_aborted) return;
  result_q_->PlayingMOH = false;
  result_q_->clear();

  g_var.logger->info("trying to connect agent is timed out");

  Close();

  dict meta;
  meta["transfer_result"] = "timeout";
  dialog_->AsyncSendText("NULL", "N", meta);

  APP_MESSAGE_T msg = { APP_COMMAND_ENUM::TRANSFER_TIMEOUT, app_call_id_ };
  zmq_send(g_var.evnt_sender, (char *)&msg, sizeof(APP_MESSAGE_T), ZMQ_NOBLOCK);
}

void AgentStream::Start() {
  StartSTT();
  send_register(g_var.agent_id.c_str(), RECV_SEND);

  read_thrd_ = std::thread([&](){
      result_q_->clear();

      bool waiting_agent = true;
      // boost::asio::deadline_timer timer(g_var.service);

      timer_.expires_from_now(boost::posix_time::seconds(g_var.agent_timeout));
      timer_.async_wait(boost::bind(&AgentStream::process_no_agent, this, _1));
      result_q_->PlayingMOH = true;

      while (!is_done_) {
        size_t received;

        received = socket_.Recv(msg_buf_.GetBuffer(), MAX_DATA_SIZE);

        if (received > 0) {
          char *msg;
          int   msg_length;
          msg_buf_.Commit(received);
          while ( (msg = msg_buf_.GetMessage(msg_length)) != NULL) {
            process_msg(msg, msg_length);
          }
          if (waiting_agent) {
            waiting_agent = false;
            result_q_->PlayingMOH = false;
            result_q_->clear();
            timer_.cancel();
          }
        } else {
          // error
          g_var.logger->info("socket recv failed: {}", strerror(errno));
          msg_buf_.Reset();
          is_done_ = true;
          break;
        }
      }
      g_var.logger->info("socket recv end");
      // grpc_stream_->WritesDone();
      agent_stt_->WritesDone();
      if (stt_thrd_.joinable()) {
        stt_thrd_.join();
        //agent_stt_->Finish();
      }
    });
}

bool AgentStream::Connect(std::string host, int port) {
  g_var.logger->debug("Start to connect AGENT");

  if (socket_.Connect(host.c_str(), port) == false) {
    g_var.logger->debug("Fail to connect AGENT: message - {}", strerror(errno));
  } else {
    connected_ = true;
    g_var.logger->debug("Success to connect AGENT");
  }

  return connected_;
}

bool AgentStream::IsConnected() {
  return connected_;
}

bool AgentStream::IsDone() {
  return is_done_;
}

void AgentStream::Close() {
  is_done_ = true;
  if (connected_) {
    shutdown(socket_.get_handle(), SHUT_RDWR);
  }
  g_var.logger->debug("AgentStream socket closed");
}

// Customer ---> Agent
void AgentStream::Write(char *buf, int size) {
  if (!is_done_) {
    send_transfer(buf, size);
    // boost::asio::write(socket_, boost::asio::buffer(buf, size));
  }
}

void AgentStream::StartSTT() {
  auto channel = grpc::CreateChannel(g_var.sttd_remote, grpc::InsecureChannelCredentials());

  // GRPC_CHANNEL_IDLE, GRPC_CHANNEL_CONNECTING, GRPC_CHANNEL_READY,
  // GRPC_CHANNEL_TRANSIENT_FAILURE, GRPC_CHANNEL_SHUTDOWN 
  if (channel->GetState(false) == GRPC_CHANNEL_TRANSIENT_FAILURE) {
    g_var.logger->debug("channel enum {}", channel->GetState(false));
    g_var.logger->error("GRPC Connection failed to STT: {}", g_var.sttd_remote);
    return;
  } else {
    // TODO
  }

  stub_ = SpeechToTextService::NewStub(channel);

  // ClientContext ctx;
  ctx_.set_wait_for_ready(false);

  ctx_.AddMetadata("in.lang", g_var.agent_lang);
  ctx_.AddMetadata("in.samplerate", std::to_string(g_var.agent_samplerate));
  ctx_.AddMetadata("in.model", g_var.agent_model);
  ctx_.AddMetadata("in.epd_timeout", std::to_string(g_var.epd_timeout));

  agent_stt_ = stub_->StreamRecognize(&ctx_);

  stt_thrd_ = std::thread(&AgentStream::ReadStream, this);
}

void AgentStream::ReadStream() {
  Segment segment;
  while (agent_stt_->Read(&segment)) {
    g_var.logger->info("AGENT ({} ~ {}): {}", segment.start(), segment.end(),
                       segment.txt());
    websock_->publish(segment.start(), segment.end(), "TX", segment.txt(), 'N');
  }
}

