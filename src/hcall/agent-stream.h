#ifndef AGENT_STREAM_H
#define AGENT_STREAM_H

#include <string>
#include <thread>
#include "socket_handler.h"
#include <grpc++/grpc++.h>
#include <maum/brain/stt/stt.grpc.pb.h>

#include "zhelpers.h"
#include "safe-queue.h"

#include "voice-dialog.h"
#include "agent_message.h"
#include "ws-client.h"
#include <pjsua-lib/pjsua.h>

#define MAX_BUF_SIZE 8192

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using maum::brain::stt::SpeechToTextService;
using maum::brain::stt::SttRealService;
using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using maum::brain::stt::Segment;

using maum::brain::stt::SttModelResolver;
using maum::brain::stt::ServerStatus;
using maum::brain::stt::Model;

class AgentStream {
 public:
  // AgentStream(boost::asio::io_service& io_service,
  //             SafeQueue<SoundFrame>* result_q);
  AgentStream();
  AgentStream(SafeQueue<SoundFrame>* result_q,
              std::shared_ptr<WebsocketClient> wsock,
              VoiceDialog *dialog,
              pjsua_call_id call_id);
  ~AgentStream();

  void Run();
  void Start();
  void Stop();
  bool Connect(std::string host, int port);
  bool IsConnected();
  bool IsDone();
  void Close();
  // void Write(std::string data);
  void Write(char *buf, int size);
  void send_register(const char *agent_id, int trans_type);
  void send_transfer(char *buf, int size);

  // 상담원 음성 인식
  void StartSTT();
  void ReadStream();

 private:
  void check_deadline();

  void process_msg(char *buf, int size);

  void process_no_agent(const boost::system::error_code& e);

  SocketHandler socket_;

  // std::function<void()> sendto_customer_;
  SafeQueue<SoundFrame>* result_q_;
  AgentMessageBuffer msg_buf_;
  std::string agent_stream_buf_;
  // char buf_[MAX_BUF_SIZE];

  std::thread read_thrd_;

  bool connected_;
  bool is_done_;

  ClientContext ctx_;

  std::thread stt_thrd_;
  std::unique_ptr<ClientReaderWriter<Speech, Segment> > agent_stt_;
  std::unique_ptr<SpeechToTextService::Stub> stub_;

  // websocket client
  std::shared_ptr<WebsocketClient> websock_;
  // for hangup
  pjsua_call_id app_call_id_;

  VoiceDialog *dialog_;
  boost::asio::deadline_timer timer_;
};

#endif /* AGENT_STREAM_H */
