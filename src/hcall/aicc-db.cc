#include <exception>
#include "app-variable.h"
#include "aicc-db.h"
#include "util.h"

#include "soci/soci.h"
#ifdef USE_MYSQL
  #include "soci/mysql/soci-mysql.h"
#elif USE_ORACLE
  #include "soci/oracle/soci-oracle.h"
#elif USE_MSSQL
  #include "soci/odbc/soci-odbc.h"
#endif

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>
#include "rest-client.h"

using namespace soci;
using namespace std;

#ifdef USE_MYSQL
	#define AICC_FACTORY mysql
	#define AICC_CONNSTR_FMT "db={0} user={1} password={2} host={3} port={4} charset=utf8"
#elif USE_ORACLE
	#define AICC_FACTORY oracle
	#define AICC_CONNSTR_FMT "service={3}:{4}/{0} user={1} password={2}"
#elif USE_MSSQL
	#define AICC_FACTORY odbc
	#define AICC_CONNSTR_FMT "DRIVER=libtdsodbc.so;SERVER={3};PORT={4};DATABASE={0};UID={1};PWD={2};clientcharset=UTF-8"
#endif

AiccDb::AiccDb() {
}

AiccDb::~AiccDb() {
}

bool AiccDb::Connect(string addr, string user, string passwd, string dbname) {
  conn_str_ = fmt::format(AICC_CONNSTR_FMT,
                          g_var.db_name,
                          g_var.db_user,
                          g_var.db_passwd,
                          g_var.db_host,
                          g_var.db_port);
  g_logger->info("connetion string: {}", conn_str_);
  return true;
}

std::string datetime_to_string(const char *date) {
  if (date == nullptr) return "0월 0일";

  char buf[128];
  struct tm tm;
  strptime(date, "%F %T", &tm);
  sprintf(buf, "%d월 %d일", tm.tm_mon, tm.tm_mday);
  return buf;
}

void AiccDb::GetSIPInfo() {
  std::unique_lock<std::mutex> lock(lock_);
  try {
    session sql(AICC_FACTORY, conn_str_);
    string query = fmt::format(QUOTE(
        SELECT OB_PREFIX, CUST_OP_ID
        FROM   SIP_ACCOUNT
        WHERE  SIP_DOMAIN = '{}' AND SIP_USER = '{}'
    ), g_var.sip_domain, g_var.sip_user);

    sql << query, into(g_var.outbound_prefix), into(g_var.agent_id);
  } catch (exception const &e) {
    g_logger->error("GetSIPInfo Error: {}", e.what());
  }
}

bool AiccDb::IsAgentReady() {
  std::string status;
  try {
    session sql(AICC_FACTORY, conn_str_);
    string query = fmt::format(QUOTE(
        SELECT CUST_OP_STATUS
        FROM   TN_USER
        WHERE  USER_ID = '{}'
    ), g_var.agent_id);

    sql << query, into(status);
  } catch (exception const &e) {
    g_logger->error("IsAgentReady Error: {}", e.what());
  }

  return status == "04";
}

bool AiccDb::GetCallData(string call_seq, string campaign_id) {
  std::unique_lock<std::mutex> lock(lock_);
  try {
    session sql(AICC_FACTORY, conn_str_);
    // session sql(oracle, "service=10.122.64.191:1521/orcl user=minds password=Minds12#$ dbname=MINDS");

    char  buf[1024];
    char *fmt = QUOTE(
      SELECT B.CUST_NM,
             T.TEL_NO,
             T.CUST_OP_ID,
             C.CHATBOT_NAME
      FROM   CM_CONTRACT T
      LEFT JOIN CUST_BASE_INFO B
             ON T.CUST_ID = B.CUST_ID
      LEFT JOIN CM_CAMPAIGN C
             ON T.CAMPAIGN_ID = C.CAMPAIGN_ID
      WHERE  T.CONTRACT_NO = '%s'
             AND T.CAMPAIGN_ID = '%s'
    );
    sprintf(buf, fmt, call_seq.c_str(), campaign_id.c_str());
    g_logger->trace("GetCallData SQL:\n{}", buf);

    rowset<row> rows(sql.prepare << buf);

    for (auto it = rows.begin(); it != rows.end(); it++) {
      g_var.cust_name     = it->get<std::string>(0, "");
      g_var.tel_no        = it->get<std::string>(1);
      g_var.chatbot_name  = it->get<std::string>(3, "");
    }

    // 한화생명 고객 전화번호 복호화
    char  buf2[1024];
    char *fmt2 = QUOTE(
      select securedb.dbo.SPINDEC('sgwapi:4792', 'MSSQL', 'P007', '%s', 'UID', '0.0.0.0')
    );
    sprintf(buf2, fmt2, g_var.tel_no.c_str());
    g_logger->trace("GetCall securedb Data SQL:\n{}", buf2);

    rowset<row> rows2(sql.prepare << buf2);

    for (auto it = rows2.begin(); it != rows2.end(); it++) {
      g_var.tel_no        = it->get<std::string>(0);
    }

  } catch (exception const &e) {
    g_logger->error("GetCallData Error: {}", e.what());
    return false;
  }
  return true;
}

bool AiccDb::InsertPhoneData()
{
  std::unique_lock<std::mutex> lock(lock_);
  long long id;
  std::time_t t = std::time(0);
  std::tm now = *std::localtime(&t);
  std::tm now2 = now;
  std::string telno = g_var.tel_no;

  session sql(AICC_FACTORY, conn_str_);

  if (g_var.encrypt_telno) {
    RestClient client(g_logger);
    std::string resp_body;
    std::string url = g_var.encrypt_url + "?type=enc&telNo=" + g_var.tel_no;
    if (client.Get(url.c_str(), resp_body)) {
      telno = resp_body;
    }
  }

#ifdef USE_ORACLE
  sql << "SELECT CM_CONTRACT_SEQ.NEXTVAL FROM DUAL", into(id);
  sql << QUOTE(
      INSERT INTO CM_CONTRACT
        (CONTRACT_NO, CAMPAIGN_ID, TEL_NO, CREATED_DTM, UPDATED_DTM, IS_INBOUND)
      VALUES
        (:id, :CAMP_ID, :TEL_NO, :NOW, :NOW2, 'Y')
      ), use(id), use(g_var.campaign_id), use(telno), use(now), use(now2);
#elif USE_MYSQL
  sql << QUOTE(
      INSERT INTO CM_CONTRACT
        (CAMPAIGN_ID, TEL_NO, CREATED_DTM, UPDATED_DTM, IS_INBOUND)
      VALUES
        (:CAMP_ID, :TEL_NO, :NOW, :NOW2, 'Y')
      ), use(g_var.campaign_id), use(telno), use(now), use(now2);

  if (!sql.get_last_insert_id("CM_CONTRACT", id)) {
    g_logger->error("failed to assign new contract number");
    return false;
  }
#elif USE_MSSQL
  sql << QUOTE(
      INSERT INTO CM_CONTRACT
        (CAMPAIGN_ID, TEL_NO, CREATED_DTM, UPDATED_DTM, IS_INBOUND)
      VALUES
        (:CAMP_ID, :TEL_NO, :NOW, :NOW2, 'Y')
      ), use(g_var.campaign_id), use(telno), use(now), use(now2);

  sql << "SELECT SCOPE_IDENTITY()", into(id);
#endif
  g_var.contract_no = std::to_string(id);
  g_logger->info("assign new contract number {}", g_var.contract_no);
  return true;
}

bool AiccDb::GetCampaignConfig()
{
  std::string stt_model, stt_agent_model;
  try {
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
////////////////////////////////////////////////////////////////////////////////
SELECT
	S.MODEL, S2.MODEL, C.TTS_TEMPO, S.ADDR, S2.ADDR
FROM
	CM_CAMPAIGN C LEFT JOIN STT_SERVER_INFO S
	ON C.STT_MODEL_ID = S.ID
	LEFT JOIN STT_SERVER_INFO S2
	ON C.STT_AGENT_MODEL_ID = S2.ID
WHERE
	CAMPAIGN_ID = :camp_id;
////////////////////////////////////////////////////////////////////////////////
    ), into(stt_model),
       into(stt_agent_model),
       into(g_var.tts_tempo),
       into(g_var.sttd_remote),
       into(g_var.sttd_agent_remote),
       use(g_var.campaign_id);
  } catch (exception const &e) {
    g_logger->error("PrepareResult Error: {}", e.what());
    return false;
  }

  g_var.model = stt_model;
  g_var.agent_model = stt_agent_model;

  return true;
}

void AiccDb::GetSttList()
{
  try {
    session sql(AICC_FACTORY, conn_str_);
    rowset<row> rows(sql.prepare << QUOTE(
////////////////////////////////////////////////////////////////////////////////
SELECT
	MODEL, ADDR, DIRECTORY
FROM
	STT_SERVER_INFO
////////////////////////////////////////////////////////////////////////////////
    ));

    for (auto it = rows.begin(); it != rows.end(); it++) {
      STT_SERVER_INFO info;
      auto model     = it->get<std::string>(0, "");
      info.addr      = it->get<std::string>(1, "");
      auto directory = it->get<std::string>(2, "");

      // example: baseline-kor-8000
      std::vector<std::string> vec;
      boost::split(vec, directory, boost::is_any_of("-"));
      if (vec.size() == 3) {
        info.model = vec[0];
        info.lang  = vec[1];
        info.samplerate = vec[2];
      } else {
        g_logger->error("Invalid STT DIRECTORY name: {}", directory);
      }
      g_var.sttd_list[model] = info;
    }
  } catch (exception const &e) {
    g_logger->error("PrepareResult Error: {}", e.what());
  }
}

bool AiccDb::UpdateStatus(std::string status, int retry_no)
{
  return UpdateStatus(status.c_str(), retry_no);
}

bool AiccDb::UpdateStatus(const char* status, int retry_no)
{
  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::string status_value(status);
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE  CM_CONTRACT
        SET     CALL_TRY_COUNT = CALL_TRY_COUNT + :retry_no
        WHERE   CONTRACT_NO = :contract_no and CAMPAIGN_ID = :camp_id
    ), use(retry_no),
       use(g_var.contract_no),
       use(g_var.campaign_id);
  } catch (exception const &e) {
    g_logger->error("UpdateStatus Error: {}", e.what());
    return false;
  }
  return true;
}

bool AiccDb::UpdatePhoneStatus(string status) {
  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::time_t t = std::time(0);
    std::tm now = *std::localtime(&t);
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE  SIP_ACCOUNT
        SET     STATUS = :st,
                CONTRACT_NO = :cno,
                LAST_EVENT_TIME = :now
        WHERE   SIP_USER = :sip_user
    ), use(status),
       use(g_var.contract_no),
       use(now),
       use(g_var.sip_user);
  } catch (exception const &e) {
    g_logger->error("UpdatePhoneStatus Error: {}", e.what());
    return false;
  }
  return true;
}

bool AiccDb::UpdatePhoneBootTime() {
  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::time_t t = std::time(0);
    std::tm now = *std::localtime(&t);
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE  SIP_ACCOUNT
        SET     STATUS = 'CS0001',
                BOOT_TIME = :now
        WHERE   SIP_USER = :sip_user
    ), use(now), use(g_var.sip_user);
  } catch (exception const &e) {
    g_logger->error("UpdatePhoneBootTime Error: {}", e.what());
    return false;
  }
  return true;
}

int AiccDb::PrepareResult(std::string call_id) {
  std::unique_lock<std::mutex> lock(lock_);
  int stt_result_id = -1;
  std::time_t t = std::time(0);
  std::tm now = *std::localtime(&t);

  try {
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        INSERT INTO CM_STT_RSLT_DETAIL (CALL_ID, UPDATED_DTM, CREATED_DTM)
        VALUES (:call_id, :now, :now)
    ), use(call_id);

    sql << QUOTE(
        SELECT  STT_RESULT_DETAIL_ID
        FROM    CM_STT_RSLT_DETAIL
        WHERE   CALL_ID = :stt_result_id
        ORDER BY STT_RESULT_ID DESC
        LIMIT 1
    ), into(stt_result_id), use(call_id), use(now), use(now);
  } catch (exception const &e) {
    g_logger->error("PrepareResult Error: {}", e.what());
  }
  return stt_result_id;
}

bool AiccDb::InsertResult(
    string speaker_code,
    int sentence_id, std::string sentence, int start_time, int end_time, char ignored)
{
  std::unique_lock<std::mutex> lock(lock_);
  std::time_t t = std::time(0);
  std::tm now = *std::localtime(&t);

  try {
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        INSERT INTO CM_STT_RSLT_DETAIL
          (CALL_ID, SPEAKER_CODE, SENTENCE_ID, SENTENCE, START_TIME, END_TIME, IGNORED, UPDATED_DTM, CREATED_DTM)
        VALUES
          (:hid, :spk, :sentid, :sent, :st, :et, :ignored, :now, :now)
    ), use(g_var.call_history_id), use(speaker_code), use(sentence_id),
       use(sentence),
       use(start_time / 100.0),
       use(end_time / 100.0),
       use(ignored), use(now), use(now);
  } catch (exception const &e) {
    g_logger->error("InsertResult Error: {}", e.what());
    return false;
  }
  return true;
}

bool AiccDb::InsertHistory(
    std::string call_seq, std::string status, timeval start_tv, std::string sip_call_id)
{
  std::tm start_time = *std::localtime(&start_tv.tv_sec);
  long long id;
  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::time_t t = std::time(0);
    std::tm now = *std::localtime(&t);
    std::tm now2 = now;
    // TODO: 2020-09-01 commented out by shinwc
    // To use CALL_STATUS field as DIAL_RESULT 
    //std::string status_value(status);
    session sql(AICC_FACTORY, conn_str_);

    std::string call_type_code;
    call_type_code = g_var.is_inbound ? "CT0001" : "CT0002";

    // TODO: add SIP_USER
#ifdef USE_ORACLE
    sql << "SELECT CALL_HISTORY_SEQ.NEXTVAL FROM DUAL", into(id);
    sql << QUOTE(
        INSERT INTO CALL_HISTORY
          (CALL_ID, CALL_DATE, CALL_TYPE_CODE, CONTRACT_NO,
// TODO: 2020-09-01 commented out by shinwc
// To insert DIAL_RESULT instead of CALL_STATUS
//           START_TIME, END_TIME, DURATION, CALL_STATUS, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
           START_TIME, END_TIME, DURATION, DIAL_RESULT, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
        VALUES
          (:id, :now, :call_type, :cseq, :st, NULL, NULL, :status, :now2, :op_id, :sip_user, :sip_call_id)
    ), use(id), use(now), use(call_type_code), use(call_seq), use(start_time),
       use(status), use(now2), use(g_var.agent_id), use(g_var.sip_user), use(sip_call_id);
#elif USE_MYSQL
    sql << QUOTE(
        INSERT INTO CALL_HISTORY
          (CALL_DATE, CALL_TYPE_CODE, CONTRACT_NO,
// TODO: 2020-09-01 commented out by shinwc
// To insert DIAL_RESULT instead of CALL_STATUS
//           START_TIME, END_TIME, DURATION, CALL_STATUS, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
           START_TIME, END_TIME, DURATION, DIAL_RESULT, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
        VALUES
          (:now, :call_type, :cseq, :st, NULL, NULL, :status, :now2, :op_id, :sip_user, :sip_call_id)
    ), use(now), use(call_type_code), use(call_seq), use(start_time),
       use(status), use(now2), use(g_var.agent_id), use(g_var.sip_user), use(sip_call_id);

    if (!sql.get_last_insert_id("CALL_HISTORY", id)) {
      g_logger->error("InsertHistory Error: failed to get last insert id");
      return false;
    }
#elif USE_MSSQL
    // To guarantee exclusive DB access, make multiple sqls into single transaction 
    sql << QUOTE(
        BEGIN TRANSACTION;
        INSERT INTO CALL_HISTORY
          (CALL_DATE, CALL_TYPE_CODE, CONTRACT_NO,
// TODO: 2020-09-11 commented out by shinwc
// To insert DIAL_RESULT instead of CALL_STATUS
//           START_TIME, END_TIME, DURATION, CALL_STATUS, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
           START_TIME, END_TIME, DURATION, DIAL_RESULT, CREATED_DTM, CUST_OP_ID, SIP_USER, SIP_CALL_ID)
        VALUES
          (:now, :call_type, :cseq, :st, NULL, NULL, :status, :now2, :op_id, :sip_user, :sip_call_id)
       SELECT SCOPE_IDENTITY();
       COMMIT TRANSACTION;
    ), use(now), use(call_type_code), use(call_seq), use(start_time),
       use(status), use(now2), use(g_var.agent_id), use(g_var.sip_user), use(sip_call_id), into(id);

    //sql << "SELECT IDENT_CURRENT('CALL_HISTORY')", into(id);
#endif
    g_logger->info("InsertHistory Success: last id {}", id);
    g_var.call_history_id = id;

    // CM_CONTRACT 테이블 LAST_CALL_ID 생성
    sql << QUOTE(
        UPDATE CM_CONTRACT
        SET    LAST_CALL_ID = :id
        WHERE  CONTRACT_NO = :cno AND CAMPAIGN_ID = :campid
    ), use(id), use(call_seq), use(g_var.campaign_id);
  } catch (exception const &e) {
    g_logger->error("InsertHistory Error: {}", e.what());
    return false;
  }
  return true;
}

// TODO: 2020-09-01 added by shinwc
// To add new function: EndHistory() to finalize CALL_HISTORY 
// and modify UpdateHistory() to update dialog.status only
bool
AiccDb::UpdateHistory(std::string call_seq, std::string status) {
  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::string status_value(status);
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE CALL_HISTORY
        SET    CALL_STATUS = :status
        WHERE  CALL_ID = :hid
    ), use(status), 
       use(g_var.call_history_id);
  } catch (exception const &e) {
    g_logger->error("UpdateHistory Error: {}", e.what());
    return false;
  }
  return true;
}

bool
AiccDb::EndHistory(std::string call_seq, std::string dial_result, timeval duration_tv, timeval end_tv) {
  std::tm end_time = *std::localtime(&end_tv.tv_sec);

  std::unique_lock<std::mutex> lock(lock_);
  try {
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE CALL_HISTORY
        SET    DIAL_RESULT = :result,
               DURATION = :duration,
               END_TIME = :et
        WHERE  CALL_ID = :hid
    ), use(dial_result), 
       use(duration_tv.tv_sec + duration_tv.tv_usec / 1000 / 1000.0),
       use(end_time),
       use(g_var.call_history_id);
  } catch (exception const &e) {
    g_logger->error("UpdateHistory Error: {}", e.what());
    return false;
  }
  return true;
}
/*
bool
AiccDb::UpdateHistory(std::string call_seq, std::string status, timeval duration_tv, timeval end_tv) {
  std::tm end_time = *std::localtime(&end_tv.tv_sec);

  std::unique_lock<std::mutex> lock(lock_);
  try {
    std::string status_value(status);
    session sql(AICC_FACTORY, conn_str_);
    sql << QUOTE(
        UPDATE CALL_HISTORY
        SET    CALL_STATUS = :status,
               DURATION = :duration,
               END_TIME = :et
        WHERE  CALL_ID = :hid
    ), use(status), 
       use(duration_tv.tv_sec + duration_tv.tv_usec / 1000 / 1000.0),
       use(end_time),
       use(g_var.call_history_id);
  } catch (exception const &e) {
    g_logger->error("UpdateHistory Error: {}", e.what());
    return false;
  }
  return true;
}
*/

std::string AiccDb::GetCurrentDateTime() {
  time_t     now = time(0);
  struct tm  tstruct;
  char       buf[80];
  tstruct = *gmtime(&now);
  tstruct.tm_hour = tstruct.tm_hour + 9;
  strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

  return buf;
}
