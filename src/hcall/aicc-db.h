#ifndef AICC_DB_H
#define AICC_DB_H

#include <string>
#include <mutex>

using std::string;

class AiccDb {
 public:
  AiccDb();
  ~AiccDb();

  bool Connect(string addr, string user, string passwd, string dbname);
  void GetSIPInfo();
  bool IsAgentReady();
  bool GetCallData(string call_seq, string campaign_id);
  bool InsertPhoneData();

  // 캠페인별 STT 모델, TTS 설정 가져오기
  bool GetCampaignConfig();

  // 전체 STT 모델 (대화 도중 모델 변경을 위한)
  void GetSttList();

  bool UpdateStatus(const char* status, int retry_no = 0);
  bool UpdateStatus(std::string status, int retry_no = 0);
  bool UpdatePhoneStatus(string status);
  bool UpdatePhoneBootTime();

  int  PrepareResult(std::string call_seq);
  // bool InsertResult(const char *speaker_code,
  bool InsertResult(string speaker_code,
                    int sentence_id, std::string sentence, int start_time, int end_time, char ignored = 'N');
  bool InsertHistory(std::string call_seq, std::string status, timeval start_tv, std::string sip_call_id);
  bool UpdateHistory(std::string call_seq, std::string status);
  bool EndHistory(std::string call_seq, std::string dial_result, timeval duration, timeval end_tv);

 private:
  std::string conn_str_;
  std::mutex lock_;

  std::string GetCurrentDateTime();
};

#endif /* AICC_DB_H */
