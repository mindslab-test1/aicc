#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "app-variable.h"
#include "util.h"
#include "cpptoml.h"
#include "zhelpers.h"
#include "version.h"

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/stdout_color_sinks.h>
#endif

#define OPT_PASSWD 1000
#define OPT_ONCE   1001
#define OPT_DOMAIN 1002
#define OPT_DOMAIN2 1003
#define OPT_DIRECT 1004

#define CFG_FILENAME "happy-call.conf"

APP_VARIABLE g_var;

std::shared_ptr<spdlog::logger> g_logger;

std::string app_cmd_text[] = {
  "HANGUP",
  "DISCONNECTED",
  "CHECK_PKT",
  "PLAY_AUDIO",
  "CALL_TRANSFER",
  "TRANSFER_TIMEOUT",
  "REFER"
};

bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

void version()
{
  printf("Version: %s\n", git_version);
  exit(0);
}

void help(char *argv)
{
  printf("Usage: %s options\n", argv);
  printf("  -u, --user     [sip user]\n"
         "  --passwd       [sip password]\n"
         "  -p, --port     [sip port]\n"
         "  --contract-no  [contract no]\n"
         "  -c, --campaign [campaign id]\n"
         "  -i, --inbound\n"
         "  --once\n"
         "  --domain       [sip server address]\n"
         "  --domain2      [sip second server address]\n"
         "  -v, --version\n"
         "  --direct\n"
         "  -h, --help\n");
  exit(0);
}

APP_VARIABLE::APP_VARIABLE() {
}

APP_VARIABLE::~APP_VARIABLE() {
}

void APP_VARIABLE::Cleanup() {
  g_var.logger->trace("APP_VARIABLE cleanup start");
  if (evnt_sender) {
    zmq_close(evnt_sender);
  }
  if (app_context) {
    zmq_ctx_destroy(app_context);
  }
  g_var.logger->trace("APP_VARIABLE cleanup end");
}

void APP_VARIABLE::ParseArgs(int argc, char *argv[])
{
  int c;

  while (true) {
    static struct option long_options[] = {
      {"user",        required_argument, 0,           'u'},
      {"passwd",      required_argument, 0,            OPT_PASSWD},
      {"port",        required_argument, 0,           'p'},
      {"contract-no", required_argument, 0,           'n'},
      {"campaign",    required_argument, 0,           'c'},
      {"inbound",     no_argument,       0,           'i'},
      {"once",        no_argument,       0,           OPT_ONCE},
      {"domain",      required_argument, 0,           OPT_DOMAIN},
      {"domain2",     required_argument, 0,           OPT_DOMAIN2},
      {"direct",      no_argument,       0,           OPT_DIRECT},
      {"help",        no_argument,       0,           'h'},
      {"version",     no_argument,       0,           'v'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "u:p:n:c:ihv", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:
        break;

      case 'u':
        sip_user = optarg;
        break;

      case OPT_PASSWD:
        sip_passwd = optarg;
        break;

      case 'p':
        sip_port = atoi(optarg);
        break;

      case 'n':
        contract_no = optarg;
        break;

      case 'c':
        campaign_id = optarg;
        break;

      case 'i':
        is_inbound = true;
        break;

      case OPT_ONCE:
        is_once = true;
        break;

      case OPT_DOMAIN:
        sip_domain = optarg;
        break;

      case OPT_DOMAIN2:
        sip_domain2 = optarg;
        break;

      case OPT_DIRECT:
        direct_call = true;
        break;

      case 'h':
        help(argv[0]);
        break;

      case 'v':
        version();
        break;

      default:
        help(argv[0]);
    }
  }

  if (sip_user.empty()) {
    printf("required option missed: --user\n");
    help(argv[0]);
  }
}

template <typename T, typename D>
void get_value(std::shared_ptr<cpptoml::table> config, const char *key, T &value,  D def_value) {
  value = config->get_qualified_as<T>(key).value_or(def_value);
}

void APP_VARIABLE::Initialize(int argc, char *argv[])
{
  is_inbound = false;
  is_once = false;
  is_ready = true;
  call_cnt = 0;
  direct_call = false;
  // TODO: 2020-08-31 shinwc
  // added record_on_early to start recording on EARLY or CONFIRMED state
  record_on_early = false;

  ParseArgs(argc, argv);

  spdlog::set_level(spdlog::level::debug);
  spdlog::flush_on(spdlog::level::debug);
  // spdlog::set_async_mode((1 << 18),
  //                        spdlog::async_overflow_policy::discard_log_msg,
  //                        nullptr,
  //                        std::chrono::milliseconds(10),
  //                        nullptr);
  string buf;
  logger = CreateLogger(sip_user.c_str());
  
  is_doing_tts = false;
  date_index   = time(NULL) % 86400;

  // config file with TOML format
  maum_root = std::getenv("MAUM_ROOT");
  std::string maum_conf_dir = maum_root + "/etc";
  std::vector<std::string> config_dirs { ".", maum_conf_dir };
  bool found_config = false;
  for (auto dir : config_dirs) {
    auto conf = dir + "/" + CFG_FILENAME;

    if (is_file_exist(conf.c_str())) {
      auto config = cpptoml::parse_file(conf);

      // LOG 설정
      logs_console = config->get_qualified_as<bool>("Log.console").value_or(false);
      logs_level   = config->get_qualified_as<int>("Log.level").value_or(1);
      if (logs_console) {
        console = spdlog::stdout_color_mt("console");
        g_logger = console;
      } else {
        g_logger = logger;
      }

      g_logger->set_level((spdlog::level::level_enum)logs_level);

      // Arg값으로 설정되지 않은 경우 Config파일에서 SIP 설정
      if (sip_domain.empty()) {
        if (is_inbound) {
          sip_domain = *config->get_qualified_as<std::string>("SIP.inbound.domain");
        } else {
          sip_domain = *config->get_qualified_as<std::string>("SIP.outbound.domain");
        }
      }
      if (sip_domain.empty()) {
        printf("There is NO config value [SIP.domain]\n");
        exit(-1);
      }

      rest_sds_url    = config->get_qualified_as<std::string>("Dialog.url").value_or("");
      sdn_addr        = config->get_qualified_as<std::string>("SDN.addr").value_or("");
      call_ws_addr    = config->get_qualified_as<std::string>("Common.call.wsserver.addr").value_or("");
      tts_tempo       = config->get_qualified_as<double>("SDN.speech.tempo").value_or(1);
      tts_deadline    = config->get_qualified_as<int>("SDN.tts.deadline").value_or(30);
      tts_echo_sample = config->get_qualified_as<bool>("SDN.tts.echo.sample").value_or(false);
      tts_sample_wav  = config->get_qualified_as<std::string>("SDN.tts.sample.wav").value_or("");

      agent_relay_host = config->get_qualified_as<std::string>("Agent.relay.host").value_or("");
      agent_relay_port = config->get_qualified_as<int>("Agent.relay.port").value_or(9999);

      get_value(config, "Dialog.response.timeout"       , resp_timeout    , 60);
      get_value(config, "Dialog.stop.keyword"           , stop_tts_keyword, "");
      get_value(config, "Dialog.send.record.event"      , da_record_event , false);
      get_value(config, "Dialog.music.on.hold"          , moh_wav         , "");

      get_value(config, "Database.host"                 , db_host         , "");
      get_value(config, "Database.port"                 , db_port         , 0);
      get_value(config, "Database.user"                 , db_user         , "");
      get_value(config, "Database.passwd"               , db_passwd       , "");
      get_value(config, "Database.name"                 , db_name         , "");

      get_value(config, "Record.stereo"                 , is_stereo       , true);
      get_value(config, "Record.early"                  , record_on_early , false);
      get_value(config, "Record.directory"              , record_dir      , ".");
      if (record_dir.back() == '/') {
        record_dir.pop_back();
      }

      // Default STT Model
      get_value(config, "Common.stt.default.host"       , sttd_remote   , "127.0.0.1:9801");
      get_value(config, "Common.stt.agent.default.host" , sttd_agent_remote   , "127.0.0.1:9801");
      get_value(config, "Common.stt.default.model"      , model         , "");
      get_value(config, "Common.stt.default.lang"       , lang          , "");
      get_value(config, "Common.stt.default.sample_rate", stt_samplerate, 8000);
      get_value(config, "Common.stt.default.epd_timeout", epd_timeout   , 12000);
      get_value(config, "Common.m2u.default.host"       , m2u_addr      , "127.0.0.1:9911");

      get_value(config, "Common.encrypt.tel.no"         , encrypt_telno , false);
      get_value(config, "Common.encrypt.url"            , encrypt_url   , "");

      get_value(config, "Agent.model"            , agent_model       , "baseline");
      get_value(config, "Agent.timeout"          , agent_timeout     , 30);
      get_value(config, "SIP.inbound.use.stun"   , use_stun          , false);
      get_value(config, "SIP.inbound.stun.server", stun_server       , "");
      get_value(config, "SIP.inbound.delaytime"  , inbound_delaytime , 0);
      get_value(config, "SIP.user_agent"         , sip_user_agent    , "");
      get_value(config, "SIP.rtp.port"           , rtp_port          , 4000);
      get_value(config, "SIP.clock.rate"         , clock_rate        , 8000);

      found_config = true;
      break;
    }
  }
  if (found_config == false) {
    printf("Cannot open %s file...\n", CFG_FILENAME);
    exit(-1);
  }

  db.Connect(db_host, db_user, db_passwd, db_name);
  db.GetSIPInfo();
  db.GetCallData(contract_no.c_str(), campaign_id.c_str());
  db.GetSttList();
  db.GetCampaignConfig();

  call_history_id = 0;

  // Zeromq
  char endpoint[128];
  app_context = zmq_ctx_new();
  sprintf(endpoint, "ipc://%s/run/%s.evt.ipc", maum_root.c_str(), sip_user.c_str());
  evnt_sender = zmq_socket(app_context, ZMQ_PUSH);
  if (zmq_connect(evnt_sender, endpoint) != 0) {
    logger->info("Cannot connect zmq event receiver ({})", endpoint);
  }

  ShowVariable();
}

void APP_VARIABLE::ShowVariable() {
  g_logger->info("------------------ START CONFIG ----------------------------");
  g_logger->info("brain-stt.sttd.front.remote   : [{}]", sttd_remote);
  g_logger->info("brain-stt.sttd.agent.remote   : [{}]", sttd_agent_remote);
  g_logger->info("brain-sttd.default.model      : [{}]", model);
  g_logger->info("sip.inbound.use.stun          : [{}]", use_stun);
  g_logger->info("sip.inbound.stun.server       : [{}]", stun_server);
  g_logger->info("sip.tts.grpc.deadline         : [{}]", tts_deadline);
  g_logger->info("sip.stt.samplerate            : [{}]", stt_samplerate);
  g_logger->info("sip.user_agent                : [{}]", sip_user_agent);
  g_logger->info("logs.console                  : [{}]", logs_console);
  g_logger->info("logs.level                    : [{}]", logs_level);
  g_logger->info("Dialog.url                    : [{}]", rest_sds_url);
  g_logger->info("Dialog.response.timeout       : [{}]", resp_timeout);
  g_logger->info("SDN.addr                      : [{}]", sdn_addr);
  g_logger->info("SDN.speech.tempo              : [{}]", tts_tempo);
  g_logger->info("Agent.relay.host              : [{}]", agent_relay_host);
  g_logger->info("Agent.relay.port              : [{}]", agent_relay_port);
  g_logger->info("Customer TEL No.              : [{}]", tel_no);

  // from process argument
  g_logger->info("------------------------------------------------------------");
  g_logger->info("sip_user                      : [{}]", sip_user);
  g_logger->info("sip_passwd                    : [{}]", sip_passwd);
  g_logger->info("sip_port                      : [{}]", sip_port);
  g_logger->info("sip_domain                    : [{}]", sip_domain);
  g_logger->info("sip_domain2                   : [{}]", sip_domain2);
  g_logger->info("contract_no                   : [{}]", contract_no);
  g_logger->info("campaign_id                   : [{}]", campaign_id);
  g_logger->info("is_inbound                    : [{}]", is_inbound);
  g_logger->info("is_once                       : [{}]", is_once);
  g_logger->info("record_on_early               : [{}]", record_on_early);
  g_logger->info("------------------ END CONFIG   ----------------------------");
}
