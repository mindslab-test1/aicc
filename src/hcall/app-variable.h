#ifndef APP_VARIABLE_H
#define APP_VARIABLE_H

#include <map>
#include <atomic>
#include <spdlog/spdlog.h>
#include "boost/asio.hpp"  // must be before pjsua

#include <pjsua-lib/pjsua.h>
#include "aicc-db.h"

#define QUOTE(...) #__VA_ARGS__

#define AGENT_ID_MAX_LEN 32

enum APP_COMMAND_ENUM {
  HANGUP,
  DISCONNECTED,
  CHECK_PKT,
  PLAY_AUDIO,
  CALL_TRANSFER,
  TRANSFER_TIMEOUT,
  REFER,
};

extern std::string app_cmd_text[];

struct APP_MESSAGE_T {
  int  command;
  int  call_id;
  char agent_id[AGENT_ID_MAX_LEN];
};

struct SIP_PKTCNT_T {
  pj_uint32_t count;
};

struct STT_SERVER_INFO {
  std::string addr;
  std::string model;
  std::string samplerate;
  std::string lang;
};

// variable, config value, etc... for this application
struct APP_VARIABLE {
  /**
   * @brief Constructor && Destructor
   *
   */
  APP_VARIABLE();
  ~APP_VARIABLE();

  /**
   * @brief arg parse
   *
   * @param argc main fuction의 argc
   * @param argv main fuction의 argv
   */
  void ParseArgs(int argc, char *argv[]);

  /**
   * @brief 초기 APP_VARIABLE값을 설정하는 함수, g_var값을 사용하기 전에 호출되어야 한다.
   * 
   * @param argc main fuction의 argc
   * @param argv main fuction의 argv
   * @return void
   */
  void Initialize(int argc, char *argv[]);

  /**
   * @brief 현재 설정된 값을 보여주기 위한 함수
   * 
   * @return void
   */
  void ShowVariable();

  /**
   * @brief 명시적으로 자원 해제시에 사용
   *
   * @return void
   */
  void Cleanup();

  /**
   * @brief 터미널 화면에 출력하기 위한 logger
   * 
   */
  std::shared_ptr<spdlog::logger> console;

  /**
   * @brief 프로그램에서 사용하는 logger
   * 
   */
  std::shared_ptr<spdlog::logger> logger;

  // config variable
  std::map<std::string, STT_SERVER_INFO> sttd_list;

  /**
   * @brief sttd 서버 주소
   * 
   */
  std::string sttd_remote;
  std::string sttd_agent_remote;

  /**
   * @brief stt엔진에서 사용될 모델
   * 
   */
  std::string model;
  std::string agent_model;

  /**
   * @brief stt엔진에서 사용될 Language (kor, eng)
   * 
   */
  std::string lang;
  std::string agent_lang;

  bool direct_call;  // 상담사 통화 전용 (AI 미사용)

  /**
   * @brief stt 음성 데이터 sample rate (sip rtp의 sample rate)
   * 
   */
  int stt_samplerate;
  int agent_samplerate;

  /**
   * @brief stt epd 되기 전까지 최대 대기 시간
   *
   */
  int epd_timeout;

  std::string agent_relay_host;
  int         agent_relay_port;

  /**
   * @brief true when not using PBX
   *
   */
  bool use_stun;                /* true when not using PBX  */

  /**
   * @brief stun server ip address
   *
   */
  std::string stun_server;

  /**
   * @brief TTS 발화 멈춤 키워드
   * 
   */
  std::string stop_tts_keyword;

  bool da_record_event;

  /**
   * @brief music on hold
   *
   */
  std::string moh_wav;

  /**
   * @brief tts 서버로 request를 보냈을 때 response timeout
   * 
   */
  int tts_deadline;             /* seconds */

  bool tts_echo_sample;
  std::string tts_sample_wav;

  bool logs_console;
  int logs_level;

  // for samsung Rest API
  std::string chat_id;
  int date_index;

  /**
   * @brief happy call DB에서 관리되는 정보
   *
   */
  std::string contract_no;
  std::string campaign_id;
  int         call_history_id;

  std::string cust_name;
  // std::string prod_name;
  std::string tel_no;
  // std::string contract_date;

  std::string m2u_addr;
  std::string rest_sds_url;
  std::string sdn_addr;

  std::string call_ws_addr;

  /**
   * @brief sip client를 실행하기 위한 정보
   * 
   */
  std::string sip_domain;
  std::string sip_domain2;
  std::string sip_user;
  std::string sip_passwd;
  int         sip_port;
  std::string sip_user_agent;
  std::string sip_call_id;
  int         rtp_port;
  std::string outbound_prefix;

  bool is_inbound;
  bool is_once;
  bool record_on_early;
  int  inbound_delaytime;

  /**
   * @brief pjsua media config 에서 사용할 sample rate
   *
   */
  int  clock_rate;

  std::string db_host;
  int         db_port;
  std::string db_user;
  std::string db_passwd;
  std::string db_name;

  std::string record_dir;

  std::string chatbot_name;

  // 로그인한 상담사 ID
  std::string agent_id;
  // 상담사 연결 최대 대기 시간
  int agent_timeout;

  int resp_timeout;

  bool is_ready;

  // for application use
  double tts_tempo;
  bool is_doing_tts;

  /**
   * @brief 녹음 파일 stereo 여부
   *
   */
  bool is_stereo;

  bool encrypt_telno;
  std::string encrypt_url;

  boost::asio::io_service service;

  // ZeroMQ - 프로세스 내에서는 하나의 context만 사용하도록 권장
  void *app_context;
  void *evnt_sender;

  std::string maum_root;

  std::atomic<int> call_cnt;

  AiccDb db;
};

extern APP_VARIABLE g_var;

// for convenience
extern std::shared_ptr<spdlog::logger> g_logger;

#endif /* APP_VARIABLE_H */
