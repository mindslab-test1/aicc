#ifndef CALL_STATUS_H
#define CALL_STATUS_H

// Status Code Description

#define CS_IDLE             "CS0000" // Voice Dialog session이 오픈되지 않은 상태.
#define CS_READY            "CS0001" // Outbound Call 시작 준비가 된 상태.
#define CS_AI_PROCESSING    "CS0002" // AI Connected 상태. (통화 중)
#define CS_STOPPED          "CS0003" // 진행 중지 상태. Retry 하지 않는다.
#define CS_RETRY            "CS0004" // 전화연결이 되지 않아서 재시도. Retry
                                     // count를 별도로 관리한다. Max. Count
                                     // 도달 시 Stopped로 변경.
#define CS_AI_COMPLETED     "CS0005" // AI 통화 완료
#define CS_CONNECTING       "CS0006" // 전화 걸기 시도 중.
#define CS_DROPPED          "CS0007" // 통화 중 끊어진 상태, Callback (unknown message) 3회 이상
#define CS_WAITING          "CS0008" // 콜 큐에 대기 중인 상태
#define CS_AGENT_PROCESSING "CS0009" // 상담사 Connected 상태. (통화 중)
#define CS_AGENT_COMPLETED  "CS0010" // 상담사 통화 완료
#define CS_AGENT_FAIL       "CS0011" // 상담사 전환 실패
#define CS_AGENT_TRANSFERRED  "CS0031" // 상담사 전환 완료 (이후 상태는 알 수 없음)

#endif /* CALL_STATUS_H */








