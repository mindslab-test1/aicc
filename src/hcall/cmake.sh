#!/bin/sh
# CentOS
cmake3 -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
	-DBOOST_INCLUDEDIR=/usr/include \
	-DBOOST_LIBRARYDIR=/usr/lib64 \
	-DUSE_MYSQL=ON \
	..

# Ubuntu
# boost 를 gcc 4.8 버전에 맞게 컴파일해서 준비해야 합니다
# cmake  -DCMAKE_BUILD_TYPE=Debug \
# 	-DCMAKE_C_COMPILER=/usr/bin/gcc-4.8 \
# 	-DCMAKE_CXX_COMPILER=/usr/bin/g++-4.8  \
# 	-DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
# 	-DBOOST_ROOT=/opt/boost_1_67_0 \
# 	-DUSE_ORACLE=on ..
