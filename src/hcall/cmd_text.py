#!/usr/bin/python

import os
import zmq
import struct

class AppCommand:
    HANGUP       = 0
    DISCONNECTED = 1
    CHECK_PKT    = 2
    PLAY_AUDIO   = 3
    CALL_TRANSFER   = 4
    REFER = 5


def send_play_audio():
    context = zmq.Context()
    print "Connecting to server..."

    MAUM_ROOT = os.getenv('MAUM_ROOT')
    endpoint = 'ipc://%s/run/%s.cmd.ipc' % (MAUM_ROOT, '7045')

    socket = context.socket(zmq.REQ)
    socket.connect(endpoint)

    command = AppCommand.PLAY_AUDIO
    call_id = 0
    agent_id = '1123'

    msg = struct.pack('@ii32s', command, call_id, agent_id)
    socket.send(msg)
    message = socket.recv()
    message = struct.unpack('@ii32s', message)
    print 'Got message: ', message


def send_refer():
    context = zmq.Context()
    print "Connecting to server..."

    MAUM_ROOT = os.getenv('MAUM_ROOT')
    endpoint = 'ipc://%s/run/%s.evt.ipc' % (MAUM_ROOT, '7045')

    socket = context.socket(zmq.PUSH)
    socket.connect(endpoint)

    command = AppCommand.REFER
    call_id = 0
    agent_id = '1123'

    msg = struct.pack('@ii32s', command, call_id, agent_id)
    socket.send(msg)

def main():
    choice = raw_input('1. PLAY AUDIO\n2. CALL TRANSFER\n--->')
    command = AppCommand.PLAY_AUDIO
    if choice == '1':
        send_play_audio()
    elif choice == '2':
        send_refer()

if __name__ == '__main__':
    main()
