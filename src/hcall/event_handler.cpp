#include "event_handler.h"

EventHandler::EventHandler()
{
}

EventHandler::~EventHandler()
{
}

void EventHandler::handle_accept(int fd)
{
}

int EventHandler::handle_recv(int fd)
{
    return 0;
}

void EventHandler::OnTimeout(int id)
{
}

void EventHandler::handle_user_event(void* info)
{
}

int EventHandler::GetEventType(int fd)
{
    return EventHandler::UNKNOWN;
}

int EventHandler::get_handle() const
{
    return fd_;
}

void EventHandler::set_handle(int handle)
{
    fd_ = handle;
}
