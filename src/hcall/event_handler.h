#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

class EventHandler
{
public:
    EventHandler();
    virtual ~EventHandler();

    enum EventType {
        CONNECT,
        ACCEPT,
        RECV,
        USER_EVENT,
        UNKNOWN
    };

    virtual void handle_accept(int fd);
    virtual int  handle_recv(int fd);
    virtual void OnTimeout(int id);
    virtual void handle_user_event(void* info);

    virtual int  GetEventType(int fd);

    int  get_handle() const;
    void set_handle(int handle);

private:
    int fd_;
};

#endif /* EVENT_HANDLER_H */



