#include "m2u-client.h"

#include <time.h>
#include <unistd.h>
#include <uuid/uuid.h>
#include <grpc++/grpc++.h>
#include <google/protobuf/text_format.h>
#include <google/protobuf/util/json_util.h>
#include <google/protobuf/util/time_util.h>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "voice-dialog.h"

using namespace maum::brain::tts;
using namespace std::chrono;
using std::string;

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReader;
using namespace maum::m2u::map;
using namespace google::protobuf::util;

#define DEVICE_ID "ID_123457"

namespace bu = boost::uuids;

shared_ptr<spdlog::logger> M2uClient::logger;
std::map<int, shared_ptr<M2uRequest> > M2uClient::request_list_;
std::mutex M2uClient::list_lock_;

#define LOGGER() M2uClient::logger

std::string gen_uuid() {
  static bu::random_generator gen;
  bu::uuid u = gen();
  return bu::to_string(u);
}

////////////////////////////////////////////////////////////////////////////////
//
// M2uRequest
//
////////////////////////////////////////////////////////////////////////////////
M2uRequest::M2uRequest() {
  stream_id = gen_uuid();

  if (!LOGGER()) {
    LOGGER() = spdlog::stdout_color_mt("console");
  }

  request_no = -1;
}

M2uRequest::M2uRequest(std::string text,
                       std::string contract_no,
                       std::string campaign_id,
                       std::string call_id,
                       std::string doing_tts,
                       std::string status) {
  stream_id = gen_uuid();

  if (!LOGGER()) {
    LOGGER() = spdlog::stdout_color_mt("console");
  }

  text_        = text;
  contract_no_ = contract_no;
  campaign_id_ = campaign_id;
  call_id_     = call_id;
  doing_tts_   = doing_tts;
  status_      = status;
  request_no = -1;
}

M2uRequest::~M2uRequest() {
  if (thrd_.joinable()) {
    thrd_.join();
  }
  LOGGER()->trace("M2uRequest no: {} dtor...", request_no);
}

void M2uRequest::cancel() {
  ctx_.TryCancel();
}

void M2uRequest::async_open_dialog(std::string addr, std::string chatbot_id) {
  thrd_ = std::thread([&, addr, chatbot_id](){
      open_dialog(addr, chatbot_id);
    });
}

void M2uRequest::async_text_to_text_talk(std::string addr,
                                         dict meta,
                                         std::function<void(dict)> callback_answer,
                                         M2uClient *client) {
  thrd_ = std::thread([&, addr, meta, callback_answer, client](){
      auto result = text_to_text_talk(addr, meta);

      if (callback_answer) {
        callback_answer(result);
      }
      client->release(this->request_no);
    });
}

void M2uRequest::open_dialog(std::string addr, std::string chatbot_id) {
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<MaumToYouProxyService::Stub> stub(MaumToYouProxyService::NewStub(channel));

  MapEvent map_event = set_open_dialog(chatbot_id);
  EventStream* event = map_event.mutable_event();
  
  ClientContext ctx;
  ctx.AddMetadata("m2u-auth-sign-in", event->operation_sync_id());
  ctx.AddMetadata("m2u-auth-token", auth_token);
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");

  auto stream = stub->EventStream(&ctx);
  stream->Write(map_event);
  stream->WritesDone();
  LOGGER()->info("sync_id {}, auth_token {}", event->operation_sync_id(), auth_token);
  MapDirective resp;
  while (stream->Read(&resp)) {
    if (resp.has_exception()) {
      LOGGER()->error("open_dialog() error: {}", resp.exception().ex_message());
    }
  }

  grpc::Status status = stream->Finish();
  if (!status.ok()) {
    LOGGER()->error("open_dialog() status error: {}", status.error_message());
  }
}

dict M2uRequest::text_to_text_talk(std::string addr, dict meta) {
  std::map<std::string, std::string> result;
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<MaumToYouProxyService::Stub> stub(MaumToYouProxyService::NewStub(channel));

  MapEvent map_event = set_text_to_text_talk(meta);
  EventStream* event = map_event.mutable_event();

  ctx_.AddMetadata("m2u-auth-sign-in" , event->operation_sync_id());
  ctx_.AddMetadata("m2u-auth-token"   , auth_token);
  ctx_.AddMetadata("m2u-auth-internal", "m2u-auth-internal");

  auto stream = stub->EventStream(&ctx_);
  stream->Write(map_event);
  stream->WritesDone();

  MapDirective resp;
  while (stream->Read(&resp)) {
    if (resp.has_exception()) {
      LOGGER()->error("text_to_text_talk() error: {}", resp.exception().ex_message());
    }
    if (resp.directive().has_payload()) {
      auto payload_map = resp.directive().payload().fields();
      for (auto kv : payload_map) {
        result[kv.first] = kv.second.string_value();
      }
    }
  }

  grpc::Status status = stream->Finish();
  if (!status.ok()) {
    LOGGER()->error("text_to_text_talk() status code: {}, error: {}", status.error_code(), status.error_message());
  }
  return result;
}

MapEvent M2uRequest::set_open_dialog(std::string chatbot_id) {
  MapEvent map_event;
  EventStream* event = map_event.mutable_event();
  event_set_interface(event, "Dialog", "Open");
  event->set_stream_id(stream_id);
  event->set_operation_sync_id(stream_id);

  event_context_add_device(event);
  event_context_add_location(event);

  auto &payload = *event->mutable_payload()->mutable_fields();
  {
    payload["utter"].set_string_value("UTTER1");
    payload["chatbot"].set_string_value(chatbot_id);
  }

  event_set_begin(event);

  return map_event;
}

MapEvent M2uRequest::set_auth_dialog() {
  MapEvent map_event;
  EventStream* event = map_event.mutable_event();
  event_set_interface(event, "Authentication", "SignIn");

  event->set_stream_id(gen_uuid());
  event->set_operation_sync_id(gen_uuid());

  google::protobuf::Struct* open_payload = event->mutable_payload();
  {
    auto &fields = *open_payload->mutable_fields();
    fields["userkey"].set_string_value("admin");
    fields["passphrase"].set_string_value("1234");
  }

  event_set_begin(event);

  return map_event;
}

MapEvent M2uRequest::set_text_to_text_talk(dict extra_meta) {
  MapEvent map_event;
  EventStream* event = map_event.mutable_event();

  event_set_interface(event, "Dialog", "TextToTextTalk");

  event->set_stream_id(gen_uuid());
  event->set_operation_sync_id(gen_uuid());

  event_context_add_device(event);
  event_context_add_location(event);

  auto &payload = *event->mutable_payload()->mutable_fields();
  {
    // payload["lang"].set_string_value("ko_KR");
    payload["lang"].set_number_value(0.0);
    payload["utter"].set_string_value(text_);
    payload["inputType"].set_string_value("KEYBOARD");
    auto &meta = *payload["meta"].mutable_struct_value()->mutable_fields();
    {
      meta["contract_no"].set_string_value(contract_no_);
      meta["campaign_id"].set_string_value(campaign_id_);
      meta["call_id"].set_string_value(call_id_);
      meta["doing_tts"].set_string_value(doing_tts_);
      meta["status"].set_string_value(status_);
      for (auto kv : extra_meta) {
        meta[kv.first].set_string_value(kv.second);
      }
    }
  }

  event_set_begin(event);

  return map_event;
}

void M2uRequest::event_set_interface(EventStream* event,
                                     std::string interface,
                                     std::string operation) {
  event->mutable_interface()->set_interface(interface);
  event->mutable_interface()->set_operation(operation);
  event->mutable_interface()->set_type(AsyncInterface_OperationType_OP_EVENT);
  event->mutable_interface()->set_streaming(false);
}

void M2uRequest::event_context_add_device(EventStream* event) {
  maum::m2u::map::EventStream_EventContext* context = event->add_contexts();
  auto device = context->mutable_device();
  {
    device->set_id(session_id);
    device->set_type("PC");
    device->set_version("0.1");
    device->set_channel("CHANNEL_12345");
    auto support = device->mutable_support();
    {
      support->set_support_render_text(true);
      support->set_support_render_card(true);
      support->set_support_speech_synthesizer(true);
      support->set_support_play_audio(true);
      support->set_support_play_video(true);
      support->set_support_action(true);
      support->set_support_move(true);
      support->set_support_expect_speech(true);
    }
    auto device_timestamp = device->mutable_timestamp();
    {
      timeval tv;
      gettimeofday(&tv, NULL);
      device_timestamp->set_seconds(tv.tv_sec);
      device_timestamp->set_nanos(tv.tv_usec * 1000);
    }
    device->set_timezone("KST+9");

    auto &meta = *device->mutable_meta()->mutable_fields();
    {
      meta["manufacture"].set_string_value("마인즈랩");
    }
  }
}

void M2uRequest::event_context_add_location(EventStream* event) {
  maum::m2u::map::EventStream_EventContext* context = event->add_contexts();
  context->mutable_location()->set_latitude(10);
  context->mutable_location()->set_longitude(120);
  context->mutable_location()->set_location("마인즈랩");
}

void M2uRequest::event_set_begin(EventStream* event) {
  timeval tv;
  gettimeofday(&tv, NULL);
  auto ts = event->mutable_begin_at();
  ts->set_seconds(tv.tv_sec);
  ts->set_nanos(tv.tv_usec * 1000);
}

MapEvent M2uRequest::set_close_dialog() {
  MapEvent map_event;
  EventStream* event = map_event.mutable_event();
  event_set_interface(event, "Dialog", "Close");

  event->set_stream_id(gen_uuid());
  event->set_operation_sync_id(gen_uuid());

  event_context_add_device(event);
  event_context_add_location(event);

  return map_event;
}

void M2uRequest::print_message(google::protobuf::Message &map_event) {
  std::string result;
#if 1
  JsonPrintOptions opt;
  opt.add_whitespace = true;
  google::protobuf::util::MessageToJsonString(map_event, &result, opt);
#else
  google::protobuf::TextFormat::PrintToString(map_event, &result);
#endif
  std::cout << result << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
//
// M2uClient
//
////////////////////////////////////////////////////////////////////////////////
M2uClient::M2uClient(ba::io_service &ios,
                     std::string addr,
                     string contract_no,
                     string campaign_id,
                     string call_id)
    : ios_(ios) {
  addr_ = addr;
  if (!logger) {
    logger = spdlog::stdout_color_mt("console");
  }
  session_id_ = gen_uuid();

  contract_no_ = contract_no;
  call_id_ = call_id;
}

M2uClient::~M2uClient() {
  for (auto kv : request_list_) {
    kv.second->cancel();
  }
#if 1
  for (int i=0; i < 100; i++) {
    {
      std::unique_lock<std::mutex> lock(list_lock_);
      if (request_list_.empty()) break;
    }
    usleep(100 * 1000);
  }
#endif
}

shared_ptr<M2uRequest> M2uClient::create_request(string text, string doing_tts, string status) {
  int num = request_seq_++;
  auto request = std::make_shared<M2uRequest>(text, contract_no_, campaign_id_, call_id_, doing_tts, status);
  std::unique_lock<std::mutex> lock(list_lock_);
  request_list_[num] = request;
  request->request_no = num;
  request->auth_token = auth_token_;
  request->session_id = session_id_;
  return request;
}

void M2uClient::release(int request_no) {
  ios_.post(([this, request_no](){
        std::unique_lock<std::mutex> lock(list_lock_);
        request_list_.erase(request_no);
      }));
}

void M2uClient::authenticate() {
  auto channel = grpc::CreateChannel(addr_, grpc::InsecureChannelCredentials());
  std::unique_ptr<MaumToYouProxyService::Stub> stub(MaumToYouProxyService::NewStub(channel));

  M2uRequest msg;
  MapEvent map_event = msg.set_auth_dialog();
  EventStream* event = map_event.mutable_event();
  
  ClientContext ctx;
  ctx.AddMetadata("m2u-auth-sign-in", event->operation_sync_id());
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");

  auto stream = stub->EventStream(&ctx);
  stream->Write(map_event);
  stream->WritesDone();
  MapDirective resp;
  msg.print_message(map_event);
  while (stream->Read(&resp)) {
    if (resp.has_exception()) {
      logger->error("authenticate() error: {}", resp.exception().ex_message());
    }
    // msg.print_message(resp);
    if (resp.has_directive()) {
      auto &fields = resp.directive().payload().fields();
      auto auth = fields.find("authSuccess");
      if (auth != fields.end()) {
        auto sub_fields = auth->second.struct_value().fields();
        auto it = sub_fields.find("authToken");
        if (it != sub_fields.end()) {
          auth_token_ = it->second.string_value();
          logger->info("authenticate() get token: {}", auth_token_);
        }
      }
    }
  }

  grpc::Status status = stream->Finish();
  if (!status.ok()) {
    LOGGER()->error("authenticate() error: {}", resp.exception().ex_message());
  }
}

void M2uClient::open_dialog(std::string chatbot_id) {
  M2uRequest req;
  req.auth_token = auth_token_;
  req.session_id = session_id_;
  req.open_dialog(addr_, chatbot_id);
}

dict M2uClient::text_to_text_talk(string text,
                                  string doing_tts,
                                  string status,
                                  dict meta) {
  M2uRequest req(text, contract_no_, campaign_id_, call_id_, doing_tts, status);
  req.auth_token = auth_token_;
  req.session_id = session_id_;
  return req.text_to_text_talk(addr_, meta);
}

void M2uClient::async_text_to_text_talk(string text,
                                        string doing_tts,
                                        string status,
                                        dict meta,
                                        std::function<void(dict)> cb) {
  auto request = create_request(text, doing_tts, status);
  request->async_text_to_text_talk(addr_, meta, cb, this);
}

void M2uClient::close_dialog() {
  auto channel = grpc::CreateChannel(addr_, grpc::InsecureChannelCredentials());
  std::unique_ptr<MaumToYouProxyService::Stub> stub(MaumToYouProxyService::NewStub(channel));

  M2uRequest req;
  req.session_id = session_id_;
  MapEvent map_event = req.set_close_dialog();
  EventStream* event = map_event.mutable_event();
  
  ClientContext ctx;
  ctx.AddMetadata("m2u-auth-sign-in" , event->operation_sync_id());
  ctx.AddMetadata("m2u-auth-token"   , auth_token_);
  ctx.AddMetadata("m2u-auth-internal", "m2u-auth-internal");

  auto stream = stub->EventStream(&ctx);
  stream->Write(map_event);
  stream->WritesDone();
  MapDirective resp;
  while (stream->Read(&resp)) {
    if (resp.has_exception()) {
      logger->error("close_dialog() error: {}", resp.exception().ex_message());
    }
    // msg.print_message(resp);
  }

  grpc::Status status = stream->Finish();
  if (!status.ok()) {
    logger->error("close_dialog() status error: {}", status.error_message());
    // g_logger->error("M2U's grpc finish() error : {}", status.error_message());
  }
}
