#ifndef M2U_CLIENT_H
#define M2U_CLIENT_H

#include <string>
#include <memory>
#include <thread>
#include "boost/asio.hpp"
#include "maum/m2u/map/map.grpc.pb.h"
#include "maum/m2u/map/map.pb.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

using namespace maum::m2u::map;
using std::shared_ptr;
using std::unique_ptr;
using dict = std::map<std::string, std::string>;
namespace ba = boost::asio;

class M2uClient;
class M2uRequest {
 public:
  M2uRequest();
  M2uRequest(std::string text,
             std::string contract_no,
             std::string campaign_id,
             std::string call_id,
             std::string doing_tts,
             std::string status);
  ~M2uRequest();

  void cancel();

  void open_dialog(std::string device_id, std::string chatbot_id);
  void authenticate(std::string device_id);
  dict text_to_text_talk(std::string addr, dict meta);
  void close_dialog(std::string addr);

  MapEvent set_open_dialog(std::string chatbot_id);
  MapEvent set_auth_dialog();
  MapEvent set_text_to_text_talk(dict extra_meta);
  MapEvent set_close_dialog();

  void print_message(google::protobuf::Message &map_event);

  // internal function
  void event_set_interface(EventStream* event, std::string interface, std::string operation);
  void event_context_add_device(EventStream* event);
  void event_context_add_location(EventStream* event);
  void event_set_begin(EventStream* event);

  void async_open_dialog(std::string addr, std::string chatbot_id);
  void async_text_to_text_talk(std::string addr,
                               dict meta,
                               std::function<void(dict)> cb,
                               M2uClient *client);

  std::string stream_id;
  int         request_no;
  std::string auth_token;
  std::string session_id;

 private:
  std::string text_;
  std::string contract_no_;
  std::string campaign_id_;
  std::string call_id_;
  std::string doing_tts_;
  std::string status_;

  std::thread thrd_;

  grpc::ClientContext ctx_;
};

class M2uClient {
 public:
  M2uClient(ba::io_service &ios, std::string addr, std::string contract_no, std::string campaign_id, std::string call_id);
  ~M2uClient();

  shared_ptr<M2uRequest> create_request(std::string text,
                                        std::string doing_tts,
                                        std::string status);
  void release(int request_no);

  // REQUEST
  void authenticate();
  void open_dialog(std::string chatbot_id);
  dict text_to_text_talk(std::string text,
                         std::string doing_tts,
                         std::string status,
                         dict meta);
  void async_text_to_text_talk(std::string text,
                               std::string doing_tts,
                               std::string status,
                               dict meta,
                               std::function<void(dict)> cb = nullptr);
  void close_dialog();

  std::string auth_token_;
  std::string session_id_;

  std::string contract_no_;
  std::string campaign_id_;
  std::string call_id_;

  std::string addr_;

  static shared_ptr<spdlog::logger> logger;

 private:
  std::atomic<int> request_seq_;
  static std::map<int, shared_ptr<M2uRequest> > request_list_;
  static std::mutex list_lock_;
  boost::asio::io_service &ios_;
};

#endif /* M2U_CLIENT_H */
