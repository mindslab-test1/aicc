#include "app-variable.h"
#include "sip-client.h"
#include "stt-stream.h"
#include <boost/thread.hpp>

int main(int argc, char *argv[]) {
  g_var.Initialize(argc, argv);

  // io_service 작업이 없는 경우 loop 종료 방지
  boost::asio::io_service::work worker(g_var.service);
  boost::thread_group io_threads;
  for (int i = 0; i < 4; i++) {
    io_threads.create_thread(boost::bind(&boost::asio::io_service::run, &g_var.service));
  }

  //SttStream::WaitServer();
  g_var.logger->info("STTD is ready");

  SipClient client((char*)g_var.tel_no.c_str());
  client.Initialize();
  client.AddTransport();
  client.Run();
  client.Cleanup();

  g_var.service.stop();
  io_threads.join_all();

  g_var.Cleanup();
  return 0;
}
