#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb
import socket
import json
import grpc
import os
import sys
from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc

def bytes_from_file(filename, chunksize=10000):
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(chunksize)
            if chunk:
                speech = stt_pb2.Speech()
                speech.bin = chunk
                yield speech
            else:
                break

def stream_recognize(filename, channel, metadata):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    stub = stt_pb2_grpc.SpeechToTextServiceStub(channel)
    segments = stub.StreamRecognize(bytes_from_file(filename), metadata=metadata)

    try:
        for seg in segments:
            print '%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt)
            data = {
                "Text": seg.txt,
                "VoiceStart": seg.start,
                "VoiceEnd": seg.end,
                "AudioFilename":"some.wav"
            }

            msg = json.dumps(data, ensure_ascii=False, encoding='utf8').encode('utf8')
            print msg
            sock.sendto(msg, ('127.0.0.1', 5000))

    except grpc.RpcError as e:
        print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))


def main():
    if len(sys.argv) < 2:
        print 'Usage: %s filename' % sys.argv[0]
        return

    filename = sys.argv[1]
    channel = grpc.insecure_channel('127.0.0.1:9801')
    metadata={(b'in.lang', b'kor'), (b'in.model', 'baseline'), (b'in.samplerate', '8000') }
    stream_recognize(filename, channel, metadata)


if __name__ == '__main__':
    main()
