#include "play-port.h"
#include "app-variable.h"
#include "agent-stream.h"

AgentStream* play_client = NULL;

pj_status_t pjmedia_play_create(pj_pool_t *pool, pjmedia_port **p_port)
{
  g_var.logger->debug("pjmedia_play_create");

  pj_status_t status;
  const pj_str_t STR_PLAY_GEN = pj_str("play");
  struct play_port *port;

  /* Create and initialize port */
  int sample_rate = 8000; // g_var.play_samplerate;
  port = PJ_POOL_ZALLOC_T(pool, struct play_port);

  status = pjmedia_port_info_init(&port->base.info, &STR_PLAY_GEN,
                                  PJMEDIA_SIG_PORT_MEM_CAPTURE,
                                  sample_rate, 1,
                                  16,
                                  sample_rate * 20 / 1000 // samples per frame
                                 );
  port->base.put_frame  = &play_put_frame;
  port->base.get_frame  = &play_get_frame;
  port->base.on_destroy = &play_on_destroy;

  *p_port = &port->base;
  // if (play_client == NULL) {
  //   play_client = new AgentStream();
  //   play_client->Connect("127.0.0.1", 5900);
  //   play_client->send_register(1234, SEND_ONLY);
  // }
  return status;
}

void pjmedia_play_register(char *agent_id)
{
  if (play_client == NULL) {
    play_client = new AgentStream();
    play_client->Connect(g_var.agent_relay_host, g_var.agent_relay_port);
  }

  play_client->send_register(agent_id, SEND_ONLY);
}

/**
 * 다른 미디어포트에서 pjmedia_port_put_frame 을 호출하면 그 안에서
 * play_put_frame이 불린다
 */
pj_status_t play_put_frame(pjmedia_port *this_port, pjmedia_frame *frame)
{
  PJ_UNUSED_ARG(this_port);
  if (frame->type == PJMEDIA_FRAME_TYPE_AUDIO) {
    // play_port* port = (play_port*)this_port;
    if (play_client) {
      play_client->send_transfer((char*)frame->buf, frame->size);
    } else {
      // there is no client
    }
  }
  return PJ_SUCCESS;
}

pj_status_t play_get_frame(pjmedia_port *this_port, pjmedia_frame *frame)
{
  printf("get frame type is %d\n", frame->type);
  if (frame->type == PJMEDIA_FRAME_TYPE_AUDIO) {
    // do nothing
  } else {
    // don't reach here
  }
  return PJ_SUCCESS;
}

pj_status_t play_on_destroy(pjmedia_port *this_port)
{
  g_logger->trace("called play_on_destroy()");

  // struct play_port *port;
  // port = (struct play_port *)this_port;

  if (play_client != NULL) {
    delete play_client;
    play_client = NULL;
  }

  g_logger->trace("play_port destroyed");

  return PJ_SUCCESS;
}
