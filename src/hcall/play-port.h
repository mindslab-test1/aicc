#ifndef PLAY_PORT_H
#define PLAY_PORT_H

#include <pjsua-lib/pjsua.h>

struct play_port
{
  pjmedia_port base;
};

pj_status_t play_put_frame(pjmedia_port *this_port, pjmedia_frame *frame);
pj_status_t play_get_frame(pjmedia_port *this_port, pjmedia_frame *frame);
pj_status_t play_on_destroy(pjmedia_port *this_port);

pj_status_t pjmedia_play_create(pj_pool_t *pool, pjmedia_port **p_port);

void pjmedia_play_register(char *agent_id);

#endif /* PLAY_PORT_H */
