#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import json
import pprint
import time
from datetime import datetime

baseurl ='http://52.79.213.190:8090/aicc-sdn'
url = baseurl + '/tts/media'
data = \
{
	"compaignId": 1,
	"messages":[
		"안녕하십니까",
		"마음손해보험 고객콜센터 ", "유인나?","입니다",
		"박미선?","고객님 맞으십니까"
		]
}

st = time.time()
response = requests.post(url, json=data)
f = open('sdn.wav', 'w')
for chunk in response.iter_content():
	f.write(chunk)
f.close()
print time.time() - st
