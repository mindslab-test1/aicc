#include <curl/curl.h>
#include "rest-client.h"

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/stdout_color_sinks.h>
#endif

RestClient::RestClient(std::shared_ptr<spdlog::logger> logger) {
  if (logger) {
    logger_ = logger;
  } else {
    logger_ = spdlog::stdout_color_mt("console");
  }
}

RestClient::~RestClient() {
}

size_t RestClient::write_data(void *ptr, size_t size, size_t nmemb, std::string *data) {
  data->append((char*)ptr, size * nmemb);
  return size * nmemb;
}

bool RestClient::Post(const char *url, std::string &resp_body, char *json_data) {
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if (curl) {
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &RestClient::write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_data);

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK) {
      logger_->error("curl_easy_perform() failed: {}", curl_easy_strerror(res));
    }

    /* always cleanup */
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
  } else {
    logger_->error("curl_easy_init() failed");
    return false;
  }
  return true;
}


bool RestClient::Get(const char *url, std::string &resp_body) {
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &RestClient::write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &resp_body);

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK) {
      logger_->error("curl_easy_perform() failed: {}", curl_easy_strerror(res));
    }

    /* always cleanup */
    curl_easy_cleanup(curl);
  } else {
    logger_->error("curl_easy_init() failed");
    return false;
  }
  return true;
}

std::string RestClient::url_encode(const std::string &s) {
  const std::string unreserved =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~";

  std::string escaped = "";
  for(size_t i = 0; i < s.length(); i++) {
    if (unreserved.find_first_of(s[i]) != std::string::npos) {
      escaped.push_back(s[i]);
    } else {
      escaped.append("%");
      char buf[3];

      sprintf(buf, "%.2X", (unsigned char) s[i]);
      escaped.append(buf);
    }
  }
  return escaped;
}
