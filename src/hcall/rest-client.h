#ifndef REST_CLIENT_H
#define REST_CLIENT_H

#include <string>
#include "spdlog/spdlog.h"

class RestClient {
 public:
  RestClient(std::shared_ptr<spdlog::logger> logger);
  ~RestClient();

  /**
   * @brief POST Call to Rest API
   * 
   * @return bool
   */
  bool Post(const char *url, std::string &resp_body, char *json_data);

  /**
   * @brief GET Call to Rest API
   * 
   * @return bool
   */
  bool Get(const char *url, std::string &resp_body);

  /**
   * @brief url 인코딩
   * 
   * @param s 인코딩 input value
   * @return std::string
   */
  std::string url_encode(const std::string &s);

  /**
   * @brief libcurl에서 사용하는 callback 함수, curl 메뉴얼 참고
   * 
   * @param ptr curl참고
   * @param size curl참고
   * @param nmemb curl참고
   * @param data curl참고
   * @return size_t
   */
  static size_t write_data(void *ptr, size_t size, size_t nmemb, std::string *data);

  // std::string url_;
  std::shared_ptr<spdlog::logger> logger_;
};

#endif /* REST_CLIENT_H */
