#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import json
import pprint
from datetime import datetime
import time
import sys

# call-manager address
addr = '127.0.0.1:5000'
#addr = '10.122.64.249:5000'
baseurl ='http://%s' % addr
url = baseurl + '/call/start'
#url = baseurl + '/call/status'
#url = baseurl + '/call/stop'
#url = baseurl + '/call/%s' % sys.argv[1]

def main():
    if len(sys.argv) == 2 and sys.argv[1] == 'record':
        response = requests.get(baseurl + '/call/record/32/1')
        f = open('hihi.wav', 'w')
        for chunk in response.iter_content():
            f.write(chunk)
        f.close()
    elif len(sys.argv) > 3 and sys.argv[1] == 'start':
        data = \
        {
          "contractNo": sys.argv[2],
          "campaignId": sys.argv[3],
        }
        response = requests.post(url, json=data)
        pp = pprint.PrettyPrinter(indent=4)
        dic = response.json()
        pp.pprint(dic)

if __name__ == '__main__':
    for i in range(1):
        main()
    i = 0
    while False:
        main()
        print 'count is %d' % i
        i += 1
        time.sleep(0.01)
