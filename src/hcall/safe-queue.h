#ifndef SAFE_QUEUE_H
#define SAFE_QUEUE_H

#include <queue>
#include <condition_variable>
#include <mutex>
#include <atomic>

/**
 * @brief RTP패킷에 포함될 audio frame
 * 
 */
struct SoundFrame {
  int utter_no;
  int sent_no;
  char data[1024];
  size_t size;
};

/**
 * @brief thread-safe하도록 구현된 Queue
 * 
 */
template <typename DATA>
class SafeQueue {
 public:
  void push(DATA const& data) {
    std::unique_lock<std::mutex> lock(lock_);
    queue_.push(data);
    lock.unlock();
    cond_.notify_one();
  }

  bool empty() {
    std::unique_lock<std::mutex> lock(lock_);
    return queue_.empty();
  }

  size_t size() {
    std::unique_lock<std::mutex> lock(lock_);
    return queue_.size();
  }

  void clear() {
    std::unique_lock<std::mutex> lock(lock_);
    std::queue<DATA> empty_queue;
    std::swap(queue_, empty_queue);
  }

  bool try_pop(DATA& popped_value) {
    std::unique_lock<std::mutex> lock(lock_);
    if (queue_.empty()) {
      return false;
    }

    popped_value = queue_.front();
    queue_.pop();
    return true;
  }

  void wait_and_pop(DATA &popped_value) {
    std::unique_lock<std::mutex> lock(lock_);
    while (queue_.empty()) {
      cond_.wait(lock);
    }

    popped_value = queue_.front();
    queue_.pop();
  }

  std::atomic<bool> PlayingMOH;
  std::atomic<int>  UtterNo;
  std::atomic<int>  SentenceNo;

 private:
  std::queue<DATA> queue_;
  std::condition_variable cond_;
  std::mutex lock_;
};

#endif /* SAFE_QUEUE_H */
