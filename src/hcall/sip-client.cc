#include "app-variable.h"
#include "sip-client.h"
#include "stt-port.h"
#include "tts-port.h"
#include "play-port.h"
#include "stt-stream.h"
#include <iostream>
#include <csignal>
#include "call-recorder.h"
#include "call-status.h"
#include <pjsua-lib/pjsua_internal.h>
#include <pjsua-lib/pjsua.h>

#define THIS_FILE	"APP"

pjsua_call_id g_call_id;
pjsua_acc_id primary_acc_id;
pjsua_acc_id secondary_acc_id;
pjsua_acc_id outbound_acc_id;
std::string outbound_domain;

struct maum_call_data {
  pj_pool_t *pool;
  pjmedia_port *sttport;
  pjmedia_port *ttsport;
  pjmedia_port *play_port;
  pjsua_conf_port_id stt_slot;
  pjsua_conf_port_id tts_slot;
  pjsua_conf_port_id play_slot;

  SafeQueue<SoundFrame>* result_q;
};

void prepare_early_media(pjsua_call_id call_id);
void prepare_media(pjsua_call_id call_id);

void on_incoming_call(pjsua_acc_id acc_id,
                      pjsua_call_id call_id,
                      pjsip_rx_data *rdata);

void on_call_state(pjsua_call_id call_id, pjsip_event *e);

void on_call_media_state(pjsua_call_id call_id);

void maum_conf_connect(pjsua_call_info *ci, maum_call_data *cd);
void maum_conf_disconnect(pjsua_call_info *ci, maum_call_data *cd);
void maum_conf_reconnect(pjsua_call_info *ci, maum_call_data *cd);

void on_reg_state(pjsua_acc_id acc_id) {
  pj_status_t status;
  pjsua_acc_info info;
  g_logger->info("on_reg_state acc_id is {}", acc_id);

  status = pjsua_acc_get_info(acc_id, &info);
  if (status != PJ_SUCCESS) {
    g_logger->error("pjsua_acc_get_info error");
  } else {
    g_logger->info("registration status is {}", std::string(info.status_text.ptr, info.status_text.slen));
    if (acc_id == primary_acc_id) {
      outbound_acc_id = primary_acc_id;
      outbound_domain = g_var.sip_domain;
    } else if (acc_id == secondary_acc_id && outbound_domain.empty()) {
      outbound_acc_id = secondary_acc_id;
      outbound_domain = g_var.sip_domain2;
    }
  }
}

void on_stream_destroyed(pjsua_call_id call_id, pjmedia_stream *strm, unsigned stream_idx) {
  g_logger->debug("CALL_LOG: on_stream_destroyed()");
}

// void on_dtmf_digit2(pjsua_call_id call_id, const pjsua_dtmf_info *info) {
//   g_logger->info("on_dtmf_digit2");
// }

void error_exit(const char *title, pj_status_t status);

void on_dtmf_digit(pjsua_call_id call_id, int digit) {
  maum_call_data *cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);
  if (cd) {
    stt_port *stt_port = (struct stt_port *)cd->sttport;
    stt_port->stream->add_number(digit);
  } else {
    g_logger->error("There is no call_id({}) on_dtmf_digit()", call_id);
  }
  g_logger->info("DTMF DIGIT ---> {}", digit);
}

// void on_dtmf_digit2(pjsua_call_id call_id, const pjsua_dtmf_info *info) {
//   g_logger->info("on_dtmf_digit2");
// }

struct maum_call_data *call_init_maum(pjsua_call_id call_id) {
  pj_pool_t *pool;
  struct maum_call_data *cd;
  pjsua_call_info ci;

  pool = pjsua_pool_create("maum", 2048, 2048);
  cd = PJ_POOL_ZALLOC_T(pool, struct maum_call_data);
  cd->pool = pool;
  cd->result_q = new SafeQueue<SoundFrame>; // send stt result to tts

  pjmedia_stt_create(cd->pool, &cd->sttport, cd->result_q);
  // pjsua_conf_add_port(cd->pool, cd->sttport, &cd->stt_slot);

  pjmedia_tts_create(cd->pool, &cd->ttsport, cd->result_q);
  // pjsua_conf_add_port(cd->pool, cd->ttsport, &cd->tts_slot);

  pjmedia_play_create(cd->pool, &cd->play_port);

  pjsua_call_get_info(call_id, &ci);

  //pjsua_conf_connect(ci.conf_slot, cd->stt_slot);
  //pjsua_conf_connect(cd->tts_slot, ci.conf_slot);

  pjsua_call_set_user_data(call_id, (void*) cd);
  g_var.logger->trace("pjsua_call_set_user_data() - call_id: {}", call_id);

  //DONE: 2020-10-07 added by shinwc
  // To complete websocket connection before call media preparation
  // (prepare_media or prepare_early_media)
  auto websocket = std::make_shared<WebsocketClient>(g_var.service);
  auto rc = websocket->connect(g_var.call_ws_addr);
  if (rc != -1) {
    websocket->wait();
    websocket->publish_event(CS_CONNECTING);
  }
  //push_sound_frame("/home/ducbin/maum/samples/8k.pcm", cd->result_q);

  // main loop 에서 콜 종료를 위해 저장
  g_call_id = call_id;

  return cd;
}

void call_deinit_maum(pjsua_call_id call_id, pjsua_conf_port_id conf_slot) {
  maum_call_data *cd;
  pjsua_call_info ci;

  pjsua_call_get_info(call_id, &ci);

  cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);
  if (!cd) {
    g_var.logger->warn("there is no call data, call_id is {}", call_id);

    auto websocket = std::make_shared<WebsocketClient>(g_var.service);
    auto rc = websocket->connect(g_var.call_ws_addr);
    if (rc != -1) {
      websocket->wait();
      websocket->publish_event("CS0003");
      websocket->publish_signal("stop");
    }
    return;
  }

  // DONE: 2020-10-13 modified by shinwc
  // Check dialog status if SttStream instance is created
  // If dialog status is CS_IDLE, then dialog session needs to open.
  // Otherwise the dialog session should be managed by SttStream thread
  stt_port *stt_port = (struct stt_port *)cd->sttport;
  if (g_var.da_record_event) {
    dict result;
    std::string status = stt_port->stream->getDialogStatus();
    if (status == CS_IDLE) {
        g_var.logger->debug("dialog status is {}", status);
        VoiceDialog dlg(g_var.m2u_addr, g_var.chatbot_name, NULL, g_var.service);
        dlg.OpenDialog(std::to_string(g_var.call_history_id));
        dict meta;
        meta["record"] = "stop";
        meta["dial_no"] = g_var.sip_user;
        meta["last_status_code"] = std::to_string(ci.last_status);
        result = dlg.SendText("#rec_stop", "N", meta);
        dlg.ResponseLog(result);

        dlg.CloseDialog();
    }
  }

  maum_conf_disconnect(&ci, cd);

  pjsua_conf_remove_port(cd->stt_slot);
  pjmedia_port_destroy(cd->sttport);

  pjsua_conf_remove_port(cd->tts_slot);
  pjmedia_port_destroy(cd->ttsport);

  pjsua_conf_remove_port(cd->play_slot);
  pjmedia_port_destroy(cd->play_port);

  pjsua_call_set_user_data(call_id, NULL);

  delete cd->result_q;

  pj_pool_release(cd->pool);
}

/* Callback called by the library upon receiving incoming call */
void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id,
                      pjsip_rx_data *rdata) {
  timeval start_tv;
  pjsua_call_info ci;

  PJ_UNUSED_ARG(acc_id);
  PJ_UNUSED_ARG(rdata);

  gettimeofday(&start_tv, NULL);
  pjsua_call_get_info(call_id, &ci);

  // DONE: 2020-09-01 added code by shinwc
  // To see the state when call is incoming
  g_logger->debug("CALL_LOG: on_incoming_call() call_id = {}, ci.media_status is {}, state = {}/{}",
                  call_id,
                  ci.media_status, ci.state,
                  std::string(ci.state_text.ptr, ci.state_text.slen));
  g_logger->debug("CALL_LOG: ci.last_status = {}/{}",
                  ci.last_status, 
                  std::string(ci.last_status_text.ptr, ci.last_status_text.slen));

  PJ_LOG(3,(THIS_FILE, "Incoming call from %.*s!!",
            (int)ci.remote_info.slen,
            ci.remote_info.ptr));

  if (ci.remote_info.slen > 2) {
    std::string uri(ci.remote_info.ptr, ci.remote_info.slen);
    auto start_index = uri.find("<sip:") + strlen("<sip:");
    auto end_index = uri.find("@", start_index);
    g_var.tel_no = uri.substr(start_index, end_index - start_index);
    g_logger->debug("INBOUND PHONE NUMBER is [{}]", g_var.tel_no);

    if (g_var.is_inbound) {
      g_var.db.InsertPhoneData();
      g_var.db.GetCallData(g_var.contract_no.c_str(), g_var.campaign_id.c_str());
      // DONE: 2020-09-01 added code by shinwc
      // To insert call history into CALL_HISTORY table when inbound call is incoming
      g_var.sip_call_id.assign(ci.call_id.ptr, ci.call_id.slen); 
      g_var.db.InsertHistory(g_var.contract_no, std::to_string(ci.last_status), start_tv, g_var.sip_call_id);
    }

  } else {
    g_logger->error("CANNOT GET INBOUND PHONE NUMBER from [{}]",
                    std::string(ci.remote_info.ptr, ci.remote_info.slen));
  }

  /* Automatically answer incoming calls with 200/OK */
  g_logger->info("current call count is {}", g_var.call_cnt);
  g_var.call_cnt++;
  if (g_var.is_inbound && !g_var.db.IsAgentReady()) {
    pjsua_call_answer(call_id, 486 /* Busy Here */, NULL, NULL);
    g_logger->info("CALL RESPONSE 480 Unavailable");

  } else if (g_var.call_cnt == 1) {
    for (int i = 0; i < g_var.inbound_delaytime; i++) {
      pjsua_call_answer(call_id, 180, NULL, NULL);
      usleep(1000 * 1000);
    }
    pjsua_call_answer(call_id, 200, NULL, NULL);
    g_logger->info("CALL RESPONSE 200 OK");
  } else {
    pjsua_call_answer(call_id, 486 /* Busy Here */, NULL, NULL);
    g_logger->info("CALL RESPONSE 486 Busy Here");
  }
}

/* Callback called by the library when call's state has changed */
void on_call_state(pjsua_call_id call_id, pjsip_event *e) {
  timeval now, duration;
  pjsua_call_info ci;

  PJ_UNUSED_ARG(e);

  gettimeofday(&now, NULL);
  pjsua_call_get_info(call_id, &ci);
  g_logger->debug("CALL_LOG: on_call_state() call_id = {}, ci.media_status is {}, state = {}/{}",
                  call_id,
                  ci.media_status, ci.state,
                  std::string(ci.state_text.ptr, ci.state_text.slen));
  // DONE: 2020-09-01 added code by shinwc
  // To see the last status when call is disconnected
  g_logger->debug("CALL_LOG: ci.last_status = {}/{}",
                  ci.last_status, 
                  std::string(ci.last_status_text.ptr, ci.last_status_text.slen));

  PJ_LOG(3,(THIS_FILE, "Call %d state=%.*s", call_id,
            (int)ci.state_text.slen,
            ci.state_text.ptr));
            
  // DONE: 2020-09-01 added code by shinwc
  // To insert call history into CALL_HISTORY table when inbound call is incoming
  if (ci.state == PJSIP_INV_STATE_CALLING) {
      g_var.sip_call_id.assign(ci.call_id.ptr, ci.call_id.slen); 
      g_var.db.InsertHistory(g_var.contract_no, std::to_string(ci.last_status), now, g_var.sip_call_id);
  }
  // PJSIP_INV_STATE_CONFIRMED,	    /**< After ACK is sent/received.	    */
  else if (ci.state == PJSIP_INV_STATE_CONFIRMED) {
    // DONE: 2020-08-31 added by shinwc
    // if g_var.record_on_early = false to start call recording on CONFIRMED state
    if (!g_var.record_on_early) {
      g_var.sip_call_id.assign(ci.call_id.ptr, ci.call_id.slen); 
      prepare_media(call_id);
    } else {
      // DONE: 2020-09-10 modified by shinwc
      // if record_on_early = true, then delay STT port creation and STT stream thread 
      // run by grpc_run() by moving related stt codes here
      maum_call_data *cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);
      g_var.db.UpdateStatus(CS_AI_PROCESSING);
      stt_port *stt_port = (struct stt_port *)cd->sttport;
      stt_port = (struct stt_port *)cd->sttport;
      stt_port->stream->run_grpc(call_id);
      stt_port->stream->wait_until_thread_start();
    }
  } else if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {
    // DONE: 2020-09-01 added code by shinwc
    // To update CALL_HISTORY when call is DISCONNECTED 
    duration.tv_sec = ci.total_duration.sec;
    duration.tv_usec = ci.total_duration.msec * 1000; 
    g_var.db.EndHistory(g_var.contract_no, std::to_string(ci.last_status), duration, now);
    // ---------------------------------------------------

    // DONE: 2020-09-10 added by code
    g_var.logger->debug("call_recording_stop()");
    call_recording_stop(call_id);
    g_var.logger->debug("start call_deinit_maum()");
    call_deinit_maum(call_id, ci.conf_slot);
    g_var.logger->debug("end call_deinit_maum()");

    g_var.logger->debug("Sent APP_COMMAND_ENUM::DISCONNECTED");
    APP_MESSAGE_T msg = { APP_COMMAND_ENUM::DISCONNECTED, call_id };
    zmq_send(g_var.evnt_sender, (char *)&msg, sizeof(APP_MESSAGE_T), ZMQ_NOBLOCK);

    sem_post(&SipClient::Done);
  }
}

/* Callback called by the library when call's media state has changed */
void on_call_media_state(pjsua_call_id call_id) {
  pj_status_t status;
  pjsua_call_info ci;
  PJ_UNUSED_ARG(status);

  pjsua_call_get_info(call_id, &ci);
  g_logger->debug("CALL_LOG: on_call_media_state() call_id = {}, ci.media_status is {}, state = {}",
                  call_id,
                  ci.media_status,
                  std::string(ci.state_text.ptr, ci.state_text.slen));

  PJ_LOG(3,(THIS_FILE, "Call %d state=%.*s", call_id,
                  (int)ci.state_text.slen,
                  ci.state_text.ptr));

  if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
    // When media is active, connect call to sound device.
    // pjsua_conf_connect(ci.conf_slot, 0);
    // pjsua_conf_connect(0, ci.conf_slot);
    maum_call_data *cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);
    if (cd != NULL) {
      maum_conf_reconnect(&ci, cd);
      call_record_conf_disconnect(call_id, &ci, g_var.is_stereo);
      call_record_conf_connect(call_id, &ci, g_var.is_stereo);
    }

    // DONE: 2020-09-10 modified by shinwc
    // if g_var.record_on_early = true to start call recording on EARLY state 
    if (ci.state == PJSIP_INV_STATE_EARLY) {
      if (g_var.record_on_early) {
        prepare_early_media(call_id);
      } 
    } else if (ci.state == PJSIP_INV_STATE_CONNECTING) {
      if (g_var.record_on_early) {
        prepare_early_media(call_id);
      } 
    } else if (ci.state == PJSIP_INV_STATE_CONFIRMED) {
    }
  }
}

void maum_conf_connect(pjsua_call_info *ci, maum_call_data *cd) {
  pjsua_conf_connect(ci->conf_slot, cd->stt_slot);
  pjsua_conf_connect(cd->tts_slot, ci->conf_slot);
  pjsua_conf_connect(ci->conf_slot, cd->play_slot);
  pjsua_conf_connect(3, cd->play_slot);
}

void maum_conf_disconnect(pjsua_call_info *ci, maum_call_data *cd) {
  pjsua_conf_disconnect(ci->conf_slot, cd->stt_slot);
  g_var.logger->debug("conf_slot -> stt_slot disconnected");
  pjsua_conf_disconnect(cd->tts_slot, ci->conf_slot);
  g_var.logger->debug("tts_slot -> conf_slot disconnected");
  pjsua_conf_disconnect(ci->conf_slot, cd->play_slot);
  g_var.logger->debug("conf_slot -> play_slot disconnected");
  pjsua_conf_disconnect(3, cd->play_slot);
  g_var.logger->debug("3 slot -> stt_slot disconnected");
}

void maum_conf_reconnect(pjsua_call_info *ci, maum_call_data *cd) {
  maum_conf_disconnect(ci, cd);
  maum_conf_connect(ci, cd);
}

void prepare_early_media(pjsua_call_id call_id) {
  pjsua_call_info ci;
  struct maum_call_data *cd;
  char buf[1024];

  pjsua_call_get_info(call_id, &ci);
  cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);

  if (!cd) {
    // DONE: 2020-08-31 modified by shinwc
    // To delay STT port creation and STT stream thread by grpc_run()
    // moved stt codes into on_call_media_state()
    // when ci.state = PJSIP_INV_STATE_CONFIRMED
    // STT PORT 생성을 하지 않음
    cd = call_init_maum(call_id);
    g_var.logger->debug("call_init_maum");
    g_var.db.UpdateStatus(CS_CONNECTING);

//    g_var.db.UpdateStatus(CS_AI_PROCESSING);
    // need reconnect?
    //pjsua_conf_disconnect(ci.conf_slot, cd->stt_slot);
    //pjsua_conf_disconnect(cd->tts_slot, ci.conf_slot);
    pjsua_conf_add_port(cd->pool, cd->sttport, &cd->stt_slot);
    pjsua_conf_add_port(cd->pool, cd->ttsport, &cd->tts_slot);
    pjsua_conf_add_port(cd->pool, cd->play_port, &cd->play_slot);

    maum_conf_connect(&ci, cd);
    // for test
    // push_sound_frame("happy_call_0.wav", cd->result_q);
    // https://www.soundjay.com/typewriter-sounds.html
    // push_sound_frame("phone-calling.16k.pcm", cd->result_q);
    //push_sound_frame("/home/ducbin/maum/samples/8k.pcm", cd->result_q);
    
    sprintf(buf, "%s/record_%d_%s_%s.wav",
            g_var.record_dir.c_str(),
            g_var.call_history_id,
            g_var.contract_no.c_str(),
            g_var.campaign_id.c_str());
    const pj_str_t fname = pj_str(buf);
    call_recording_start(call_id, &fname, g_var.is_stereo);
    // call_recording_conf_connect(call_id, cd->play_slot);
  }
}

void prepare_media(pjsua_call_id call_id) {
  pjsua_call_info ci;
  struct maum_call_data *cd;
  char buf[1024];

  pjsua_call_get_info(call_id, &ci);
  cd = (struct maum_call_data*) pjsua_call_get_user_data(call_id);

  if (!cd) {
    // STT PORT 생성
    cd = call_init_maum(call_id);
    g_var.logger->debug("call_init_maum");
    g_var.db.UpdateStatus(CS_CONNECTING);
    //DONE: 2020-10-07 by shinwc
    stt_port *stt_port = (struct stt_port *)cd->sttport;

    g_var.db.UpdateStatus(CS_AI_PROCESSING);
    // need reconnect?
    //pjsua_conf_disconnect(ci.conf_slot, cd->stt_slot);
    //pjsua_conf_disconnect(cd->tts_slot, ci.conf_slot);
    pjsua_conf_add_port(cd->pool, cd->sttport, &cd->stt_slot);
    pjsua_conf_add_port(cd->pool, cd->ttsport, &cd->tts_slot);
    pjsua_conf_add_port(cd->pool, cd->play_port, &cd->play_slot);

    maum_conf_connect(&ci, cd);
    // for test
    // push_sound_frame("happy_call_0.wav", cd->result_q);
    // https://www.soundjay.com/typewriter-sounds.html
    // push_sound_frame("phone-calling.16k.pcm", cd->result_q);
    //push_sound_frame("/home/ducbin/maum/samples/8k.pcm", cd->result_q);
    
    //stt_port = (struct stt_port *)cd->sttport;
    stt_port->stream->run_grpc(call_id);
    stt_port->stream->wait_until_thread_start();

    sprintf(buf, "%s/record_%d_%s_%s.wav",
            g_var.record_dir.c_str(),
            g_var.call_history_id,
            g_var.contract_no.c_str(),
            g_var.campaign_id.c_str());
    const pj_str_t fname = pj_str(buf);
    call_recording_start(call_id, &fname, g_var.is_stereo);
    // call_recording_conf_connect(call_id, cd->play_slot);
  }
}

/* Display error and exit application */
void error_exit(const char *title, pj_status_t status) {
  pjsua_perror(THIS_FILE, title, status);
  pjsua_destroy();
  exit(1);
}

// DONE: 2020-08-25 added by shinwc
// To process SIGTERM and SIGINT
// FYI, SIGKILL cannot be caught by kernel design
void sig_handler(int signo) {
  g_var.logger->info("[sig_handler] {}", signo);
  if(signo == SIGTERM || signo == SIGINT) {
    g_var.logger->info("SYSTEM: caught signal... signo = {}", signo);
    SipClient::Hangup(g_call_id);
    g_var.logger->info("pjsua_destroy called()");
    pjsua_destroy();

    exit(1);
  }
}

sem_t SipClient::Done;

SipClient::SipClient(char* number) {
  sip_domain_ = g_var.sip_domain;
  sip_user_ = g_var.sip_user;
  sip_passwd_ = g_var.sip_passwd;
#ifdef USE_SIP_BRACKET
  id_ += "<sip:" + sip_user_ + "@" + sip_domain_ + ">";
#else
  id_ += "sip:" + sip_user_ + "@" + sip_domain_;
#endif
  reg_uri_ += "sip:" + sip_domain_;
  outbound_num_ = g_var.outbound_prefix + number;

  char endpoint[128];
  int  linger = 0;

  evt_receiver_ = zmq_socket(g_var.app_context, ZMQ_PULL);
  sprintf(endpoint, "ipc://%s/run/%s.evt.ipc", g_var.maum_root.c_str(), sip_user_.c_str());
  if (zmq_bind(evt_receiver_, endpoint) != 0) {
    g_var.logger->error("ZeroMQ event receiver bind() error ({})", endpoint);
  }

  responder_ = zmq_socket(g_var.app_context, ZMQ_REP);
  zmq_setsockopt(responder_, ZMQ_LINGER, &linger, sizeof(linger));

  sprintf(endpoint, "ipc://%s/run/%s.cmd.ipc", g_var.maum_root.c_str(), sip_user_.c_str());
  if (zmq_bind(responder_, endpoint) != 0) {
    g_var.logger->error("ZeroMQ cmd responder bind() error ({})", endpoint);
  }

  sem_init(&Done, 0, 0);
}

SipClient::~SipClient() {
}

void SipClient::Cleanup() {
  zmq_close(evt_receiver_);
  zmq_close(responder_);
  sem_destroy(&Done);
}

/**
 * pjsua 라이브러리 초기화
 *
 * @param void
 * @return void
 */
void SipClient::Initialize() {
  pj_status_t status;
  pjsua_config cfg;
  pjsua_logging_config log_cfg;

  /* Create pjsua first! */
  status = pjsua_create();
  if (status != PJ_SUCCESS) error_exit("Error in pjsua_create()", status);

  pjsua_config_default(&cfg);

  cfg.user_agent = pj_str(
      g_var.sip_user_agent.empty() ? NULL : (char*)g_var.sip_user_agent.c_str());
  
  if (g_var.use_stun) {
    cfg.stun_srv_cnt = 1;
    cfg.stun_srv[0] = pj_str((char*)g_var.stun_server.c_str());
    // cfg.stun_srv[0] = pj_str("stun.pjsip.org");
  }
  cfg.cb.on_incoming_call = &on_incoming_call;
  cfg.cb.on_call_media_state = &on_call_media_state;
  cfg.cb.on_call_state = &on_call_state;
  cfg.cb.on_reg_state = &on_reg_state;
  cfg.cb.on_dtmf_digit = &on_dtmf_digit;
  // TODO: TEST
  cfg.cb.on_stream_destroyed = &on_stream_destroyed;

  pjsua_logging_config_default(&log_cfg);
  log_cfg.console_level = 4;

  pjsua_media_config media_config;
  pjsua_media_config_default(&media_config);
  // echo cancellation test
  media_config.ec_options = PJMEDIA_ECHO_WEBRTC;
  media_config.clock_rate = g_var.clock_rate;
  status = pjsua_init(&cfg, &log_cfg, &media_config);

  // 특정 codec 설정을 원하면 아래 라인 주석 삭제
  // codec_set_priority("pcma");
  if (status != PJ_SUCCESS) error_exit("Error in pjsua_init()", status);

  g_var.db.UpdatePhoneBootTime();
}

void SipClient::codec_set_priority(char *codec) {
  /* Optionally disable some codec */
  pjmedia_codec_mgr* mgr = pjmedia_endpt_get_codec_mgr(pjsua_var.med_endpt);
  pj_str_t codec_id = {NULL, 0};

  for (int i = mgr->codec_cnt - 1; i >= 0; i--) {
    codec_id = pj_str(mgr->codec_desc[i].id);
    pjsua_codec_set_priority(&codec_id, PJMEDIA_CODEC_PRIO_DISABLED);
  }

  const pj_str_t pcma = pj_str(codec);
  pjsua_codec_set_priority(&pcma, PJMEDIA_CODEC_PRIO_HIGHEST);
}

void SipClient::StartAgent() {
  struct maum_call_data *cd;
  cd = (struct maum_call_data*) pjsua_call_get_user_data(g_call_id);

  if (!cd) {
    // g_var.logger->info("CALL_LOG: there is no pjsua_call_id ({})", g_call_id);
  } else {
    stt_port *stt_port = (struct stt_port *)cd->sttport;
    if (stt_port) {
      stt_port->stream->start_agent();
    }
  }
}

void SipClient::StopAgent() {
  struct maum_call_data *cd;
  cd = (struct maum_call_data*) pjsua_call_get_user_data(g_call_id);

  if (!cd) {
    // g_var.logger->info("CALL_LOG: there is no pjsua_call_id ({})", g_call_id);
  } else {
    stt_port *stt_port = (struct stt_port *)cd->sttport;
    if (stt_port) {
      stt_port->stream->stop_agent();
    }
  }
}

void SipClient::AddTransport() {
  /* Add UDP transport. */
  pjsua_transport_config cfg;

  pjsua_transport_config_default(&cfg);
  cfg.port = g_var.sip_port;
  auto status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
  if (status != PJ_SUCCESS) error_exit("Error creating transport", status);
}

void SipClient::Hangup(int call_id) {
  // 시나리오가 끝난 경우 고객이 전화를 끊지 않더라도 N초간 대기 후 종료를 실행하도록 처리
  PJSUA_LOCK();
  pj_status_t status;
  if (pjsua_call_is_active(call_id)) {
    g_var.logger->info("CALL_LOG: trying pjsua_call_hangup()... call_id = {}", call_id);
    status = pjsua_call_hangup(call_id, 0, NULL, NULL);
    if (status != PJ_SUCCESS) {
      PJSUA_UNLOCK();
      error_exit("Error hangup call", status);
    } else {
      g_var.logger->info("CALL_LOG: success to pjsua_call_hangup() call_id = {}", call_id);
    }
  }
  PJSUA_UNLOCK();
}

int SipClient::ProcessEvent(char *msg, int length) {
  int rc = 0;
  APP_MESSAGE_T *cmd = (APP_MESSAGE_T *)msg;
  g_logger->info("APP_MESSAGE, call_id: {}, agent_id: {}, command: {}",
                 cmd->call_id, cmd->agent_id, app_cmd_text[cmd->command]);

  switch (cmd->command) {
    case APP_COMMAND_ENUM::HANGUP: {
      Hangup(cmd->call_id);
      break;
    }
    case APP_COMMAND_ENUM::DISCONNECTED: {
      g_var.logger->info("Recv APP_COMMAND_ENUM DISCONNECTED");
      rc = 1;  // mark call done
      break;
    }
    case APP_COMMAND_ENUM::REFER: {
      char buf[128];
      sprintf(buf, "sip:%s@%s", cmd->agent_id, g_var.sip_domain.c_str());
      const pj_str_t tel_uri = pj_str(buf);
      pjsua_call_xfer(cmd->call_id, &tel_uri, NULL);
      g_var.logger->info("Recv APP_COMMAND_ENUM REFER");
      break;
    }
    case APP_COMMAND_ENUM::CALL_TRANSFER: {
      // 상담사 전환 요청 - anyone
      // g_var.agent_id = cmd->agent_id;
      StartAgent();
      g_var.logger->info("Recv APP_COMMAND_ENUM CALL_TRANSFER");
      break;
    }
    case APP_COMMAND_ENUM::TRANSFER_TIMEOUT: {
      StopAgent();
      g_var.logger->info("Recv APP_COMMAND_ENUM TRANSFER_TIMEOUT");
      break;
    }
    default:
      break;
  }

  return rc;
}

void SipClient::ProcessRequest(char *msg, int length) {
  SIP_PKTCNT_T resp;
  APP_MESSAGE_T *cmd = (APP_MESSAGE_T *)msg;
  g_logger->info("APP_MESSAGE, call_id: {}, agent_id: {}, command: {}",
                 cmd->call_id, cmd->agent_id, app_cmd_text[cmd->command]);

  switch (cmd->command) {

    case APP_COMMAND_ENUM::CHECK_PKT: {
      pjsua_stream_stat stat;
      pjsua_call_get_stream_stat(cmd->call_id, 0, &stat);
      resp.count = stat.rtcp.rx.pkt;
      zmq_send(responder_, (char *)&resp, sizeof(SIP_PKTCNT_T), ZMQ_NOBLOCK);
      break;
    }

    case APP_COMMAND_ENUM::PLAY_AUDIO: {
      // 실시간 재생 요청
      pjmedia_play_register(cmd->agent_id);
      zmq_send(responder_, msg, length, ZMQ_NOBLOCK);
      break;
    }

    case APP_COMMAND_ENUM::CALL_TRANSFER: {
      // 상담사 전환 요청
      g_var.agent_id = cmd->agent_id;
      StartAgent();
      // TODO : 응답코드 정의
      zmq_send(responder_, msg, length, ZMQ_NOBLOCK);
      break;
    }

    case APP_COMMAND_ENUM::HANGUP: {
      // 상담사 통화 종료 요청
      Hangup(cmd->call_id);
      // TODO : 응답코드 정의
      zmq_send(responder_, msg, length, ZMQ_NOBLOCK);
      break;
    }

    default: {
      // TODO : invalid message, just echo now
      zmq_send(responder_, msg, length, ZMQ_NOBLOCK);
      break;
    }
  }
}

void SipClient::Run() {
  // DONE: 2020-08-25 added by shinwc
  // register sig_handler for SIGTERM and SIGINT processing
  struct sigaction act;
  act.sa_handler = sig_handler;
  sigaction(SIGTERM, &act, 0);
  sigaction(SIGINT, &act, 0);

  /* Initialization is done, now start pjsua */
  pj_status_t status;

  // If you want to use no device (that is null sound device in
  // pjsua's term), then call pjsua_set_null_snd_dev() before calling
  // pjsua_start().
  status = pjsua_set_null_snd_dev();
  if (status != PJ_SUCCESS) error_exit("Error set_null_snd_dev pjsua", status);

  status = pjsua_start();
  if (status != PJ_SUCCESS) error_exit("Error starting pjsua", status);

#ifdef USE_SIP_BRACKET
  std::string id1 = "<sip:" + g_var.sip_user + "@" + g_var.sip_domain + ">";
  std::string id2 = "<sip:" + g_var.sip_user + "@" + g_var.sip_domain2 + ">";
#else
  std::string id1 = "sip:" + sip_user_ + "@" + g_var.sip_domain;
  std::string id2 = "sip:" + sip_user_ + "@" + g_var.sip_domain2;
#endif

  /* Register to SIP server by creating SIP account. */
  {
    pjsua_acc_config cfg;

    pjsua_acc_config_default(&cfg);
    cfg.id = pj_str((char*)id_.c_str());
    cfg.reg_uri = pj_str((char*)reg_uri_.c_str());
    cfg.cred_count = 1;
    cfg.cred_info[0].realm = pj_str((char*)"*");
    cfg.cred_info[0].scheme = pj_str((char*)"digest");
    cfg.cred_info[0].username = pj_str((char*)sip_user_.c_str());
    cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    cfg.cred_info[0].data = pj_str((char*)sip_passwd_.c_str());

    // RTP start port
    cfg.rtp_cfg.port = g_var.rtp_port;

    status = pjsua_acc_add(&cfg, PJ_TRUE, &primary_acc_id);
    if (status != PJ_SUCCESS) error_exit("Error adding account", status);
  }

  if (!g_var.sip_domain2.empty()) {
    pjsua_acc_config cfg;

    pjsua_acc_config_default(&cfg);
    cfg.id = pj_str((char*)id2.c_str());
    std::string reg_uri2 = "sip:" + g_var.sip_domain2;
    cfg.reg_uri = pj_str((char*)reg_uri2.c_str());
    cfg.cred_count = 1;
    cfg.cred_info[0].realm = pj_str((char*)"*");
    cfg.cred_info[0].scheme = pj_str((char*)"digest");
    cfg.cred_info[0].username = pj_str((char*)sip_user_.c_str());
    cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    cfg.cred_info[0].data = pj_str((char*)sip_passwd_.c_str());

    // RTP start port
    cfg.rtp_cfg.port = g_var.rtp_port;

    status = pjsua_acc_add(&cfg, PJ_TRUE, &secondary_acc_id);
    if (status != PJ_SUCCESS) error_exit("Error adding account", status);
  }

  /* If URL is specified, make call to the URL. */
  //if (argc > 1) {
  pjsua_call_id call_id;

  if (!g_var.is_inbound) {
    usleep(1 * 1000 * 1000);
    char buf[128];
#ifdef USE_SIP_BRACKET
    sprintf(buf, "<sip:%s@%s>", outbound_num_.c_str(), outbound_domain.c_str());
#else
    sprintf(buf, "sip:%s@%s", outbound_num_.c_str(), outbound_domain.c_str());
#endif
    pj_str_t uri = pj_str(buf);
    status = pjsua_call_make_call(outbound_acc_id, &uri, 0, NULL, NULL, &call_id);
    g_var.call_cnt++;
    if (status != PJ_SUCCESS) {
      error_exit("Error making call", status);
    } else {
      g_call_id = call_id;
    }
  }
  
  while (true) {
    int rc = 0;
    char msg[256];
    zmq_pollitem_t items [] = {
        { evt_receiver_, 0, ZMQ_POLLIN, 0 },
        { responder_,    0, ZMQ_POLLIN, 0 }
    };
    zmq_poll(items, 2, -1 /* no timeout */);
    if (items[0].revents & ZMQ_POLLIN) {
      int size = zmq_recv(evt_receiver_, msg, 255, 0);
      if (size != -1) {
        rc = ProcessEvent(msg, size);
      }
    }
    if (items[1].revents & ZMQ_POLLIN) {
      int size = zmq_recv(responder_, msg, 255, 0);
      if (size != -1) {
        ProcessRequest(msg, size);
      }
    }
    if (rc == 1 /* hangup done */) {
      g_var.call_cnt--;
      if (g_var.call_cnt == 0) {
        if (g_var.is_once || !g_var.is_inbound)	break;
      }
    }
  }

  g_var.logger->trace("sip main loop done");

  // call_state가 DISCONNECTED로 바뀔 때
  sem_wait(&Done);

  /* Destroy pjsua */
  pjsua_destroy();

  g_var.logger->info("pjsua_destroy called()");
}

