#ifndef SIP_CLIENT_H
#define SIP_CLIENT_H

#include <string>
#include <semaphore.h>

class SipClient {
 public:
  SipClient(char* number);
  ~SipClient();

  void Initialize();
  void AddTransport();
  void Run();
  void Cleanup();

  int  ProcessEvent(char *msg, int length);
  void ProcessRequest(char *msg, int length);

  static void StartAgent();
  static void StopAgent();
  static void Hangup(int call_id);

  static sem_t Done;

 private:
  // 기본적으로 지원하는 codec list를 모두 삭제하고 입력된 codec만 설정한다.
  void codec_set_priority(char *codec);

  std::string sip_domain_;
  std::string sip_user_;
  std::string sip_passwd_;
  std::string id_;
  std::string reg_uri_;
  std::string outbound_num_;

  void *evt_receiver_;
  void *responder_;
};

#endif /* SIP_CLIENT_H */
