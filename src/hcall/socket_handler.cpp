#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <cstddef>
#include <sys/select.h>
#include <sys/fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include "socket_handler.h"

SocketHandler::SocketHandler()
{
}

SocketHandler::~SocketHandler()
{
}

void SocketHandler::handle_accept(int sockfd)
{

}

int SocketHandler::handle_recv(int sockfd)
{
    return 0;
}

void SocketHandler::OnTimeout(int id)
{
}

void SocketHandler::handle_user_event(void* info)
{
}

int SocketHandler::GetEventType(int sockfd)
{
    return SocketHandler::UNKNOWN;
}

bool SocketHandler::Connect(const char* addr, unsigned short port, int nsec)
{
    int client_fd;
    sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(serveraddr));

    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    set_handle(client_fd);

    if (client_fd < 0) {
        printf("Can't create socket\n");
        return false;
    }

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(port);
    serveraddr.sin_addr.s_addr = inet_addr(addr);

    if (connect_nonb(client_fd, &serveraddr, sizeof(serveraddr), nsec) < 0) {
        return false;
    }
    return true;
}

int SocketHandler::
connect_nonb(int sockfd, const sockaddr_in *saptr, socklen_t salen, int nsec)
{
    int                flags, n, error;
    socklen_t        len;
    fd_set            rset, wset;
    struct timeval    tval;

    flags = fcntl(sockfd, F_GETFL, 0);
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

    error = 0;
    if ( (n = connect(sockfd, (struct sockaddr *) saptr, salen)) < 0)
        if (errno != EINPROGRESS)
            return(-1);

    /* Do whatever we want while the connect is taking place. */

    if (n == 0)
        goto done;    /* connect completed immediately */

    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    wset = rset;
    tval.tv_sec = nsec;
    tval.tv_usec = 0;

    if ( (n = select(sockfd+1, &rset, &wset, NULL,
                     nsec ? &tval : NULL)) == 0) {
        close(sockfd);        /* timeout */
        errno = ETIMEDOUT;
        return(-1);
    }

    if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset)) {
        len = sizeof(error);
        if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
            return(-1);            /* Solaris pending error */
    } else
    {
        //err_quit("select error: sockfd not set");    
    }

done:
    fcntl(sockfd, F_SETFL, flags);    /* restore file status flags */

    if (error) {
        close(sockfd);        /* just in case */
        errno = error;
        return(-1);
    }
    return(0);
}

void SocketHandler::Disconnect()
{
    int client_fd = get_handle();
    shutdown(client_fd, SHUT_RDWR);
    close(client_fd);
}

int SocketHandler::Recv(char* buf, size_t len, int msec)
{
    if (msec == 0) {
        return recv(get_handle(), buf, len, 0);
    }
    int sockfd;
    timeval* ptv;
    timeval  tv;

    sockfd = get_handle();
    tv.tv_sec  = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;

    ptv = msec > 0 ? &tv : NULL;

    int ret;
    fd_set handle_set;

    FD_ZERO(&handle_set);
    FD_SET(sockfd, &handle_set);

    ret = select(sockfd + 1 , &handle_set, NULL, NULL, ptv);

    if (ret == 0) {
      // timeout
      errno = ETIME;
      return -1;
    }
    else if (FD_ISSET(sockfd, &handle_set)) {
      return recv(sockfd, buf, len, 0);
    }
    else {
      return ret;
    }
}

int SocketHandler::RecvAll(char* buf, size_t len, int msec)
{
    int sockfd;
    size_t nleft;
    ssize_t nread;
    char* ptr;

    sockfd = get_handle();
    ptr = buf;
    nleft = len;

    timeval* ptv;
    timeval  tv;
    tv.tv_sec  = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;

    ptv = msec > 0 ? &tv : NULL;

    while (nleft > 0) {
        int ret;
        fd_set handle_set;

        FD_ZERO(&handle_set);
        FD_SET(sockfd, &handle_set);

        ret = select(sockfd + 1 , &handle_set, NULL, NULL, ptv);

        if (ret == 0) {
            // timeout
            errno = ETIME;
            break;
        } else if (FD_ISSET(sockfd, &handle_set)) {
            nread = recv(sockfd, ptr, nleft, 0);
            if (nread < 0)
            {
                if (errno == EINTR)
                    nread = 0;
                else
                    return -1;
            }
            else if (nread == 0)
                break;    /* EOF */

            nleft -= nread;
            ptr += nread;
        } else {

        }
    }

    return len-nleft;
}

int SocketHandler::SendAll(char* buf, size_t len, int msec)
{
    int sockfd;
    size_t nleft;
    ssize_t nwritten;
    char* ptr;

    sockfd = get_handle();
    ptr = buf;
    nleft = len;

    timeval* ptv;
    timeval  tv;
    tv.tv_sec  = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;

    ptv = msec > 0 ? &tv : NULL;

    while (nleft > 0) {
        int ret;
        fd_set handle_set;

        FD_ZERO(&handle_set);
        FD_SET(sockfd, &handle_set);

        ret = select(sockfd + 1 , NULL, &handle_set, NULL, ptv);

        if (ret == 0) {
            // timeout
            errno = ETIME;
            break;
        } else if (FD_ISSET(sockfd, &handle_set)) {
            // prevent SIGPIPE
            nwritten = send(sockfd, ptr, nleft, MSG_NOSIGNAL | MSG_DONTWAIT);
            if (nwritten <= 0) {
                if (nwritten < 0 && errno == EINTR) {
                    nwritten = 0;
                }
                else
                    return -1;
            }

            nleft -= nwritten;
            ptr += nwritten;
        } else {
        }
    }

    return len-nleft;
}

int SocketHandler::LingerOption(int sockfd, int Enabled, int LingerTime)
{
    int ret;
    struct linger ling;
    ling.l_onoff = Enabled;
    ling.l_linger = LingerTime;
    ret = setsockopt(sockfd, SOL_SOCKET, SO_LINGER, &ling, sizeof(ling));

    return ret;
}

int SocketHandler::SetTcpNoDelay(int sockfd, int On)
{
    int opt_val = On;
    return setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &opt_val, sizeof(opt_val));
}
