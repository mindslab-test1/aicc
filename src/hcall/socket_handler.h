#ifndef SOCKET_HANDLER_H
#define SOCKET_HANDLER_H

#include <netinet/in.h>
#include "event_handler.h"

class SocketHandler : public EventHandler
{
public:
    SocketHandler();
    virtual ~SocketHandler();

    virtual void handle_accept(int sockfd);
    virtual int  handle_recv(int sockfd);
    virtual void OnTimeout(int id);
    virtual void handle_user_event(void* info);

    virtual int  GetEventType(int sockfd);

    // Socket IO
    bool Connect(const char* addr, unsigned short port, int nsec = 1);
    int  connect_nonb(int sockfd, const sockaddr_in *saptr, socklen_t salen, int nsec);
    void Disconnect();

    int  Recv(char* buf, size_t len, int msec = 0);
    // recv n bytes exactly until msec
    // if msec is 0, then block indefinitely
    int  RecvAll(char* buf, size_t len, int msec = 0);

    int  Send(int sockfd, char *buf, int len);
    int  SendAll(char *buf, size_t len, int msec = 0);

    bool ReadyForSend(int sockfd, int msec);
    int  SetTcpNoDelay(int sockfd, int On);
    int  LingerOption(int sockfd, int Enabled, int LingerTime);

    void* State;
};

#endif /* SOCKET_HANDLER_H */
