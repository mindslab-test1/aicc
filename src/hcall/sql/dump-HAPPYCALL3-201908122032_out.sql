-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 10.122.64.152    Database: HAPPYCALL3
-- ------------------------------------------------------
-- Server version	5.6.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AUTHORITY`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE AUTHORITY';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE AUTHORITY (
  id number(10) NOT NULL,
  username varchar2(15) DEFAULT NULL,
  authority_name varchar2(20) DEFAULT NULL,
  PRIMARY KEY (id)
)  ;

-- Generate ID using sequence and trigger
CREATE SEQUENCE AUTHORITY_seq START WITH 13 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER AUTHORITY_seq_tr
 BEFORE INSERT ON AUTHORITY FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT AUTHORITY_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_EXECUTION';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_EXECUTION (
  JOB_EXECUTION_ID number(19) NOT NULL,
  VERSION number(19) DEFAULT NULL,
  JOB_INSTANCE_ID number(19) NOT NULL,
  CREATE_TIME timestamp(0) NOT NULL,
  START_TIME timestamp(0) DEFAULT NULL,
  END_TIME timestamp(0) DEFAULT NULL,
  STATUS varchar2(10) DEFAULT NULL,
  EXIT_CODE varchar2(2500) DEFAULT NULL,
  EXIT_MESSAGE varchar2(2500) DEFAULT NULL,
  LAST_UPDATED timestamp(0) DEFAULT NULL,
  JOB_CONFIGURATION_LOCATION varchar2(2500) DEFAULT NULL,
  PRIMARY KEY (JOB_EXECUTION_ID)
 ,
  CONSTRAINT JOB_INST_EXEC_FK FOREIGN KEY (JOB_INSTANCE_ID) REFERENCES BATCH_JOB_INSTANCE (JOB_INSTANCE_ID)
) ;

CREATE INDEX JOB_INST_EXEC_FK ON BATCH_JOB_EXECUTION (JOB_INSTANCE_ID);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_CONTEXT`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_EXECUTION_CONTEXT';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT (
  JOB_EXECUTION_ID number(19) NOT NULL,
  SHORT_CONTEXT varchar2(2500) NOT NULL,
  SERIALIZED_CONTEXT clob,
  PRIMARY KEY (JOB_EXECUTION_ID),
  CONSTRAINT JOB_EXEC_CTX_FK FOREIGN KEY (JOB_EXECUTION_ID) REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_PARAMS`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_EXECUTION_PARAMS';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_EXECUTION_PARAMS (
  JOB_EXECUTION_ID number(19) NOT NULL,
  TYPE_CD varchar2(6) NOT NULL,
  KEY_NAME varchar2(100) NOT NULL,
  STRING_VAL varchar2(250) DEFAULT NULL,
  DATE_VAL timestamp(0) DEFAULT NULL,
  LONG_VAL number(19) DEFAULT NULL,
  DOUBLE_VAL binary_double DEFAULT NULL,
  IDENTIFYING char(1) NOT NULL
 ,
  CONSTRAINT JOB_EXEC_PARAMS_FK FOREIGN KEY (JOB_EXECUTION_ID) REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID)
) ;

CREATE INDEX JOB_EXEC_PARAMS_FK ON BATCH_JOB_EXECUTION_PARAMS (JOB_EXECUTION_ID);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_SEQ`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_EXECUTION_SEQ';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_EXECUTION_SEQ (
  ID number(19) NOT NULL,
  UNIQUE_KEY char(1) NOT NULL,
  CONSTRAINT UNIQUE_KEY_UN UNIQUE  (UNIQUE_KEY)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_INSTANCE`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_INSTANCE';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_INSTANCE (
  JOB_INSTANCE_ID number(19) NOT NULL,
  VERSION number(19) DEFAULT NULL,
  JOB_NAME varchar2(100) NOT NULL,
  JOB_KEY varchar2(32) NOT NULL,
  PRIMARY KEY (JOB_INSTANCE_ID),
  CONSTRAINT JOB_INST_UN UNIQUE  (JOB_NAME,JOB_KEY)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_SEQ`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_JOB_SEQ';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_JOB_SEQ (
  ID number(19) NOT NULL,
  UNIQUE_KEY char(1) NOT NULL,
  CONSTRAINT UNIQUE_KEY_UN UNIQUE  (UNIQUE_KEY)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_STEP_EXECUTION';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_STEP_EXECUTION (
  STEP_EXECUTION_ID number(19) NOT NULL,
  VERSION number(19) NOT NULL,
  STEP_NAME varchar2(100) NOT NULL,
  JOB_EXECUTION_ID number(19) NOT NULL,
  START_TIME timestamp(0) NOT NULL,
  END_TIME timestamp(0) DEFAULT NULL,
  STATUS varchar2(10) DEFAULT NULL,
  COMMIT_COUNT number(19) DEFAULT NULL,
  READ_COUNT number(19) DEFAULT NULL,
  FILTER_COUNT number(19) DEFAULT NULL,
  WRITE_COUNT number(19) DEFAULT NULL,
  READ_SKIP_COUNT number(19) DEFAULT NULL,
  WRITE_SKIP_COUNT number(19) DEFAULT NULL,
  PROCESS_SKIP_COUNT number(19) DEFAULT NULL,
  ROLLBACK_COUNT number(19) DEFAULT NULL,
  EXIT_CODE varchar2(2500) DEFAULT NULL,
  EXIT_MESSAGE varchar2(2500) DEFAULT NULL,
  LAST_UPDATED timestamp(0) DEFAULT NULL,
  PRIMARY KEY (STEP_EXECUTION_ID)
 ,
  CONSTRAINT JOB_EXEC_STEP_FK FOREIGN KEY (JOB_EXECUTION_ID) REFERENCES BATCH_JOB_EXECUTION (JOB_EXECUTION_ID)
) ;

CREATE INDEX JOB_EXEC_STEP_FK ON BATCH_STEP_EXECUTION (JOB_EXECUTION_ID);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_CONTEXT`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_STEP_EXECUTION_CONTEXT';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT (
  STEP_EXECUTION_ID number(19) NOT NULL,
  SHORT_CONTEXT varchar2(2500) NOT NULL,
  SERIALIZED_CONTEXT clob,
  PRIMARY KEY (STEP_EXECUTION_ID),
  CONSTRAINT STEP_EXEC_CTX_FK FOREIGN KEY (STEP_EXECUTION_ID) REFERENCES BATCH_STEP_EXECUTION (STEP_EXECUTION_ID)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_SEQ`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE BATCH_STEP_EXECUTION_SEQ';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE BATCH_STEP_EXECUTION_SEQ (
  ID number(19) NOT NULL,
  UNIQUE_KEY char(1) NOT NULL,
  CONSTRAINT UNIQUE_KEY_UN UNIQUE  (UNIQUE_KEY)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CALL_HISTORY`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CALL_HISTORY';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CALL_HISTORY (
  call_id number(10) check (call_id > 0) NOT NULL ,
  call_date timestamp(0) DEFAULT NULL ,
  call_type_code varchar2(6) DEFAULT NULL ,
  contract_no number(10) DEFAULT NULL check (contract_no > 0) zerofill ,
  start_time timestamp(0) DEFAULT NULL ,
  end_time timestamp(0) DEFAULT NULL ,
  duration binary_double DEFAULT NULL ,
  call_status char(6) DEFAULT 'CS0001' ,
  camp_status varchar2(45) DEFAULT NULL,
  create_dtm timestamp(0) DEFAULT NULL ,
  call_memo varchar2(255) DEFAULT NULL ,
  monitor_cont varchar2(255) DEFAULT NULL ,
  callback_dt timestamp(0) DEFAULT NULL ,
  mnt_status varchar2(45) DEFAULT NULL,
  mnt_status_name varchar2(100) DEFAULT NULL,
  PRIMARY KEY (call_id),
  CONSTRAINT call_id_UNIQUE UNIQUE  (call_id)
)   ;

COMMENT ON TABLE CALL_HISTORY IS '콜 정보'

-- Generate ID using sequence and trigger
CREATE SEQUENCE CALL_HISTORY_seq START WITH 4020 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER CALL_HISTORY_seq_tr
 BEFORE INSERT ON CALL_HISTORY FOR EACH ROW
 WHEN (NEW.call_id IS NULL)
BEGIN
 SELECT CALL_HISTORY_seq.NEXTVAL INTO :NEW.call_id FROM DUAL;
END;
/

CREATE INDEX CNTRCT_IDX ON CALL_HISTORY (contract_no);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CAMPAIGN_MNG`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CAMPAIGN_MNG';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CAMPAIGN_MNG (
  camp_id number(10) check (camp_id > 0) NOT NULL ,
  camp_nm varchar2(50) DEFAULT NULL ,
  description varchar2(255) DEFAULT NULL ,
  mnt_cd varchar2(10) DEFAULT NULL ,
  chatbot_name varchar2(50) NOT NULL,
  start_date timestamp(0) DEFAULT NULL ,
  end_date timestamp(0) DEFAULT NULL ,
  limit_date number(10) DEFAULT '5',
  limit_call_count number(10) DEFAULT '30',
  use_yn char(1) DEFAULT 'Y',
  creator_id varchar2(50) DEFAULT NULL ,
  modifier_id varchar2(50) DEFAULT NULL ,
  create_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  modify_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  PRIMARY KEY (camp_id),
  CONSTRAINT CAMPAIGN_MNG_camp_id_UNIQUE UNIQUE  (camp_id)
)   ;

COMMENT ON TABLE CAMPAIGN_MNG IS '캠페인 관리'

-- Generate ID using sequence and trigger
CREATE SEQUENCE CAMPAIGN_MNG_seq START WITH 100 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER CAMPAIGN_MNG_seq_tr
 BEFORE INSERT ON CAMPAIGN_MNG FOR EACH ROW
 WHEN (NEW.camp_id IS NULL)
BEGIN
 SELECT CAMPAIGN_MNG_seq.NEXTVAL INTO :NEW.camp_id FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CAMPAIGN_SERVICE`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CAMPAIGN_SERVICE';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CAMPAIGN_SERVICE (
  service_name varchar2(50) NOT NULL,
  camp_id number(10) NOT NULL,
  camp_nm varchar2(45) DEFAULT NULL,
  PRIMARY KEY (service_name,camp_id)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `COMMON_CD`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE COMMON_CD';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE COMMON_CD (
  first_cd varchar2(10) NOT NULL ,
  second_cd varchar2(10) DEFAULT NULL ,
  third_cd varchar2(10) DEFAULT NULL ,
  code varchar2(10) DEFAULT '' NOT NULL ,
  cd_desc varchar2(255) DEFAULT NULL ,
  note varchar2(255) DEFAULT NULL ,
  creator_id varchar2(50) DEFAULT NULL ,
  modifier_id varchar2(50) DEFAULT NULL ,
  create_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  modify_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  PRIMARY KEY (first_cd,code)
)  ;

COMMENT ON TABLE COMMON_CD IS '공통코드'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_BASE_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_BASE_INFO (
  cust_id number(10) NOT NULL ,
  cust_nm varchar2(50) DEFAULT '' ,
  jumin_no char(13) DEFAULT NULL ,
  cust_tel_no varchar2(20) DEFAULT NULL ,
  cust_tel_comp varchar2(3) DEFAULT NULL ,
  tel_comp_save_yn char(1) DEFAULT NULL ,
  certifi_no char(6) DEFAULT NULL ,
  create_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  modify_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  cust_address varchar2(50) DEFAULT NULL ,
  cust_detail_address varchar2(50) DEFAULT NULL ,
  PRIMARY KEY (cust_id)
)   ;

COMMENT ON TABLE CUST_BASE_INFO IS 'IB 고객 정보 테이블'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_SPECIAL_MAPPING_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_BASE_SPECIAL_MAPPING_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_BASE_SPECIAL_MAPPING_INFO (
  cust_id number(10) NOT NULL,
  special_contract_id number(10) NOT NULL,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  prod_id number(10) NOT NULL,
  PRIMARY KEY (cust_id,special_contract_id,prod_id)
)  ;

COMMENT ON TABLE CUST_BASE_SPECIAL_MAPPING_INFO IS '고객 정보와 특약의 매핑 테이블'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_CAR_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_CAR_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_CAR_INFO (
  cust_id number(10) NOT NULL ,
  prod_id number(10) NOT NULL ,
  contract_type varchar2(5) DEFAULT NULL ,
  store_nm varchar2(11) DEFAULT NULL ,
  signature varchar2(11) DEFAULT NULL ,
  plate_num varchar2(11) DEFAULT NULL ,
  planner_nm varchar2(45) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  contract_no number(10) DEFAULT NULL,
  PRIMARY KEY (cust_id,prod_id)
)  ;

COMMENT ON TABLE CUST_CAR_INFO IS '자동차 보험 관련 가입 정보'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_INS_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_INS_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_INS_INFO (
  cust_id number(10) NOT NULL ,
  prod_id number(10) NOT NULL ,
  bank_name varchar2(10) DEFAULT NULL ,
  bank_acc_num varchar2(20) DEFAULT NULL ,
  bank_payment_cnt number(10) DEFAULT NULL ,
  recent_payment_date date DEFAULT NULL ,
  first_payment_date date DEFAULT NULL ,
  loan_avail_money number(10) DEFAULT NULL ,
  loan_interest_rate number(5,2) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  cancel_refund_rate number(5,2) DEFAULT NULL ,
  cancel_refund number(10) DEFAULT NULL ,
  maturity_refund number(10) DEFAULT NULL ,
  ins_join_date date DEFAULT NULL ,
  PRIMARY KEY (cust_id,prod_id)
)  ;

COMMENT ON TABLE CUST_INS_INFO IS '고객 id - 보험 상품 별 정보 ( 상품명, 거래 계좌 정보, 거래 날짜 정보)'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_DETAIL_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_LOAN_DETAIL_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_LOAN_DETAIL_INFO (
  loan_seq number(10) NOT NULL ,
  cust_id number(10) NOT NULL ,
  prod_id number(10) NOT NULL ,
  prod_loan_price number(10) DEFAULT NULL ,
  prod_interest_rate number(5,2) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  PRIMARY KEY (loan_seq,cust_id,prod_id)
)  ;

COMMENT ON TABLE CUST_LOAN_DETAIL_INFO IS '고객 id & 대출 id 기준 - 대출받은 각 보험 상품별 정보'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_LOAN_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_LOAN_INFO (
  seq number(10) NOT NULL ,
  cust_id number(10) NOT NULL ,
  loan_month_inter_price number(10) DEFAULT NULL ,
  loan_total_inter_price number(10) DEFAULT NULL ,
  loan_price number(10) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  bank_name varchar2(10) DEFAULT NULL ,
  bank_acc_num varchar2(20) DEFAULT NULL ,
  interest_recent_date timestamp(0) DEFAULT NULL ,
  interest_first_date timestamp(0) DEFAULT NULL ,
  bank_payment_day number(10) DEFAULT NULL ,
  calc_inter_price number(10) DEFAULT NULL ,
  contract_no number(10) DEFAULT NULL ,
  PRIMARY KEY (seq,cust_id)
)   ;

COMMENT ON TABLE CUST_LOAN_INFO IS '고객 id - 대출 고객 계좌 및 대출한 금액 정보'

-- Generate ID using sequence and trigger
CREATE SEQUENCE CUST_LOAN_INFO_seq START WITH 40 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER CUST_LOAN_INFO_seq_tr
 BEFORE INSERT ON CUST_LOAN_INFO FOR EACH ROW
 WHEN (NEW.seq IS NULL)
BEGIN
 SELECT CUST_LOAN_INFO_seq.NEXTVAL INTO :NEW.seq FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_OP_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_OP_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_OP_INFO (
  cust_op_uid number(10) check (cust_op_uid > 0) NOT NULL ,
  cust_op_id varchar2(50) DEFAULT NULL ,
  cust_op_nm varchar2(50) NOT NULL ,
  password varchar2(50) DEFAULT NULL ,
  ip_addr varchar2(50) DEFAULT '127.0.0.1',
  dept_cd varchar2(10) DEFAULT NULL ,
  position_cd varchar2(10) DEFAULT NULL ,
  cust_op_status char(2) DEFAULT '01' ,
  use_yn char(1) DEFAULT NULL ,
  creator_id varchar2(50) DEFAULT NULL ,
  modifier_id varchar2(50) DEFAULT NULL ,
  create_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  modify_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  PRIMARY KEY (cust_op_uid),
  CONSTRAINT cust_op_uid_UNIQUE UNIQUE  (cust_op_uid)
)  ;

COMMENT ON TABLE CUST_OP_INFO IS '상담원 정보'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PIBO_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE CUST_PIBO_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CUST_PIBO_INFO (
  cust_id number(10) NOT NULL ,
  prod_id number(10) NOT NULL ,
  pibo_nm varchar2(45) DEFAULT NULL ,
  pibo_tel_no varchar2(20) DEFAULT NULL ,
  pibo_address varchar2(50) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  contract_no number(10) NOT NULL ,
  PRIMARY KEY (cust_id,prod_id,contract_no)
)  ;

COMMENT ON TABLE CUST_PIBO_INFO IS '피보험자 정보 테이블'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FLOW_TASK`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE FLOW_TASK';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE FLOW_TASK (
  seq number(10) NOT NULL,
  task varchar2(20) NOT NULL,
  task_info varchar2(200) DEFAULT NULL,
  system_answer varchar2(1000) DEFAULT NULL,
  customer_utter varchar2(1000) DEFAULT NULL,
  intent varchar2(100) DEFAULT NULL,
  target_task varchar2(40) DEFAULT NULL,
  PRIMARY KEY (seq,task)
)  ;

-- Generate ID using sequence and trigger
CREATE SEQUENCE FLOW_TASK_seq START WITH 8 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER FLOW_TASK_seq_tr
 BEFORE INSERT ON FLOW_TASK FOR EACH ROW
 WHEN (NEW.seq IS NULL)
BEGIN
 SELECT FLOW_TASK_seq.NEXTVAL INTO :NEW.seq FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_CAMPAIGN_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE HC_HH_CAMPAIGN_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE HC_HH_CAMPAIGN_INFO (
  seq number(10) NOT NULL,
  camp_id number(10) check (camp_id > 0) NOT NULL ,
  category varchar2(45) DEFAULT NULL ,
  task varchar2(45) DEFAULT NULL,
  task_type varchar2(15) DEFAULT NULL,
  task_answer varchar2(45) DEFAULT NULL,
  task_info varchar2(45) DEFAULT NULL ,
  PRIMARY KEY (seq,camp_id)
)   ;

COMMENT ON TABLE HC_HH_CAMPAIGN_INFO IS '시나리오 정보'

-- Generate ID using sequence and trigger
CREATE SEQUENCE HC_HH_CAMPAIGN_INFO_seq START WITH 126 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER HC_HH_CAMPAIGN_INFO_seq_tr
 BEFORE INSERT ON HC_HH_CAMPAIGN_INFO FOR EACH ROW
 WHEN (NEW.seq IS NULL)
BEGIN
 SELECT HC_HH_CAMPAIGN_INFO_seq.NEXTVAL INTO :NEW.seq FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_CAMPAIGN_SCORE`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE HC_HH_CAMPAIGN_SCORE';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE HC_HH_CAMPAIGN_SCORE (
  seq_num number(10) NOT NULL,
  call_id number(10) DEFAULT NULL check (call_id > 0) ,
  contract_no number(10) DEFAULT NULL ,
  info_seq number(10) DEFAULT NULL,
  info_task varchar2(45) DEFAULT NULL,
  task_value varchar2(30) DEFAULT NULL,
  review_coment clob ,
  PRIMARY KEY (seq_num)
)   ;

COMMENT ON TABLE HC_HH_CAMPAIGN_SCORE IS '시나리오 탐지 결과'

-- Generate ID using sequence and trigger
CREATE SEQUENCE HC_HH_CAMPAIGN_SCORE_seq START WITH 22839 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER HC_HH_CAMPAIGN_SCORE_seq_tr
 BEFORE INSERT ON HC_HH_CAMPAIGN_SCORE FOR EACH ROW
 WHEN (NEW.seq_num IS NULL)
BEGIN
 SELECT HC_HH_CAMPAIGN_SCORE_seq.NEXTVAL INTO :NEW.seq_num FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_LOAN_LIST`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE HC_HH_LOAN_LIST';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE HC_HH_LOAN_LIST (
  seq number(10) NOT NULL,
  contract_no number(10) DEFAULT NULL,
  category varchar2(10) DEFAULT NULL,
  loan_name varchar2(30) DEFAULT NULL,
  price number(10) DEFAULT NULL,
  interest_rate number(5,2) DEFAULT NULL,
  PRIMARY KEY (seq)
)   ;

COMMENT ON TABLE HC_HH_LOAN_LIST IS '해피콜 대출시나리오 관련 - 대출 정보 리스트'

-- Generate ID using sequence and trigger
CREATE SEQUENCE HC_HH_LOAN_LIST_seq START WITH 14 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER HC_HH_LOAN_LIST_seq_tr
 BEFORE INSERT ON HC_HH_LOAN_LIST FOR EACH ROW
 WHEN (NEW.seq IS NULL)
BEGIN
 SELECT HC_HH_LOAN_LIST_seq.NEXTVAL INTO :NEW.seq FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HI_PHONE`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE HI_PHONE';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE HI_PHONE (
  sip_domain varchar2(64) NOT NULL,
  number char(20) NOT NULL,
  passwd char(20) DEFAULT NULL,
  tel_uri char(20) DEFAULT NULL,
  pbx_name varchar2(45) DEFAULT NULL,
  status char(20) DEFAULT NULL ,
  contract_no number(10) DEFAULT NULL,
  customer_phone char(20) DEFAULT NULL,
  boot_time timestamp(0) DEFAULT NULL,
  last_event char(20) DEFAULT NULL,
  last_event_time timestamp(0) DEFAULT NULL,
  campaign_id number(10) DEFAULT NULL,
  is_inbound char(1) DEFAULT 'N' NOT NULL,
  PRIMARY KEY (sip_domain,number)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MNT_EXCEL_UP_TMP`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE MNT_EXCEL_UP_TMP';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE MNT_EXCEL_UP_TMP (
  cust_uid varchar2(128) NOT NULL ,
  cust_nm varchar2(50) DEFAULT NULL ,
  jumin_no char(13) DEFAULT NULL ,
  cust_tel_no varchar2(20) DEFAULT NULL ,
  cust_type varchar2(45) DEFAULT NULL ,
  prod_name varchar2(128) DEFAULT NULL,
  cust_op_id varchar2(50) DEFAULT NULL,
  target_dt timestamp(0) DEFAULT NULL
)  ;

COMMENT ON TABLE MNT_EXCEL_UP_TMP IS '엑셀 업로드 템프 테이블'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MNT_TARGET_MNG`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE MNT_TARGET_MNG';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE MNT_TARGET_MNG (
  contract_no number(10) check (contract_no > 0) NOT NULL ,
  campaign_id number(10) DEFAULT NULL check (campaign_id > 0) ,
  cust_uid varchar2(128) DEFAULT '' ,
  assigned_dt timestamp(0) DEFAULT NULL ,
  assigned_yn char(1) DEFAULT 'Y',
  target_dt timestamp(0) DEFAULT NULL ,
  prod_name varchar2(128) DEFAULT '고객상담',
  cust_nm varchar2(50) DEFAULT '',
  jumin_no char(13) DEFAULT '9901011234567',
  cust_tel_no varchar2(20) DEFAULT NULL ,
  cust_type varchar2(50) DEFAULT NULL ,
  cust_op_id varchar2(50) DEFAULT NULL ,
  call_try_count number(10) DEFAULT '0' ,
  call_status char(6) DEFAULT NULL ,
  call_result_cd char(6) DEFAULT NULL ,
  call_result char(2) DEFAULT NULL ,
  callback_dt timestamp(0) DEFAULT NULL ,
  callback_status char(6) DEFAULT 'CB0001',
  call_final_result char(6) DEFAULT NULL ,
  call_memo varchar2(255) DEFAULT NULL ,
  creator_id varchar2(50) DEFAULT NULL ,
  modifier_id varchar2(50) DEFAULT NULL ,
  create_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  modify_dt timestamp(0) DEFAULT SYSTIMESTAMP ,
  task varchar2(20) DEFAULT NULL ,
  task_seq number(10) DEFAULT NULL ,
  insured_contractor varchar2(10) DEFAULT NULL ,
  insured_person varchar2(10) DEFAULT NULL ,
  addr_main varchar2(50) DEFAULT NULL ,
  addr_sub varchar2(100) DEFAULT NULL ,
  addr_code varchar2(10) DEFAULT NULL ,
  bank_name varchar2(20) DEFAULT NULL,
  bank_acc_num varchar2(30) DEFAULT NULL,
  bank_payment_date timestamp(0) DEFAULT SYSTIMESTAMP,
  is_inbound char(1) DEFAULT 'N',
  PRIMARY KEY (contract_no),
  CONSTRAINT contract_no_UNIQUE UNIQUE  (contract_no)
)   ;

COMMENT ON TABLE MNT_TARGET_MNG IS '모니터링 대상관리'

-- Generate ID using sequence and trigger
CREATE SEQUENCE MNT_TARGET_MNG_seq START WITH 100476 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER MNT_TARGET_MNG_seq_tr
 BEFORE INSERT ON MNT_TARGET_MNG FOR EACH ROW
 WHEN (NEW.contract_no IS NULL)
BEGIN
 SELECT MNT_TARGET_MNG_seq.NEXTVAL INTO :NEW.contract_no FROM DUAL;
END;
/

CREATE INDEX CallbackIDX ON MNT_TARGET_MNG (callback_status,callback_dt);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHONE_BOOK`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE PHONE_BOOK';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE PHONE_BOOK (
  id number(10) NOT NULL,
  department varchar2(45) DEFAULT NULL,
  tel_no varchar2(45) DEFAULT NULL,
  create_date timestamp(0) DEFAULT NULL,
  update_date timestamp(0) DEFAULT NULL,
  PRIMARY KEY (id)
)  ;

-- Generate ID using sequence and trigger
CREATE SEQUENCE PHONE_BOOK_seq START WITH 8 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER PHONE_BOOK_seq_tr
 BEFORE INSERT ON PHONE_BOOK FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT PHONE_BOOK_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROD_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE PROD_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE PROD_INFO (
  prod_id number(10) NOT NULL ,
  prod_nm varchar2(50) DEFAULT NULL ,
  create_dtm timestamp(0) DEFAULT NULL ,
  update_dtm timestamp(0) DEFAULT NULL ,
  campaign_id number(10) DEFAULT NULL,
  PRIMARY KEY (prod_id)
)  ;

COMMENT ON TABLE PROD_INFO IS '상품 정보'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ROLE`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE ROLE';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE ROLE (
  role_id number(10) NOT NULL,
  role_name varchar2(45) NOT NULL,
  PRIMARY KEY (role_id)
) ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SESSION_HISTORY`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SESSION_HISTORY';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE SESSION_HISTORY (
  seq number(10) NOT NULL,
  session_id varchar2(100) NOT NULL,
  current_task varchar2(100) DEFAULT NULL,
  intent varchar2(100) DEFAULT NULL,
  PRIMARY KEY (seq,session_id)
)  ;

-- Generate ID using sequence and trigger
CREATE SEQUENCE SESSION_HISTORY_seq START WITH 109 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER SESSION_HISTORY_seq_tr
 BEFORE INSERT ON SESSION_HISTORY FOR EACH ROW
 WHEN (NEW.seq IS NULL)
BEGIN
 SELECT SESSION_HISTORY_seq.NEXTVAL INTO :NEW.seq FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SPECIAL_CONTRACT_INFO`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE SPECIAL_CONTRACT_INFO';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE SPECIAL_CONTRACT_INFO (
  special_contract_id number(10) NOT NULL ,
  special_contract_nm varchar2(45) DEFAULT NULL ,
  contract_type varchar2(11) NOT NULL ,
  PRIMARY KEY (special_contract_id)
)  ;

COMMENT ON TABLE SPECIAL_CONTRACT_INFO IS '특약 테이블'
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STT_RESULT_DETAIL`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE STT_RESULT_DETAIL';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE STT_RESULT_DETAIL (
  stt_result_detail_id number(10) NOT NULL ,
  stt_result_id number(10) DEFAULT NULL ,
  call_id number(10) check (call_id > 0) NOT NULL ,
  speaker_code char(6) DEFAULT NULL ,
  sentence_id number(10) DEFAULT NULL ,
  sentence clob ,
  start_time timestamp(2) DEFAULT NULL ,
  end_time timestamp(2) DEFAULT NULL ,
  speed binary_double DEFAULT NULL ,
  slience_yn char(1) DEFAULT NULL ,
  created_dtm timestamp(0) DEFAULT SYSTIMESTAMP ,
  updated_dtm timestamp(0) DEFAULT SYSTIMESTAMP ,
  creator_id varchar2(50) DEFAULT NULL ,
  updator_id varchar2(50) DEFAULT NULL ,
  ignored char(1) DEFAULT 'N',
  PRIMARY KEY (stt_result_detail_id,call_id)
)   ;

COMMENT ON TABLE STT_RESULT_DETAIL IS 'STT 결과'

-- Generate ID using sequence and trigger
CREATE SEQUENCE STT_RESULT_DETAIL_seq START WITH 57717 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER STT_RESULT_DETAIL_seq_tr
 BEFORE INSERT ON STT_RESULT_DETAIL FOR EACH ROW
 WHEN (NEW.stt_result_detail_id IS NULL)
BEGIN
 SELECT STT_RESULT_DETAIL_seq.NEXTVAL INTO :NEW.stt_result_detail_id FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `USER`
--

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE USER';
EXCEPTION
   WHEN OTHERS THEN NULL;
END;
/
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE USER (
  id number(10) NOT NULL,
  username varchar2(32) NOT NULL,
  name varchar2(45) NOT NULL,
  password varchar2(500) NOT NULL,
  isAccountNonExpired number(3) DEFAULT NULL,
  isAccountNonLocked number(3) DEFAULT NULL,
  isCredentialsNonExpired number(3) DEFAULT NULL,
  isEnabled number(3) DEFAULT NULL,
  created_dtm timestamp(0) DEFAULT SYSTIMESTAMP,
  modify_dtm timestamp(0) DEFAULT SYSTIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT username_UNIQUE UNIQUE  (username)
)   ;

-- Generate ID using sequence and trigger
CREATE SEQUENCE USER_seq START WITH 12 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER USER_seq_tr
 BEFORE INSERT ON USER FOR EACH ROW
 WHEN (NEW.id IS NULL)
BEGIN
 SELECT USER_seq.NEXTVAL INTO :NEW.id FROM DUAL;
END;
/
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'HAPPYCALL3'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-12 20:32:48
