#!/usr/bin/python

import sys

f = open(sys.argv[1], 'r')
out = open('new' + sys.argv[1], 'w')
for line in f.readlines():
    if line.startswith('/') or line.startswith('COMMENT'):
        continue
    out.write(line)
out.close()
f.close()
