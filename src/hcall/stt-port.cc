#include "stt-port.h"
#include "stt-stream.h"

pj_status_t pjmedia_stt_create(pj_pool_t *pool, pjmedia_port **p_port,
                               SafeQueue<SoundFrame>* result_q) {
  g_var.logger->debug("pjmedia_stt_create");

  pj_status_t status;
  const pj_str_t STR_STT_GEN = pj_str("stt");
  struct stt_port *port;

  /* Create and initialize port */
  int sample_rate = g_var.stt_samplerate;
  port = PJ_POOL_ZALLOC_T(pool, struct stt_port);
  port->result_q = result_q;
  port->result_q->PlayingMOH = false;

  status = pjmedia_port_info_init(&port->base.info, &STR_STT_GEN,
                                  PJMEDIA_SIG_PORT_MEM_CAPTURE,
                                  sample_rate, 1,
                                  16,
                                  sample_rate * 20 / 1000 // samples per frame
                                 );
  port->base.put_frame = &stt_put_frame;
  port->base.get_frame = &stt_get_frame;
  port->base.on_destroy = &stt_on_destroy;

  auto channel = grpc::CreateChannel(g_var.sttd_remote, grpc::InsecureChannelCredentials());
  g_var.logger->debug("sttd addr {}", g_var.sttd_remote);
  g_var.logger->debug("LB is [{}]", channel->GetLoadBalancingPolicyName());
  g_var.logger->debug("Service Config is [{}]", channel->GetServiceConfigJSON());

  // GRPC_CHANNEL_IDLE, GRPC_CHANNEL_CONNECTING, GRPC_CHANNEL_READY, GRPC_CHANNEL_TRANSIENT_FAILURE, 
  //  GRPC_CHANNEL_SHUTDOWN 
  if (channel->GetState(false) == GRPC_CHANNEL_TRANSIENT_FAILURE) {
    g_var.logger->debug("channel enum {}", channel->GetState(false));
    port->stream = NULL;
  } else {
    // TODO
    port->stream = new SttStream(channel, result_q);
  }

  g_var.logger->debug("channel enum {}", channel->GetState(false));
  if (!port->stream) {
    printf("create stt stream failed\n");
  } else {
    //port->stream->run_grpc();
    //port->stream->wait_until_thread_start();
  }
  *p_port = &port->base;
  g_var.is_doing_tts = false;
  return status;
}

/**
 * 다른 미디어포트에서 pjmedia_port_put_frame 을 호출하면 그 안에서
 * stt_put_frame이 불린다
 */
pj_status_t stt_put_frame(pjmedia_port *this_port, pjmedia_frame *frame) {
  if (frame->type == PJMEDIA_FRAME_TYPE_AUDIO) {
    // if (!g_var.listening_on_tts && g_var.is_doing_tts) {
    //   return PJ_SUCCESS;
    // }
    stt_port* port = (stt_port*)this_port;
    if (!port->stream->greeting_done) {
      return PJ_SUCCESS;
    }
    SttStream* stream = (SttStream*)port->stream;
    if (stream) {
      stream->write((char*)frame->buf, frame->size);
    }
  }
  return PJ_SUCCESS;
}

pj_status_t stt_get_frame(pjmedia_port *this_port, pjmedia_frame *frame) {
  if (frame->type == PJMEDIA_FRAME_TYPE_AUDIO) {
    // do nothing
  } else {
    // don't reach here
  }
  return PJ_SUCCESS;
}

pj_status_t stt_on_destroy(pjmedia_port *this_port) {
  g_logger->debug("called stt_on_destroy()");

  struct stt_port *port;
  port = (struct stt_port *)this_port;
  if (port->stream) {
    port->stream->disconnected = true;
    port->stream->stop();
    port->result_q->clear();
    delete port->stream;
  }
  g_var.is_doing_tts = false;

  g_logger->debug("stt_port destroyed");

  return PJ_SUCCESS;
}
