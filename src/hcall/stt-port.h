#ifndef STT_PORT_H
#define STT_PORT_H

#include <pjsua-lib/pjsua.h>
#include "safe-queue.h"

class SttStream;

struct stt_port {
  pjmedia_port base;
  SttStream *stream;
  SafeQueue<SoundFrame> *result_q;
};

pj_status_t stt_put_frame(pjmedia_port *this_port, pjmedia_frame *frame);
pj_status_t stt_get_frame(pjmedia_port *this_port, pjmedia_frame *frame);
pj_status_t stt_on_destroy(pjmedia_port *this_port);

pj_status_t pjmedia_stt_create(pj_pool_t *pool, pjmedia_port **p_port,
                               SafeQueue<SoundFrame>* result_q);

#endif /* STT_PORT_H */
