#include <time.h>
#include <unistd.h>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>

#include "stt-stream.h"
#include "util.h"
#include "call-status.h"
#include "tts-port.h"

#include "tts/ng_tts.grpc.pb.h"
#include "tts/ng_tts.pb.h"

#include "agent-stream.h"
#include "app-variable.h"
#include "zhelpers.h"

#define BUFF_SIZE 6720

using namespace maum::brain::tts;
using namespace std::chrono;

class RtpTimer {
 public:
  RtpTimer(int seconds, SttStream *stream, pjsua_call_id call_id) :
      timer_(g_var.service) {
    stream_ = stream;
    cmd_sender_ = NULL;
    call_id_ = call_id;
    prev_pkt_cnt_ = 0;
    no_rtp_cnt_ = 0;
    seconds_ = seconds;

    // 내부 이벤트를 위한 zmq
    char endpoint[128];

    cmd_sender_ = zmq_socket(g_var.app_context, ZMQ_REQ);
    sprintf(endpoint, "ipc://%s/run/%s.cmd.ipc", g_var.maum_root.c_str(), g_var.sip_user.c_str());
    if (zmq_connect(cmd_sender_, endpoint) != 0) {
      g_var.logger->error("Cannot connect zmq command receiver ({})", endpoint);
    }
  }

  virtual ~RtpTimer() {
    timer_.cancel();
    if (cmd_sender_) zmq_close(cmd_sender_);
    g_var.logger->trace("grpc stream read() timer is destructed");
  }

  void Start() {
    timer_.expires_from_now(boost::posix_time::seconds(seconds_));
    timer_.async_wait(boost::bind(&RtpTimer::OnTimeout, this, _1));
  }

 private:
  void OnTimeout(const boost::system::error_code& e) {
    if (e == boost::asio::error::operation_aborted) return;

    char buf[256];
    // 현재 패킷 카운트 확인
    APP_MESSAGE_T msg = { APP_COMMAND_ENUM::CHECK_PKT, call_id_ };
    zmq_send(cmd_sender_, (char *)&msg, sizeof(APP_MESSAGE_T), ZMQ_NOBLOCK);
    int size = zmq_recv(cmd_sender_, buf, 255, 0);
    if (size == -1) {
      g_var.logger->error("RTP query packet error");
      // return false;
    }

    SIP_PKTCNT_T *resp = (SIP_PKTCNT_T *)buf;

    // N초 동안 변화한 패킷이 없다면 RTP가 들어오지 않는 상태
    if (prev_pkt_cnt_ == resp->count) {
      no_rtp_cnt_++;
      if (no_rtp_cnt_ == 3) {
        g_var.logger->error("RTP Timeout, packet count is {}", resp->count);
        stream_->stop();
      }
    } else {
      g_var.logger->debug("RTP current packet count is {}", resp->count);
      no_rtp_cnt_ = 0;
    }
    prev_pkt_cnt_ = resp->count;
    Start();
  }

  SttStream*    stream_;
  pjsua_call_id call_id_;
  void*         cmd_sender_;

  pj_uint32_t prev_pkt_cnt_;
  int         no_rtp_cnt_;
  int         seconds_;

  boost::asio::deadline_timer timer_;
};

SttStream::SttStream(std::shared_ptr<Channel> channel, SafeQueue<SoundFrame>* result_q) :
    disconnected(false),
    stub_(SpeechToTextService::NewStub(channel)),
    grpc_done_(true),
    result_q_(result_q),
    dialog_(g_var.m2u_addr, g_var.chatbot_name, result_q, g_var.service)
{
  dialog_.StopSTT = std::bind(&SttStream::stop, this);
  M2uClient::logger = g_logger;
  sem_init(&start_sem_, 0, 0);
  greeting_done = false;

  websock = std::make_shared<WebsocketClient>(g_var.service);
  auto rc = websock->connect(g_var.call_ws_addr);
  if (rc == -1) {
    g_logger->error("Connect Initialization error");
  }
  dialog_.websock = websock;

  // DONE: 2020-10-13 added by shinwc
  // Set dialog status to CS_IDLE to indicate if SttStream instance is created
  // but dialog session isn't started yet
  dialog_.SetStatus(CS_IDLE);
}

SttStream::~SttStream()
{
  sem_destroy(&start_sem_);
  g_logger->debug("try to join grpc_stream");
  if (result_thrd_.joinable()) {
    result_thrd_.join();
    g_logger->debug("join grpc_stream");
  } else {
    g_logger->debug("grpc_stream not joinable");
  }
}

bool SttStream::ping(std::string addr) {
  // Find STT Child Process
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SttModelResolver::Stub> stub(SttModelResolver::NewStub(channel));

  ClientContext ctx;
  ctx.set_wait_for_ready(false);

  ServerStatus server_status;

  Model model;
  model.set_model(g_var.model);
  model.set_sample_rate(8000);
  model.set_lang(maum::common::LangCode::kor);

  grpc::Status status = stub->Find(&ctx, model, &server_status);
  if (status.error_code() != grpc::StatusCode::OK) {
    g_var.logger->error("GRPC Find({}) error is {}", addr, status.error_message());
    return false;
  }
  return true;
}

void SttStream::write(char* buf, int size) {
  if (agent_) {
    if (agent_->IsConnected()) {
      agent_->Write(buf, size);
    }
  }

  Speech speech;
  speech.set_bin(buf, size);

  std::unique_lock<std::mutex> lock(grpc_lock_);
  if (!grpc_done_) {
    grpc_stream_->Write(speech);
  }
}

void SttStream::run_grpc(pjsua_call_id call_id) {
  result_thrd_ = std::thread(&SttStream::start, this, call_id);
}

int calculate_duration(std::string &pcm) {
  return pcm.size() * 100 / (16000 * 2);
}

void SttStream::start_agent() {
  if (!agent_) {
    agent_.reset(new AgentStream(result_q_, websock, &dialog_, app_call_id_));
    if (agent_->Connect(g_var.agent_relay_host, g_var.agent_relay_port)) {
      g_var.db.UpdateStatus(CS_AGENT_PROCESSING);
      agent_->Start();
    }
  } else {
    g_logger->debug("Already Start to connect agent");
  }
}

void SttStream::stop_agent() {
  agent_.reset();
}

bool SttStream::connect_stt(std::string &model) {
  std::unique_lock<std::mutex> lock(grpc_lock_);
  auto it = g_var.sttd_list.find(model);
  if (it == g_var.sttd_list.end()) {
    g_logger->error("Cannot find stt model from STT_SERVER_INFO");
    return false;
  }

  STT_SERVER_INFO info = it->second;
  auto channel = grpc::CreateChannel(info.addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SpeechToTextService::Stub> stub(SpeechToTextService::NewStub(channel));
  stub_ = std::move(stub);
  ctx_.reset(new ClientContext);
  ctx_->set_wait_for_ready(false);

  ctx_->AddMetadata("in.lang", info.lang);
  g_logger->info("in.lang = {}", info.lang);
  ctx_->AddMetadata("in.samplerate", info.samplerate);
  ctx_->AddMetadata("in.model", info.model);
  ctx_->AddMetadata("in.epd_timeout", std::to_string(g_var.epd_timeout));
  grpc_stream_ = stub_->StreamRecognize(ctx_.get());
  dialog_.SttContext = ctx_;

  return true;
}

bool SttStream::is_grpc_done() {
  std::unique_lock<std::mutex> lock(grpc_lock_);
  return grpc_done_;
}

void SttStream::start(pjsua_call_id call_id) {
  timeval start_tv, now, duration_tv;
  std::string model_name = g_var.model;
  Text resp;
  Segment segment;

  app_call_id_ = call_id;
  gettimeofday(&start_tv, NULL);
  dialog_.SetStatus(CS_AI_PROCESSING);
  dialog_.SetSttModelName(model_name);
  dialog_.SetCallID(call_id);

  ping(g_var.sttd_remote);
  g_logger->debug("Start to run stt-client");
  g_var.db.UpdateStatus(dialog_.GetStatus(), 1);
  g_var.db.UpdatePhoneStatus(dialog_.GetStatus());
  // DONE: 2020-09-01 commented out by shinwc
  // To remove InsertHistory() cause it moved to INCOMING or CALLING state
  //g_var.db.InsertHistory(g_var.contract_no, dialog_.GetStatus(), start_tv, g_var.sip_call_id);
  websock->publish_event(dialog_.GetStatus().c_str());
  websock->publish_signal("start");

  // status = CS_STOPPED;

  connect_stt(g_var.model);
  grpc_done_ = false;

  if (g_var.direct_call) {
    start_agent();
  } else {
    dialog_.OpenDialog(std::to_string(g_var.call_history_id));

    // 대화를 시작하고 인사말을 받아온다
    dialog_.SetStatus("CS0000");
    if (g_var.da_record_event) {
      dict meta;
      meta["record"] = "start";
      meta["dial_no"] = g_var.sip_user;
      dialog_.AsyncSendText("#rec_start", "N", meta);
    }
    dialog_.AsyncSendText("시작", "N");
    dialog_.SetStatus(CS_STOPPED);
  }

  RtpTimer rtp_timer(10, this, call_id);
  rtp_timer.Start();

  sem_post(&start_sem_);
  g_logger->debug("Start Stt ReadStream...............");
  greeting_done = true;

  while (!is_grpc_done() && !dialog_.IsComplete) {
    if (grpc_stream_->Read(&segment)) {
      if (g_var.tts_echo_sample) {
        push_sound_frame(g_var.tts_sample_wav, result_q_);
      }
      char ignored = 'N';
      if (!agent_ && !result_q_->empty()) {  // 상담사가 얘기하는 중이 아니고 TTS 중인 상태
        ignored = 'Y';
      }
      g_logger->info("STT Result: [{}], {} ~ {}", segment.txt(), segment.start(), segment.end());
      g_var.db.InsertResult(
          "ST0001", dialog_.SentenceNum++, segment.txt(), segment.start(), segment.end(), ignored);
      websock->publish(segment.start(), segment.end(), "RX", segment.txt(), ignored);

      if (agent_) {
        dialog_.IsComplete = agent_->IsDone();
        if (dialog_.IsComplete) {
          dialog_.SetStatus(CS_AGENT_COMPLETED);
        }
        continue;
      }

      if (dialog_.IsComplete) {
        break;
      }

      // TTS 중일 경우 음성인식결과 무시
      if (!result_q_->empty() && segment.txt() == g_var.stop_tts_keyword) {
        g_logger->debug("detected stop keyword, stop tts");
        result_q_->clear();
      }

      if (!dialog_.IsMultimodal && dialog_.IsDTMF) {
        g_logger->debug("ignored stt result in DTMF only mode!");
        continue;
      }
      if (result_q_->PlayingMOH) {
        g_logger->debug("ignored stt result during long_task");
        continue;
      }

      // M2U or MaumChatbot 에서 답변을 받아온다. 인식 결과가 없는 경우 처리하지 않는다.
      if (segment.txt().empty())
        continue;

      // Do not send STT result if disconnected is true, which means 
      // callback(stt_on_destroy) is called due to call disconnect
      if(!disconnected) {
          dialog_.AsyncSendText(segment.txt(), std::string(1, ignored));
      }

    } else {
      Status status = grpc_stream_->Finish();
      if (status.error_code() == grpc::StatusCode::CANCELLED &&
          model_name != dialog_.GetSttModelName()) {
        g_logger->info("STT Model changed ({} -> {})", model_name, dialog_.GetSttModelName());
        model_name = dialog_.GetSttModelName();
        if (connect_stt(model_name) == false) {
          break;
        }
      } else {
        // break?
        g_logger->info("STT gRPC stream finished: {}", status.error_message());
      }
    }
  }

  if (agent_) {
    agent_.reset();
  }

  if (!agent_ && !disconnected) {
    g_logger->debug("Waiting until TTS is done...");
    for (int i = 0; i < 600 /* 0.1초 * 600 = 60초 */ && !result_q_->empty(); i++) {
      usleep(100 * 1000);
    }
    if (!result_q_->empty())
      g_logger->debug("TTS Queue is not empty, but 60 seconds have passed");

    result_q_->clear();
  }
  g_logger->debug("Waiting for 2 seconds...");
  usleep(2 * 1000 * 1000);

  APP_MESSAGE_T msg = { APP_COMMAND_ENUM::HANGUP, call_id };
  zmq_send(g_var.evnt_sender, (char *)&msg, sizeof(APP_MESSAGE_T), ZMQ_NOBLOCK);

  if (!g_var.direct_call) {
    if (g_var.da_record_event) {
      dict meta;
      meta["record"] = "stop";
      meta["dial_no"] = g_var.sip_user;
      meta["last_status_code"] = "200";
      auto result = dialog_.SendText("#rec_stop", "N", meta);
      dialog_.ResponseLog(result);
    }
    dialog_.CloseDialog();
  }

  // 통화 시간 계산
  gettimeofday(&now, NULL);
  timersub(&now, &start_tv, &duration_tv);

  g_var.db.UpdateStatus(dialog_.GetStatus());
  g_var.db.UpdatePhoneStatus(CS_READY);
  // DONE: 2020-09-01 added code by shinwc
  // To modify UpdateHistory() to change dialog.status only 
  g_var.db.UpdateHistory(g_var.contract_no, dialog_.GetStatus());
  websock->publish_event(dialog_.GetStatus().c_str());
  websock->publish_signal("stop");

  g_logger->debug("END of Stt ReadStream thread");
}

void SttStream::wait_until_thread_start() {
  sem_wait(&start_sem_);
}

void SttStream::stop() {
  g_logger->debug("prepare to stop stt client");
  if (!grpc_stream_)
    g_logger->debug("grpc_stream null");

  std::unique_lock<std::mutex> lock(grpc_lock_);
  if (!grpc_done_) {
    grpc_stream_->WritesDone();
    grpc_done_ = true;
  } else {
    g_logger->debug("already SttStream stopped");
  }
}

void SttStream::WaitServer() {
  auto resolver_channel =
    grpc::CreateChannel(g_var.sttd_remote, grpc::InsecureChannelCredentials());
  auto resolver_stub = SttModelResolver::NewStub(resolver_channel);

  ClientContext ctx;
  ServerStatus ss;
  Model model;

  if (g_var.lang == "kor") {
    model.set_lang(maum::common::LangCode::kor);
  } else {
    model.set_lang(maum::common::LangCode::eng);
  }
  model.set_sample_rate(g_var.stt_samplerate);
  model.set_model(g_var.model);

  Status st = resolver_stub->Find(&ctx, model, &ss);

  auto real_channel =
    grpc::CreateChannel(ss.server_address(), grpc::InsecureChannelCredentials());
  auto real_stub = SttRealService::NewStub(real_channel);

  for (int i = 0; i < 3; i++) {
    ClientContext real_ctx;
    g_var.logger->info("Waiting for stt server to be ready...");
    st = real_stub->Ping(&real_ctx, model, &ss);

    if (st.ok()) break;
    usleep(1000 * 1000);
  }
}

void SttStream::add_number(int digit) {
  if (dialog_.IsDTMF) {
    if (dialog_.IsDTMFSingle) {
      dialog_.RefreshTimer();
      dtmf_number_.append(1, (char)digit);
      g_var.db.InsertResult(
          "ST0001", dialog_.SentenceNum++, dtmf_number_, 0, 0, 'N');
      websock->publish(0, 0, "RX", dtmf_number_, 'N');
      dialog_.AsyncSendText(dtmf_number_, "N");
      dtmf_number_.clear();
    } else if ((char)digit == '#' || (char)digit == '*') {
      g_var.db.InsertResult(
          "ST0001", dialog_.SentenceNum++, dtmf_number_, 0, 0, 'N');
      websock->publish(0, 0, "RX", dtmf_number_, 'N');
      dialog_.AsyncSendText(dtmf_number_, "N");
      dtmf_number_.clear();
    } else {
      dialog_.RefreshTimer();
      dtmf_number_.append(1, (char)digit);
    }
  }
  g_var.logger->info("DTMF NUMBER is {}", dtmf_number_);
}
