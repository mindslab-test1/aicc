#ifndef STT_STREAM_H
#define STT_STREAM_H

#include <memory>
#include <vector>
#include <iostream>
#include <thread>
#include <signal.h>
#include <semaphore.h>
#include <stdio.h>
#include <math.h>
#include <queue>
#include <grpc++/grpc++.h>

#include <maum/brain/stt/stt.grpc.pb.h>
#include <libmaum/common/config.h>
#include <spdlog/spdlog.h>

#include <pjsua-lib/pjsua.h>
#include "app-variable.h"
#include "safe-queue.h"
#include "ws-client.h"

#include "agent-stream.h"
#include "voice-dialog.h"

using std::string;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using maum::brain::stt::SpeechToTextService;
using maum::brain::stt::SttRealService;
using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using maum::brain::stt::Segment;

using maum::brain::stt::SttModelResolver;
using maum::brain::stt::ServerStatus;
using maum::brain::stt::Model;

struct ST_DA_PARAM_T {
  std::string dlg_event;
  std::string detect_no;
};

class SttStream {
 public:
  SttStream(std::shared_ptr<Channel> channel, SafeQueue<SoundFrame>* result_q);
  ~SttStream();

  bool ping(std::string addr);
  void write(char* buf, int size);

  void run_grpc(pjsua_call_id call_id);

  void start(pjsua_call_id call_id);
  void wait_until_thread_start();
  void stop();

  void start_agent();
  void stop_agent();
  std::string getDialogStatus() { return dialog_.GetStatus(); }

  // void publish(int start, int end, char *direction, std::string txt, char ignored = 'N');
  // void publish_event(std::string call_status);
  // void publish_signal(char *signal_event);

  // Get DTMF Number
  void add_number(int digit);

  static void WaitServer();

  bool greeting_done;
  bool disconnected;

  std::shared_ptr<WebsocketClient> websock;

private:
  bool connect_stt(std::string &model);
  bool is_grpc_done();

  std::unique_ptr<ClientReaderWriter<Speech, Segment> > grpc_stream_;
  std::unique_ptr<SpeechToTextService::Stub> stub_;
  std::shared_ptr<ClientContext> ctx_;
  std::mutex grpc_lock_;
  std::thread result_thrd_;
  bool grpc_done_;
  std::mutex wr_lock_;
  sem_t start_sem_;
  pjmedia_port* port_;
  pjsua_call_id app_call_id_;
  SafeQueue<SoundFrame>* result_q_;
  VoiceDialog dialog_;

  int record_f_;

  std::string dtmf_number_;

  std::unique_ptr<AgentStream> agent_;
};

#endif /* STT_STREAM_H */
