#include "catch.hpp"

#include "soci/soci.h"
#ifdef USE_MYSQL
  #include "soci/mysql/soci-mysql.h"
#elif USE_ORACLE
  #include "soci/oracle/soci-oracle.h"
#elif USE_MSSQL
  #include "soci/odbc/soci-odbc.h"
#endif
#include "soci/mysql/soci-mysql.h"
#include <mysqld_error.h>

#include "m2u-client.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>
#include "rest-client.h"

#define QUOTE(...) #__VA_ARGS__

using namespace soci;
using namespace rapidjson;

TEST_CASE("json", "[rapid-json]") {
  rapidjson::Document document;
  document.SetObject();

  // create a rapidjson array type with similar syntax to std::vector
  rapidjson::Value array(rapidjson::kArrayType);

  // must pass an allocator when the object may need to allocate memory
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  // chain methods as rapidjson provides a fluent interface when modifying its objects
  array.PushBack("hello", allocator).PushBack("world", allocator);//"array":["hello","world"]

  std::string test_val("12345");
  char a[10] = "123";
  // const char *a = "123";
  document.AddMember("Name", "XYZ   \n", allocator);
  document.AddMember("Rollnumer", 2, allocator);
  document.AddMember("Rollnumer1", StringRef(a), allocator);
  sprintf(a, "%.2f", 100.456);
  document.AddMember("Rollnumer2", StringRef(a), allocator);
  document.AddMember("array", array, allocator);
  document.AddMember("12345key", Value(test_val.c_str(), allocator).Move(), allocator);

  // create a rapidjson object type
  rapidjson::Value object(rapidjson::kObjectType);
  object.AddMember("Math", "50", allocator);
  object.AddMember("Science", "70", allocator);
  object.AddMember("English", "50", allocator);
  object.AddMember("Social Science", "70", allocator);
  document.AddMember("Marks", object, allocator);
  //	fromScratch["object"]["hello"] = "Yourname";

  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  std::cout << strbuf.GetString() << std::endl;
  std::cout << test_val << std::endl;
}

TEST_CASE("db", "[aicc-db]" ) {
#ifdef USE_MYSQL
  std::string conn_str("db=HAPPYCALL4 user=minds password=msl1234~ host=10.122.64.152 charset=utf8");
  session sql(mysql, conn_str);
#elif USE_MSSQL
  std::string conn_str("DRIVER=libtdsodbc.so;SERVER=10.122.64.152;PORT=1433;DATABASE=master;UID=sa;PWD=msl1234~");
  // std::string conn_str("UID=sa;PWD=msl1234~; Connection Driver=TDS;Database=master; Server=10.122.64.152;Port=1433;");
  session sql(odbc, conn_str);
#endif
  // ORACLE 설정
  // session sql(oracle, "service=10.122.64.191:1521/orcl user=minds password=Minds12#$ dbname=MINDS");

  std::string call_seq = "1";
  std::string campaign_id = "4";

  char *qry = QUOTE(
    SELECT B.CUST_NM,
           T.TEL_NO,
           T.CUST_OP_ID,
           C.CHATBOT_NAME
    FROM   CM_CONTRACT T
    LEFT JOIN CUST_BASE_INFO B
           ON T.CUST_ID = B.CUST_ID
    LEFT JOIN CM_CAMPAIGN C
           ON T.CAMPAIGN_ID = C.CAMPAIGN_ID
    WHERE  T.CONTRACT_NO = :call_seq
           AND T.CAMPAIGN_ID = :campaign_id
  );
  printf("SQL:\n%s\n", qry);

  rowset<row> rows = (sql.prepare << qry, use(call_seq), use(campaign_id));

  for (auto it = rows.begin(); it != rows.end(); it++) {
    auto cust_name     = it->get<std::string>(0);
    auto tel_no        = it->get<std::string>(1);
    auto chatbot_name  = it->get<std::string>(3);
    // NULL값 확인 - indicator 이용하는 방식
    // indicator ind = it->get_indicator(4);
    // int  agent_id      = ind == i_null ? 0 : std::stoi(it->get<std::string>(4));
    // NULL값 확인 - default값 지정하는 방식
    // int  agent_id      = std::stoi(it->get<std::string>(4, "0"));

    printf("cust_name    = %s\t", cust_name.c_str());
    printf("tel_no       = %s\t", tel_no.c_str());
    printf("chatbot_name = %s\n", chatbot_name.c_str());
  }
}

TEST_CASE("m2u", "[m2u-test]" ) {
  boost::asio::io_service ios;
  boost::asio::io_service::work worker(ios);
  std::thread s([&]{
      ios.run();
    });

  auto cb = [](dict result){
    std::string answer;
    for (auto kv : result) {
      answer += "\tKEY: " + kv.first + ",\tVALUE: " + kv.second + "\n";
    }
    M2uClient::logger->info("M2U 응답: \n{}", answer);
  };

  M2uClient m2u(ios, "10.122.64.152:9911", "1", "1");
  std::map<std::string, std::string> result;
  std::map<std::string, std::string> meta;
  std::string doing_tts = "N";
  std::string status = "CS0001";
  m2u.authenticate();
  sleep(1);
  m2u.open_dialog("HC_CS_LOAN");
  sleep(1);
  m2u.async_text_to_text_talk("시작", doing_tts, status, meta, cb);
  sleep(1);
  m2u.async_text_to_text_talk("네", doing_tts, status, meta, cb);
  sleep(1);
  m2u.async_text_to_text_talk("네", doing_tts, status, meta, cb);
  sleep(1);
  m2u.close_dialog();
  // sleep(1);

  ios.stop();
  s.join();
}

TEST_CASE("rest", "[rest-api]") {
  RestClient client(NULL);
  std::string resp_body;
  std::string telno = "01012345678";
  // url = "http://10.2.12.151:8080/ksign/convertTelNo?type=enc&telNo=01076238656";
  // std::string url = "http://10.2.12.151:8080/ksign/convertTelNo?type=enc";
  // url += "&telNo=" + telno;
  std::string url = "http://10.122.64.152:5002/call_stat?start_time=20200301&campaign_id=13";

  if (client.Get(url.c_str(), resp_body)) {
    printf("response is \n%s\n", resp_body.c_str());
  }
}
