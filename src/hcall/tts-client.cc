#include <chrono>
#include "tts-client.h"

using namespace std::chrono;

// mulaw converter
extern "C" short Snack_Mulaw2Lin(unsigned char);

TtsClient::TtsClient(std::shared_ptr<Channel> channel, SafeQueue<SoundFrame> *result_q)
    : stub_(TextToSpeechService::NewStub(channel)),
      result_q_(result_q) {
  sem_init(&start_sem_, 0, 0);
}

TtsClient::~TtsClient() {
  sem_destroy(&start_sem_);
}

void TtsClient::Request(string text) {
  if (!result_thrd_.joinable()) {
    g_logger->debug("TtsClient thread start");
    result_thrd_ = std::thread(&TtsClient::run, this);
    wait_until_thread_start();
  }
  TextUtter param;
  param.set_utter(text);
  request_q_.push(param);
}

void TtsClient::WaitThread() {
  is_done_ = true;
  // send dummy data
  TextUtter param;
  param.set_utter("EOF");
  request_q_.push(param);

  if (result_thrd_.joinable()) {
    result_thrd_.join();
  } else {
    g_logger->debug("There is no joinable TtsClient thread");
  }
}

void TtsClient::wait_until_thread_start() {
  sem_wait(&start_sem_);
}

void TtsClient::notify_thread_start() {
  sem_post(&start_sem_);
}

void TtsClient::run() {
  notify_thread_start();

  while (!is_done_) {
    TextUtter param;
    request_q_.wait_and_pop(param);

    if (is_done_) {
      break;
    }

    ClientContext ctx;
    ctx.AddMetadata("in.lang", g_var.lang);
    system_clock::time_point deadline = system_clock::now() +
      seconds(g_var.tts_deadline);
    ctx.set_deadline(deadline);

    grpc_stream_ = stub_->Translate(&ctx, param);

    AudioUtter audio;
    g_logger->debug("Start TTS's grpc Read()");

    g_var.is_doing_tts = true;
    while (grpc_stream_->Read(&audio)) {
      if (g_var.stop_tts) {
        g_var.stop_tts = false;
        break;
      }
      int idx = 0;
      short buf[1024];

      for (auto c : audio.utter()) {
        buf[idx++] = Snack_Mulaw2Lin(c);
        if (idx == g_var.tts_samplerate * 20 / 1000 ) {
          SoundFrame data;
          memcpy(data.data, (char*)buf, idx * 2);
          data.size = idx * 2;
          result_q_->push(data);
          idx = 0;
        }
      }

      if (idx > 0) {
        SoundFrame data;
        memcpy(data.data, (char*)buf, idx * 2);
        data.size = idx * 2;
        result_q_->push(data);
      }
    }
    g_var.is_doing_tts = false;

    Status status = grpc_stream_->Finish();
    if (!status.ok()) {
      g_logger->error("TTS's grpc finish() error : {}",
                      status.error_message());
    }
  }
  g_logger->debug("END of TTS's grpc READ()");
}
