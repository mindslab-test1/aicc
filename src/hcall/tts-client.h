#ifndef TTS_CLIENT_H
#define TTS_CLIENT_H

#include <memory>
#include <vector>
#include <iostream>
#include <thread>
#include <signal.h>
#include <semaphore.h>
#include <stdio.h>
#include <math.h>
#include <queue>
#include <condition_variable>
#include <grpc++/grpc++.h>

#include "tts.grpc.pb.h"
#include <libminds/common/config.h>
#include <spdlog/spdlog.h>

#include <pjsua-lib/pjsua.h>
#include "app-variable.h"
#include "sip-client.h"
#include "stt-port.h"
#include "safe-queue.h"

using std::string;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReader;

using elsa::facade::TextToSpeechService;
using elsa::facade::TextUtter;
using elsa::facade::AudioUtter;

class TtsClient {
 public:

  /**
   * TtsClient Constructor
   *
   * @param result_q TTS결과를 보낼 Queue
   */
  TtsClient(std::shared_ptr<Channel> channel, SafeQueue<SoundFrame> *result_q);
  ~TtsClient();

  void Request(string text);
  void WaitThread();

 private:

  /**
   * TtsClient main loop
   */
  void run();
  void wait_until_thread_start();
  void notify_thread_start();

  std::unique_ptr<ClientReader<AudioUtter> > grpc_stream_;
  std::unique_ptr<TextToSpeechService::Stub> stub_;
  std::thread result_thrd_;

  SafeQueue<TextUtter> request_q_;
  SafeQueue<SoundFrame> *result_q_;

  bool is_done_ = false;
  sem_t start_sem_;
};

#endif /* TTS_CLIENT_H */
