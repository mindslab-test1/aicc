#include "app-variable.h"
#include "tts-port.h"
#include <libmaum/common/config.h>

extern void push_sound_frame(std::string file_name, SafeQueue<SoundFrame>* q);

pj_status_t tts_put_frame(pjmedia_port *this_port, pjmedia_frame *frame) {
  // remove warning
  (void)this_port;
  (void)frame;
  // do nothing
  return PJ_SUCCESS;
}

pj_status_t tts_get_frame(pjmedia_port *this_port, pjmedia_frame *frame) {
  if (frame->type == PJMEDIA_FRAME_TYPE_AUDIO) {
    tts_port *port = (tts_port*)this_port;
    SoundFrame sf;
    if (port->result_q->try_pop(sf)) {
      pj_memcpy((char*)frame->buf, sf.data, sf.size);
      port->result_q->UtterNo = sf.utter_no;
      port->result_q->SentenceNo = sf.sent_no;
    } else if (port->result_q->PlayingMOH) {
      push_sound_frame(g_var.moh_wav, port->result_q);
    } else {
      // 에코 현상 제거
      pj_memset(frame->buf, 0, frame->size);
    }
  } else {
    g_logger->debug("frame->type is not PJMEDIA_FRAME_TYPE_AUDIO but {}", frame->type);
  }
  return PJ_SUCCESS;
}

pj_status_t tts_on_destroy(pjmedia_port *this_port) {
  g_logger->debug("tts port destroyed");
  return PJ_SUCCESS;
}

pj_status_t pjmedia_tts_create(pj_pool_t *pool,
                               pjmedia_port **p_port,
                               SafeQueue<SoundFrame>* result_q) {
  g_var.logger->debug("pjmedia_tts_create");

  pj_status_t status;
  struct tts_port *port;
  const pj_str_t TTS_GEN = pj_str("tts_8k");

  /* Create and initialize port */
  port = PJ_POOL_ZALLOC_T(pool, struct tts_port);
  status = pjmedia_port_info_init(&port->base.info, &TTS_GEN,
                                  PJMEDIA_SIG_PORT_MEM_CAPTURE, g_var.clock_rate, 1,
                                  16,
                                  g_var.clock_rate * 20 / 1000 // samples per frame
                                  );
  port->base.put_frame = &tts_put_frame;
  port->base.get_frame = &tts_get_frame;
  port->base.on_destroy = &tts_on_destroy;
  port->result_q = result_q;
  *p_port = &port->base;
  return status;
}

void push_sound_frame(std::string file_name, SafeQueue<SoundFrame>* q) {
  //int fd = open("/home/ducbin/maum/samples/8k.pcm", O_RDONLY);
  int fd = open(file_name.c_str(), O_RDONLY);
  if (fd < 0) {
    perror("");
    return;
  } else {
    // Success
  }

  SoundFrame frame;
  //g_var.tts_samplerate * 20 / 1000
  int read_size = g_var.clock_rate * 20 / 1000 * 2;
  if (read(fd, frame.data, read_size) < 44) {
    // TODO : not wav file
    close(fd);
    return;
  }

  //while ((frame.size = read(fd, frame.data, sizeof(frame.data))) > 0) {
  while ((frame.size = read(fd, frame.data, read_size)) > 0) {
    q->push(frame);
  }
  close(fd);
}

void push_sound_stream(std::string &stream,
                       SafeQueue<SoundFrame>* q,
                       int utter_no, int sent_no,
                       bool is_last) {
  SoundFrame frame;
  frame.utter_no = utter_no;
  frame.sent_no = sent_no;
  // 20 ms = 0.02초
  size_t read_size = g_var.clock_rate * 20 / 1000 * 2;
  int consume = 0;

  // 정확히 프레임 사이즈만큼 보내지 않으면 음성에 잡음이 많이 섞입니다
  for (size_t pos = 0; stream.size() - pos >= read_size; pos += frame.size) {
    frame.size = stream.copy(frame.data, read_size, pos);
    consume += frame.size;
    q->push(frame);
  }

  if (is_last) {
    frame.size = stream.copy(frame.data, read_size, consume);
    consume += frame.size;
    q->push(frame);
  }
  stream = stream.substr(consume);
}

void push_sound_silence(SafeQueue<SoundFrame> *q, float seconds) {
  SoundFrame frame;
  // sample_rate * 시간 * sample 크기
  frame.size = g_var.clock_rate * 20 / 1000 * 2;
  memset(frame.data, 0, frame.size);

  // 1 : (1000 / 20) = seconds : x
  for (int i = 0; i < seconds * (1000 / 20) /* 1 second */; i++) {
    q->push(frame);
  }
}
