#ifndef TTS_PORT_H
#define TTS_PORT_H

#include <pjsua-lib/pjsua.h>
#include "safe-queue.h"

struct tts_port {
  pjmedia_port base;
  pjsua_conf_port_id p_id;
  SafeQueue<SoundFrame>* result_q;
};

/**
 * tts_port put_frame callback function
 *
 * @param tts_port의 포인터
 * @param tts_port 음성 데이타
 * @return 성공, 실패 표시
 */
pj_status_t tts_put_frame(pjmedia_port *this_port, pjmedia_frame *frame);

/**
 * tts_port get_frame callback function
 *
 * @param tts_port의 포인터
 * @param tts_port 음성 데이타
 * @return 성공, 실패 표시
 */
pj_status_t tts_get_frame(pjmedia_port *this_port, pjmedia_frame *frame);

/**
 * tts_port on_destroy callback function
 *
 * @param tts_port의 포인터
 * @param tts_port 음성 데이타
 * @return 성공, 실패 표시
 */
pj_status_t tts_on_destroy(pjmedia_port *this_port);

/**
 * tts_port 생성 함수
 *
 * @param 메모리 할당에 필요한 pool
 * @param 새로 할당될 pjmedia_port 포인터
 * @param sip로 전송될 음성 데이터큐
 * @return 성공, 실패 표시
 */
pj_status_t pjmedia_tts_create(pj_pool_t *pool,
                               pjmedia_port **p_port,
                               SafeQueue<SoundFrame>* result_q);

void push_sound_frame(std::string file_name, SafeQueue<SoundFrame>* q);

void push_sound_stream(std::string &stream,
                       SafeQueue<SoundFrame>* q,
                       int utter_no, int sent_no, bool is_last = false);

void push_sound_silence(SafeQueue<SoundFrame>* q);

#endif /* TTS_PORT_H */
