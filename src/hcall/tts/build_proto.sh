#!/bin/sh
#python -m grpc_tools.protoc -I. -I$MAUM_ROOT/include --python_out=. --grpc_python_out=. ng_tts.proto
#python -m grpc_tools.protoc -I. -I$MAUM_ROOT/include --python_out=. --grpc_python_out=. tts.proto

protoc -I. -I$MAUM_ROOT/include --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ng_tts.proto
protoc -I. -I$MAUM_ROOT/include --cpp_out=. ng_tts.proto
