#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import grpc
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2
from multiprocessing import Process
import biz_log

logger = biz_log.getLogger('test')

TEXT = '''안녕하세요 마인즈랩입니다'''
TEXT = '''지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다'''
TEXT = '''말씀해주셔서 감사합니다, 고객님의 주소는 경기도 성남시 중원구 성남동, 사공오일번지로'''
TEXT = '''지금부터 진행하는 내용은 고객님의 권리 보호를 위해 녹음되며'''
# TTS 발음이 이상한 경우
TEXT = '''지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다'''
TEXT = '''00보험에서 00만원 00보험에서 00만원 대출 실행됩니다'''
TEXT = '고객님께서 선택하신 계약대출 신청을 도와 드리겠습니다'
TEXT = '또한 금융사기 예방을 위해 본인 명의의 휴대폰 소지하셔야 합니다'
ADDR = ''
SDN = '127.0.0.1:50051'
#SDN = '10.122.64.152:50051'
# OLD
#TTS = '10.122.64.83:9999'
# 상담사 목소리
TTS = '10.122.64.65:10000'
TTS = '10.122.64.191:9999'
TTS = '10.122.64.192:9999'
# 시니어케어 목소리
#TTS = '10.122.64.82:9999'
TTS = '182.162.19.20:9999'

def test(index):
    channel = grpc.insecure_channel(ADDR)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    #req.sampleRate = 16000
    req.sampleRate = 8000
    req.text = TEXT
    req.speaker = 0

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        logger.info('[%03d ] start read wav file' % index)
        f = open('test%02d.wav' % index, 'w')
        for tts in resp:
            #logger.debug('write file')
            f.write(tts.mediaData)
    except grpc.RpcError as e:
        print e
    logger.info('end time is %.3f' % (time.time() - st))
    f.close()

def main():
    global ADDR
    cmd = raw_input('Select Server - 1. SDN (%s), 2. TTS (%s): ' % (SDN, TTS))
    if cmd == '1':
        ADDR = SDN
    else:
        ADDR = TTS

    cmd = raw_input('Select Concurrent count: ')
    concurrent_cnt = int(cmd)

    logger.info('start...')
    plist = []
    st = time.time()
    for i in range(concurrent_cnt):
        p = Process(target=test, args=(i,))
        p.start()
        plist.append(p)

    for p in plist:
        p.join()
    logger.info('total time is %.3f' % (time.time() - st))

if __name__ == '__main__':
    main()

