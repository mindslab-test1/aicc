#include "util.h"
#include <stdio.h>

#include <libmaum/common/config.h>
#include <spdlog/spdlog.h>
#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/rotating_file_sink.h>
#endif

using namespace std;

std::vector<string> split(const string &str, const string &delim) {
  std::vector<string> tokens;

  string::size_type prev = 0, pos = 0;
  do {
    pos = str.find(delim, prev);
    if (pos == string::npos)
      pos = str.length();
    string token = str.substr(prev, pos - prev);
    if (!token.empty())
      tokens.push_back(token);
    prev = pos + delim.length();
  } while (pos < str.length() && prev < str.length());
  return tokens;
}

std::string to_string(timeval &tv) {
  char buf[64];
  tm tm;

  localtime_r(&tv.tv_sec, &tm);
  snprintf(buf, sizeof(buf), "%d-%02d-%02d %02d:%02d:%02d.%03ld",
           tm.tm_year + 1900,
           tm.tm_mon + 1,
           tm.tm_mday,
           tm.tm_hour,
           tm.tm_min,
           tm.tm_sec,
           tv.tv_usec / 1000
           );
  return std::string(buf);
}

std::shared_ptr<spdlog::logger> CreateLogger(std::string logger_name) {
  std::string maum_root = std::getenv("MAUM_ROOT");
  std::string file_path = maum_root + "/logs/" + logger_name + ".log";

  auto rotating_logger =
      spdlog::rotating_logger_mt(logger_name,
                                 file_path,
                                 1048576 * 100,  // 100 MBytes
                                 10);            // backup count
  return rotating_logger;
}
