#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <string>
#include <sys/time.h>
#include <spdlog/spdlog.h>

std::vector<std::string> split(const std::string &str, const std::string &delim);
std::string to_string(timeval &tv);
std::shared_ptr<spdlog::logger> CreateLogger(std::string logger_name);

#endif /* UTIL_H */

