#include <time.h>
#include "app-variable.h"
#include "voice-dialog.h"
#include "call-status.h"
#include "tts-port.h"

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <grpc++/grpc++.h>
#include "tts/ng_tts.grpc.pb.h"
#include "tts/ng_tts.pb.h"

#include "zhelpers.h"

using grpc::ClientContext;
using grpc::Status;

using namespace maum::brain::tts;
using namespace std::chrono;
namespace placeholders = std::placeholders;

extern void push_sound_silence(SafeQueue<SoundFrame> *q, float seconds);
extern void push_sound_stream(std::string &stream, SafeQueue<SoundFrame>* q, bool is_last = false);

VoiceDialog::VoiceDialog(std::string m2u_addr,
                         std::string chatbot_id, TtsQueue *q, ba::io_service &ios) :
    tts_q_(q),
    ios_(ios),
    timer_(ios),
    chatbot_id_(chatbot_id),
    m2u_(ios, m2u_addr, g_var.contract_no, g_var.campaign_id, std::to_string(g_var.call_history_id)) {
  IsDTMF = false;
  IsDTMFSingle = false;
  IsComplete = false;
  IsMultimodal = false;
  SentenceNum = 1;
  sentence_no_ = 0;
}

VoiceDialog::~VoiceDialog() {
}

void VoiceDialog::OpenDialog(std::string call_id) {
  m2u_.call_id_ = call_id;
  m2u_.authenticate();
  m2u_.open_dialog(chatbot_id_);
  // 2021-06-16 by shinwc
  // To calculate timestamp related TTS task
  gettimeofday(&start_tv, NULL);
}

dict VoiceDialog::SendText(std::string text, std::string doing_tts, dict meta) {
  timer_.cancel();

  if (doing_tts == "Y") {
    meta["current_utter"] =
        std::to_string(tts_q_->UtterNo) + "-" + std::to_string(tts_q_->SentenceNo);
  }
  meta["campaign_id"] = g_var.campaign_id;
  g_logger->info("campaign_id: {}", g_var.campaign_id);
  g_logger->info("SendText: {}", text);
  return m2u_.text_to_text_talk(text, doing_tts, GetStatus(), meta);
}

void VoiceDialog::AsyncSendText(std::string text, std::string doing_tts, dict meta) {
  timer_.cancel();

  if (doing_tts == "Y") {
    meta["current_utter"] =
        std::to_string(tts_q_->UtterNo) + "-" + std::to_string(tts_q_->SentenceNo);
  }
  meta["campaign_id"] = g_var.campaign_id;
  g_logger->info("campaign_id: {}", g_var.campaign_id);
  g_logger->info("SendText: {}", text);
  m2u_.async_text_to_text_talk(
      text, doing_tts, GetStatus(), meta, std::bind(&VoiceDialog::ProcessAnswer, this, placeholders::_1));
}

void VoiceDialog::CloseDialog() {
  g_logger->info("CloseDialog");
  m2u_.close_dialog();
}

int VoiceDialog::DoTTS(std::string text, SafeQueue<SoundFrame> *result_q, string utter_no, int sent_no) {
  auto channel = grpc::CreateChannel(g_var.sdn_addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<NgTtsService::Stub> stub(NgTtsService::NewStub(channel));

  ClientContext ctx;

  TtsRequest req;
  req.set_lang(maum::common::Lang::ko_KR);
  // NO EFFECT ! Not implemented yet?
  // req.set_samplerate(16000);
  req.set_text(text);

  ctx.AddMetadata("tempo", std::to_string(g_var.tts_tempo));
  ctx.AddMetadata("campaign", g_var.campaign_id);
  ctx.AddMetadata("samplerate", std::to_string(g_var.clock_rate));
  auto deadline = system_clock::now() + seconds(g_var.tts_deadline);
  ctx.set_deadline(deadline);

  auto stream = stub->SpeakWav(&ctx, req);

  size_t duration = 0;
  bool remove_header = false;
  TtsMediaResponse resp;
  std::string remained;
  bool wait_a_moment = false;

  while (stream->Read(&resp)) {
    std::string bytes = resp.mediadata();
    if (!remove_header && bytes.size() >= 44) {
      bytes = bytes.substr(44);
      remove_header = true;
    }
    remained += bytes;
    duration += bytes.size();
    // if (remained.size() >= (16000 * 500 / 1000 * 2) /* 500ms */) {
    //   wait_a_moment = false;
    // }
    if (!wait_a_moment && !remained.empty()) {
      push_sound_stream(remained, result_q, atoi(utter_no.c_str()), sent_no);
    }
  }
  Status status = stream->Finish();

  if (!status.ok()) {
    g_logger->debug("SDN error message: {}", status.error_message());
  }

  if (!remained.empty()) {
    push_sound_stream(remained, result_q, sentence_no_, true);
  }

  g_logger->debug("TTS Done");
  return (duration * 100) / (g_var.clock_rate * 2);
}

void VoiceDialog::CheckStatus(dict &result) {
  auto status_kv = result.find("status");
  if (status_kv != result.end()) {
    if (status_kv->second == "complete") {
      IsComplete = true;
      SetStatus(CS_AI_COMPLETED);
    } else if (status_kv->second == "callback") {
      IsComplete = true;
      SetStatus(CS_DROPPED);
    } else if (status_kv->second == "unConnected") {
      IsComplete = true;
      SetStatus(CS_STOPPED);
    } else if (status_kv->second == "transferred") {
      // IsComplete = true;
      // SetStatus(CS_AI_COMPLETED);
      if (result["transfer"] != "agent") {
        SetStatus(CS_AGENT_TRANSFERRED);
      }
    }
  }
}

void VoiceDialog::GenerateTTS(string &text, string delimiter, string &utter_no) {
  // 답변을 문장별로 분리한 후 문장에 대한 음성을 TTS로부터 받아와서 내보낸다
  std::vector<std::string> sentences;
  if (delimiter.empty()) {
    delimiter = ".";
  }
  boost::split(sentences, text, boost::is_any_of(delimiter));

  // FIXME
  // TTS 관련 시간 계산
  timeval now, duration_tv;
  gettimeofday(&now, NULL);
  timersub(&now, &start_tv, &duration_tv);
  long int millis = (duration_tv.tv_sec * 1000) + (duration_tv.tv_usec / 1000);

  //int last_speak_time = 0;
  int last_speak_time = (int)(millis/10);
  int sent_no = 1;
  std::unique_lock<std::mutex> lock_(tts_lock_);
  for (auto sentence : sentences) {
    sentence.erase(0, sentence.find_first_not_of("\t\n\v\f\r "));
    if (sentence.empty()) {
      continue;
    }
    g_logger->info("TTS Request: [{}]", sentence);

    // TTS 로부터 음성을 가져와서 tts_port 큐에 입력
    int duration = DoTTS(sentence, tts_q_, utter_no, sent_no);
    g_logger->info("TTS Result: [{}], {} ~ {}", sentence, last_speak_time, last_speak_time + duration);
    g_var.db.InsertResult("ST0002", SentenceNum++, sentence, last_speak_time, last_speak_time + duration);
    websock->publish(last_speak_time, last_speak_time + duration, "TX", sentence, 'N');
    last_speak_time += duration;
    sent_no++;
  }
}

void VoiceDialog::ResponseLog(dict &result) {
  // LOG
  std::string tmp;
  for (auto kv : result) {
    tmp += "\tKEY: " + kv.first + ",\tVALUE: " + kv.second + "\n";
  }
  g_logger->info("M2U Response: \n{}", tmp);
}

void VoiceDialog::ProcessAnswer(dict result) {
  ResponseLog(result);

  // 답변
  std::string answer = result["text"];

  if (result["extra"] == "both") {
    IsDTMF = true;
    IsMultimodal = true;
  } else if (result["extra"] == "dtmf_on") {
    IsDTMF = true;
    IsMultimodal = false;
  } else if (result["extra"] == "dtmf_single") {
    IsDTMF = true;
    IsDTMFSingle = true;
    IsMultimodal = false;
  } else if (result["extra"] == "dtmf_off") {
    IsDTMF = false;
    IsDTMFSingle = false;
    IsMultimodal = false;
  }

  // TTS 중지 설정 (TTS 중에 고객 발화가 입력된 경우 처리)
  if (tts_q_ != NULL && result["tts"] == "stop") {
    std::unique_lock<std::mutex> lock(tts_lock_);
    tts_q_->clear();
    std::string silence_duration = result["silence"];
    if (!silence_duration.empty()) {
      // DA 설정 묵음 추가
      float seconds = std::stof(silence_duration);
      push_sound_silence(tts_q_, seconds);
    } else {
      // 기본 묵음 1초 추가
      push_sound_silence(tts_q_, 1);
    }
  }

  if (!result["tts_tempo"].empty()) {
    g_var.tts_tempo = std::stof(result["tts_tempo"]);
  }

  if (!result["stt"].empty() && result["stt"] != stt_model_name_) {
    SetSttModelName(result["stt"]);
    if (SttContext) SttContext->TryCancel();
  }

  if (tts_q_->PlayingMOH) {
    tts_q_->PlayingMOH = false;
  }

  if (result["long_task"] == "prepared") {
    dict meta;
    meta["long_task"] = "start";
    AsyncSendText("long_task", "N", meta);
    tts_q_->PlayingMOH = true;
  }

  CheckStatus(result);
  GenerateTTS(answer, result["delimiter"], result["utter_no"]);

  // agent 전환
  if (!result["transfer"].empty()) {
    SetPhoneNo(result["transfer"]);
    g_logger->debug("Waiting TTS transfer voice...");
    for (int i = 0; i < 600 /* 0.1초 * 600 = 60초 */ && !tts_q_->empty(); i++) {
      usleep(100 * 1000);
    }
    if (result["moh"] == "true") {
      tts_q_->PlayingMOH = true;
    }

    APP_MESSAGE_T msg;
    msg.call_id = call_id_;
    msg.command = GetPhoneNo() == "agent" ?
                  APP_COMMAND_ENUM::CALL_TRANSFER : APP_COMMAND_ENUM::REFER;
    strncpy(msg.agent_id, GetPhoneNo().c_str(), AGENT_ID_MAX_LEN);
    zmq_send(g_var.evnt_sender, (char *)&msg, sizeof(APP_MESSAGE_T), ZMQ_NOBLOCK);
    if (GetPhoneNo() == "agent") {
      websock->publish_transfer();
    }
    return;
  }

  if (IsComplete) {
    StopSTT();
  } else {
    int duration = ((g_var.resp_timeout + tts_q_->size() * 0.02) * 1000);
    timer_.expires_from_now(boost::posix_time::milliseconds(duration));
    timer_.async_wait(boost::bind(&VoiceDialog::OnTimeout, this, _1));
  }
}

void VoiceDialog::RefreshTimer() {
  int duration = ((g_var.resp_timeout + tts_q_->size() * 0.02) * 1000);
  // Return Value
  // The number of asynchronous operations that were cancelled.
  timer_.expires_from_now(boost::posix_time::milliseconds(duration));
  timer_.async_wait(boost::bind(&VoiceDialog::OnTimeout, this, _1));
}

void VoiceDialog::OnTimeout(const boost::system::error_code& e) {
  if (e != boost::asio::error::operation_aborted) {
    // timer was not cancelled
    g_var.logger->debug("Response timer expired...");
    std::string ignored = tts_q_->empty() ? "N" : "Y";
    AsyncSendText("...", ignored);
  }
}

std::string VoiceDialog::GetStatus() {
  std::unique_lock<std::mutex> lock(prop_lock_);
  return status_;
}

std::string VoiceDialog::GetSttModelName() {
  std::unique_lock<std::mutex> lock(prop_lock_);
  return stt_model_name_;
}

std::string VoiceDialog::GetPhoneNo() {
  std::unique_lock<std::mutex> lock(prop_lock_);
  return phone_no_;
}

void VoiceDialog::SetStatus(std::string status) {
  std::unique_lock<std::mutex> lock(prop_lock_);
  status_ = status;
}

void VoiceDialog::SetSttModelName(std::string name) {
  std::unique_lock<std::mutex> lock(prop_lock_);
  stt_model_name_ = name;
}

void VoiceDialog::SetPhoneNo(std::string num) {
  std::unique_lock<std::mutex> lock(prop_lock_);
  phone_no_ = num;
}

void VoiceDialog::SetCallID(int id) {
  std::unique_lock<std::mutex> lock(prop_lock_);
  call_id_ = id;
}
