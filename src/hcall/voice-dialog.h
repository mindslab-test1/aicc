#ifndef VOICE_DIALOG_H
#define VOICE_DIALOG_H

#include <string>
#include "ws-client.h"
#include "m2u-client.h"
#include "safe-queue.h"

using TtsQueue = SafeQueue<SoundFrame>;
namespace ba = boost::asio;

class VoiceDialog {
 public:
  VoiceDialog(std::string m2u_addr,
              std::string chatbot_id,
              TtsQueue *q,
              ba::io_service &ios);
  ~VoiceDialog();

  void OpenDialog(std::string call_id);
  dict SendText(std::string text, std::string doing_tts, dict meta = dict());
  void AsyncSendText(std::string text, std::string doing_tts, dict meta = dict());
  void CloseDialog();

  int  DoTTS(std::string text, TtsQueue *result_q, std::string utter_no, int sent_no);
  void ResponseLog(dict &result);

  std::function<void()> StopSTT;

  // CALLBACK
  void ProcessAnswer(dict result);

  void RefreshTimer();
  void OnTimeout(const boost::system::error_code& e);

  // PROPERTY
  std::string GetStatus();
  std::string GetSttModelName();
  std::string GetPhoneNo();

  void SetStatus(std::string status);
  void SetSttModelName(std::string status);
  void SetPhoneNo(std::string status);
  void SetCallID(int id);

  std::atomic<bool> IsDTMF;
  std::atomic<bool> IsDTMFSingle;
  std::atomic<bool> IsComplete;
  std::atomic<int>  SentenceNum;
  std::atomic<bool> IsMultimodal;

  std::shared_ptr<grpc::ClientContext> SttContext;
  std::shared_ptr<WebsocketClient> websock;

 private:
  // INTERNAL
  void CheckStatus(dict &result);
  void GenerateTTS(std::string &text, std::string delimiter, std::string &utter_no);

  int call_id_;
  int utter_no_;
  int sentence_no_;
  std::string status_;
  std::string stt_model_name_;
  std::string phone_no_;

  std::mutex prop_lock_;
  std::mutex tts_lock_;

  TtsQueue *tts_q_;

  boost::asio::io_service &ios_;
  boost::asio::deadline_timer timer_;

  std::string chatbot_id_;
  M2uClient m2u_;

  timeval start_tv;
};

#endif /* VOICE_DIALOG_H */
