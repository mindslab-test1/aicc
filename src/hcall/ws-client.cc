#include "ws-client.h"
#include "app-variable.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>

using namespace rapidjson;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

namespace asio = websocketpp::lib::asio;

// pull out the type of messages sent by our config
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;

WebsocketClient::WebsocketClient(asio::io_service &service)
    : service_(service),
      timer_(service),
      is_opened_(false) {
  m_endpoint.set_access_channels(websocketpp::log::alevel::all);
  m_endpoint.clear_access_channels(websocketpp::log::alevel::frame_payload);

  // m_endpoint.clear_access_channels(websocketpp::log::alevel::all);
  // m_endpoint.clear_error_channels(websocketpp::log::elevel::all);

  m_endpoint.init_asio(&service_);
  // m_endpoint.start_perpetual();
}

WebsocketClient::~WebsocketClient() {
  websocketpp::lib::error_code ec;
  if (is_opened_) {
    m_endpoint.close(con_->get_handle(), websocketpp::close::status::going_away, "", ec);
  }
  // m_endpoint.stop_perpetual();
}

void WebsocketClient::on_timeout(const boost::system::error_code& error) {
  cnt++;
  if (timer_.expires_at() <= boost::asio::deadline_timer::traits_type::now()) {
    printf("%03d: timeout\n", cnt);
    send("123456789012345");
  }
  else {
    printf("not timeout \n");
  }
  timer_.expires_from_now(boost::posix_time::seconds(2));
  timer_.async_wait(boost::bind(&WebsocketClient::on_timeout,
                                shared_from_this(),
                                boost::asio::placeholders::error));
}

void WebsocketClient::on_open(websocketpp::connection_hdl) {
  is_opened_ = true;
}

void WebsocketClient::wait() {
  while (!is_opened_) {
    if (cnt > 200) break;
    usleep(10 * 1000);
  }
}

int WebsocketClient::connect(std::string const & uri) {
  websocketpp::lib::error_code ec;

  con_ = m_endpoint.get_connection(uri, ec);
  // client::connection_ptr con = m_endpoint.get_connection(uri, ec);

  if (ec) {
    std::cout << "> Connect initialization error: " << ec.message() << std::endl;
    return -1;
  }

  con_->set_open_handler(websocketpp::lib::bind(
      &WebsocketClient::on_open,
      this,
      websocketpp::lib::placeholders::_1
  ));

  // int new_id = m_next_id++;
  // connection_metadata::ptr metadata_ptr = websocketpp::lib::make_shared<connection_metadata>(new_id, con->get_handle(), uri);
  // m_connection_list[new_id] = metadata_ptr;
#if 0
  con->set_open_handler(websocketpp::lib::bind(
      &connection_metadata::on_open,
      metadata_ptr,
      &m_endpoint,
      websocketpp::lib::placeholders::_1
                                               ));
  con->set_fail_handler(websocketpp::lib::bind(
      &connection_metadata::on_fail,
      metadata_ptr,
      &m_endpoint,
      websocketpp::lib::placeholders::_1
                                               ));
  con->set_close_handler(websocketpp::lib::bind(
      &connection_metadata::on_close,
      metadata_ptr,
      &m_endpoint,
      websocketpp::lib::placeholders::_1
                                                ));
  con->set_message_handler(websocketpp::lib::bind(
      &connection_metadata::on_message,
      metadata_ptr,
      websocketpp::lib::placeholders::_1,
      websocketpp::lib::placeholders::_2
                                                  ));
#endif
  m_endpoint.connect(con_);

  // timer_.expires_from_now(boost::posix_time::seconds(2));
  // timer_.async_wait(boost::bind(&WebsocketClient::on_timeout, shared_from_this(), boost::asio::placeholders::error));

  // return new_id;
  return 0;
}

void WebsocketClient::close(int id, websocketpp::close::status::value code, std::string reason) {
  websocketpp::lib::error_code ec;
        
  // con_list::iterator metadata_it = m_connection_list.find(id);
  // if (metadata_it == m_connection_list.end()) {
  //     std::cout << "> No connection found with id " << id << std::endl;
  //     return;
  // }
        
  // m_endpoint.close(metadata_it->second->get_hdl(), code, reason, ec);
  // if (ec) {
  //     std::cout << "> Error initiating close: " << ec.message() << std::endl;
  // }
}

void WebsocketClient::send(std::string message) {
  if (!con_) {
    return;
  }

  websocketpp::lib::error_code ec;
  m_endpoint.send(con_->get_handle(), message, websocketpp::frame::opcode::text, ec);
  if (ec) {
    std::cout << "> Error sending message: " << ec.message() << std::endl;
    return;
  }
}

void WebsocketClient::publish(int start, int end, char *direction, std::string txt, char ignored) {
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  char str_start[64];
  char str_end[64];
  char str_ignored[2];
  sprintf(str_start, "%.2f", start / 100.0);
  sprintf(str_end, "%.2f", end / 100.0);
  sprintf(str_ignored, "%c", ignored);

  std::string strstart = std::to_string(start / 100.0);
  document.AddMember("EventType",   "STT", allocator);
  document.AddMember("Event",       "interim", allocator);
  document.AddMember("Caller",      Value(g_var.tel_no.c_str(), allocator).Move(), allocator);
  document.AddMember("Agent",       Value(g_var.sip_user.c_str(), allocator).Move(), allocator);
  document.AddMember("Direction",   StringRef(direction), allocator); // RX, TX
  document.AddMember("Start",       StringRef(str_start), allocator);
  document.AddMember("End",         StringRef(str_end), allocator);
  document.AddMember("Text",        StringRef(txt.c_str()), allocator);
  document.AddMember("contract_no", StringRef(g_var.contract_no.c_str()), allocator);
  document.AddMember("ignored",     StringRef(str_ignored), allocator);

  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  this->send(strbuf.GetString());
}


void WebsocketClient::publish_event(string call_status) {
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  document.AddMember("EventType",   "CALL",   allocator);
  document.AddMember("Event",       "status", allocator);
  document.AddMember("Caller",      Value(g_var.tel_no.c_str(), allocator).Move(), allocator);
  document.AddMember("Agent",       Value(g_var.sip_user.c_str(), allocator).Move(), allocator);
  document.AddMember("contract_no", Value(g_var.contract_no.c_str(), allocator).Move(), allocator);
  document.AddMember("call_status", Value(call_status.c_str(), allocator).Move(), allocator);

  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  this->send(strbuf.GetString());
}

void WebsocketClient::publish_transfer() {
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  document.AddMember("EventType",   "CALL",   allocator);
  document.AddMember("Event",       "transfer", allocator);
  document.AddMember("Caller",      Value(g_var.tel_no.c_str(), allocator).Move(), allocator);
  document.AddMember("Agent",       Value(g_var.sip_user.c_str(), allocator).Move(), allocator);
  document.AddMember("contract_no", Value(g_var.contract_no.c_str(), allocator).Move(), allocator);

  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  this->send(strbuf.GetString());
}

void WebsocketClient::publish_signal(char *signal_event) {
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

  document.AddMember("EventType",   "CALL",   allocator);
  document.AddMember("Event",       StringRef(signal_event), allocator);
  document.AddMember("Caller",      Value(g_var.tel_no.c_str(), allocator).Move(), allocator);
  document.AddMember("Agent",       Value(g_var.sip_user.c_str(), allocator).Move(), allocator);
  document.AddMember("contract_no", Value(g_var.contract_no.c_str(), allocator).Move(), allocator);

  StringBuffer strbuf;
  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);

  this->send(strbuf.GetString());
}

