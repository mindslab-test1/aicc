#ifndef WS_CLIENT_H
#define WS_CLIENT_H

#include <memory>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include "websocketpp/config/asio_no_tls_client.hpp"
#include "websocketpp/client.hpp"

typedef websocketpp::client<websocketpp::config::asio_client> wspp_client;

class WebsocketClient : public boost::enable_shared_from_this<WebsocketClient>
{
public:
  WebsocketClient (boost::asio::io_service &service);
  ~WebsocketClient();

  int  connect(std::string const & uri);
  void close(int id, websocketpp::close::status::value code, std::string reason);
  void send(std::string message);
  void wait();

  // AICC Specific Message
  void publish(int start, int end, char *direction, std::string txt, char ignored = 'N');
  void publish_event(std::string call_status);
  void publish_transfer();
  void publish_signal(char *signal_event);

private:
  void on_open(websocketpp::connection_hdl);
  void on_timeout(const boost::system::error_code& error);

  boost::asio::io_service &service_;
  wspp_client::connection_ptr con_;
  wspp_client m_endpoint;
  boost::asio::deadline_timer timer_;

  bool is_opened_;
  int  cnt;
};


#endif /* WS_CLIENT_H */
