#include "ws-client.h"

int main(int argc, char *argv[])
{
  boost::asio::io_service service;

  std::string uri = "ws://localhost:13254/callsocket";

  std::shared_ptr<WebsocketClient> ep = std::make_shared<WebsocketClient>(service);
  // websocket_endpoint ep(service);
  ep->connect(uri);

  // client c;
  // // c.set_access_channels(websocketpp::log::alevel::all);
  // c.clear_access_channels(websocketpp::log::alevel::frame_payload);
  // c.init_asio(&service);

  websocketpp::lib::error_code ec;
  // client::connection_ptr con = c.get_connection(uri, ec);
  // if (ec) {
  //   std::cout << "could not create connection because: " << ec.message() << std::endl;
  //   return 0;
  // }

  // con->set_fail_handler(websocketpp::lib::bind(
  //     on_fail,
  //     &client,
  //     websocketpp::lib::placeholders::_1));

  // c.connect(con);
  try {
    service.run();
  }
  catch (std::exception e) {
  }
  return 0;
}
