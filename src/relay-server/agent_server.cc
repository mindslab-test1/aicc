#include <iostream>
#include "agent_server.h"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "app-variable.h"
#include "relay_session.h"
#include "http_api.h"

void connection_data::send_disconnect(int stype) {
  websocketpp::connection<custom_config> *conn;
  conn = (websocketpp::connection<custom_config> *)this;
  std::string payload("123");
  conn->send(payload);
}

void connection_data::do_write(char *data, std::size_t len) {
  websocketpp::connection<custom_config> *conn;
  conn = (websocketpp::connection<custom_config> *)this;
  printf("len is %ld\n", len);
  if (len > sizeof(AgentMessageHeader) ) {
    // remove header
    data += sizeof(AgentMessageHeader);
    len  -= sizeof(AgentMessageHeader);
  }
  conn->send((void *)data, len);
}

void connection_data::send_ringing() {
}

void connection_data::unregister_this() {
  g_var.pc_agents.erase(agent_id_);
  if (ai_agent_) {
    ai_agent_->send_disconnect(PC_AGENT);
    ai_agent_->unregister_other();
    ai_agent_.reset();
  }
}

AgentServer::AgentServer() {
}

AgentServer::~AgentServer() {
}

void AgentServer::Start(int port) {
  run_thread_ = std::thread(([this, port](){
        try {
          // Set logging settings
          // server_.set_access_channels(websocketpp::log::alevel::all);
          // server_.clear_access_channels(websocketpp::log::alevel::frame_payload);
          server_.clear_access_channels(websocketpp::log::alevel::all);

          // Initialize Asio
          server_.init_asio();
          server_.set_reuse_addr(true);

          // Register our message handler
          server_.set_open_handler(bind(&AgentServer::on_open, this, &server_,::_1));
          server_.set_close_handler(bind(&AgentServer::on_close, this, &server_,::_1));
          server_.set_message_handler(bind(&AgentServer::on_message, this, &server_,::_1,::_2));

          // Listen on port 9002
          server_.listen(port);

          // Start the server accept loop
          server_.start_accept();

          // Start the ASIO io_service run loop
          server_.run();
        } catch (websocketpp::exception const & e) {
          std::cout << e.what() << std::endl;
#if 0
        } catch (...) {
          std::cout << "other exception" << std::endl;
#endif
        }
      }));
}

void AgentServer::on_open(server* s, websocketpp::connection_hdl hdl) {
  printf("on open\n");
  server::connection_ptr con = s->get_con_from_hdl(hdl);
  printf("uri: %s\n", con->get_resource().c_str());
}

void AgentServer::on_close(server* s, websocketpp::connection_hdl hdl) {
  server::connection_ptr con = s->get_con_from_hdl(hdl);
  con->unregister_this();
  printf("on close\n");
}

void AgentServer::on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg) {
    // std::cout << "on_message called with hdl: " << hdl.lock().get()
    //           << " and message: " << msg->get_payload()
    //           << std::endl;

    auto opcode = msg->get_opcode();
    std::cout << "op_code:" << opcode << std::endl;
    if (msg->get_opcode() == websocketpp::frame::opcode::text) {
        process_message(s, hdl, msg);
        printf("get text opcode\n");
    } else if (opcode == websocketpp::frame::opcode::binary) {
        process_transfer(s, hdl, msg);
        // TRANSFER
        printf("get blob opcode\n");
    }
    // check for a special command to instruct the server to stop listening so
    // it can be cleanly exited.
    if (msg->get_payload() == "stop-listening") {
        s->stop_listening();
        return;
    }

    // try {
    //     s->send(hdl, msg->get_payload(), msg->get_opcode());
    // } catch (websocketpp::exception const & e) {
    //     std::cout << "Echo failed because: "
    //               << "(" << e.what() << ")" << std::endl;
    // }
}

#define GET_JSON_VALUE(r, p)                    \
  do {                                          \
    rapidjson::Value* v;                        \
    if ((v = GetValueByPointer(document, p))) { \
      r = v->GetString();                       \
    }                                           \
  } while (0)

void AgentServer::process_register(
    server* s, websocketpp::connection_hdl hdl, rapidjson::Document &document) {
  std::string agent_id, trans_type;
  GET_JSON_VALUE(agent_id, "/agent_id");
  GET_JSON_VALUE(trans_type, "/trans_type");

  if (agent_id.empty()) {
    return;
  }

  auto conn = s->get_con_from_hdl(hdl);
  conn->agent_id_ = agent_id;
  g_var.pc_agents[agent_id] = conn;
  g_logger->info("상담사 ID {} 가 등록되었습니다. 현재 등록된 상담사는 {} 명입니다.",
                 agent_id, g_var.pc_agents.size());

  if (trans_type == "recv_only") {
    std::string dn;
    GET_JSON_VALUE(dn, "/dial_no");
    auto agent_iter = g_var.play_agents.find(dn);
    if (agent_iter != g_var.play_agents.end()) {
      (*agent_iter).second->register_subscriber(agent_id, conn);
    } else {
      g_logger->info("ai agent not registered. dn = {}", dn);
      std::string body = fmt::format(QUOTE(
          {{
            "dialNo": "{}",
            "agentId": "{}"
          }}
      ), dn, agent_id);
      if (!send_http_post_json(g_var.play_url, body)) {
        g_logger->error("http post failed, agent_id = {}", agent_id);
      }
    }
  }
  else if (trans_type == "recv_send") {
    std::string dn;
    GET_JSON_VALUE(dn, "/dial_no");
    auto it = g_var.ai_agents.find(dn);
    if (it != g_var.ai_agents.end()) {
      it->second->register_other(PC_AGENT, conn);
      conn->ai_agent_ = it->second;
    } else {
      g_logger->info("해당 DialNo({})의 소프트폰이 존재하지 않습니다.", dn);
      std::string body = fmt::format(QUOTE(
          {{
            "dialNo": "{}",
            "agentId": "{}"
          }}
      ), dn, agent_id);
      if (!send_http_post_json(g_var.transfer_url, body)) {
        g_logger->error("http post failed, agent_id = {}", agent_id);
      }
    }
  }
}

// std::string getJsonValue(rapidjson::Document &document, const char path[]) {
//   rapidjson::Value* v;
//   std::string value;
//   if ((v = GetValueByPointer(document, path))) {
//     value = v->GetString();
//   }
//   return std::move(value);
// }

void AgentServer::process_transfer(server* s, websocketpp::connection_hdl hdl, message_ptr msg) {
  auto conn = s->get_con_from_hdl(hdl);
  if (conn && conn->ai_agent_) {
    const std::string &payload(msg->get_payload());
    AgentMessage message;
    message.SetVersion();
    message.SetSystemType(PC_AGENT);
    message.SetOpCode(TRANSFER);

    size_t offset = 0;
    while (offset < payload.length()) {
      std::string body = payload.substr(offset, 1024);
      int msg_len = sizeof(AgentMessageHeader) + body.length();
      message.SetLength(msg_len);
      memcpy(message.Body, body.data(), body.length());
      printf("msg_len is %d\n", msg_len);
      conn->ai_agent_->do_write((char *)&message, msg_len);
      offset += 1024;
    }
  } else {
    printf("conn is invalid\n");
  }
}

void AgentServer::process_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg) {
  rapidjson::Document document;
  document.Parse(msg->get_payload().c_str());

  std::string msg_opcode, system_type, contract_no;
  // std::string msg_opcode  = document["opcode"].GetString();
  // std::string system_type = document["system_type"].GetString();
  // std::string contract_no = document["contract_no"].GetString();

  GET_JSON_VALUE(msg_opcode, "/opcode");
  GET_JSON_VALUE(system_type, "/system_type");

  printf("contract_no is %s\n", contract_no.c_str());
  printf("opcode is %s\n", msg_opcode.c_str());
  // { "system_type": "ai_agent",
  //   "transfer_type": "recv_only",
  //   "opcode": "register",
  //   "contract_no": "1234"}

  if (msg_opcode == "REGISTER") {
    process_register(s, hdl, document);
  }
  else if (msg_opcode == "PUBLISH") {
  }
  else if (msg_opcode == "PUBLISH") {
  }
  else if (msg_opcode == "PUBLISH") {
  }
  else if (msg_opcode == "PUBLISH") {
  }
  else if (msg_opcode == "PUBLISH") {
  }
  else if (msg_opcode == "PUBLISH") {
  }
}
