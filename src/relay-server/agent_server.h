#ifndef AGENT_SERVER_H
#define AGENT_SERVER_H

#include <unordered_map>
#include <thread>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include "rapidjson/document.h"

class session;

struct connection_data {
  std::unordered_map<std::string, std::shared_ptr<session> > subscribers_;
  // for transfer
  std::shared_ptr<session> ai_agent_;
  std::shared_ptr<session> pc_agent_;

  std::string dn_;
  std::string agent_id_;
  int system_type_;
  void do_write(char *data, std::size_t len);
  void send_disconnect(int stype);
  void send_ringing();
  void unregister_other() {
  }
  void unregister_this();
};

struct custom_config : public websocketpp::config::asio {
    // pull default settings from our core config
    typedef websocketpp::config::asio core;
    
    typedef core::concurrency_type concurrency_type;
    typedef core::request_type request_type;
    typedef core::response_type response_type;
    typedef core::message_type message_type;
    typedef core::con_msg_manager_type con_msg_manager_type;
    typedef core::endpoint_msg_manager_type endpoint_msg_manager_type;
    typedef core::alog_type alog_type;
    typedef core::elog_type elog_type;
    typedef core::rng_type rng_type;
    typedef core::transport_type transport_type;
    typedef core::endpoint_base endpoint_base;
    
    // Set a custom connection_base class
    typedef connection_data connection_base;
};

// typedef websocketpp::server<websocketpp::config::asio> server;
typedef websocketpp::server<custom_config> server;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

// pull out the type of messages sent by our config
typedef server::message_ptr message_ptr;

class AgentServer {
 public:
  AgentServer();
  ~AgentServer();

  void Start(int port);

 private:
  // Define a callback to handle incoming messages
  void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg);
  void on_open(server* s, websocketpp::connection_hdl hdl);
  void on_close(server* s, websocketpp::connection_hdl hdl);

  void process_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg);
  void process_transfer(server* s, websocketpp::connection_hdl hdl, message_ptr msg);
  void process_register(server* s, websocketpp::connection_hdl hdl, rapidjson::Document &document);
  // void process_message(message_ptr msg);
  // void process_register(rapidjson::Document &document);

  server server_;
  std::thread run_thread_;
};

void ws_send_ringing(server::connection_ptr conn);
void ws_is_anybody_there(server::connection_ptr conn);

#endif /* AGENT_SERVER_H */
