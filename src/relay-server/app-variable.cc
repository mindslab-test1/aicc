#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <fstream>

#include "app-variable.h"
#include "cpptoml.h"

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/rotating_file_sink.h>
#endif

#define OPT_PASSWD 1000
#define OPT_ONCE   1001
#define OPT_DOMAIN 1002

#define CFG_FILENAME "relay-server.conf"

APP_VARIABLE g_var;

std::shared_ptr<spdlog::logger> g_logger;

std::shared_ptr<spdlog::logger> CreateLogger(std::string logger_name) {
  std::string maum_root = std::getenv("MAUM_ROOT");
  std::string file_path = maum_root + "/logs/" + logger_name + ".log";

  auto rotating_logger =
      spdlog::rotating_logger_mt(logger_name,
                                 file_path,
                                 1048576 * 100,  // 100 MBytes
                                 10);            // backup count
  return rotating_logger;
}

bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

APP_VARIABLE::APP_VARIABLE() {
}

APP_VARIABLE::~APP_VARIABLE() {
  g_var.logger->trace("APP_VARIABLE dtor start");
  g_var.logger->trace("APP_VARIABLE dtor end");
}

void APP_VARIABLE::ParseArgs(int argc, char *argv[])
{
}

template <typename T, typename D>
void get_value(std::shared_ptr<cpptoml::table> config, const char *key, T &value,  D def_value) {
  value = config->get_qualified_as<T>(key).value_or(def_value);
}

void APP_VARIABLE::Initialize(int argc, char *argv[])
{
  spdlog::set_level(spdlog::level::debug);
  spdlog::flush_on(spdlog::level::debug);
  logger = CreateLogger("relay-server");
  g_logger = logger;

  std::string maum_root = std::getenv("MAUM_ROOT");
  std::string maum_conf_dir = maum_root + "/etc";
  std::vector<std::string> config_dirs { ".", maum_conf_dir };

  bool found_config = false;
  for (auto dir : config_dirs) {
    auto conf = dir + "/" + CFG_FILENAME;
    if (is_file_exist(conf.c_str())) {
      auto config = cpptoml::parse_file(conf);
      std::string url;
      get_value(config, "call-manager.url"      , url         , "http://127.0.0.1:5000");
      get_value(config, "call-manager.transfer" , transfer_url, "/call/transfer");
      get_value(config, "call-manager.play"     , play_url    , "/call/play");
      get_value(config, "common.listen.port"    , relay_port  , 5900);
      get_value(config, "websocket.listen.port" , websock_port, 5800);
      transfer_url = url + transfer_url;
      play_url = url + play_url;
      found_config = true;
      break;
    }
  }

  if (found_config == false) {
    printf("Cannot open %s file...\n", CFG_FILENAME);
    exit(-1);
  }

  // ShowVariable();
}

void APP_VARIABLE::ShowVariable() {
}
