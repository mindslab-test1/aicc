#ifndef APP_VARIABLE_H
#define APP_VARIABLE_H

#include <map>
#include <atomic>
#include <spdlog/spdlog.h>
#include "agent_server.h"

#define QUOTE(...) #__VA_ARGS__

class session;

// variable, config value, etc... for this application
struct APP_VARIABLE {
  /**
   * @brief Constructor && Destructor
   *
   */
  APP_VARIABLE();
  ~APP_VARIABLE();

  /**
   * @brief arg parse
   *
   * @param argc main fuction의 argc
   * @param argv main fuction의 argv
   */
  void ParseArgs(int argc, char *argv[]);

  /**
   * @brief 초기 APP_VARIABLE값을 설정하는 함수, g_var값을 사용하기 전에 호출되어야 한다.
   * 
   * @param argc main fuction의 argc
   * @param argv main fuction의 argv
   * @return void
   */
  void Initialize(int argc, char *argv[]);

  /**
   * @brief 현재 설정된 값을 보여주기 위한 함수
   * 
   * @return void
   */
  void ShowVariable();

  std::string transfer_url;
  std::string play_url;

  int relay_port;
  int websock_port;

  /**
   * @brief 프로그램에서 사용하는 logger
   * 
   */
  std::shared_ptr<spdlog::logger> logger;
  std::mutex ai_lock;
  std::mutex pc_lock;

  // Key: 내선번호, Value: Session
  std::unordered_map<std::string, std::shared_ptr<session> > ai_agents;

  // 듣기 전용
  // Key: 내선번호, Value: Session
  std::unordered_map<std::string, std::shared_ptr<session> > play_agents;

  // Key: AgentID, Value: Session
  std::unordered_map<std::string, server::connection_ptr> pc_agents;
};

extern APP_VARIABLE g_var;

// for convenience
extern std::shared_ptr<spdlog::logger> g_logger;

#endif /* APP_VARIABLE_H */
