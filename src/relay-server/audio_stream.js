function StartAudioStream(host, port, dial_no, agent_id, mic_on) {
    var audioQueue = [];
    if (window.ws_agent != null) {
        window.ws_agent.close();
        window.ws_agent = null;
    }
    window.ws_agent = new WebSocket("ws://" + host + ":" + port);
    window.ws_agent.binaryType = 'arraybuffer';
    var ws = window.ws_agent;

    var trans_type = 'recv_only';
    if (mic_on) {
        if (navigator.getUserMedia){
            trans_type = 'recv_send';
            navigator.getUserMedia({audio:true},
                                   function(stream) {
                                       send_audio(ws, stream);
                                   },
                                   function(e) {
                                       alert('Error capturing audio.');
                                   });
        } else alert('getUserMedia not supported in this browser.');
    }

    var audioCtx = new (window.AudioContext || window.webkitAudioContext)({
        sampleRate: 8000
    });
    var source = audioCtx.createBufferSource();
    var scriptNode = audioCtx.createScriptProcessor(4096, 0, 1);
    scriptNode.onaudioprocess = function(audioProcessingEvent) {
        var outputBuffer = audioProcessingEvent.outputBuffer;
        var outputData = outputBuffer.getChannelData(0);
        // console.log("outputBuffer length is " + outputBuffer.length);
        // console.log("audioQueue length is " + audioQueue.length);
        if (audioQueue.length < (outputBuffer.length * 2) ) {
            outputData.fill(0);
            return;
        }
        for (var sample = 0; sample < outputBuffer.length; sample++) {
            var hRawAudio = audioQueue.shift();
            var lRawAudio = audioQueue.shift();
            // For little endian
            var unsignedWord = (hRawAudio & 0xff) + ((lRawAudio & 0xff) << 8);
            // For big endian
            //var unsignedWord = ((hRawAudio & 0xff) << 8) + (lRawAudio & 0xff);
            var signedWord = (unsignedWord + 32768) % 65536 - 32768;
            outputData[sample] = signedWord / 32768.0;
        }
    }

    if ("WebSocket" in window) {
        ws.onopen = function () {
            console.log("Connection is established...");
            source.connect(scriptNode);
            scriptNode.connect(audioCtx.destination);
            source.start();
            var msg = {
                "opcode": "REGISTER",
                "system_type" : "PC_AGENT",
                "dial_no": dial_no,
                "trans_type": trans_type,
                "agent_id": agent_id
            };
            var data = JSON.stringify(msg);
            ws.send(data);
            console.log("send REGISTER (" + trans_type + ")");
        };

        ws.onmessage = function (evt) {
            var received_msg = new Uint8Array(evt.data); // firefox에서 동작
            // console.log("Message is received... " + received_msg.length);
            // audioQueue.push(received_msg);
            for (var i = 0; i < received_msg.length; i++) {
                //console.log("push audio samples");
                audioQueue.push(received_msg[i]);
            }
            // console.log("Queue is received... " + audioQueue.length);
        };

        ws.onclose = function () {
            try {
                source.disconnect(scriptNode);
                scriptNode.disconnect(audioCtx.destination);
                if (mic_on) {
                    console.log("Disconnect mic");
                    microphone_stream.disconnect(micScriptNode);
                    micScriptNode.disconnect(micAudioCtx.destination);
                    voice_stream.getTracks()[0].stop();
                }
            }
            catch (e) {
                console.log(e);
            }
            console.log("Connection is closed...");
        };
    } else {

        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
}

function StopAudioStream() {
    if (window.ws_agent) {
        window.ws_agent.close();
    }
}

function floatTo16BitPCM(input) {
    var output = new DataView(new ArrayBuffer(input.length * 2));
    for (var i = 0; i < input.length; i++) {
        var multiplier = input[i] < 0 ? 0x8000 : 0x7fff; // 16-bit signed range is -32768 to 32767
        output.setInt16(i * 2, input[i] * multiplier | 0, true); // index, value, little edian
    }
    return output.buffer;
}

function send_audio(ws, stream /* from getUserMedia */) {
    voice_stream = stream;
    //var micAudioCtx = new (window.AudioContext || window.webkitAudioContext)({
    micAudioCtx = new (window.AudioContext || window.webkitAudioContext)({
        sampleRate: 8000
    });

    microphone_stream = micAudioCtx.createMediaStreamSource(stream);

    // Create a ScriptProcessorNode with a bufferSize of 4096 and a single input and output channel
    //var scriptNode = micAudioCtx.createScriptProcessor(1024, 1, 1);
    micScriptNode = micAudioCtx.createScriptProcessor(1024, 1, 1);
    // Give the node a function to process audio events
    micScriptNode.onaudioprocess = function (audioProcessingEvent) {
        // The input buffer is the song we loaded earlier
        var inputBuffer = audioProcessingEvent.inputBuffer;
        var inputData = inputBuffer.getChannelData(0);
        var sendBuffer = floatTo16BitPCM(inputData);
        ws.send(sendBuffer);
    }
    microphone_stream.connect(micScriptNode);
    micScriptNode.connect(micAudioCtx.destination);
}
