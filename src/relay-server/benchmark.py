#!/usr/bin/python

import subprocess
import multiprocessing

base = './pc_client.py -t -d 0.01 -a {}'

def run(cmd):
    subprocess.call(cmd, shell=True)

for i in range(100):
    cmd = base.format(str(i+1))
    p = multiprocessing.Process(target=run, args=(cmd,))
    p.start()



