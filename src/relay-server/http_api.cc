#include "http_api.h"
#include <curl/curl.h>
#include <iostream>
#include <stdio.h>

using std::string;

size_t write_data(void *ptr, size_t size, size_t nmemb, std::string *data) {
  data->append((char*)ptr, size * nmemb);
  return size * nmemb;
}

bool send_http_post_json(string url, string &body) {
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if (curl) {
    std::string result;
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body.c_str());

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if (res != CURLE_OK) {
      printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    }

    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);

    return true;
  }
  return false;
}
