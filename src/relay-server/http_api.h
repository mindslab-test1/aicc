#ifndef HTTP_API_H
#define HTTP_API_H

#include <string>

bool send_http_post_json(std::string url, std::string &body);

#endif /* HTTP_API_H */
