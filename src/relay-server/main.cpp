#include "relay_session.h"
#include "agent_server.h"

class RelayServer
{
public:
  RelayServer(boost::asio::io_service& io_service, short port)
    : acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
      socket_(io_service)
  {
    do_accept();
  }

private:
  void do_accept()
  {
    acceptor_.async_accept(socket_,
        [this](boost::system::error_code ec)
        {
          if (!ec)
          {
            std::make_shared<session>(std::move(socket_))->start();
          }

          do_accept();
        });
  }

  tcp::acceptor acceptor_;
  tcp::socket socket_;
};

int main(int argc, char* argv[])
{
  g_var.Initialize(argc, argv);

  // websocket interface for agent
  AgentServer agent_server;
  agent_server.Start(g_var.websock_port);

  try
  {
    boost::asio::io_service io_service;

    RelayServer s(io_service, g_var.relay_port);

    g_logger->info("Relay Server started...");
    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
