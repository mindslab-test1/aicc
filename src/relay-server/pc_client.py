#!/usr/bin/env python

import struct
import socket
import time
import argparse
import biz_log


LOGGER = biz_log.getLogger(__name__)
PCM = ''
for i in range(160):
    PCM += ' '

class SystemType:
    AI_AGENT = 0
    PC_AGENT = 1

class OpCode:
    REGISTER = 0
    TRANSFER = 1

class TransferType:
    RECV_ONLY = 0
    SEND_ONLY = 1
    RECV_SEND = 2

class MsgHeader:
    def __init__(self, raw):
        self.version, self.system_type, self.opcode, self.length = raw

def send_regi(conn, system_type, agent_id):
    contract_no = 123
    dial_no = '6006'
    trans_type = TransferType.SEND_ONLY
    if system_type == SystemType.PC_AGENT:
        trans_type = TransferType.RECV_ONLY

    # uint contract_no
    # char agent_id
    # char dial_no
    # uint trans_type
    body = struct.pack('!I20s20sI', contract_no, agent_id, dial_no, trans_type)

    # Version, System-type, OpCode, Length
    hdr = struct.pack('!2shhh', 'ML', system_type, OpCode.REGISTER, 8 + len(body))
    #print msg.encode('hex')
    conn.send(hdr + body)

def send_regi2(conn, system_type, agent_id):
    contract_no = 123
    dial_no = '6006'
    trans_type = TransferType.RECV_SEND
    if system_type == SystemType.PC_AGENT:
        trans_type = TransferType.RECV_ONLY

    # uint contract_no
    # char agent_id
    # char dial_no
    # uint trans_type
    body = struct.pack('!I20s20sI', contract_no, agent_id, dial_no, trans_type)

    # Version, System-type, OpCode, Length
    hdr = struct.pack('!2shhh', 'ML', system_type, OpCode.REGISTER, 8 + len(body))
    #print msg.encode('hex')
    conn.send(hdr + body)

def send_pcm(conn, seq, system_type, pcm):
    if system_type == SystemType.PC_AGENT:
        return
    hdr = struct.pack('!2shhh', 'ML', system_type, OpCode.TRANSFER, 8 + len(pcm))
    conn.sendall(hdr + pcm)
    LOGGER.debug('sent transfer message')

def recv_msg(s):
    data = ''
    try:
        # data = s.recv(1024, socket.MSG_DONTWAIT)
        data = s.recv(1024)
        print('data len is %d' % len(data))
    except socket.timeout as e:
        print('timeout')
        pass
    except socket.error as e:
        if e.errno == 11:
            pass
        else:
            print str(e), e
    return data

def main():
    parser = argparse.ArgumentParser(description='Phone Agent Simulator')
    parser.add_argument('-t', help="system type is PC Agent", action='store_true')
    parser.add_argument('-d', help="rtp delay time", type=float)
    parser.add_argument('-a', help="agent id", type=str)
    args = parser.parse_args()
    system_type = SystemType.AI_AGENT
    agent_id = args.a
    if args.t:
        system_type = SystemType.PC_AGENT
    if not args.d:
        args.d = 0.05
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
    s.connect(('127.0.0.1', 5900))

    send_regi2(s, system_type, agent_id)

    seq = 0
    data = ''
    st = time.time()
    #while time.time() - st < 3:
    f = open('2018.edit.pcm')
    while True:
        pcm = f.read(160)
        # send_pcm(s, seq, system_type, pcm)
        try:
            #data += recv_msg(s)
            buf = recv_msg(s)
            data += buf
            if len(buf) == 0:
                break
            while len(data) >= 8:
                hdr = struct.unpack('!2shhh', data[:8])
                if hdr[0] != 'ML':
                    s.close()
                    break
                if len(data) >= hdr[3]:
                    print hdr, len(data[8:])
                    body = data[8: hdr[3]]
                    data = data[hdr[3]:]
                    seq += 1
                    # echo message
                    send_pcm(s, seq, system_type, body)
                else:
                    print 'not yet all data'
                    break
        except socket.error as e:
            if e.errno == 32:
                print str(e)
                break
            pass
        #time.sleep(args.d)
        #time.sleep(80 / 16000.0)
    print seq


if __name__ == '__main__':
    main()
