#ifndef RELAY_SESSION_H
#define RELAY_SESSION_H

#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <mutex>
#include <unordered_map>
#include <boost/asio.hpp>

#include "app-variable.h"
#include "message_buffer.h"
#include "agent_message.h"

using std::string;
using boost::asio::ip::tcp;

class session;

#if 0
class AgentMap
{
public:
  static AgentMap* getInstance()
  {
    return &(getSingleton());
  }

  static AgentMap& getSingleton() 
  { 
    static AgentMap instance; 
    return instance; 
  }

  bool Insert(int dial_no, std::shared_ptr<session> s)
  {
    std::unique_lock<std::mutex> lock(ai_lock_);
    g_var.ai_agents_[dial_no] = s;
  }

  void Remove(int dial_no)
  {
    std::unique_lock<std::mutex> lock(ai_lock_);
    g_var.ai_agents_.erase(dial_no);
  }

  bool Insert(std::string agent_id, std::shared_ptr<session> s)
  {
    std::unique_lock<std::mutex> lock(pc_lock_);
    g_var.pc_agents_.erase(agent_id);
  }

  void Remove(std::string agent_id)
  {
    std::unique_lock<std::mutex> lock(pc_lock_);
    g_var.pc_agents_.erase(agent_id);
  }

private:
  AgentMap() {}
  std::mutex ai_lock_;
  std::mutex pc_lock_;

  // Key: 내선번호, Value: Session
  std::d::unordered_map<int, std::shared_ptr<session> > g_var.ai_agents_;

  // Key: AgentID, Value: Session
  std::unordered_map<std::string, std::shared_ptr<session> > g_var.pc_agents_;
};

typedef AgentMap::getInstance() AGENT_MAP();

#endif

// TODO: 종료시 세션 제거
//       양자간 통화
class session : public std::enable_shared_from_this<session>
{
public:
  session(tcp::socket socket) : socket_(std::move(socket))
  {
  }

  ~session()
  {
    g_logger->info("session destructor called");
  }

  void start()
  {
    do_read();
  }

  void register_subscriber(std::string agent_id, server::connection_ptr subscriber)
  {
    std::unique_lock<std::mutex> lock(lock_);
    auto it = subscribers_.find(agent_id);
    if (it != subscribers_.end()) {
      g_logger->info("{} ID는 이미 등록된 상담사입니다.", agent_id);
    } else {
      subscribers_[agent_id] = subscriber;
    }
  }

  void unregister_subscriber(std::string agent_id)
  {
    std::unique_lock<std::mutex> lock(lock_);
    subscribers_.erase(agent_id);
  }

  void register_other(int system_type, std::shared_ptr<session> other)
  {
    // if (system_type == AI_AGENT) {
    //   ai_agent_ = other;
    // } else if (system_type == PC_AGENT) {
    //   pc_agent_ = other;
    // }
  }

  void register_other(int system_type, server::connection_ptr other)
  {
    pc_agent_ = other;
  }

  void unregister_other()
  {
    if (ai_agent_) {
      ai_agent_.reset();
    }
    if (pc_agent_) {
      pc_agent_.reset();
    }
  }

  void publish(char data)
  {
    std::unique_lock<std::mutex> lock(lock_);
    // for (auto subscriber : subscribers_) {
    //   subscriber->do_write(data);
    // }
  }

private:
  // 실시간 재생 FLOW
  // 1. 브라우저에서 실시간 재생 REST 요청 (브라우저 -> 콜매니저)
  // 2. 소프트폰으로 ZeroMQ이벤트 전송 (콜매니저 -> 소프트폰)
  // 3. 중개서버로 접속 및 REGISTER 전송 (소프트폰 -> 중개서버)
  
  bool process_message(char *data, size_t len)
  {
    AgentMessage* msg = (AgentMessage*)data;
    if (msg->Version[0] != 'M' || msg->Version[1] != 'L') {
      socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
      socket_.close();
      return false;
    }
    switch (msg->GetOpCode()) {
      case REGISTER: {
        process_register(msg);
        break;
      }
      case PUBLISH: {
        // process_publish(msg);
        break;
      }
      case SUBSCRIBE: {
        // process_subscribe(msg);
        break;
      }
      case TRANSFER: {
        process_transfer(data, len);
        break;
      }
      case NOTIFY: {
        process_notify(msg);
        break;
      }
      case DISCONNECT: {
        process_disconnect(data, len);
        break;
      }
      default:
        break;
    }
    return true;
  }

  void send_connect(RegisterBody *body, string dn, string &agent_id)
  {
    g_logger->info("{} AI 상담사가 ID {} 상담사와 통화 연결을 시도합니다", dn, agent_id);

    // 플레이 중이었다면 제거
    auto play_it = g_var.play_agents.find(dn);
    if (play_it != g_var.play_agents.end()) {
      (*play_it).second->unregister_subscriber(agent_id);
    }

    auto agent_iter = g_var.pc_agents.find(agent_id);
    if (agent_iter != g_var.pc_agents.end()) {
      g_logger->info("FOUND PC Agent (AgentID = {})", agent_id);
      // (*agent_iter).second->send_ringing(body);
      //agent_iter->second->send_ringing();
      agent_iter->second->ai_agent_ = shared_from_this();
      pc_agent_ = agent_iter->second;
      // (*agent_iter).second->register_other(AI_AGENT, shared_from_this());
      // pc_agent_ = (*agent_iter).second;
    } else {
      g_logger->info("ai agent not registered. agent_id = {}", agent_id);
    }
  }

  void is_anybody_there(RegisterBody *body)
  {
    g_logger->info("{} AI 상담사가 대기중인 상담사와 통화 연결을 요청합니다", body->DialNo);

    for (auto kv /* agent_id, session */: g_var.pc_agents) {
      if (!kv.second->ai_agent_) {
        kv.second->send_ringing();
      }
    }
  }

  void process_register(AgentMessage* msg)
  {
    RegisterBody* body = (RegisterBody*)msg->Body;
    auto sys_type = msg->GetSystemType();

    if (sys_type == PC_AGENT) {
      // TODO: web agent
      // std::string id = body->AgentID;
      // agent_id_ = id;
      // g_var.pc_agents[id] = shared_from_this();
      // g_logger->info("상담사 ID {} 가 등록되었습니다. 현재 등록된 상담사는 {} 명입니다.",
      //                id, g_var.pc_agents.size());
    }

    auto trans_type = body->GetTransType();
    switch (trans_type) {
      case SEND_ONLY: {
        if (sys_type == AI_AGENT) {
          string dn(body->DialNo);
          g_var.play_agents[dn] = shared_from_this();
          g_logger->info("{} AI 상담사가 실시간 음성 재생을 시작합니다", dn);
        }
        std::string id = body->AgentID;
        std::unique_lock<std::mutex> lock(lock_);
        auto subscriber = g_var.pc_agents.find(id);
        if (subscriber != g_var.pc_agents.end()) {
          subscribers_[id] = (*subscriber).second;
          g_logger->info("상담사 ID {} 가 음성 청취를 시작합니다", id);
        } else {
          g_logger->info("상담사 ID {} 가 음성 청취를 위해 등록되지 않았습니다", id);
        }
        break;
      }
      case RECV_ONLY: {
        // TEST ONLY from PC Agent
        // int dn = atoi(body->DialNo);
        std::string agent_id = body->AgentID;
        std::unique_lock<std::mutex> lock(lock_);

#if 0
        auto agent_iter = g_var.play_agents.find(dn);
        if (agent_iter != g_var.play_agents.end()) {
          (*agent_iter).second->register_subscriber(agent_id, shared_from_this());
        } else {
          g_logger->info("ai agent not registered. dn = {}", dn);
        }
        break;
#endif
      }
      case RECV_SEND: {
        string dn(body->DialNo);
        if (sys_type == AI_AGENT) {
          dn_ = dn;
          g_var.ai_agents[dn] = shared_from_this();

          std::string agent_id = body->AgentID;

          // 특정 상담사가 받는 경우
          if (!agent_id.empty()) {
            send_connect(body, dn, agent_id);
          }
          // 아무나 받기를 원할 때
          else {
            is_anybody_there(body);
          }
        }
        // PC_AGENT
        else {
          auto it = g_var.ai_agents.find(dn);
          if (it != g_var.ai_agents.end()) {
            (*it).second->register_other(PC_AGENT, shared_from_this());
            ai_agent_ = (*it).second;
          } else {
            g_logger->info("해당 DialNo({})의 소프트폰이 존재하지 않습니다.", dn);
          }
        }
        break;
      }
      default:
        break;
    }
  }

  void process_transfer(char *data, size_t len)
  {
    AgentMessage* msg = (AgentMessage*)data;
    //g_logger->info("GOT TRANSFER, subscribers cnt is %ld\n", subscribers_.size());
    if (msg->GetSystemType() == PC_AGENT && ai_agent_) {
      ai_agent_->do_write(data, len);
    } else if (pc_agent_) {
      pc_agent_->dn_ = "1";
      pc_agent_->do_write(data, len);
    }

    std::unique_lock<std::mutex> lock(lock_);

    // int cnt = 0;
    for (auto subscriber : subscribers_) {
      subscriber.second->do_write(data, len);
      //g_logger->info("SEND TRANSFER %d\n", cnt++);
    }
  }

  void process_disconnect(char *data, size_t len)
  {
    AgentMessage* msg = (AgentMessage*)data;
    if (msg->GetSystemType() == PC_AGENT && ai_agent_) {
      ai_agent_->do_write(data, len);
    } else if (pc_agent_) {
      pc_agent_->do_write(data, len);
    }
  }

public:
  void send_disconnect(int sys_type)
  {
    AgentMessage message;
    // memcpy(message.Body, body, sizeof(RegisterBody));
    int msg_len = sizeof(AgentMessageHeader);

    message.SetVersion();
    message.SetSystemType(sys_type);
    message.SetOpCode(DISCONNECT);
    message.SetLength(msg_len);

    do_write((char *)&message, msg_len);
  }
private:

  void send_ringing(RegisterBody *body)
  {
    AgentMessage message;
    memcpy(message.Body, body, sizeof(RegisterBody));
    int msg_len = sizeof(AgentMessageHeader) + sizeof(RegisterBody);

    message.SetVersion();
    message.SetSystemType(AI_AGENT);
    message.SetOpCode(RINGING);
    message.SetLength(msg_len);

    do_write((char *)&message, msg_len);
  }

  void process_notify(AgentMessage* msg)
  {
    // RegisterBody* body = (RegisterBody*)msg->Body;
    // auto sys_type = msg->GetSystemType();
  }

  void do_process()
  {
    // int length;
    // when AI
    //     send_only - publish
    //     recv_send - transfer
    // when PC
    //     recv_only - publish
    //     recv_send - transfer

  }

  void unregister_this()
  {
    // PC_AGENT 인 경우
    if (!agent_id_.empty()) {
      g_logger->info("UNREGISTER PC Agent (agent_id = {})", agent_id_);
      g_var.pc_agents.erase(agent_id_);
      if (ai_agent_) {
        ai_agent_->send_disconnect(PC_AGENT);
        ai_agent_->unregister_other();
      }
    }
    // AI_AGENT 인 경우
    else if (!dn_.empty()) {
      g_logger->info("UNREGISTER AI Agent (dial_no = {})", dn_);
      g_var.play_agents.erase(dn_);
      g_var.ai_agents.erase(dn_);
      if (pc_agent_) {
        pc_agent_->send_disconnect(AI_AGENT);
        pc_agent_->unregister_other();
      }
    }
    // 기타 or 초기화 되지 않은 경우
    else {
      // whoami?
    }
  }

  void do_read()
  {
    auto self(shared_from_this());
    //socket_.async_read_some(boost::asio::buffer(data_, max_length),
    socket_.async_read_some(boost::asio::buffer(buf_.GetBuffer(), max_length),
        [this, self](boost::system::error_code ec, std::size_t length)
        {
          if (!ec) {
            bool is_valid = true;
            char *msg;
            int   msg_length;
            buf_.Commit(length);

            while ( (msg = buf_.GetMessage(msg_length)) != NULL) {
              is_valid = process_message(msg, msg_length);
              if (!is_valid) {
                // error
                unregister_this();
                break;
              }
            }
            if (is_valid) do_read();
          }
          else {
            // error
            unregister_this();
          }
        });
  }

  void do_write(char *data, std::size_t length)
  {
    auto self(shared_from_this());
    boost::asio::async_write(socket_, boost::asio::buffer(data, length),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            // do_read();
            //socket_.close();
          }
        });
  }

  friend class AgentServer;

  std::mutex lock_;
  tcp::socket socket_;
  enum { max_length = 65530 };
  char data_[max_length];
  AgentMessageBuffer buf_;

  std::unordered_map<string, server::connection_ptr> subscribers_;
  // for transfer
  std::shared_ptr<session> ai_agent_;
  server::connection_ptr pc_agent_;

  string dn_;
  string agent_id_;
  int system_type_;
};

#endif /* RELAY_SESSION_H */
