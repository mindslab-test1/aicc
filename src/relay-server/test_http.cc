#include "http_api.h"

#define QUOTE(...) #__VA_ARGS__

int main(int argc, char *argv[])
{
  std::string url = "http://127.0.0.1:5000/call/transfer";
  std::string body = QUOTE(
    {"body": "1234"}
  );
  send_http_post_json(url, body);
  return 0;
}
