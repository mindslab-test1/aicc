#!/usr/bin/env python

import json
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    print(len(message))

def on_data(ws, message, data_type, cont_flag):
    if data_type == websocket.ABNF.OPCODE_BINARY:
        print('get binary', len(message))
    else:
        print('get text')

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("### opened ###")
    msg = {
        "opcode": "REGISTER",
        "system_type": "PC_AGENT",
        "contract_no": "123",
        "dial_no": "6006",
        "trans_type": "recv_send",
        "agent_id": "aicc301"
    }
    data = json.dumps(msg)
    ws.send(data)
    # ws.send(data, websocket.ABNF.OPCODE_BINARY)
    # def run(*args):
    #     for i in range(3):
    #         time.sleep(1)
    #         ws.send("Hello %d" % i)
    #     time.sleep(1)
    #     ws.close()
    #     print("thread terminating...")
    # thread.start_new_thread(run, ())
    def run():
        f = open('2018.edit.pcm')
        while True:
            pcm = f.read(160)
            if len(pcm) < 160:
                break
            ws.send(pcm, websocket.ABNF.OPCODE_BINARY)
            time.sleep(160 / 16000.0)
    thread.start_new_thread(run, ())


if __name__ == "__main__":
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp("ws://127.0.0.1:8888/",
                              on_message = on_message,
                              on_error = on_error,
                              on_data = on_data,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
