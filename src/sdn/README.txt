< SDN - Speech Delivery Network >

* requirements (CentOS 기준)
sudo yum install python-devel
sudo yum install epel-release
sudo yum install python-pip
sudo pip install --upgrade pip
sudo pip install virtualenv

* 빌드 환경 설정
$ virtualenv -p python2 venv
$ source venv/bin/activate
$ export PYTHONPATH=$MAUM_ROOT/lib/python
$ pip install -r requirements.txt

* pysoundtouch 설치
1. 소스의 root로 이동하여 submodule clone
$ cd ../..
$ git submodule init 
$ git submodule update src/sdn/pysoundtouch
$ cd src/sdn/pysoundtouch

2. build
- pysoundtouch 디렉토리의 REAEME.md 참조
$ cd soundtouch
$ ./bootstrap
$ ./configure --enable-integer-samples CXXFLAGS="-fPIC"
$ make
$ sudo make install
$ cd ..

$ python setup.py install

* grpc proto build
$ make pb
- grpc가 빌드가 되지 않을 경우 protobuf를 업그레이드 해본다
$ sudo pip install -U protobuf

* SDN 빌드 및 설치
$ make install

