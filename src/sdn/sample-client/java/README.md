# brain tts -  java sample client

## build

```bash
mvn compile
mvn package
```

## run (TTS 서버는 127.0.0.1:50051 준비)

```bash
java -jar tts-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```

