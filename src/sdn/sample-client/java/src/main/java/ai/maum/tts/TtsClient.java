package ai.maum.tts;

import com.google.protobuf.ByteString;
import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import io.grpc.Metadata;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.common.LangOuterClass;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.concurrent.Executor;

public class TtsClient {
    public static void main(String[] args) throws Exception {
        String ip = "127.0.0.1";
        int port = 50051;

        ManagedChannel channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext().build();
        NgTtsServiceGrpc.NgTtsServiceBlockingStub stub =
                NgTtsServiceGrpc.newBlockingStub(channel).withCallCredentials(new CallCredentials() {
                    @Override
                    public void applyRequestMetadata(RequestInfo requestInfo, Executor appExecutor, MetadataApplier applier) {
                        Metadata headers = new Metadata();
                        Metadata.Key<String> sampleRate = Metadata.Key.of("samplerate", Metadata.ASCII_STRING_MARSHALLER);
                        headers.put(sampleRate, "8000");
                        applier.apply(headers);
                    }

                    @Override
                    public void thisUsesUnstableApi() {

                    }
                });

        TtsRequest.Builder req = TtsRequest.newBuilder();
        req.setLang(LangOuterClass.Lang.ko_KR);
        req.setText("안녕하세요 마인즈랩입니다.");
        Iterator<TtsMediaResponse> iter = stub.speakWav(req.build());
        try {
            FileOutputStream fos = new FileOutputStream("aaa.wav");
            FileChannel fc = fos.getChannel();

            while(iter.hasNext()) {
                TtsMediaResponse resp = iter.next();
                ByteString data = resp.getMediaData();
                ByteBuffer buff = data.asReadOnlyByteBuffer();

                // do your work with buff
                fc.write(buff);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


