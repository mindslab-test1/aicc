#!/usr/bin/python

import soundtouch
import array


class SdnTempo:
    def __init__(self, framerate, channel, tempo):
        self.framerate = framerate
        self.channel = channel
        self.st = soundtouch.SoundTouch(framerate, channel)
        self.st.set_tempo(tempo)

    def __del__(self):
        del self.st

    def put_samples(self, buf):
        self.st.put_samples(buf)

    def get_samples(self):
        res = ''
        while self.st.ready_count() > 0:
            res += self.st.get_samples(4000)
        return res

    def get_last_samples(self):
        resstr = ''
        waiting = self.st.waiting_count()

        # Flush any additional samples
        waiting = self.st.waiting_count()
        ready = self.st.ready_count()
        flushed = ""

        # Add silence until another chunk is pushed out
        silence = array.array('h', [0] * 64)
        while self.st.ready_count() == ready:
            self.st.put_samples(silence)

        # Get all of the additional samples
        while self.st.ready_count() > 0:
            flushed += self.st.get_samples(4000)

        self.st.clear()

        if len(flushed) > 2 * self.channel * waiting:
            flushed = flushed[0:(2 * self.channel * waiting)]

        resstr += flushed

        return resstr
