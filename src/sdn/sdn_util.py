#!/usr/bin/python

import sys
import struct


def get_samplerate(header):
    return struct.unpack('<L', header[24:28])[0]


if __name__ == '__main__':
    fname = sys.argv[1]
    f = open(fname, 'r')
    header = f.read(44)
    print get_samplerate(header)

