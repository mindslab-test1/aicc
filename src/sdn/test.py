#!/usr/bin/python

import unittest
import sdn
from sdn import SdnConfig


class SdnTests(unittest.TestCase):
    def test_runs(self):
        sdn.LOGGER = sdn.getLogger('test')
        config = SdnConfig('sdn.conf.sample')
        print config.tts_list
        self.assertEqual(len(config.tts_list), 3)
        config = SdnConfig()
        print config.tts_list
        self.assertEqual(len(config.tts_list), 2)
        pass


if __name__ == '__main__':
    unittest.main()

