#!/usr/bin/python

import unittest
import sdn_util
import sys
import sdn_tempo
import samplerate as sr
import numpy as np


class TestResample(unittest.TestCase):

    def test_resample(self):
        resampler = sr.Resampler()
        f = open('news.22k.wav')
        outf = open('resampled.pcm', 'w')
        chunksize = 1024
        wav_header = f.read(44)
        input_rate = sdn_util.get_samplerate(wav_header)
        print 'input_rate is %d' % input_rate
        ratio = 8000 / float(input_rate)
        # st = sdn_tempo.SdnTempo(input_rate, 1, tempo)
        st = sdn_tempo.SdnTempo(input_rate, 1, 1.5)
        while True:
            chunk = f.read(chunksize)
            if chunk:
                st.put_samples(chunk)
                raw_data = st.get_samples()
                # raw_data = chunk
                if len(raw_data) > 0:
                    data = np.fromstring(raw_data, dtype=np.int16)
                    resampled_data = resampler.process(data, ratio).astype('int16')
                    new_data = resampled_data.tostring()
                    print len(resampled_data), len(new_data), resampled_data.dtype
                    outf.write(resampled_data.tostring())
            else:
                break

        raw_data = st.get_last_samples()
        if len(raw_data) > 0:
            data = np.fromstring(raw_data, dtype=np.int16)
            resampled_data = resampler.process(data, ratio).astype('int16')
            outf.write(resampled_data.tostring())

        outf.close()
        f.close()


if __name__ == '__main__':
    unittest.main()
