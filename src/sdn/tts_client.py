#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import grpc
from maum.brain.tts import ng_tts_pb2
from maum.brain.tts import ng_tts_pb2_grpc
from maum.common import lang_pb2
from multiprocessing import Process
import biz_log
import subprocess
import os
import argparse
import json

logger = biz_log.getLogger('test')

TEXT = '안녕하세요 | 마인즈랩입니다'
ADDR = ''
SDN = '127.0.0.1:50051'
# 상담사 목소리
TTS = '10.122.64.192:9999'


def test(index, use_cache, infinite, codec):
    while True:
        channel = grpc.insecure_channel(ADDR)
        stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
        req = ng_tts_pb2.TtsRequest()
        req.lang = lang_pb2.kor
        req.sampleRate = 8000
        req.audioEncoding = ng_tts_pb2.PCM
        req.text = TEXT
        # req.speaker = 0

        st = time.time()
        try:
            metadata={(b'tempo', b'1.0'), (b'campaign', b'1'), (b'samplerate', b'8000'), (b'use_cache', use_cache)}
            # resp = stub.SpeakWav(req, timeout=60)
            resp = stub.SpeakWav(req, metadata=metadata)
            logger.info('[%03d ] start read wav file' % index)
            pcm_name = 'test%02d.pcm' % index
            if ADDR == TTS:
                pcm_name = 'test%02d.wav' % index
            f = open(pcm_name, 'w')
            discard_header = True if ADDR == SDN else False
            got_first = False
            for tts in resp:
                if discard_header:
                    tts.mediaData = tts.mediaData[44:]
                    discard_header = False
                if got_first is False:
                    got_first = True
                    logger.debug('[%03d ] got first packet, delay time is %.3f' % (index, time.time() - st))
                f.write(tts.mediaData)
        except grpc.RpcError as e:
            print e
        logger.info('[%03d ] end time is %.3f' % (index, time.time() - st))
        f.close()
        if ADDR == SDN:
            if codec == 'ulaw':
                cmd = 'sox --channels 1 --type raw --rate 8000 -e u-law %s %s' % (pcm_name, pcm_name[:-4] + '.wav')
            elif codec == 'alaw':
                cmd = 'sox --channels 1 --type raw --rate 8000 -e a-law %s %s' % (pcm_name, pcm_name[:-4] + '.wav')
            else:
                cmd = 'sox -r 8000 -e signed -b 16 -c 1 -t raw %s %s' % (pcm_name, pcm_name[:-4] + '.wav')
            subprocess.call(cmd, shell=True)
            os.remove(pcm_name)
        if infinite is False:
            break


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', nargs='?', const='', metavar='text', help='input text')
    parser.add_argument('--sdn', nargs='?', const='', metavar='sdn', help='sdn address')
    parser.add_argument('--tts', nargs='?', const='', metavar='tts', help='tts address')
    parser.add_argument('--codec', nargs='?', const='pcm', metavar='codec', help='wav codec')
    args = parser.parse_args()

    global ADDR, SDN, TTS, TEXT
    use_cache = "true"
    codec = 'pcm'
    if args.sdn:
        SDN = args.sdn
    if args.tts:
        TTS = args.tts
    if args.file:
        with open(args.file) as dat:
            TEXT = json.load(dat)['text']
    if args.codec:
        codec = args.codec

    while True:
        cmd = raw_input('Select command \n'
                        '1. Run on SDN ({0}) \n'
                        '2. Run on SDN with no cache ({0}) \n'
                        '3. Run on TTS ({1}) \n'
                        '4. Remove cache \n'
                        'Q. Quit: '.format(SDN, TTS))
        if cmd == '1':
            ADDR = SDN
            use_cache = "true"
        elif cmd == '2':
            ADDR = SDN
            use_cache = "false"
        elif cmd == '3':
            ADDR = TTS
        elif cmd == '4':
            subprocess.call('./sdn.py -c 1', shell=True)
            continue
        elif cmd.lower() == 'q':
            break
        else:
            continue

        cmd = raw_input('Select Concurrent count: ')
        concurrent_cnt = int(cmd)
        cmd = raw_input('Run infinite loop? (y/n): ')
        infinite = True if cmd == 'y' else False

        logger.info('start...')
        plist = []
        st = time.time()
        for i in range(concurrent_cnt):
            p = Process(target=test, args=(i, use_cache, infinite, codec))
            p.start()
            plist.append(p)

        for p in plist:
            p.join()
        logger.info('total time is %.3f' % (time.time() - st))


if __name__ == '__main__':
    main()

