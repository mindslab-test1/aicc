-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 10.122.64.152    Database: HAPPYCALL3
-- ------------------------------------------------------
-- Server version	5.6.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `HAPPYCALL3`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `HAPPYCALL3` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `HAPPYCALL3`;

--
-- Table structure for table `ACCIDENT_INFO`
--

DROP TABLE IF EXISTS `ACCIDENT_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACCIDENT_INFO` (
  `seq` int(20) NOT NULL AUTO_INCREMENT COMMENT 'AUTO INCRE',
  `accident_no` char(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '사고 접수 번호',
  `p_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT '환자명',
  `p_mobile` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT '환자 핸드폰번호',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='사고 접수 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUTHORITY`
--

DROP TABLE IF EXISTS `AUTHORITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTHORITY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) DEFAULT NULL,
  `authority_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  KEY `JOB_INST_EXEC_FK` (`JOB_INSTANCE_ID`),
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `BATCH_JOB_INSTANCE` (`JOB_INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_CONTEXT`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_CONTEXT` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_PARAMS`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_PARAMS` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) NOT NULL,
  `KEY_NAME` varchar(100) NOT NULL,
  `STRING_VAL` varchar(250) DEFAULT NULL,
  `DATE_VAL` datetime DEFAULT NULL,
  `LONG_VAL` bigint(20) DEFAULT NULL,
  `DOUBLE_VAL` double DEFAULT NULL,
  `IDENTIFYING` char(1) NOT NULL,
  KEY `JOB_EXEC_PARAMS_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_SEQ`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_INSTANCE`
--

DROP TABLE IF EXISTS `BATCH_JOB_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_INSTANCE` (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_NAME` varchar(100) NOT NULL,
  `JOB_KEY` varchar(32) NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`),
  UNIQUE KEY `JOB_INST_UN` (`JOB_NAME`,`JOB_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_SEQ`
--

DROP TABLE IF EXISTS `BATCH_JOB_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime NOT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) DEFAULT NULL,
  `READ_COUNT` bigint(20) DEFAULT NULL,
  `FILTER_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_COUNT` bigint(20) DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  KEY `JOB_EXEC_STEP_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_CONTEXT`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION_CONTEXT` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `BATCH_STEP_EXECUTION` (`STEP_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_SEQ`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CALL_HISTORY`
--

DROP TABLE IF EXISTS `CALL_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CALL_HISTORY` (
  `call_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '콜 메타 아이디\n(stt_result_detail_tb에서 call_id 값)',
  `call_date` datetime DEFAULT NULL COMMENT '콜 생성일',
  `call_type_code` varchar(6) DEFAULT NULL COMMENT '콜 타입\n. \n\ncommon_cd.first_cd = ''06\n''\nCT0001: Inbound Call\nCT0002: Outbound Call',
  `contract_no` int(11) unsigned zerofill DEFAULT NULL COMMENT '고객code 값, \r\ncampaign_target_list_tb 테이블 참고',
  `start_time` datetime DEFAULT NULL COMMENT '콜 시작 시간',
  `end_time` datetime DEFAULT NULL COMMENT '콜 종료시간',
  `duration` float DEFAULT NULL COMMENT '콜 통화 시간',
  `call_status` char(6) DEFAULT 'CS0001' COMMENT '상태(콜백예약)\ncommon_cd.first_cd = ''02''\nCS0001 : 미실행\nCS0002 : 진행중\nCS0003 : 중지\nCS0004 : 미응답\nCS0005 : 완료\nCS0006 : 연결중\nCS0007 : 콜백\nCS0008 : 콜대기중',
  `camp_status` varchar(45) DEFAULT NULL,
  `create_dtm` datetime DEFAULT NULL COMMENT '실행일자',
  `call_memo` varchar(255) DEFAULT NULL COMMENT '콜메모',
  `monitor_cont` varchar(255) DEFAULT NULL COMMENT '모니터내용',
  `callback_dt` datetime DEFAULT NULL COMMENT '콜백요청일',
  `mnt_status` varchar(45) DEFAULT NULL,
  `mnt_status_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`call_id`),
  UNIQUE KEY `call_id_UNIQUE` (`call_id`),
  KEY `CNTRCT_IDX` (`contract_no`)
) ENGINE=InnoDB AUTO_INCREMENT=4619 DEFAULT CHARSET=utf8 COMMENT='콜 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CAMPAIGN_MNG`
--

DROP TABLE IF EXISTS `CAMPAIGN_MNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPAIGN_MNG` (
  `camp_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'campaign seq',
  `camp_nm` varchar(50) DEFAULT NULL COMMENT 'campaign 이름',
  `description` varchar(255) DEFAULT NULL COMMENT 'campaign 설명',
  `mnt_cd` varchar(10) DEFAULT NULL COMMENT '모니터링 코드',
  `chatbot_name` varchar(50) NOT NULL,
  `start_date` datetime DEFAULT NULL COMMENT '시작일',
  `end_date` datetime DEFAULT NULL COMMENT '종료일',
  `limit_date` int(11) DEFAULT '5',
  `limit_call_count` int(11) DEFAULT '30',
  `use_yn` char(1) DEFAULT 'Y',
  `creator_id` varchar(50) DEFAULT NULL COMMENT '등록자ID, 데이터 등록 발생시 등록자 ID',
  `modifier_id` varchar(50) DEFAULT NULL COMMENT '수정자ID, 데이터 등록 수정시 수정자 ID',
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  PRIMARY KEY (`camp_id`),
  UNIQUE KEY `CAMPAIGN_MNG_camp_id_UNIQUE` (`camp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='캠페인 관리';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CAMPAIGN_SERVICE`
--

DROP TABLE IF EXISTS `CAMPAIGN_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CAMPAIGN_SERVICE` (
  `service_name` varchar(50) NOT NULL,
  `camp_id` int(11) NOT NULL,
  `camp_nm` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`service_name`,`camp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `COMMON_CD`
--

DROP TABLE IF EXISTS `COMMON_CD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMMON_CD` (
  `first_cd` varchar(10) NOT NULL COMMENT '01 : 모니터링 종류\n02 : 통화상태 코드\n03 : 상담원 부서코드\n04 : 상담원 직급코드\n05 : 상담원 상태',
  `second_cd` varchar(10) DEFAULT NULL COMMENT '두번째 코드 분류 값',
  `third_cd` varchar(10) DEFAULT NULL COMMENT '세번째 코드 분류 값',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '코드. 코드 설명과 매칭되는 코드 값',
  `cd_desc` varchar(255) DEFAULT NULL COMMENT '코드 설명. 코드 설명',
  `note` varchar(255) DEFAULT NULL COMMENT '코드 부연 or 상세 설명',
  `creator_id` varchar(50) DEFAULT NULL COMMENT '등록자ID, 데이터 등록 발생시 등록자 ID',
  `modifier_id` varchar(50) DEFAULT NULL COMMENT '수정자ID, 데이터 등록 수정시 수정자 ID',
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  PRIMARY KEY (`first_cd`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공통코드';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_INFO`
--

DROP TABLE IF EXISTS `CUST_BASE_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_BASE_INFO` (
  `cust_id` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `cust_nm` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '고객명',
  `jumin_no` char(13) COLLATE utf8_bin DEFAULT NULL COMMENT '고객 주민번호',
  `cust_tel_no` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '고객 핸드폰 번호',
  `cust_tel_comp` varchar(3) COLLATE utf8_bin DEFAULT NULL COMMENT '고객 통신사명',
  `tel_comp_save_yn` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '알뜰폰 여부',
  `certifi_no` char(6) COLLATE utf8_bin DEFAULT NULL COMMENT '본인 인증 위한 인증번호 ',
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시',
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시',
  `cust_address` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '고객 주소 \nex) 건물이름까지',
  `cust_detail_address` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '고객 주소 동호수',
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='IB 고객 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_SPECIAL_MAPPING_INFO`
--

DROP TABLE IF EXISTS `CUST_BASE_SPECIAL_MAPPING_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_BASE_SPECIAL_MAPPING_INFO` (
  `cust_id` int(11) NOT NULL,
  `special_contract_id` int(11) NOT NULL,
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  `prod_id` int(11) NOT NULL,
  PRIMARY KEY (`cust_id`,`special_contract_id`,`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 정보와 특약의 매핑 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_CAR_INFO`
--

DROP TABLE IF EXISTS `CUST_CAR_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_CAR_INFO` (
  `cust_id` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `prod_id` int(11) NOT NULL COMMENT '상품 아이디',
  `contract_type` varchar(5) DEFAULT NULL COMMENT '계약 타입 : TM / OFF 계약 중 택 1',
  `store_nm` varchar(11) DEFAULT NULL COMMENT 'OFF 가입시 대리점명 또는 플래너 인지 , tm인 경우 null',
  `signature` varchar(11) DEFAULT NULL COMMENT 'OFF 가입시 팩스서명 또는 전자서명 여부, TM은 NULL',
  `plate_num` varchar(11) DEFAULT NULL COMMENT '차량 번호',
  `planner_nm` varchar(45) DEFAULT NULL COMMENT '대리점 또는 플래너 이름',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  `contract_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`cust_id`,`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='자동차 보험 관련 가입 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_INS_INFO`
--

DROP TABLE IF EXISTS `CUST_INS_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_INS_INFO` (
  `cust_id` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `prod_id` int(11) NOT NULL COMMENT '상품 아이디',
  `bank_name` varchar(10) DEFAULT NULL COMMENT '보험상품 입금 은행',
  `bank_acc_num` varchar(20) DEFAULT NULL COMMENT '보험상품 입금 계좌',
  `bank_payment_cnt` int(10) DEFAULT NULL COMMENT '보험금 납부 횟수',
  `recent_payment_date` date DEFAULT NULL COMMENT '보험금 최근 납부 일자',
  `first_payment_date` date DEFAULT NULL COMMENT '보험금 최초 납부 일자',
  `loan_avail_money` int(20) DEFAULT NULL COMMENT '대출 가능 금액 ( 상품 별 받을 수 있는 대출 금액 )',
  `loan_interest_rate` decimal(5,2) DEFAULT NULL COMMENT '이율',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  `cancel_refund_rate` decimal(5,2) DEFAULT NULL COMMENT '해지 환급률',
  `cancel_refund` int(20) DEFAULT NULL COMMENT '해지 환급금',
  `maturity_refund` int(20) DEFAULT NULL COMMENT '만기 환급금',
  `ins_join_date` date DEFAULT NULL COMMENT '보험가입월일',
  `cancel_payment_day` int(2) DEFAULT NULL COMMENT '이체일 해지 이력',
  PRIMARY KEY (`cust_id`,`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 id - 보험 상품 별 정보 ( 상품명, 거래 계좌 정보, 거래 날짜 정보)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_DETAIL_INFO`
--

DROP TABLE IF EXISTS `CUST_LOAN_DETAIL_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_LOAN_DETAIL_INFO` (
  `loan_seq` int(11) NOT NULL COMMENT '계약 대출 매핑 번호',
  `cust_id` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `prod_id` int(11) NOT NULL COMMENT '보험 상품 코드',
  `prod_loan_price` int(20) DEFAULT NULL COMMENT '대출 받는 금액',
  `prod_interest_rate` decimal(5,2) DEFAULT NULL COMMENT '상품별 이율',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  PRIMARY KEY (`loan_seq`,`cust_id`,`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 id & 대출 id 기준 - 대출받은 각 보험 상품별 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_INFO`
--

DROP TABLE IF EXISTS `CUST_LOAN_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_LOAN_INFO` (
  `seq` int(8) NOT NULL AUTO_INCREMENT COMMENT '대출받은 순서',
  `cust_id` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `loan_month_inter_price` int(20) DEFAULT NULL COMMENT '월 이자 금액',
  `loan_total_inter_price` int(20) DEFAULT NULL COMMENT '총 이자 금액',
  `loan_price` int(20) DEFAULT NULL COMMENT '대출 받는 금액',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  `bank_name` varchar(10) DEFAULT NULL COMMENT '대출 이자 납부하는 은행명',
  `bank_acc_num` varchar(20) DEFAULT NULL COMMENT '대출 이자 납부하는  은행 계좌번호',
  `interest_recent_date` datetime DEFAULT NULL COMMENT '이자 최근 납부 일자',
  `interest_first_date` datetime DEFAULT NULL COMMENT '이자 첫 납부 일자',
  `bank_payment_day` int(2) DEFAULT NULL COMMENT '납부 이자일',
  `calc_inter_price` int(7) DEFAULT NULL COMMENT '정산 이자 금액',
  `contract_no` int(11) DEFAULT NULL COMMENT '고객 code 값',
  PRIMARY KEY (`seq`,`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='고객 id - 대출 고객 계좌 및 대출한 금액 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_OP_INFO`
--

DROP TABLE IF EXISTS `CUST_OP_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_OP_INFO` (
  `cust_op_uid` int(8) unsigned NOT NULL COMMENT '상담원UID',
  `cust_op_id` varchar(50) DEFAULT NULL COMMENT '상담원ID',
  `cust_op_nm` varchar(50) NOT NULL COMMENT '상담원명',
  `password` varchar(50) DEFAULT NULL COMMENT '비밀번호',
  `ip_addr` varchar(50) DEFAULT '127.0.0.1',
  `dept_cd` varchar(10) DEFAULT NULL COMMENT '부서코드\ncommon_cd.first_cd = ''03''',
  `position_cd` varchar(10) DEFAULT NULL COMMENT '직급코드.\ncommon_cd.first_cd = ''04''',
  `cust_op_status` char(2) DEFAULT '01' COMMENT '상태\ncommon_cd.first_cd = ''05''\n 01:근무\n 02:휴식\n 03:휴가',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용여부',
  `creator_id` varchar(50) DEFAULT NULL COMMENT '등록자ID. 데이터 등록 발생시 등록자 ID',
  `modifier_id` varchar(50) DEFAULT NULL COMMENT '수정자ID. 데이터 등록 수정시 수정자 ID',
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시, 데이터 등록 발생 일시',
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시, 데이터 수정 발생 일시',
  PRIMARY KEY (`cust_op_uid`),
  UNIQUE KEY `cust_op_uid_UNIQUE` (`cust_op_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='상담원 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PIBO_INFO`
--

DROP TABLE IF EXISTS `CUST_PIBO_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_PIBO_INFO` (
  `cust_id` int(11) NOT NULL COMMENT '고객(계약자) 아이디',
  `prod_id` int(11) NOT NULL COMMENT '상품 아이디',
  `pibo_nm` varchar(45) DEFAULT NULL COMMENT '피보험자 이름',
  `pibo_tel_no` varchar(20) DEFAULT NULL COMMENT '피보험자 전화번호',
  `pibo_address` varchar(50) DEFAULT NULL COMMENT '피보험자 주소',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경날짜',
  `contract_no` int(11) NOT NULL COMMENT 'OB 대상자 고유 id',
  PRIMARY KEY (`cust_id`,`prod_id`,`contract_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='피보험자 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FLOW_TASK`
--

DROP TABLE IF EXISTS `FLOW_TASK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FLOW_TASK` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(20) NOT NULL,
  `task_info` varchar(200) DEFAULT NULL,
  `system_answer` varchar(1000) DEFAULT NULL,
  `customer_utter` varchar(1000) DEFAULT NULL,
  `intent` varchar(100) DEFAULT NULL,
  `target_task` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`seq`,`task`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_CAMPAIGN_INFO`
--

DROP TABLE IF EXISTS `HC_HH_CAMPAIGN_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HC_HH_CAMPAIGN_INFO` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `camp_id` int(11) unsigned NOT NULL COMMENT 'campaign seq',
  `category` varchar(45) DEFAULT NULL COMMENT '대분류',
  `task` varchar(45) DEFAULT NULL,
  `task_type` varchar(15) DEFAULT NULL,
  `task_answer` varchar(45) DEFAULT NULL,
  `task_info` varchar(45) DEFAULT NULL COMMENT 'C(choose) : 양자 선택\r\nV(value) : 값 입력\r\nN(nothing) : 받지 않음',
  PRIMARY KEY (`seq`,`camp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='시나리오 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_CAMPAIGN_SCORE`
--

DROP TABLE IF EXISTS `HC_HH_CAMPAIGN_SCORE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HC_HH_CAMPAIGN_SCORE` (
  `seq_num` int(11) NOT NULL AUTO_INCREMENT,
  `call_id` int(11) unsigned DEFAULT NULL COMMENT '콜ID',
  `contract_no` int(11) DEFAULT NULL COMMENT '고객 code 값',
  `info_seq` int(11) DEFAULT NULL,
  `info_task` varchar(45) DEFAULT NULL,
  `task_value` varchar(30) DEFAULT NULL,
  `review_coment` text COMMENT '상세보기 리뷰 data',
  PRIMARY KEY (`seq_num`)
) ENGINE=InnoDB AUTO_INCREMENT=24920 DEFAULT CHARSET=utf8 COMMENT='시나리오 탐지 결과';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HC_HH_LOAN_LIST`
--

DROP TABLE IF EXISTS `HC_HH_LOAN_LIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HC_HH_LOAN_LIST` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `contract_no` int(11) DEFAULT NULL,
  `category` varchar(10) DEFAULT NULL,
  `loan_name` varchar(30) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `interest_rate` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='해피콜 대출시나리오 관련 - 대출 정보 리스트';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HI_PHONE`
--

DROP TABLE IF EXISTS `HI_PHONE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HI_PHONE` (
  `sip_domain` varchar(64) NOT NULL,
  `number` char(20) NOT NULL,
  `passwd` char(20) DEFAULT NULL,
  `tel_uri` char(20) DEFAULT NULL,
  `pbx_name` varchar(45) DEFAULT NULL,
  `status` char(20) DEFAULT NULL COMMENT 'online, offline',
  `contract_no` int(11) DEFAULT NULL,
  `customer_phone` char(20) DEFAULT NULL,
  `boot_time` datetime DEFAULT NULL,
  `last_event` char(20) DEFAULT NULL,
  `last_event_time` datetime DEFAULT NULL,
  `campaign_id` varchar(100) DEFAULT NULL,
  `is_inbound` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`sip_domain`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HI_PHONE3`
--

DROP TABLE IF EXISTS `HI_PHONE3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HI_PHONE3` (
  `sip_domain` varchar(64) NOT NULL,
  `number` char(20) NOT NULL,
  `passwd` char(20) DEFAULT NULL,
  `tel_uri` char(20) DEFAULT NULL,
  `pbx_name` varchar(45) DEFAULT NULL,
  `status` char(20) DEFAULT NULL COMMENT 'online, offline',
  `contract_no` int(11) DEFAULT NULL,
  `customer_phone` char(20) DEFAULT NULL,
  `boot_time` datetime DEFAULT NULL,
  `last_event` char(20) DEFAULT NULL,
  `last_event_time` datetime DEFAULT NULL,
  `campaign_id` varchar(100) DEFAULT NULL,
  `is_inbound` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`sip_domain`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HOSPITAL_INFO`
--

DROP TABLE IF EXISTS `HOSPITAL_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOSPITAL_INFO` (
  `seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '고유코드 (AUTO INCRE)',
  `tel_no` varchar(20) DEFAULT NULL COMMENT '병원 전화번호',
  `name` varchar(45) DEFAULT NULL COMMENT '병원명',
  `fax_no` varchar(20) DEFAULT NULL COMMENT '병원 팩스번호',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='병원정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MNT_EXCEL_UP_TMP`
--

DROP TABLE IF EXISTS `MNT_EXCEL_UP_TMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MNT_EXCEL_UP_TMP` (
  `cust_uid` varchar(128) NOT NULL COMMENT '고객관리번호(=psid)',
  `cust_nm` varchar(50) DEFAULT NULL COMMENT '고객명',
  `jumin_no` char(13) DEFAULT NULL COMMENT '주민번호',
  `cust_tel_no` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `cust_type` varchar(45) DEFAULT NULL COMMENT '고객구분',
  `prod_name` varchar(128) DEFAULT NULL,
  `cust_op_id` varchar(50) DEFAULT NULL,
  `target_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='엑셀 업로드 템프 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MNT_TARGET_MNG`
--

DROP TABLE IF EXISTS `MNT_TARGET_MNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MNT_TARGET_MNG` (
  `contract_no` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '고객 code 값',
  `campaign_id` int(11) unsigned DEFAULT NULL COMMENT '캠페인ID',
  `cust_uid` varchar(128) DEFAULT '' COMMENT '고객관리번호',
  `assigned_dt` datetime DEFAULT NULL COMMENT '배정일자',
  `assigned_yn` char(1) DEFAULT 'Y',
  `target_dt` datetime DEFAULT NULL COMMENT '대상일자',
  `prod_name` varchar(128) DEFAULT '고객상담',
  `cust_nm` varchar(50) DEFAULT '',
  `jumin_no` char(13) DEFAULT '9901011234567',
  `cust_tel_no` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `cust_type` varchar(50) DEFAULT NULL COMMENT '고객구분',
  `cust_op_id` varchar(50) DEFAULT NULL COMMENT '상담원ID',
  `call_try_count` int(11) DEFAULT '0' COMMENT '통화횟수',
  `call_status` char(6) DEFAULT NULL COMMENT '통화상태 코드\n(상태(콜백예약))\n\ncommon_cd.first_cd = ''02''\nCS0001 : 미실행\nCS0002 : 진행중\nCS0003 : 중지\nCS0004 : 미응답\nCS0005 : 완료\nCS0006 : 연결중\nCS0007 : 콜백\nCS0008 : 콜대기중',
  `call_result_cd` char(6) DEFAULT NULL COMMENT '모니터링 결과 코드값\ncommon_cd.first_cd=''09''\nMR0001:전체완료\nMR0002:통화거부\nMR0003:통화중\nMR0004:콜백요청\nMR0005:미완료',
  `call_result` char(2) DEFAULT NULL COMMENT '통화결과',
  `callback_dt` datetime DEFAULT NULL COMMENT '통화가능시간',
  `callback_status` char(6) DEFAULT 'CB0001',
  `call_final_result` char(6) DEFAULT NULL COMMENT '최종결과\ncommon_cd.first_cd=''10''',
  `call_memo` varchar(255) DEFAULT NULL COMMENT '메모',
  `creator_id` varchar(50) DEFAULT NULL COMMENT '등록자ID. 데이터 등록 발생시 등록자 ID',
  `modifier_id` varchar(50) DEFAULT NULL COMMENT '수정자ID. 데이터 등록 수정시 수정자 ID',
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  `task` varchar(20) DEFAULT NULL COMMENT '시나리오 진행도 체크 로직용',
  `task_seq` int(11) DEFAULT NULL COMMENT 'DA max task 진행 위치 저장',
  `insured_contractor` varchar(10) DEFAULT NULL COMMENT '보험 가입자 확인 로직용 ',
  `insured_person` varchar(10) DEFAULT NULL COMMENT '보험 피보험자 확인용 로직',
  `addr_main` varchar(50) DEFAULT NULL COMMENT '메인주소',
  `addr_sub` varchar(100) DEFAULT NULL COMMENT '서브주소',
  `addr_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `bank_name` varchar(20) DEFAULT NULL,
  `bank_acc_num` varchar(30) DEFAULT NULL,
  `bank_payment_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_inbound` char(1) DEFAULT 'N',
  PRIMARY KEY (`contract_no`),
  UNIQUE KEY `contract_no_UNIQUE` (`contract_no`),
  KEY `CallbackIDX` (`callback_status`,`callback_dt`)
) ENGINE=InnoDB AUTO_INCREMENT=200153 DEFAULT CHARSET=utf8 COMMENT='모니터링 대상관리';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OB_CALL_QUEUE`
--

DROP TABLE IF EXISTS `OB_CALL_QUEUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OB_CALL_QUEUE` (
  `contract_no` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OB_CALL_QUEUE3`
--

DROP TABLE IF EXISTS `OB_CALL_QUEUE3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OB_CALL_QUEUE3` (
  `contract_no` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  PRIMARY KEY (`contract_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OB_PHONE_CAMPAIGN`
--

DROP TABLE IF EXISTS `OB_PHONE_CAMPAIGN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OB_PHONE_CAMPAIGN` (
  `dial_no` varchar(100) NOT NULL,
  `campaign_id` varchar(100) NOT NULL,
  PRIMARY KEY (`dial_no`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OB_PHONE_CAMPAIGN2`
--

DROP TABLE IF EXISTS `OB_PHONE_CAMPAIGN2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OB_PHONE_CAMPAIGN2` (
  `dial_no` varchar(100) NOT NULL,
  `campaign_id` varchar(100) NOT NULL,
  PRIMARY KEY (`dial_no`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHONE_BOOK`
--

DROP TABLE IF EXISTS `PHONE_BOOK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHONE_BOOK` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(45) DEFAULT NULL,
  `tel_no` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROD_INFO`
--

DROP TABLE IF EXISTS `PROD_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROD_INFO` (
  `prod_id` int(11) NOT NULL COMMENT '상품 코드',
  `prod_nm` varchar(50) DEFAULT NULL COMMENT '보험 상품명',
  `create_dtm` datetime DEFAULT NULL COMMENT '생성 날짜',
  `update_dtm` datetime DEFAULT NULL COMMENT '변경 날짜',
  `campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='상품 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ROLE`
--

DROP TABLE IF EXISTS `ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ROLE` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(45) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SESSION_HISTORY`
--

DROP TABLE IF EXISTS `SESSION_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SESSION_HISTORY` (
  `seq` int(100) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL,
  `current_task` varchar(100) DEFAULT NULL,
  `intent` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`seq`,`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SPECIAL_CONTRACT_INFO`
--

DROP TABLE IF EXISTS `SPECIAL_CONTRACT_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SPECIAL_CONTRACT_INFO` (
  `special_contract_id` int(11) NOT NULL COMMENT '특약 id',
  `special_contract_nm` varchar(45) DEFAULT NULL COMMENT '특약 이름',
  `contract_type` varchar(11) NOT NULL COMMENT 'tm 또는 off 인지 ',
  PRIMARY KEY (`special_contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='특약 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STT_RESULT_DETAIL`
--

DROP TABLE IF EXISTS `STT_RESULT_DETAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STT_RESULT_DETAIL` (
  `stt_result_detail_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '세부 STT 결과 아이디',
  `stt_result_id` int(11) DEFAULT NULL COMMENT 'STT 결과 아이디(추후 삭제 예정)',
  `call_id` int(11) unsigned NOT NULL COMMENT 'call 아이디',
  `speaker_code` char(6) DEFAULT NULL COMMENT '화자\nST0001 : Client\r\nST0002 : Agent\r\nST0003 : Mono',
  `sentence_id` int(11) DEFAULT NULL COMMENT '문장 번호',
  `sentence` text COMMENT '인식 결과',
  `start_time` time(2) DEFAULT NULL COMMENT '시작 시간',
  `end_time` time(2) DEFAULT NULL COMMENT '종료 시간',
  `speed` float DEFAULT NULL COMMENT '스피드',
  `slience_yn` char(1) DEFAULT NULL COMMENT '묵음 여부',
  `created_dtm` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `updated_dtm` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시',
  `creator_id` varchar(50) DEFAULT NULL COMMENT '생성자 아이디',
  `updator_id` varchar(50) DEFAULT NULL COMMENT '수정자 아이디',
  `ignored` char(1) DEFAULT 'N',
  PRIMARY KEY (`stt_result_detail_id`,`call_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64123 DEFAULT CHARSET=utf8 COMMENT='STT 결과';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 NOT NULL,
  `isAccountNonExpired` tinyint(1) DEFAULT NULL,
  `isAccountNonLocked` tinyint(1) DEFAULT NULL,
  `isCredentialsNonExpired` tinyint(1) DEFAULT NULL,
  `isEnabled` tinyint(1) DEFAULT NULL,
  `created_dtm` datetime DEFAULT CURRENT_TIMESTAMP,
  `modify_dtm` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-08 19:05:30
