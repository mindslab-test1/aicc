-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 10.122.64.141    Database: HAPPYCALL4
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `HAPPYCALL4`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `HAPPYCALL4` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `HAPPYCALL4`;

--
-- Table structure for table `BATCH_JOB_EXECUTION`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  KEY `JOB_INST_EXEC_FK` (`JOB_INSTANCE_ID`),
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `BATCH_JOB_INSTANCE` (`JOB_INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_CONTEXT`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_CONTEXT` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_PARAMS`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_PARAMS` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) NOT NULL,
  `KEY_NAME` varchar(100) NOT NULL,
  `STRING_VAL` varchar(250) DEFAULT NULL,
  `DATE_VAL` datetime DEFAULT NULL,
  `LONG_VAL` bigint(20) DEFAULT NULL,
  `DOUBLE_VAL` double DEFAULT NULL,
  `IDENTIFYING` char(1) NOT NULL,
  KEY `JOB_EXEC_PARAMS_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_EXECUTION_SEQ`
--

DROP TABLE IF EXISTS `BATCH_JOB_EXECUTION_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_EXECUTION_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_INSTANCE`
--

DROP TABLE IF EXISTS `BATCH_JOB_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_INSTANCE` (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_NAME` varchar(100) NOT NULL,
  `JOB_KEY` varchar(32) NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`),
  UNIQUE KEY `JOB_INST_UN` (`JOB_NAME`,`JOB_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_JOB_SEQ`
--

DROP TABLE IF EXISTS `BATCH_JOB_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_JOB_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime NOT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) DEFAULT NULL,
  `READ_COUNT` bigint(20) DEFAULT NULL,
  `FILTER_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_COUNT` bigint(20) DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  KEY `JOB_EXEC_STEP_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `BATCH_JOB_EXECUTION` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_CONTEXT`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION_CONTEXT` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `BATCH_STEP_EXECUTION` (`STEP_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BATCH_STEP_EXECUTION_SEQ`
--

DROP TABLE IF EXISTS `BATCH_STEP_EXECUTION_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BATCH_STEP_EXECUTION_SEQ` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CALL_HISTORY`
--

DROP TABLE IF EXISTS `CALL_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CALL_HISTORY` (
  `CALL_ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '콜 메타 아이디\n(stt_result_detail_tb에서 call_id 값)',
  `CALL_DATE` datetime DEFAULT NULL COMMENT '콜 생성일',
  `CALL_TYPE_CODE` varchar(6) DEFAULT NULL COMMENT '콜 타입\n. \n\ncommon_cd.first_cd = ''06\n''\nCT0001: Inbound Call\nCT0002: Outbound Call',
  `CONTRACT_NO` int(11) unsigned DEFAULT NULL COMMENT '고객code 값, \r\ncampaign_target_list_tb 테이블 참고',
  `START_TIME` datetime DEFAULT NULL COMMENT '콜 시작 시간',
  `END_TIME` datetime DEFAULT NULL COMMENT '콜 종료시간',
  `DURATION` float DEFAULT NULL COMMENT '콜 통화 시간',
  `CALL_STATUS` char(6) DEFAULT 'CS0001' COMMENT '상태(콜백예약)\ncommon_cd.first_cd = ''02''\nCS0001 : 미실행\nCS0002 : 진행중\nCS0003 : 중지\nCS0004 : 미응답\nCS0005 : 완료\nCS0006 : 연결중\nCS0007 : 콜백\nCS0008 : 콜대기중',
  `CAMP_STATUS` varchar(45) DEFAULT NULL,
  `CALL_MEMO` varchar(255) DEFAULT NULL COMMENT '콜메모',
  `MONITOR_CONT` varchar(255) DEFAULT NULL COMMENT '모니터내용',
  `CALLBACK_STATUS` char(6) DEFAULT NULL,
  `CALLBACK_DT` datetime DEFAULT NULL COMMENT '콜백요청일',
  `MNT_STATUS` varchar(45) DEFAULT NULL,
  `MNT_STATUS_NAME` varchar(100) DEFAULT NULL,
  `TASK_ID` int(11) DEFAULT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CALL_ID`),
  KEY `CNTRCT_IDX` (`CONTRACT_NO`,`CALL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='콜 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_AUTH_ROLE`
--

DROP TABLE IF EXISTS `CM_AUTH_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_AUTH_ROLE` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_CD` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ROLE_NM` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_AUTH_USER`
--

DROP TABLE IF EXISTS `CM_AUTH_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_AUTH_USER` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PASSWORD` varchar(500) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CUST_OP_ID` int(11) DEFAULT NULL,
  `isAccountNonExpired` tinyint(1) DEFAULT NULL,
  `isAccountNonLocked` tinyint(1) DEFAULT NULL,
  `isCredentialsNonExpired` tinyint(1) DEFAULT NULL,
  `isEnabled` tinyint(1) DEFAULT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `username_UNIQUE` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_AUTH_USER_ROLE_MAP`
--

DROP TABLE IF EXISTS `CM_AUTH_USER_ROLE_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_AUTH_USER_ROLE_MAP` (
  `AUTH_USER_ROLE_MAP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(15) CHARACTER SET utf8 NOT NULL,
  `ROLE_ID` varchar(20) CHARACTER SET utf8 NOT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`AUTH_USER_ROLE_MAP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_CAMPAIGN`
--

DROP TABLE IF EXISTS `CM_CAMPAIGN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_CAMPAIGN` (
  `CAMPAIGN_ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'campaign seq',
  `CAMPAIGN_NM` varchar(50) DEFAULT NULL COMMENT 'campaign 이름',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT 'campaign 설명',
  `MNT_CD` varchar(10) DEFAULT NULL COMMENT '모니터링 코드',
  `CHATBOT_NAME` varchar(50) NOT NULL,
  `STT_MODEL` varchar(50) DEFAULT 'baseline-kor-8000',
  `STT_AGENT_MODEL` varchar(50) DEFAULT 'baseline-kor-16000',
  `TTS_TEMPO` float DEFAULT '1',
  `START_DT` date DEFAULT NULL COMMENT '시작일',
  `END_DT` date DEFAULT NULL COMMENT '종료일',
  `LIMIT_DATE` int(11) DEFAULT '5',
  `LIMIT_CALL_COUNT` int(11) DEFAULT '30',
  `USE_YN` char(1) DEFAULT 'Y',
  `CREATOR_ID` int(11) DEFAULT NULL COMMENT '등록자ID, 데이터 등록 발생시 등록자 ID',
  `UPDATER_ID` int(11) DEFAULT NULL COMMENT '수정자ID, 데이터 등록 수정시 수정자 ID',
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  `STT_MODEL_ID` int(11) DEFAULT NULL,
  `STT_AGENT_MODEL_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CAMPAIGN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='캠페인 관리';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_CAMPAIGN_INFO`
--

DROP TABLE IF EXISTS `CM_CAMPAIGN_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_CAMPAIGN_INFO` (
  `CAMPAIGN_INFO_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SEQ` int(11) NOT NULL,
  `CAMPAIGN_ID` int(11) NOT NULL COMMENT 'campaign seq',
  `CATEGORY` varchar(45) DEFAULT NULL COMMENT '대분류',
  `TASK` varchar(45) DEFAULT NULL,
  `TASK_TYPE` varchar(15) DEFAULT NULL,
  `TASK_ANSWER` varchar(45) DEFAULT NULL,
  `TASK_INFO` varchar(45) DEFAULT NULL COMMENT 'C(choose) : 양자 선택\r\nV(value) : 값 입력\r\nN(nothing) : 받지 않음',
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CAMPAIGN_INFO_ID`),
  KEY `SEQ_IDX` (`CAMPAIGN_ID`,`SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시나리오 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_CAMPAIGN_SCORE`
--

DROP TABLE IF EXISTS `CM_CAMPAIGN_SCORE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_CAMPAIGN_SCORE` (
  `SEQ_NUM` int(11) NOT NULL AUTO_INCREMENT,
  `CALL_ID` int(11) unsigned DEFAULT NULL COMMENT '콜ID',
  `CONTRACT_NO` int(11) DEFAULT NULL COMMENT '고객 code 값',
  `CAMPAIGN_ID` int(11) DEFAULT NULL,
  `INFO_SEQ` int(11) DEFAULT NULL,
  `INFO_TASK` varchar(45) DEFAULT NULL,
  `TASK_VALUE` varchar(30) DEFAULT NULL,
  `REVIEW_COMMENT` text COMMENT '상세보기 리뷰 data',
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SEQ_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='시나리오 탐지 결과';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_CAMPAIGN_SERVICE`
--

DROP TABLE IF EXISTS `CM_CAMPAIGN_SERVICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_CAMPAIGN_SERVICE` (
  `SERVICE_NM` varchar(50) NOT NULL,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `CAMPAIGN_NM` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SERVICE_NM`,`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_COMMON_CD`
--

DROP TABLE IF EXISTS `CM_COMMON_CD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_COMMON_CD` (
  `FIRST_CD` varchar(10) NOT NULL COMMENT '01 : 모니터링 종류\n02 : 통화상태 코드\n03 : 상담원 부서코드\n04 : 상담원 직급코드\n05 : 상담원 상태',
  `SECOND_CD` varchar(10) DEFAULT NULL COMMENT '두번째 코드 분류 값',
  `THIRD_CD` varchar(10) DEFAULT NULL COMMENT '세번째 코드 분류 값',
  `CODE` varchar(10) NOT NULL DEFAULT '' COMMENT '코드. 코드 설명과 매칭되는 코드 값',
  `CD_DESC` varchar(255) DEFAULT NULL COMMENT '코드 설명. 코드 설명',
  `NOTE` varchar(255) DEFAULT NULL COMMENT '코드 부연 or 상세 설명',
  `CREATOR_ID` int(11) DEFAULT NULL COMMENT '등록자ID, 데이터 등록 발생시 등록자 ID',
  `UPDATER_ID` int(11) DEFAULT NULL COMMENT '수정자ID, 데이터 등록 수정시 수정자 ID',
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  PRIMARY KEY (`FIRST_CD`,`CODE`),
  KEY `CD_IDX` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공통코드';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_CONTRACT`
--

DROP TABLE IF EXISTS `CM_CONTRACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_CONTRACT` (
  `CONTRACT_NO` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '고객 code 값',
  `CAMPAIGN_ID` int(11) DEFAULT NULL COMMENT '캠페인ID',
  `CUST_ID` int(11) DEFAULT NULL,
  `TEL_NO` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `CUST_OP_ID` int(11) DEFAULT NULL COMMENT '상담원ID',
  `PROD_ID` int(11) DEFAULT NULL,
  `CALL_TRY_COUNT` int(11) DEFAULT '0' COMMENT '통화횟수',
  `LAST_CALL_ID` int(11) DEFAULT NULL COMMENT '통화상태 코드\n(상태(콜백예약))\n\ncommon_cd.first_cd = ''02''\nCS0001 : 미실행\nCS0002 : 진행중\nCS0003 : 중지\nCS0004 : 미응답\nCS0005 : 완료\nCS0006 : 연결중\nCS0007 : 콜백\nCS0008 : 콜대기중',
  `IS_INBOUND` char(1) DEFAULT 'N',
  `TASK_SEQ` int(11) DEFAULT NULL COMMENT 'TASK 진행 상황을 저장',
  `CREATOR_ID` int(11) DEFAULT NULL COMMENT '등록자ID. 데이터 등록 발생시 등록자 ID',
  `UPDATER_ID` int(11) DEFAULT NULL COMMENT '수정자ID. 데이터 등록 수정시 수정자 ID',
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시. 데이터 등록 발생 일시',
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시. 데이터 수정 발생 일시',
  PRIMARY KEY (`CONTRACT_NO`),
  KEY `MNT_TARGET_MNG_cust_tel_no_IDX` (`CAMPAIGN_ID`,`IS_INBOUND`,`TEL_NO`) USING BTREE,
  KEY `CUST_ID_IDX` (`CUST_ID`),
  KEY `LASTCALL_IDX` (`LAST_CALL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='모니터링 대상관리';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_EXCEL_UPLD_TMP`
--

DROP TABLE IF EXISTS `CM_EXCEL_UPLD_TMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_EXCEL_UPLD_TMP` (
  `CAMPAIGN_ID` varchar(128) DEFAULT NULL,
  `CUST_ID` varchar(50) DEFAULT NULL COMMENT '고객명',
  `CUST_OP_ID` varchar(50) DEFAULT NULL,
  `TARGET_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='엑셀 업로드 템프 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_MNTR_TRGT_MNGMT_IB_INFO`
--

DROP TABLE IF EXISTS `CM_MNTR_TRGT_MNGMT_IB_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_MNTR_TRGT_MNGMT_IB_INFO` (
  `IB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTRACT_NO` int(11) NOT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`IB_ID`),
  UNIQUE KEY `id_UNIQUE` (`IB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_MNTR_TRGT_MNGMT_OB_INFO`
--

DROP TABLE IF EXISTS `CM_MNTR_TRGT_MNGMT_OB_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_MNTR_TRGT_MNGMT_OB_INFO` (
  `OB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTRACT_NO` int(11) NOT NULL,
  `ASSIGNED_DT` date DEFAULT NULL,
  `ASSIGNED_YN` char(1) DEFAULT 'N',
  `CUST_TYPE` varchar(50) DEFAULT NULL,
  `TARGET_DT` date DEFAULT NULL,
  `TARGET_YN` char(1) DEFAULT NULL,
  `CALLBACK_DT` date DEFAULT NULL,
  `CALLBACK_STATUS` char(6) DEFAULT 'CB0001',
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OB_ID`),
  UNIQUE KEY `id_UNIQUE` (`OB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_OP_INFO`
--

DROP TABLE IF EXISTS `CM_OP_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_OP_INFO` (
  `CUST_OP_ID` varchar(50) NOT NULL COMMENT '상담원ID',
  `CUST_OP_NM` varchar(50) NOT NULL COMMENT '상담원명',
  `PASSWORD` varchar(50) DEFAULT NULL COMMENT '비밀번호',
  `ID_ADDR` varchar(50) DEFAULT '127.0.0.1',
  `DEPT_CD` varchar(10) DEFAULT NULL COMMENT '부서코드\ncommon_cd.first_cd = ''03''',
  `POSITION_CD` varchar(10) DEFAULT NULL COMMENT '직급코드.\ncommon_cd.first_cd = ''04''',
  `CUST_OP_STATUS` char(2) DEFAULT '01' COMMENT '상태\ncommon_cd.first_cd = ''05''\n 01:근무\n 02:휴식\n 03:휴가',
  `USE_YN` char(1) DEFAULT NULL COMMENT '사용여부',
  `CREATOR_ID` int(11) DEFAULT NULL COMMENT '등록자ID. 데이터 등록 발생시 등록자 ID',
  `UPDATER_ID` int(11) DEFAULT NULL COMMENT '수정자ID. 데이터 등록 수정시 수정자 ID',
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시, 데이터 등록 발생 일시',
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시, 데이터 수정 발생 일시',
  PRIMARY KEY (`CUST_OP_ID`),
  UNIQUE KEY `cust_op_id_UNIQUE` (`CUST_OP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='상담원 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_PHONE_BOOK`
--

DROP TABLE IF EXISTS `CM_PHONE_BOOK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_PHONE_BOOK` (
  `PHONE_BOOK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DEPARTMENT` varchar(45) DEFAULT NULL,
  `TEL_NO` varchar(45) DEFAULT NULL,
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT NULL,
  `UPDATED_DTM` datetime DEFAULT NULL,
  `MANAGER` varchar(45) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`PHONE_BOOK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CM_STT_RSLT_DETAIL`
--

DROP TABLE IF EXISTS `CM_STT_RSLT_DETAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CM_STT_RSLT_DETAIL` (
  `STT_RESULT_DETAIL_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '세부 STT 결과 아이디',
  `STT_RESULT_ID` int(11) DEFAULT NULL COMMENT 'STT 결과 아이디(추후 삭제 예정)',
  `CALL_ID` int(11) unsigned NOT NULL COMMENT 'call 아이디',
  `SPEAKER_CODE` char(6) DEFAULT NULL COMMENT '화자\nST0001 : Client\r\nST0002 : Agent\r\nST0003 : Mono',
  `SENTENCE_ID` int(11) DEFAULT NULL COMMENT '문장 번호',
  `SENTENCE` text COMMENT '인식 결과',
  `START_TIME` decimal(10,2) DEFAULT NULL COMMENT '시작 시간',
  `END_TIME` decimal(10,2) DEFAULT NULL COMMENT '종료 시간',
  `SPEED` float DEFAULT NULL COMMENT '스피드',
  `SILENCE_YN` char(1) DEFAULT NULL COMMENT '묵음 여부',
  `IGNORED` char(1) DEFAULT 'N',
  `CREATOR_ID` int(11) DEFAULT NULL COMMENT '생성자 아이디',
  `UPDATER_ID` int(11) DEFAULT NULL COMMENT '수정자 아이디',
  `UPDATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시',
  `CREATED_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  PRIMARY KEY (`STT_RESULT_DETAIL_ID`,`CALL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='STT 결과';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_INFO`
--

DROP TABLE IF EXISTS `CUST_BASE_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_BASE_INFO` (
  `CUST_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '가입자 고유 아이디',
  `CUST_NM` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '고객명',
  `JUMIN_NO` char(13) CHARACTER SET utf8 DEFAULT NULL COMMENT '고객 주민번호',
  `CUST_TEL_NO` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '고객 핸드폰 번호',
  `CUST_COMPANY_NO` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `CUST_HOME_NO` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `CUST_ETC_NO` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `CUST_TEL_COMP` varchar(3) CHARACTER SET utf8 DEFAULT NULL COMMENT '고객 통신사명',
  `TEL_COMP_SAVE_YN` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT '알뜰폰 여부',
  `CERTIFI_NO` char(6) CHARACTER SET utf8 DEFAULT NULL COMMENT '본인 인증 위한 인증번호 ',
  `CREATE_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '등록일시',
  `MODIFY_DTM` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '수정일시',
  `CUST_ADDRESS` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '고객 주소 \nex) 건물이름까지',
  `CUST_DETAIL_ADDRESS` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '고객 주소 동호수',
  PRIMARY KEY (`CUST_ID`),
  UNIQUE KEY `CUST_ID_UNIQUE` (`CUST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='IB 고객 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_BASE_SPECIAL_MAPPING_INFO`
--

DROP TABLE IF EXISTS `CUST_BASE_SPECIAL_MAPPING_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_BASE_SPECIAL_MAPPING_INFO` (
  `CUST_ID` int(11) NOT NULL,
  `SPECIAL_CONTRACT_ID` int(11) NOT NULL,
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  `PROD_ID` int(11) NOT NULL,
  `AGE_LIMIT` varchar(45) DEFAULT NULL,
  `DRIVER_LIMIT` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CUST_ID`,`SPECIAL_CONTRACT_ID`,`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 정보와 특약의 매핑 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_CAR_INFO`
--

DROP TABLE IF EXISTS `CUST_CAR_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_CAR_INFO` (
  `CUST_ID` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `PROD_ID` int(11) NOT NULL COMMENT '상품 아이디',
  `CONTRACT_TYPE` varchar(5) DEFAULT NULL COMMENT '계약 타입 : TM / OFF 계약 중 택 1',
  `STORE_NM` varchar(11) DEFAULT NULL COMMENT 'OFF 가입시 대리점명 또는 플래너 인지 , tm인 경우 null',
  `SIGNATURE` varchar(11) DEFAULT NULL COMMENT 'OFF 가입시 팩스서명 또는 전자서명 여부, TM은 NULL',
  `PLATE_NUM` varchar(11) DEFAULT NULL COMMENT '차량 번호',
  `PLANNER_NM` varchar(45) DEFAULT NULL COMMENT '대리점 또는 플래너 이름',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  `CONTRACT_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`CUST_ID`,`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='자동차 보험 관련 가입 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_HOSPITAL_INFO`
--

DROP TABLE IF EXISTS `CUST_HOSPITAL_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_HOSPITAL_INFO` (
  `SEQ` int(20) NOT NULL AUTO_INCREMENT COMMENT '고유seq (AUTO INCRE)',
  `TEL_NO` varchar(45) DEFAULT NULL COMMENT '병원 전화번호',
  `NAME` varchar(45) DEFAULT NULL COMMENT '병원명(의료기관명)',
  `FAX_NO` varchar(45) DEFAULT NULL COMMENT '병원(의료기관) 팩스번호',
  PRIMARY KEY (`SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='병원정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_INS_COVERAGE_INFO`
--

DROP TABLE IF EXISTS `CUST_INS_COVERAGE_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_INS_COVERAGE_INFO` (
  `INS_COVERAGE_ID` int(20) NOT NULL COMMENT '담보 아이디',
  `INS_COVERAGE_NM` varchar(45) DEFAULT NULL COMMENT '담보명',
  `CREATE_DTM` datetime DEFAULT NULL,
  `UPDATE_DTM` datetime DEFAULT NULL,
  PRIMARY KEY (`INS_COVERAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='담보 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_INS_INFO`
--

DROP TABLE IF EXISTS `CUST_INS_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_INS_INFO` (
  `CUST_ID` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `PROD_ID` int(11) NOT NULL COMMENT '상품 아이디',
  `DB_TYPE` varchar(45) NOT NULL,
  `BANK_NAME` varchar(10) DEFAULT NULL COMMENT '보험상품 입금 은행',
  `BANK_ACC_NUM` varchar(20) DEFAULT NULL COMMENT '보험상품 입금 계좌',
  `BANK_PAYMENT_CNT` int(10) DEFAULT NULL COMMENT '보험금 납부 횟수',
  `RECENT_PAYMENT_DATE` date DEFAULT NULL COMMENT '보험금 최근 납부 일자',
  `FIRST_PAYMENT_DATE` date DEFAULT NULL COMMENT '보험금 최초 납부 일자',
  `LOAN_AVAIL_MONEY` int(20) DEFAULT NULL COMMENT '대출 가능 금액 ( 상품 별 받을 수 있는 대출 금액 )',
  `LOAN_INTEREST_RATE` decimal(5,2) DEFAULT NULL COMMENT '이율',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  `CANCEL_REFUND_RATE` decimal(5,2) DEFAULT NULL COMMENT '해지 환급률',
  `CANCEL_REFUND` int(20) DEFAULT NULL COMMENT '해지 환급금',
  `MATURITY_REFUND` int(20) DEFAULT NULL COMMENT '만기 환급금',
  `INS_JOIN_DATE` date DEFAULT NULL COMMENT '보험가입월일',
  `CANCEL_PAYMENT_DAY` int(2) DEFAULT NULL COMMENT '해지이체일 이력',
  PRIMARY KEY (`CUST_ID`,`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 id - 보험 상품 별 정보 ( 상품명, 거래 계좌 정보, 거래 날짜 정보)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_DETAIL_INFO`
--

DROP TABLE IF EXISTS `CUST_LOAN_DETAIL_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_LOAN_DETAIL_INFO` (
  `LOAN_SEQ` int(11) NOT NULL COMMENT '계약 대출 매핑 번호',
  `CUST_ID` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `PROD_ID` int(11) NOT NULL COMMENT '보험 상품 코드',
  `PROD_LOAN_PRICE` int(20) DEFAULT NULL COMMENT '대출 받는 금액',
  `PROD_INTEREST_RATE` decimal(5,2) DEFAULT NULL COMMENT '상품별 이율',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  PRIMARY KEY (`CUST_ID`,`PROD_ID`,`LOAN_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 id & 대출 id 기준 - 대출받은 각 보험 상품별 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_LOAN_INFO`
--

DROP TABLE IF EXISTS `CUST_LOAN_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_LOAN_INFO` (
  `SEQ` int(8) NOT NULL AUTO_INCREMENT COMMENT '대출받은 순서',
  `CUST_ID` int(11) NOT NULL COMMENT '가입자 고유 아이디',
  `LOAN_MONTH_INTER_PRICE` int(20) DEFAULT NULL COMMENT '월 이자 금액',
  `LOAN_TOTAL_INTER_PRICE` int(20) DEFAULT NULL COMMENT '총 이자 금액',
  `LOAN_PRICE` int(20) DEFAULT NULL COMMENT '대출 받는 금액',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  `BANK_NAME` varchar(10) DEFAULT NULL COMMENT '대출 이자 납부하는 은행명',
  `BANK_ACC_NUM` varchar(20) DEFAULT NULL COMMENT '대출 이자 납부하는  은행 계좌번호',
  `INTEREST_RECENT_DATE` datetime DEFAULT NULL COMMENT '이자 최근 납부 일자',
  `INTEREST_FIRST_DATE` datetime DEFAULT NULL COMMENT '이자 첫 납부 일자',
  `BANK_PAYMENT_DAY` int(2) DEFAULT NULL COMMENT '납부 이자일',
  `CALC_INTER_PRICE` int(7) DEFAULT NULL COMMENT '정산 이자 금액',
  `CONTRACT_NO` int(11) DEFAULT NULL COMMENT '고객 code 값',
  PRIMARY KEY (`SEQ`,`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='고객 id - 대출 고객 계좌 및 대출한 금액 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PATIENT_INFO`
--

DROP TABLE IF EXISTS `CUST_PATIENT_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_PATIENT_INFO` (
  `SEQ` int(20) NOT NULL AUTO_INCREMENT COMMENT '고유 seq (AUTO INCRE)',
  `ACCIDENT_NO` varchar(45) DEFAULT NULL COMMENT '사고접수번호',
  `P_NAME` varchar(45) DEFAULT NULL COMMENT '환자명',
  `P_MOBILE` varchar(45) DEFAULT NULL COMMENT '환자 핸드폰번호',
  `FEE_PAYDAY` date DEFAULT NULL COMMENT '진료비 지급일자',
  `P_MNG_NM` varchar(10) DEFAULT NULL COMMENT '대인담당자 이름',
  `P_MNG_TEL` varchar(45) DEFAULT NULL COMMENT '대인 담당자 연락처',
  `CREATE_DTM` datetime DEFAULT NULL,
  `UPDATE_DTM` datetime DEFAULT NULL,
  PRIMARY KEY (`SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='사고접수';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PATIENT_INS_COVERAGE_INFO`
--

DROP TABLE IF EXISTS `CUST_PATIENT_INS_COVERAGE_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_PATIENT_INS_COVERAGE_INFO` (
  `INS_COVERAGE_ID` int(20) NOT NULL COMMENT '담보ID',
  `P_ID` int(20) NOT NULL COMMENT '환자ID (= SEQ IN CUST_PATIENT_INFO 테이블)',
  `JOIN_FEE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '가입금액',
  `ACC_LEVEL` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '사고급수',
  `LIMIT_AMOUNT` int(20) DEFAULT NULL COMMENT '한도금액',
  `CONFIRM_YN` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '확정여부 Y/N',
  `CREATE_DTM` datetime DEFAULT NULL,
  `UPDATE_DTM` datetime DEFAULT NULL,
  PRIMARY KEY (`INS_COVERAGE_ID`,`P_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='환자 별 담보 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PIBO_INFO`
--

DROP TABLE IF EXISTS `CUST_PIBO_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_PIBO_INFO` (
  `CUST_ID` int(11) NOT NULL COMMENT '고객(계약자) 아이디',
  `PROD_ID` int(11) NOT NULL COMMENT '상품 아이디',
  `PIBO_NM` varchar(45) DEFAULT NULL COMMENT '피보험자 이름',
  `PIBO_TEL_NO` varchar(20) DEFAULT NULL COMMENT '피보험자 전화번호',
  `PIBO_ADDRESS` varchar(50) DEFAULT NULL COMMENT '피보험자 주소',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경날짜',
  `CONTRACT_NO` int(11) NOT NULL COMMENT 'OB 대상자 고유 id',
  PRIMARY KEY (`CUST_ID`,`PROD_ID`,`CONTRACT_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='피보험자 정보 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_PROD_INFO`
--

DROP TABLE IF EXISTS `CUST_PROD_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_PROD_INFO` (
  `PROD_ID` int(11) NOT NULL COMMENT '상품 코드',
  `PROD_NM` varchar(50) DEFAULT NULL COMMENT '보험 상품명',
  `CREATE_DTM` datetime DEFAULT NULL COMMENT '생성 날짜',
  `UPDATE_DTM` datetime DEFAULT NULL COMMENT '변경 날짜',
  `CAMPAIGN_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PROD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='상품 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CUST_SPECIAL_CONTRACT_INFO`
--

DROP TABLE IF EXISTS `CUST_SPECIAL_CONTRACT_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CUST_SPECIAL_CONTRACT_INFO` (
  `SPECIAL_CONTRACT_ID` int(11) NOT NULL COMMENT '특약 id',
  `SPECIAL_CONTRACT_NM` varchar(45) DEFAULT NULL COMMENT '특약 이름',
  `CONTRACT_TYPE` varchar(11) NOT NULL COMMENT 'tm 또는 off 인지 ',
  PRIMARY KEY (`SPECIAL_CONTRACT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='특약 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OB_CALL_QUEUE`
--

DROP TABLE IF EXISTS `OB_CALL_QUEUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OB_CALL_QUEUE` (
  `CONTRACT_NO` int(11) NOT NULL,
  `CAMPAIGN_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SESSION_HISTORY`
--

DROP TABLE IF EXISTS `SESSION_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SESSION_HISTORY` (
  `SEQ` int(100) NOT NULL AUTO_INCREMENT,
  `SESSION_ID` varchar(100) NOT NULL,
  `CURRENT_TASK` varchar(100) DEFAULT NULL,
  `INTENT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`SEQ`,`SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIP_ACCOUNT`
--

DROP TABLE IF EXISTS `SIP_ACCOUNT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIP_ACCOUNT` (
  `SIP_DOMAIN` varchar(64) NOT NULL,
  `SIP_USER` char(20) NOT NULL,
  `PASSWD` char(20) DEFAULT NULL,
  `TEL_URI` char(20) DEFAULT NULL,
  `PBX_NAME` varchar(45) DEFAULT NULL,
  `STATUS` char(20) DEFAULT NULL COMMENT 'online, offline',
  `CONTRACT_NO` int(11) DEFAULT NULL,
  `CUSTOMER_PHONE` char(20) DEFAULT NULL,
  `BOOT_TIME` datetime DEFAULT NULL,
  `LAST_EVENT` char(20) DEFAULT NULL,
  `LAST_EVENT_TIME` datetime DEFAULT NULL,
  `CAMPAIGN_ID` varchar(100) DEFAULT NULL,
  `IS_INBOUND` char(1) NOT NULL DEFAULT 'N',
  `OB_PREFIX` varchar(10) NOT NULL DEFAULT '',
  `CREATOR_ID` int(11) DEFAULT NULL,
  `UPDATER_ID` int(11) DEFAULT NULL,
  `CREATED_DTM` datetime DEFAULT NULL,
  `UPDATED_DTM` datetime DEFAULT NULL,
  PRIMARY KEY (`SIP_DOMAIN`,`SIP_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STT_SERVER_INFO`
--

DROP TABLE IF EXISTS `STT_SERVER_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STT_SERVER_INFO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDR` varchar(100) NOT NULL,
  `MODEL` varchar(100) NOT NULL,
  `DESC` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-16  9:25:51
