#!/usr/bin/python
# -*- coding: utf-8 -*-

import socket

class RecordClient:
    start_delim = chr(0x02)
    end_delim = chr(0x03)

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def dump_header(self, msg):
        # start delimiter: 1byte, header: 10byte
        if len(msg) < 11:
            print 'not enough msg'
            return
        header = msg[1:11]
        packet_len = int(header[0:4])
        command = header[4:8]
        param_cnt = header[8:10]
        print 'packet len:%d, command:%s, param_cnt:%d' % (packet_len, command, param_cnt)

    def send(self, msg):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
            s.connect((self.host, self.port))
            s.settimeout(30)
	    s.sendall(msg)
	    resp = s.recv(8192)
            print resp
            self.dump_header(resp)
        except socket.error as e:
            print str(e)
        finally:
            s.close()

    def send_start(self, sip_num, agent_id, agent_name, key_code, customer_name,
                   bank_name, account_no, department, result_code):
        self.send_record("RSTQ", sip_num, agent_id, agent_name, key_code, customer_name,
                        bank_name, account_no, department, result_code)
    def send_stop(self, sip_num, agent_id, agent_name, key_code, customer_name,
                  bank_name, account_no, department, result_code):
        self.send_record("RENQ", sip_num, agent_id, agent_name, key_code, customer_name,
                        bank_name, account_no, department, result_code)

    def send_record(self, command,
                    sip_num, agent_id, agent_name, key_code, customer_name,
                    bank_name, account_no, department, result_code):
        '''
        三)실데이타 
        자리수 : 정해지지않음 
        상세 : ‘|’ + 파라메타 + ‘|’ + 파라메타 + ‘|’ + 파라메타 ...
        숫자만큼 델리미터코드값을 증가시키며 연속됨 
        예) 파라메타가 2개이며 첫번째, 고객명, 두번째, 전화번호일시  
        “|고객명|01011112222”
        '''
        body = ''
        for i in range(9):
            body += '|{%d}' % i
        body = body.format(sip_num, agent_id, agent_name, key_code, customer_name,
                          bank_name, account_no, department, result_code)

        packet_len = 1 + 10 + len(body) + 1
        #command = 'CHKT'
        param_cnt = 9
        header = "%04d%s%02d" % (packet_len, command, param_cnt)

        msg = RecordClient.start_delim + header + body + RecordClient.end_delim
        print msg
        print " ".join("{:02x}".format(ord(c)) for c in msg)
        self.send(msg)

if __name__ == '__main__':
    client = RecordClient('127.0.0.1', 8099)
    client.send_start('5061', '1234', 'Tom', 'key_code', 'hong',
                      'shinhan', '000-123-4567', 'contract', 'N')
    client.send_stop('5061', '1234', 'Tom', 'key_code', 'hong',
                     'shinhan', '000-123-4567', 'contract', 'N')
